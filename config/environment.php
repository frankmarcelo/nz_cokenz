<?php

if (isset($_SERVER['SF_ENVIRONMENT'])) {

  $environment = $_SERVER['SF_ENVIRONMENT'];

} else {

  if ($_SERVER['SERVER_NAME'] == '64.14.225.195' or $_SERVER['SERVER_NAME'] == 'staging.coke.co.nz' ) {
    $environment = 'staging';
  } else {
      $environment = 'prod';
  }
}

$debug = ( !in_array($environment, array('prod','staging')) );
