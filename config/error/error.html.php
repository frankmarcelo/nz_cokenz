
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="title" content="Coca-Cola Oceania" />
<meta name="keywords" content="live positively, powerade, vitamin water, open happiness, giant bottle, coca cola, coca-cola, coke, soft drinks, coke zero, diet coca cola, diet coke, coca cola zero, coke, the coke side of life, competitions, promotions, real coke taste zero sugar, make every drop matter" />
<meta name="language" content="en" />
<meta name="robots" content="index, follow" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <title>Coca-Cola Oceania</title>
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="stylesheet" type="text/css" media="screen" href="/css/style.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/css/jquery.jscrollpane.css" />
<link rel="stylesheet" type="text/css" media="screen" href="/plugins/fancybox/jquery.fancybox.css" />
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script type="text/javascript" src="/js/jquery.mousewheel.js"></script>
<script type="text/javascript" src="/js/mwheelIntent.js"></script>
<script type="text/javascript" src="/js/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" src="/js/modernizr-1.5.min.js"></script>
<script type="text/javascript" src="/js/cufon.js"></script>
<script type="text/javascript" src="/js/VAGRoundedfont.js"></script>
<script type="text/javascript" src="/plugins/fancybox/jquery.fancybox.js"></script>
<script type="text/javascript" src="/js/script.js"></script>

    <script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1765108-10']);
  _gaq.push(['_setDomainName', 'coke.co.nz']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>  </head>
  <body class="uwb cocacola">
  <div id="container">

    <header>
      <div class="pageWidthTop">
	      <div id="mainlogo">
	      	<a href="/"><img id="logo" src="/images/logo-coca-cola.png" alt="Coca-Cola" /></a>
	      </div>

				<div class="loginButton" id="loginShowStart"><a href="#">Show Login Panel</a></div>
<div id="login_spacer">
  <div id="loginWidgetContainer" class="startView">
    <div id="loginWidget">
      <iframe src="/security/login" scrolling="no" height="68" width="390" allowtransparency="true">
      </iframe>
    </div>
    <div class="loginButton" id="loginHideStart"><a href="#">Hide Login Panel</a></div>
  </div>
</div>	      <div id="mainlinks">
		      <a id="home" href="/">Home</a>
		      <a id="prod" target="_blank" href="http://www.livepositively.co.nz/Page/OurDrinks/ourdrinks">Our Products</a>
		      <a id="comp" target="_blank" href="http://www.thecocacolacompany.com">Our Company</a>
		      <a id="sus" target="_blank" href="http://www.livepositively.co.nz">Sustainability</a>
          		      <a id="contact" href="/contact-us">Contact Us</a>
	      </div>
      </div>
    </header>

    <div class="pageWidth">

      <section id="main" class="clearfix">
        <div class="cokebottle typography">

          <h1>Server Error</h1>

          <section id="content" class="form">
            <p>An error occurred in processing your request. Please try again later.</p>
            <p>If you continue to see this error, please <a href="/contact-us">contact us</a> and provide a detailed explanation on what you did before seeing this message.</p>
          </section>
        </div>

      </section>

      <footer>
        <nav>
          <a href="/privacy-policy">Privacy Policy</a>
          <a href="/terms-of-use">Terms of Use</a>
          <a href="/promo-terms">Promotion Terms &amp; Conditions</a>
        </nav>

        <div id="legal_footer">
          &copy 2011 The Coca-Cola Company. 'Coca-Cola', 'Coke', 'Coca-Cola Zero', 'Coke Zero', 'Open Happiness', the Contour Bottle, the Dynamic Ribbon device and the 'Grip & Go' bottle are
     		  <br/>
     		  registered trade marks of The Coca-Cola Company.
     		</div>

      </footer>

    </div>

  </div>

  </body>
</html>
