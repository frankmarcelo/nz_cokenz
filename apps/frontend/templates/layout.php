<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="https://www.facebook.com/2008/fbml">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>

    <?php include_partial('global/ga') ?>
  </head>
  <body class="uwb <?php echo $sf_request->getAttribute('pageClass','cocacola') ?>">
  <div id="container">

    <header>
      <div class="pageWidthTop">
	      <div id="mainlogo">
	      	<a href="<?php echo url_for('@homepage') ?>"><img id="logo" src="/images/logo-coca-cola.png" alt="Coca-Cola" /></a>
	      </div>

				<?php
          if( $sf_user->isAuthenticated() ) {
            include_partial('sfGuardAuth/authorised',array('name' => $sf_user->getGuardUser()->getName()));
          } else {
            include_partial('sfGuardAuth/not_authorised');
          }

        ?>
	      <div id="mainlinks">
		      <a id="<?php echo $sf_request->getParameter('action','index') == 'index' ? "home_sel" : "home" ?>" href="<?php echo url_for('@homepage') ?>">Home</a>
		      <a id="prod" target="_blank" href="http://www.livepositively.co.nz/Page/OurDrinks/ourdrinks">Our Products</a>
		      <a id="comp" target="_blank" href="http://www.thecocacolacompany.com">Our Company</a>
		      <a id="sus" target="_blank" href="http://www.livepositively.co.nz">Sustainability</a>
          <?php //<!--<a id="echo false ? "career_sel":"career" " href="/careers.jsp">Careers</a>--> ?>
		      <a id="<?php echo $sf_request->getParameter('action') == 'contact' ? "contact_sel" : "contact" ?>" href="<?php echo url_for('@contact') ?>">Contact Us</a>
	      </div>
      </div>
    </header>

    <div class="pageWidth">

      <section id="main" class="clearfix">
        <?php echo $sf_content ?>
        
      </section>
      
      <footer>     
        <nav>
          <a href="<?php echo url_for('@privacy_policy') ?>">Privacy Policy</a>
          <a href="<?php echo url_for('@terms_of_use') ?>">Terms of Use</a>
          <a href="<?php echo url_for('@promo_terms') ?>">Promotion Terms &amp; Conditions</a>
          <?php /*<a href="<?php echo 'http://' . sfConfig::get('app_mobile_url') ?>" style="float: right;">View Mobile version</a> */ ?>
        </nav>
             
        <div id="legal_footer">
          &copy <?php echo date('Y') ?> The Coca-Cola Company. 'Coca-Cola', 'Coke', 'Coca-Cola Zero', 'Coke Zero', 'Open Happiness', the Contour Bottle, the Dynamic Ribbon device and the 'Grip & Go' bottle are
     		  <br/>
     		  registered trade marks of The Coca-Cola Company.
     		</div>
             
      </footer>
           
    </div>
           
  </div>
  
  </body>
</html>
