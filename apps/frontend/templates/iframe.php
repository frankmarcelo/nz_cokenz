<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>

    <?php include_partial('global/ga') ?>
  </head>
  <body style="background: transparent;" class="uwb <?php echo $sf_request->getAttribute('pageClass','cocacola') ?>">
    <?php echo $sf_content ?>
  </body>
</html>
