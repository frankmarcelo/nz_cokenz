<div class="cokebottle typography">

  <h1>Page not found</h1>

  <section id="content" class="form">
    <p>The page you've requested could not be found. It may have been renamed or removed.</p>
    <p>Please use the navigation menu above to find the page you are looking for, provided it is still available.</p>
  </section>
</div>