  <?php $sf_response->setTitle('Promotion Terms & Conditions');?>

<?php if( $page !== null ): ?>
  <?php include_partial('default/promo-terms/'.$page) ?>
<?php else: ?>

<div class="typography">

<h1>Promotion Terms &amp; Conditions</h1>

<section id="content">
  <p><a href="<?php echo url_for('@promo_terms_page?page=london-2012-new-world') ?>">COCA-COLA London 2012 Olympic Games Promotion &mdash; in association with New World</a></p>
  <p><a href="<?php echo url_for('@promo_terms_page?page=london-2012-countdown') ?>">COCA-COLA London 2012 Olympic Games Promotion &mdash; in association with Countdown</a></p>
  <p><a href="<?php echo url_for('@promo_terms_page?page=all-blacks-new-world') ?>">COCA-COLA ZERO All Blacks Promotion - in association with New World</a></p>
  <p><a href="<?php echo url_for('@promo_terms_page?page=coke-zero-morph-hero') ?>">'COCA-COLA ZERO Morph Heroes' APPLICATION Terms of Use</a></p>
  <p><a href="<?php echo url_for('@promo_terms_page?page=coke-zero-all-blacks-countdown') ?>">COCA-COLA ZERO All Blacks Promotion - in association with Countdown</a></p>
</section>

</div>

<?php endif; ?>