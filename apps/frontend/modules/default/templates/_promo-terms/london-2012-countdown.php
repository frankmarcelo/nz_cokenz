<div class="typography">

  <h1>COCA-COLA London 2012 Olympic Games Promotion &mdash; in association with Countdown</h1>

<section id="content">

<h2>ONECARD PROMOTION TERMS AND CONDITIONS </h2>
<h3>Prize</h3>
<p>Prize is a trip for two to the London 2012 Olympic Games and includes:</p>
<ol type="a">
<li>return premium economy airfares (including all applicable airport taxes and government levies) for
two people to London from Auckland;</li>
<li>           domestic flights to Auckland from the closest regional airport for those situated outside of the Auckland area (from Taupo South including New Plymouth and Napier) subject to flight availability, OR MTA petrol vouchers to the value of $200 (inc GST) for winners in the Auckland region (from Taupo North);</li>
<li>            the winner&apos;s choice of stopovers both to and from London of either Auckland, San Francisco, London, Las Vegas, Auckland OR Auckland, Hong Kong, London, Phuket, Auckland;</li>
<li>           destination stopovers include either 3 nights&rsquo; accommodation at The Westin Hotel in San Francisco OR the Shangri-La Hotel in Hong Kong on the way to London and 4 nights&rsquo; accommodation at the Signature MGM hotel in Las Vegas OR the JW Marriot in Phuket on the way back from London and airport transfers; </li>
<li> deluxe motor coach transfers to/from events and programme activities whilst in London, and return airport transfers to and from The Langham Hotel in London;</li>
<li>            three nights&apos; accommodation for two persons (twin share) at The Langham Hotel in London; </li>
<li>exclusive access to TCCC Hospitality Lounges, located at The Langham and the Olympic Park; </li>
<li> four full meals per day, all-day snack s, and a full premium open bar whilst in London;</li>
<li> London 2012 Olympic Games event tickets to a selection of popular events;</li>
<li> a customized amenity package that will include outerwear, accessories, and sports bag;</li>
<li> NZ$4,500 spending money.</li>
</ol>
<h3>Terms binding</h3>
<p>Information on how to enter form part of these terms and conditions. By entering into this promotion you agree to be bound by these terms and conditions</p>
<h3>Who is eligible</h3>
<p>This promotion is open to Onecard holders only.</p>
<h3>How to Enter</h3>
<p>Purchase any 420mL, 600mL, 1.5L or 2.25L bottle or 8, 12, 18 or 30 multi-can pack, and 4x 330mL glass multi-pack of Coca-Cola, Coca-Cola Zero, Diet Coca-Cola, Vanilla Coca-Cola, Diet Coca-Cola Caffeine Free, Sprite, Sprite Zero, Fanta, Lift or Diet Lift  (&quot;Promotional Product&quot;) from a participating Countdown, Woolworths or Foodtown supermarket, swipe your Onecard <b>or </b>if you are shopping online enter your Onecard number at the time of purchase. Delivery date must be within the Promotion Period for online purchases and automatically go into the draw to win a trip for two to the London 2012 Olympic Games.</p>
<p>Entry is not limited per transaction. There is 1 entry into the prize draw for each promotional product purchased.</p>
<p>The Promotion commences Monday 30<span class="s1">th </span>April 2012 &amp; closes 11.59pm on Sunday 10<span class="s1">th </span>June 2012 (&ldquo;Promotion Period&rdquo;).</p>
<h3>Winner(s)</h3>
<p>The prize draw will take place on Wednesday 13th June 2012 (the Draw Date).
The first winner(s) valid entries drawn will each win a prize. The winner(s) will be notified by telephone and/or mail using the details listed on the Onecard database. Entrants should ensure that their personal details on the Onecard database are correct.
In the event a winner cannot be contacted within five business days of the prize draw, the prize will be redrawn.</p>
<p>The winners agree to make themselves available for reasonable promotional and publicity purposes.
If a winner is under the age of 18, the prize may be awarded to the winner&rsquo;s parent or guardian.</p>
<h3>Prize terms</h3>
<ol>
<li><b>Prize </b>The Prize travel dates are departing 27th July returning on 9th August, 2012. No alterations, extensions or variation on these travel dates or arrangements will be permitted. All components of the Prize must be taken together.  Any changes or additions to the booking may incur a cancellation or amendment fee, at the cost of the winner. The winner will be solely responsible for his/her travel companion, including ensuring that the travel companion complies with all applicable laws and regulations.</li>
<li> The Prize excludes the items listed below (such list is not exhaustive) and are therefore the responsibility of the winner and his/her travelling companion:
<ol type="a">
  <li> transportation expenses to the closest regional airport/flight to Auckland airport, (where the flight to London leaves from);
    </li>
  <li> valid passports and applicable visas;
  </li>
  <li> all other costs and personal expenses (other than the NZ$4,500 spending money) such as shopping, transportation (other than the transportation specified), food and beverage expenses and taxes;
    </li>
  <li> accommodation related charges other than the room rates such as phone calls, room service, in- room movies, laundry, mini-bar charges, loss of hotel property, damage to hotel property, or any other expenses incurred in the hotel other than those stated above;
    </li>
  <li> accidental insurance coverage; and
    </li>
  <li> travel insurance.</li>
</ol>
</li>
<li> Onecard holders who are (a) employees of the Promoter), its related companies, its agencies, and anyone else professionally connected with the promotion; (b)  are a spouse, de facto spouse, parent, child or sibling (whether natural or by adoption) of such an employee, and anyone else
professionally connected with this Promotion; or (c) are a director, shareholder, officer, employee, contractor or agent of any Prize Pack supplier participating in the Promotion are ineligible to enter.
</li>
<li> The winner must notify the Promoter of the name, age and contact details of the person that the winner wishes to nominate as his/her travelling companion within five business days (Monday to Friday) of the winner being notified.
</li>
<li> The winner must notify the Promoter of any special needs or requirements he/she or his/her travelling companion has that may impact on the winner or the travelling companion&rsquo;s redemption of the Prize (for example, whether wheelchair access is required).
</li>
<li> Travel details including flights and spending money will be sent out to the winners as soon as  practicable following the Prize draw and after all the winner&rsquo;s details have been confirmed.  By
accepting the Prize, the winner then gives express consent to the Promoter to be able to provide his/her and their travelling companion&rsquo;s personal information to the airline, transfer providers and
accommodation providers to make and confirm their bookings.
</li>
<li> If a winner does not wish to claim the Prize, they may nominate another person to whom the Prize can be transferred to. However, the Prize must not be sold or used, donated or given away as part of another promotion.
</li>
<li> If a winner is under 18 years of age at the start of the promotion, their nominated travelling companion must be a parent or guardian who is 18 years of age or older.
</li>
<li> The travelling companion of the winner must be 12 years or older.
</li>
<li> If the winner is unable to take the Prize or the winner&rsquo;s travelling companion is unable to accompany the winner, due to any illness or personal circumstance beyond the winner&rsquo;s or travel companion&rsquo;s control, the winner must immediately notify the Promoter. The Promoter may, at its sole discretion, allow the winner to nominate another person to whom the Prize can be transferred to and the Promoter will use all reasonable endeavours to transfer the Prize accordingly. The Promoter will not be liable if any part of the Prize that is unable to be transferred to the person nominated by the winner.
</li>
<li> Prior to being declared the winner of the Prize, the winner will be required to (a) show proof of identity with a copy of their passport; and (b) sign a &quot;Participation Agreement&quot; in the form required by this Promotion, confirming acceptance of these Terms and Conditions of Entry; accepting the Terms and Conditions relating to the use and enjoyment of the Prize; releasing the Promotor and its related companies and each of their officer, directors, shareholders, employees, advisors, assignees, agents, licencees, representatives, advertising and promotional agencies from any and all liability in connection with this promotion and/or the Prize; and confirming that their possesion and use of their London 2012 Olympic Games tickets are subject to the Ticket Conditions which can be found at www.tickets.london2012.com/purchaseterms.html .
</li>
<li> The winner&apos;s travelling companion will also be required show proof of identity with a copy of their passport and sign a Participation Agreement in a form similar to that provided by the winner.
</li>
<li> If the Participation Agreements are not signed by the winner and his/her travelling companion and returned to the Promotor within five business days (Monday to Friday) of it being provided to the relevant party, the Prize will be forfeited.
</li>
<li> The winner and his/her travelling companion take the Prize entirely at their own risk.
</li>
<li> The total prize is valued at no less than $41,774NZD.
</li>
<li> The stopover destinations are subject to availability and preferred travel routes may not be available during the dates the Promoter requires the winner to travel. If a preferred route is unavailable, the
Promoter reserves the right to book the winner on another route.</li>
<li> The winner must advise the Promoter no later than 27 June which stopover route they would like to choose (Asia or America) and who their travelling companion will be. Once a stopover route has been chosen, it cannot be altered.
</li>
<li> Air tickets are available on the regular scheduled services of each airline and are subject to seasonal embargos. Flight itinerary may have to be adjusted depending on the airline&rsquo;s departure city and their current flight schedule. All travel is subject to availability at all times and maybe dependent on select seat class with airlines. The Promoter is not responsible if any scheduled event is delayed, postponed or cancelled for any reason and winner will not be reimbursed for tickets. Travel is valid for specific dates as outlined. The winner and his/her travelling companion are responsible for any amendments fees issued by airlines or suppliers once booking is confirmed and ticketed.  Travel suppliers/airlines to be chosen at the Promoter&rsquo;s discretion.
</li>
<li> The winner is solely responsible for their entry and the entry of his/her companion into all relevant countries, at their own expense, including ensuring all necessary passports, visas, travel authorisations, medical advice and recommended vaccinations and immunisations have been obtained prior to travel and, if required, on or by a date nominated by the Promoter. Failure to do so may result in the Prize being forfeited.
</li>
<li> Taking the Prize is also subject to any prevailing terms and conditions of any accommodation / transport / services / transfers / travel insurance / tour or ticket providers, and in particular, any health, behaviour, age and safety requirements.  No compensation will be payable if a winner or his/her travel companion are unable to use any element of the Prize as stated for whatever reason, including ejection, refusal of entry into or departure from London or the London 2012 Olympic Games events or participation in certain activities for health, age, behaviour or safety reasons. Any tickets, passes or vouchers issued as part of the Prize are subject to prevailing terms and conditions
of use, are only valid for use within the stated duration on the tickets, passes or vouchers issued, and are not replaceable if lost, stolen or damaged.
</li>
<li> The winner must have a credit card which is required for hotel expenditures not covered under the Prize package.
</li>
</ol>
<p >Prize value is correct at time of printing but no responsibility is accepted for any variation in the value of any prize items.</p>
<p >In the event that any prize item is unavailable despite the Promoter&rsquo;s reasonable endeavours, the Promoter reserves the right to substitute a different prize item of equal or greater value. </p>
<p >Prizes are not transferable, redeemable and may not be exchanged for cash.</p>
<p > Prizes will only be delivered in New Zealand.</p>
<p>The Promoter reserves the right to verify the validity of entries and reserves the right to disqualify any entrant for tampering with the entry process, including but not limited to making multiple entries that are not associated with the purchase of a Promotion Product, or for submitting an entry that is not in accordance with these terms and conditions.</p>
<p> The Promoter&rsquo;s decision is final and no correspondence will be entered into.</p>
<h3>Variations</h3>
<p>The Promoter reserves the right to vary any of the terms of entry applying to this promotion or to modify, terminate, suspend or reschedule this promotion.</p>
<h3>Exclusions and Liability</h3>
<p>The Promoter and Progressive Enterprises Limited are not responsible for late or misdirected entries and take no responsibility for any entries not correctly lodged through the Onecard system.</p>
<p>The Promoter and Progressive Enterprises Limited and its related companies, employees and agencies shall not be liable for any loss, damage or personal injury (including but not limited to indirect or consequential loss) suffered by any person arising directly or indirectly out of or in connection with entering this promotion or claiming/winning any prize, except as required by law.</p>
<p>The Promoter and Progressive Enterprises Limited are not responsible for the transmission or receipt of any incorrect information associated with entries, either caused by user error or any equipment or programming malfunction associated with the promotion.</p>
<h3>Use of Personal Information</h3>
<p>All entries remain the property of Onecard and Progressive Enterprises Limited. All personal information will be collected and stored by Onecard (Progressive Enterprises Limited) in accordance with the Privacy Act 1993. You have the right to access your personal information and request correction of any errors in it pursuant to the Privacy Act 1993. Progressive Enterprises Limited may use entrants&apos; personal information from entries to conduct the promotion and for its own future promotional and publicity purposes in accordance with standard Onecard terms and conditions.</p>
<p>By entering this promotion entrants consent to the use of their information as described above.</p>
<h3>Promoter&rsquo;s Details</h3>
<p>The Promoter is Coca-Cola Amatil (N.Z) Ltd of The Oasis, Mt Wellington, Auckland. For further assistance or queries regarding this Promotion please call 0800 505 123.</p>
<h3>Additional Terms and Conditions</h3>
<p>The Promoter is not responsible for entries that are not lodged, recorded, rendered incomplete or lost for any reason including any failures of the Onecard system. The Promoter is also not responsible if the winner cannot be contacted due to incorrect contact details being recorded on the Onecard system. Each winner may be required to provide proof of identity (e.g. driver&rsquo;s licence or passport) that is consistent with the winning Onecard holder&rsquo;s details.</p>
  </section>

  </div>