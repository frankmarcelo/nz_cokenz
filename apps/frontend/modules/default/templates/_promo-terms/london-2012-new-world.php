<div class="typography">

    <h1>COCA-COLA London 2012 Olympic Games Promotion &mdash; in association with New World</h1>

    <section id="content">

              <h2>COCA-COLA London 2012 Olympic Games Promotion in association with New World and Fly Buys </h2>
              <h3>ENTRY </h3>
              <ol>
                <li>
                  <p>Entry into the competition constitutes acceptance of these terms and conditions. </p>
                </li>
                <li>
                  <p>Information on how to enter and prizes form part of these terms and conditions. Any entry not complying with these terms and conditions is invalid. </p>
      </li>
                <li>
                  <p>Entry is open to Fly Buys cardholders who are (a) not employees and their immediate families, of the Promoter (Coca-Cola Amatil (N.Z) Ltd), Loyalty New Zealand Limited (<strong>Fly Buys</strong>), Foodstuffs (Auckland) Limited, Foodstuffs (Wellington) Cooperative Society Limited, Foodstuffs South Island Limited, their respective related companies, its agencies, and anyone else professionally connected with the competition; (b) are not a spouse, de facto spouse, parent, child or sibling (whether natural or by adoption) of such an employee, and anyone else professionally connected with this competition; and (c) are not a director, shareholder, officer, employee, contractor or agent of any Prize Pack supplier participating in the competition. Employees of Fly Buys and Foodstuffs, and employees and owners of New World stores, are ineligible to win. Subject to these terms and conditions, all Fly Buys cardholders are eligible to win. </p>
      </li>
                <li>
                  <p>To qualify for automatic entry into the competition, you must purchase any 600mL, 1.5L or 2.25L bottle OR 8, 12, 18 or 24 multi-can pack, from the <em>Coca-Cola</em>, <em>Coca-Cola Zero</em>, <em>Diet Coca-Cola</em>, <em>Vanilla Coca-Cola</em>, Diet<em> Coca-Cola Caffeine Free</em>, <em>Sprite</em>, <em>Sprite Zero</em>, <em>Fanta</em>, <em>Lift</em> or <em>Diet Lift</em> range of products (&quot;Promotional Product&quot;) from any New World Supermarket and swipe your Fly Buys card at the checkout. You may enter as many times as you wish during the competition period. </p>
                </li>
                <li>
                  <p>To be eligible to win the Major Prize you must be able to travel to and remain in London, United Kingdom on 30 July and return to New Zealand on the 9 August 2012. </p>
                </li>
                <li>
                  <p>These terms and conditions and this competition cover all New World stores across New Zealand. </p>
                </li>
      </ol>
              <h3>PRIZE </h3>
              <ol start="7">
                <li>
                  <p>There is one major prize of a trip for two to the London 2012 Olympic Games (the &ldquo;Major Prize&rdquo;) and 136* x Panasonic 32&rdquo; LED Televisions (valued at RRP $899.99) (each a &ldquo;Secondary Prize&rdquo;), and both being the &ldquo;Prizes&rdquo;. *There is one Secondary Prize to be won in each New World store <strong>and the Prize item may be used for display purposes during this competition</strong>. </p>
                </li>
                <li>
                  <p>The Major Prize includes: </p>
                  <ol style="list-style:lower-alpha;">
                    <li>
                      <p>Return economy flights from the winner&rsquo;s closest domestic airport to Auckland for those situated outside of the Auckland region (from Taupo south including New Plymouth and Napier), OR MTA petrol vouchers to the value of $200 (inc GST) for winners in the Auckland region (from Taupo north); </p>
      </li>
                    <li>
                      <p>Return premium economy airfares (including all applicable airport taxes and government levies) for two people to London from Auckland; </p>
                    </li>
                    <li>
                      <p>Deluxe motor coach transfers to/from events and program activities, and return airport transfers to and from The Langham Hotel/Sheraton Park Tower Hotel in London;</p></li>
                    <li>
                      <p>Three nights for two persons (twin share) at The Langham Hotel in London; then four nights for two persons (twin share) at the Sheraton Hotel in London;</p></li>
                    <li>
                      <p>Exclusive access to The Coca-Cola Company Hospitality Lounges, located at The Langham and the Olympic Park;</p></li>
                    <li>
                      <p>Four full meals per day, all-day snacks, and a full premium open bar (only while at the Langham hotel for the first three nights);</p></li>
                    <li>
                      <p>Olympic Games event tickets to a selection of popular events;</p></li>
                    <li>
                      <p>A customised amenity package that will include outerwear, accessories, and sports bag;                </p>
                    </li>
                    <li>
                      <p>NZD$3,000 spending money. </p>
                    </li>
                  </ol>
                </li>
      </ol>
      <ol start="9">
                <li>
                  <p>The Major Prize has a total value of not less than $37,500 NZD. </p>
        </li>
        <li>
                  <p>The Major Prize excludes the items listed below (such list is not exhaustive) and are therefore the responsibility of the winner; </p>
                  <ol style="list-style:lower-alpha;">
                    <li>
                      <p>Travel insurance; </p>
                    </li>
                    <li>
                      <p>Accommodation related charges other than the room rates; </p>
                    </li>
                    <li>
                      <p>Airport parking; </p>
                    </li>
                    <li>
                      <p>Any other ancillary costs which may be incurred by the winner in relation to the Prize (unless expressly stated). </p>
      </li>
          </ol>
                </li>
                <li>All prizes are not redeemable for cash. </li>
              </ol>
              <h3>PRIZE DRAW </h3>
              <ol start="12">
                <li>
                  <p>The competition commences at 00.01am on 30 April 2012 and closes at 11.59pm on 10 June 2012. </p>
                </li>
                <li>
                  <p>The Major Prize and Secondary Prizes will be drawn on 20 June 2012 by Fly Buys by random electronic selection from all valid entries. The first eligible winner drawn will win the Main Prize. There will also be one winner drawn from each participating New World store, each of whom will win a Secondary Prize. </p>
      </li>
                <li>
                  <p>The winners will be notified via phone or letter sent from Fly Buys. If the Major Prize winner cannot be contacted by 27 June 2012 a redraw will occur and a new winner will be determined from the valid entries. </p>
      </li>
                <li>
                  <p>For your entry to be eligible to win, your Fly Buys account details must be complete and up to date (including a phone number). </p>
      </li>
                <li>
                  <p>Fly Buys membership terms and conditions also apply and can be viewed at www.flybuys.co.nz. </p>
                </li>
                <li>
                  <p>The Promoter&rsquo;s decisions and the draw results are final and not subject to appeal. The Promoter may refuse to award a prize to any person, for any reason. </p>
      </li>
                <li>
                  <p>If the Major Prize winner is under 18 years of age at the start of the competition, their nominated travelling companion must be a parent or guardian who is 18 years of age or older. </p>
      </li>
                <li>
                  <p>The travelling companion of the Major Prize winner must be 12 years of age or older. </p>
                </li>
                <li>
                  <p>The Major Prize winner will be solely responsible for his/her travel companion, including ensuring that the travel companion complies with all applicable laws and regulations. </p>
      </li>
                <li>
                  <p>The Major Prize must be taken on the travel dates specified by the Promoter otherwise the entire Prize Pack is forfeited. No extensions or variation on these travel dates or arrangements will be permitted. All components of the Prize must be taken together. Any changes or additions to the booking may incur a
                  cancellation or amendment fee, at the cost of the winner. </p>
      </li>
      </ol>
              <ol start="22">
                <li>
                  <p>The Major Prize winner must notify the Promoter of the name, age and contact details of the person that the winner wishes to nominate as his/her travelling companion. The Promoter may, at its sole discretion, allow the winner to change his/her nominated travelling companion following notification under this clause. </p>
      </li>
                <li>
                  <p>The Major Prize winner must notify the Promoter of any special needs or requirements he/she or the winner&rsquo;s travelling companion has that may impact on the winner or the travelling companion&rsquo;s redemption of the Prize (for example, whether wheelchair access is required). </p>
                </li>
                <li>
                  <p>The Promoter reserves the right to draw an alternate winner/s if the Promoter determines in its sole discretion that the winner/s do not comply with these Terms and Conditions of Entry or where the Promoter has reasonable grounds to believe the winner will not comply with these Terms and Conditions of Entry. </p>
                </li>
                <li>
                  <p>If the Major Prize winner does not wish to claim the Major Prize, they may nominate another person to whom the Major Prize can be transferred to. However, the Major Prize must not be sold or used, donated or given away as part of another competition. </p>
                </li>
                <li>
                  <p>If the Major Prize winner is unable to take the Major Prize or the winner&rsquo;s nominated travel companion is unable to accompany the winner, due to any illness or personal circumstance beyond the winner&rsquo;s or travel companion&rsquo;s control, the winner must immediately notify the Promoter. The Promoter may, at its sole discretion, allow the winner to nominate another person to whom the Major Prize can be transferred to and the Promoter will use all reasonable endeavours to transfer the Prize accordingly. The Promoter will not be liable if any part of the Major Prize that is unable to be transferred to the person nominated by the winner. </p>
                </li>
                <li>
                  <p>Travel details including flights and spending money will be sent out to the Major Prize winner as soon as practicable following the draw date and after the Major Prize winner&rsquo;s details have been confirmed. By accepting the Major Prize, the winner then gives express consent to the Promoter to be able to provide his/her and their travelling companion&rsquo;s personal information to the airline, transfer providers and accommodation providers to make and confirm their bookings. </p>
                </li>
                <li>
                  <p>Prior to being declared the winner of the Major Prize, the winner will be required to (a) show proof of identity with a copy of their passport; and (b) sign a &quot;Participation Agreement&quot; in the form required by this competition, confirming acceptance of these Terms and Conditions of Entry; accepting the Terms and Conditions relating to the use and enjoyment of the Major Prize; releasing the Promoter and its related companies and each of their officer, directors, shareholders, employees, advisors, assignees, agents, licensees, representatives, advertising and promotional agencies from any and all liability in connection with this competition and/or the Major Prize. </p>
                </li>
                <li>
                  <p>The Major Prize winner's travelling companion will also be required show proof of identity with a copy of their passport and sign a Participation Agreement in a form similar to that provided by the winner. </p>
                </li>
      </ol>
              <ol start="30">
              <li>
                <p>If the Participation Agreements are not signed by the Major Prize winner and his/her travelling companion and returned to the Promoter within five business days (Monday to Friday) of it being provided to the relevant party, the Major Prize will be forfeited. </p>
                </li>
                <li>
                  <p>The Secondary Prizes will be available for collection at the NewWorld store,at which the winner of the Secondary Prize swiped his/her Fly Buys card, post notification by Fly Buys. In some cases the Secondary Prize may have been used for display purposes, in the store, and may therefore not be in brand new condition. </p>
                </li>
                <li>
                  <p>The possession and use of the London 2012 Olympic Games Tickets will be subject to LOCOG&rsquo;s Terms and Conditions of Ticket Purchase which can be found at www.tickets.london2012.com/purchaseterms.html. Tickets are strictly non-transferable and must not be sold nor advertised for sale whether on the Internet, in newspapers or elsewhere. </p>
                </li>
                <li>
                  <p>The Major Prize winner and the winner&rsquo;s travelling companion take the Prize entirely at their own risk. </p>
                </li>
                <li>
                  <p>The total prizes are valued at not less than $159,764 NZD. </p>
                </li>
                <li>
                  <p>The Major Prize winner and their companion must hold a valid passport, and be able to travel on the dates specified by the Promoter. </p>
      </li>
                <li>
                  <p>The Major Prize winner must have a credit card which is required for hotel expenditures not covered under the prize package. </p>
      </li>
              </ol>
              <h3>EXCLUSIONS AND LIABILITY </h3>
              <ol start="37">
                <li>
                  <p>In the event that any Prize is unavailable despite the Promoter&rsquo;s reasonable endeavours, the Promoter reserves the right to substitute a different prize of equal or greater value. </p>
                </li>
                <li>
                  <p>The Promoter will not be responsible for any incorrect or inaccurate information concerning this competition or any technical errors that may occur during administration of this competition. </p>
                </li>
                <li>
                  <p>The Promoter reserves the right to modify, suspend or terminate this competition at any time. </p>
                </li>
      </ol>
              <h3>USE OF PERSONAL INFORMATION </h3>
              <ol start="40">
                <li>
                  <p>Under the Privacy Act, you have the right to access and correct any personal information held by Loyalty New Zealand Ltd (Fly Buys). For full Fly Buys terms and conditions, please visit www.flybuys.co.nz </p>
                </li>
                <li>
                  <p>By entering the competition, Prize winners agree that the Promoter may use their name, image and photograph/s for publicity and promotion purposes, without compensation, and agrees that the Promoter will own copyright in any such images and photograph/s and in all material incorporating the photograph/s. </p>
                </li>
      </ol>
              <h3>PROMOTERS DETAILS </h3>
              <ol start="42">
                <li>
                  <p>This competition is being run in conjunction with Foodstuffs (Auckland) Limited. </p>
                </li>
                <li>
                  <p>The Promoter is Coca-Cola Amatil (N.Z) Ltd of The Oasis, Mt Wellington, Auckland. For further assistance or queries regarding this promotion please call 0800 505 123. </p>
      </li>
      </ol>

    </section>

    </div>
