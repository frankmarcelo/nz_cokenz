<div class="typography">

<h1>COCA-COLA ZERO All Blacks Promotion - in association with Countdown</h1>

	<section id="content">

<h2>Terms &amp; Conditions</h2>
<h3><strong>Prize</strong></h3>
<p>  Prize is one of five trips for four to see the All Blacks play Australia in Brisbane on Saturday 20 October 2012 and includes: </p>
<ol type="a">
  <li>return economy airfares (including all applicable airport taxes and government levies) for four people to Brisbane from Auckland (departing on 18 October 2012) and returning from Coolangatta to Auckland (on 24 October 2012);<br />
    </li>
  <li>domestic flights to Auckland from the closest regional airport for those situated outside of the Auckland area (from Taupo South including New Plymouth and Napier) subject to flight availability, OR MTA petrol vouchers to the value of $200 (inc GST) for winners in the Auckland region (from Taupo North); <br />
    </li>
  <li> three nights' accommodation for four people (two bedroom apartment) at Quay West Suites in Brisbane;<br />
    </li>
  <li> three nights' accommodation for four people (family room) at Seaworld Resort and Water Park in the Gold Coast (including a Dolphin Discovery presentation); <br />
    </li>
  <li> 4 x Gold level tickets to All Blacks vs Australia at Suncorp stadium on Saturday 20 October 2012;<br />
    </li>
  <li> Brisbane Airport to Quay West Suites transfer, game transfers, Brisbane to Gold Coast transfer and Sea World Resort to Coolangatta Airport transfer;<br />
    </li>
  <li> 4 x Gold Coast theme park passes to Warner Bros. Movie World, Sea World and Wet'n'Wild Water World; <br />
    </li>
  <li> NZ$2,000 spending money.  </li>
</ol>
<h3><strong>Terms binding</strong></h3>
<p> Information on how to enter form part of these terms and conditions. By entering into this promotion you agree to be bound by these terms and conditions</p>
<h3><strong>Who is eligible?</strong></h3>
<p>
  This promotion is open to Onecard holders only.</p>
<h3><strong>How to Enter</strong></h3>
<p>  Purchase any Coca-Cola, Coca-Cola Zero, Diet Coca-Cola, Vanilla Coca-Cola, Diet Coca-Cola Caffeine Free,  Sprite, Sprite Zero, Fanta, Lift or Diet Lift product (&quot;Promotional Product&quot;)  from a participating Countdown, Woolworths or Foodtown supermarket, swipe your Onecard or if you are shopping online enter your Onecard number at the time of purchase. Delivery date must be within the Promotion Period for online purchases and automatically go into the draw to win one of five trips for four to see the All Blacks in Brisbane.</p>
<p>Entry is not limited per transaction. There is 1 entry into the prize draw for each promotional product purchased. </p>
<p>The Promotion commences Monday 13 August 2012 &amp; closes 11.59pm on Sunday 16 September 2012 (&ldquo;Promotion Period&rdquo;). </p>
<h3><strong>Winner(s)</strong></h3>
<p>  The prize draw will take place on Monday 17 September 2012 (the Draw Date).<br />
  The first winner(s) valid entries drawn will each win a prize. The winner(s) will be notified by telephone and/or mail using the details listed on the Onecard database. Entrants should ensure that their personal details on the Onecard database are correct. </p>
<p>In the event a winner cannot be contacted within &lsquo;five (5) business days (Monday – Friday)&rsquo; of the prize draw, the prize will be redrawn.</p>
<p>The winners agree to make themselves available for reasonable promotional and publicity purposes. </p>
<p>If a winner is under the age of 18, the prize may be awarded to the winner&rsquo;s parent or guardian.</p>
<h3>Prize terms</h3>
<ol>
  <li>
    The Prize travel dates are departing 18 October returning on 24 October 2012.  No alterations, extensions or variation on these travel dates or arrangements will be permitted.  All components of the Prize must be taken together.  Any changes or additions to the booking may incur a cancellation or amendment fee, at the cost of the winner. The winner will be solely responsible for his/her travel companions, including ensuring that the travel companions comply with all applicable laws and regulations.<br />
    </li>
  <li> The Prize excludes the items listed below (such list is not exhaustive) and are therefore the responsibility of each winner and his/her travelling companions:<br />
    <ol type="a">
      <li> transportation expenses to the closest regional airport/flight to Auckland airport (where the flight to Brisbane leaves from);<br />
      </li>
      <li> valid passports and applicable visas;<br />
      </li>
      <li> all other costs and personal expenses (other than the NZ$2,000 spending money) such as shopping, transportation (other than the transportation specified), food and beverage expenses and taxes;<br />
      </li>
      <li> accommodation related charges other than the room rates such as phone calls, room service, in-room movies, laundry, mini-bar charges, loss of hotel property, damage to hotel property, or any other expenses incurred in the hotel other than those stated above;<br />
      </li>
      <li> accidental insurance coverage; and<br />
      </li>
      <li> travel insurance.<br />
        </li>
    </ol>
  </li>
  <li>Onecard holders who are (a) employees of the Promoter, Progressive Enterprises Limited, their respective related companies, or their agencies; (b) a spouse, de facto spouse, parent, child or sibling (whether natural or by adoption) of such an employee; or (c) professionally connected with this Promotion, are ineligible to enter.   <br />
    </li>
  <li>Each winner must notify the Promoter of the name, age and contact details of the people that the winner wishes to nominate as his/her travelling companions within five business days (Monday to Friday) of the winner being notified.  <br />
    </li>
  <li>Each winner must notify the Promoter of any special needs or requirements he/she or his/her travelling companions has that may impact on the winner or a travelling companion&rsquo;s redemption of the Prize (for example, whether wheelchair access is required). <br />
    </li>
  <li>Travel details including flights and spending money will be sent out to the winners as soon as practicable following the Prize draw and after each winner&rsquo;s details have been confirmed.  By accepting the Prize, each winner then gives express consent to the Promoter to be able to provide his/her and their travelling companions' personal information to the airline, transfer providers and accommodation providers to make and confirm their bookings.<br />
    </li>
  <li>If a winner does not wish to claim a Prize, they may nominate another person to whom their Prize can be transferred to. However, the Prize must not be sold or used, donated or given away as part of another promotion. <br />
    </li>
  <li>If a winner is under 18 years of age at the start of the promotion, one of their nominated travelling companions must be a parent or guardian who is 18 years of age or older.<br />
    </li>
  <li>Travelling companions of the winner can be any age.<br />
    </li>
  <li>If a winner is unable to take the Prize or a winner&rsquo;s travelling companions are unable to accompany the winner, due to any illness or personal circumstance beyond the winner&rsquo;s or travel companions' control, the winner must immediately notify the Promoter.  The Promoter may, at its sole discretion, allow the winner to nominate another person(s) to whom the Prize can be transferred to and the Promoter will use all reasonable endeavours to transfer the Prize accordingly.  The Promoter will not be liable if any part of the Prize is unable to be transferred to the person nominated by the winner. <br />
    </li>
  <li>Prior to being declared the winner of a Prize, the winner will be required to: <br />
    <ol type="a">
      <li> show proof of identity with a copy of their passport; and <br />
      </li>
      <li> sign a &quot;Participation Agreement&quot; in the form required by the Promoter, confirming acceptance of these Terms and Conditions of Entry; accepting the Terms and Conditions relating to the use and enjoyment of the Prize; releasing the Promotor, Progressive Enterprises Limited and their respective related companies and each of their officers, directors, shareholders, employees, advisors, assignees, agents, licensees, representatives, advertising and promotional agencies from any and all liability in connection with this promotion and/or the Prize; confirming that their possession and use of their All Blacks match tickets are subject to the terms and conditions on each ticket and on www.allblacks.com; and confirming that their possession and use of their Gold Theme Park Passes are subject to their terms and conditions found on the passes and are valid until 30 June 2013.<br />
      </li>
    </ol>
  </li>
  <li>The winner's travelling companions may also be required show proof of identity with a copy of their passports and sign the Participation Agreement form provided to the winner.<br />
    </li>
  <li>If the Participation Agreements are not signed by each winner and, if required, by his/her travelling companions and returned to the Promotor within five business days (Monday to Friday) of it being provided to the relevant party, the Prize will be forfeited.<br />
    </li>
  <li>The winner and his/her travelling companions take the Prize entirely at their own risk. <br />
    </li>
  <li>The total prize pool is valued at not more than $54,360NZD. <br />
    </li>
  <li>The Promoter is not responsible if any scheduled event is delayed, postponed or cancelled for any reason and winner will not be reimbursed for tickets. Travel is valid for specific dates as outlined. The winner and his/her travelling companions are responsible for any amendment  fees issued by airlines or suppliers once booking is confirmed and ticketed.  Travel suppliers/airlines to be chosen at the Promoter&rsquo;s discretion. <br />
    </li>
  <li> Each winner is solely responsible for their entry and the entry of his/her companions into Australia, at their own expense, including ensuring all necessary passports, visas, travel authorisations, medical advice and recommended vaccinations and immunisations have been obtained prior to travel and, if required, on or by a date nominated by the Promoter.  Failure to do so may result in the Prize being forfeited.<br />
    </li>
  <li> Taking the Prize is also subject to any prevailing terms and conditions of any accommodation / transport / services / transfers / travel insurance / tour or ticket providers, and in particular, any health, behaviour, age and safety requirements.  No compensation will be payable if a winner or his/her travel companion are unable to use any element of the Prize as stated for whatever reason, including ejection, refusal of entry into or departure from Australia or the All Blacks match at Suncorp Stadium or participation in certain activities for health, age, behaviour or safety reasons. Any tickets, passes or vouchers issued as part of the Prize are subject to prevailing terms and conditions of use, are only valid for use within the stated duration on the tickets, passes or vouchers issued, and are not replaceable if lost, stolen or damaged.<br />
    </li>
  <li> The winner must have a credit card which is required for hotel and rental vehicle expenditures not covered under the Prize package.</li>
</ol>
<p>Prize value is correct at time of printing but no responsibility is accepted for any variation in the value of any prize items.</p>
<p>In the event that any prize item is unavailable despite the Promoter&rsquo;s reasonable endeavours, the Promoter reserves the right to substitute a different prize item of equal or greater value.</p>
<p>Prizes are not transferable, redeemable and may not be exchanged for cash.</p>
<p>Prizes will only be delivered in New Zealand and each winner should allow 28 days from the date of the prize draw for delivery of their prize.</p>
<p>The Promoter reserves the right to verify the validity of entries and reserves the right to disqualify any entrant for tampering with the entry process, including but not limited to making multiple entries that are not associated with the purchase of a Promotion Product, or for submitting an entry that is not in accordance with these terms and conditions.</p>
<p>The Promoter&rsquo;s decision is final and no correspondence will be entered into.</p>
<h3><strong>Variations</strong></h3>
<p> The Promoter reserves the right to vary any of the terms of entry applying to this promotion or to modify, terminate, suspend or reschedule this promotion.</p>
<h3><strong>Exclusions and Liability</strong></h3>
<p>  The Promoter and Progressive Enterprises Limited are not responsible for late or misdirected entries and take no responsibility for any entries not correctly lodged through the Onecard system.<strong></strong></p>
<p>The Promoter and Progressive Enterprises Limited and its related companies, employees and agencies shall not be liable for any loss, damage or personal injury (including but not limited to indirect or consequential loss) suffered by any person arising directly or indirectly out of or in connection with entering this promotion or claiming/winning any prize, except as required by law. </p>
<h3><strong>Use of Personal Information</strong></h3>
<p>  All entries remain the property of Onecard and Progressive Enterprises Limited.  All personal information will be collected and stored by Onecard (Progressive Enterprises Limited) in accordance with the Privacy Act 1993. You have the right to access your personal information and request correction of any errors in it pursuant to the Privacy Act 1993. Progressive Enterprises Limited may use entrants' personal information from entries to conduct the promotion and for its own future promotional and publicity purposes in accordance with standard Onecard terms and conditions. <br />
  <br />
  By entering this promotion entrants consent to the use of their information as described above.  </p>
<h3><strong>Promoter&rsquo;s Details</strong></h3>
<p> The Promoter is Coca-Cola Amatil (N.Z) Ltd of The Oasis, Mt Wellington, Auckland. For further assistance or queries regarding this Promotion please call 0800 505 123.</p>
<h3><strong>Additional Terms and Conditions</strong></h3>
<p> The Promoter is not responsible for entries that are not lodged, recorded, rendered incomplete or lost for any reason including any failures of the Onecard system. The Promoter is also not responsible if a winner cannot be contacted due to incorrect contact details being recorded on the Onecard system. Each winner may be required to provide proof of identity (e.g. driver&rsquo;s licence or passport) that is consistent with the winning Onecard holder&rsquo;s details.</p>
    </section>
   </div>