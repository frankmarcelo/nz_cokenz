<div class="typography">

<h1>'COCA-COLA ZERO Morph Heroes' APPLICATION</h1>

	<section id="content">

		<h2 style="color: red">If you wish to have your video removed, please call our customer care line on 0800 505 123</h2>
		<br />
		<br />

		<h2>TERMS OF USE</h2>
		<p >These 'Terms of Use' comprise the terms on which you use the "COCA COLA ZERO Morph Heroes" Application on Facebook. <br />
			In these Terms of Use: <br />
			App refers to the Application, which is the "COCA COLA ZERO Morph Heroes" Application housed on Facebook <br />
			CCO refers to Coca Cola Oceania Limited <br />
			Carbine Road, Mt Wellington<br />
			PO Box 62-042<br />
			Mt Wellington<br />
			Auckland, NZ 1060<br />
			Facebook URL refers to this page on Facebook, <a href="http://www.facebook.com/CocaColaNZ">http://www.facebook.com/CocaColaNZ</a><br />
			Output refers to the image created by you when you use the App<br />
		</p>

		<p>Affiliates refers to any direct or indirect parent, subsidiaries, sponsors, or affiliated companies of CCO and shall include any authorised bottlers of The Coca Cola Company</p>

		<ol>

			<li>
			Your Agreement to these Terms and Your Use of this App
			<br /><br />
			Your use of this App, including accessing or browsing this App, is deemed to be your acceptance of these Terms of Use. Please read them carefully. If you do not agree and accept, without limitation or qualification, these Terms of Use, please exit the App immediately and refrain from using this App in the future.
			<br /><br />
			Your use of this App and the Output is also subject to any other law or regulation that applies to this App, the internet or the worldwide web, and all of the Facebook conditions of use.
			<br /><br />
			This App, its content and the Output are made available for your personal entertainment, information and communication only. All content, including the Output, is provided for your non-commercial use. You may not distribute, modify, transmit, reuse, repost, or use the content of the App for public or commercial purposes, including the text, images, audio and video without the written permission of CCO. The Ouput is authorised for posting solely on your own Facebook pages, and may not be used for any other purpose without the prior written permission of CCO.
			<br /><br />
			This App is intended for use by residents of New Zealand only. These Terms of Use are governed by the laws of New Zealand. By using the App you agree to submit to the exclusive jurisdiction of the New Zealand courts in the event of any dispute. If any of these Terms of Use are held to be invalid, unenforceable or illegal for any reason, the remaining Terms of Use will continue in full force.
			<br /><br />
			CCO may at any time revise these Terms of Use by updating this page. You are bound by any such revisions and should therefore periodically visit this page to review the then current Terms of Use to which you are bound. You will be bound by changes even if you do not re-visit this page to re-read this notice.
			<br />
			<br />
			<br />
			</li>

			<li>
			Registration
			<br /><br />
			The App is accessed through the Facebook URL, and therefore, you will be logged on and registered in Facebook when you use the App. CCO, as the administrator of the "Coca Cola" New Zealand Facebook Fan page, has the ability to contact you using your Facebook registration and may do so for administration purposes in support of the App, but will not otherwise retain or use any personal data (for example, your name, address, telephone number or e-mail address) you transmit to the App. Any other communication or material you transmit to the App, such as photos or images, posting, questions, comments, suggestions or the like, will be treated as non-confidential and non-proprietary.
			<br /><br />
			Note that this application is in no way sponsored, endorsed or administered by, or associated with Facebook.
			<br /><br />
			CCO reserves the right to release any details obtained through your use of the App to Facebook's administrators or to regulatory authorities and your internet service provider if you conduct any illegal activities or otherwise breach these Terms of Use.
			<br />
			<br />
			<br />
			</li>

			<li>
			Conduct
			<br /><br />
			You agree not to engage in any activity that is disruptive or is detrimental to the use of this App.
			<br /><br />
			Subject to your compliance with these Terms of Use, and solely for so long as you are permitted by CCO to access and use the App, you may download and view one (1) copy of any content and software on the App to which we provide you access or download, on any single computer, solely for your personal, non-commercial home use, provided that you keep intact all copyright and other proprietary notices. Except as otherwise expressly authorised in writing in advance by CCO, you agree not to reproduce, modify, rent, lease, loan, sell, distribute, adapt, translate, create derivative works based (whether in whole or in part) on, reverse engineer, decompile or disassemble any services or software, all or any part of the App, or any materials made available through the App including the Output. Certain software may be governed by an additional end user licence agreement to which you may be required to agree before using such software.
			<br /><br />
			Although CCO may from time to time, monitor discussions, chats postings, transmissions, bulletin boards, and the like on the App, CCO is under no obligation to do so and assumes no responsibility or liability arising from the content of any such locations nor for any error, defamation, omission, falsehood, obscenity, profanity, danger, or inaccuracy contained in any information within such locations on the App.
			<br /><br />
			You are prohibited from posting, transmitting or otherwise using any unlawful, threatening, defamatory, obscene, scandalous, inflammatory or profane material or any material that could constitute or encourage conduct that would be considered a criminal offence, give rise to civil liability, or otherwise violate any law in connection with your use of the App. CCO will fully cooperate with any law enforcement authorities or court order requesting or directing CCO to disclose the identity of anyone posting any such information or materials and reserves the right to remove any such materials at any time from this App or to delete any Output composed of such materials.
			<br /><br />
			Please note that other App visitors may post messages or make statements in the services that are inaccurate, misleading, deceptive, or offensive. CCO and the affiliated entities neither endorse nor are responsible for any opinion, advice, information or statements made in any services by third parties. Without limitation, CCO and the affiliated entities are not responsible for any information or materials made available through the services (including without limitation errors or omissions in postings or links or images embedded in messages or profiles) or results obtained by using any such information or materials.
			<br />
			<br />
			<br />
			</li>


			<li>
			CCO's Liabilities to You
			<br /><br />
			While CCO endeavours to include accurate and up to date information in the App, CCO makes no warranties or representations as to its accuracy. CCO assumes no liability or responsibility for any errors or omissions in the content of the App or the Ouput.
			<br /><br />
			Your use of and browsing in the App are at your risk. Neither CCO nor its Affiliates, nor any of its agencies, nor any other party involved in creating, producing, or delivering the App, is liable for any direct, incidental, consequential, indirect, or punitive damages or losses arising out of your access to, or use of, the App or the Output provided that we do not limit in any way our liability by law for death or personal injury caused by our negligence. You are advised that there are risks associated with activities related to the world wide web and incurred by using Facebook, the internet and world wide web, and these risks arising from your use are borne by you.
			<br /><br />
			In addition you should note: the App displays videos which have been designed and performed by specialists to provide entertainment for use in this digital context only. The participants shown in these videos are professional performers and stuntmen, filmed in setups specifically designed for them. The activities shown in the video are not designed for replication and CCO specifically discourages all App users from attempting to replicate or mimic any of the activities shown in any video. CCO and its Affiliates exclude all liability for any loss, damage or claim arising from any use of the information on this App, and specifically from any physical replication of the activities shown within it.
			<br /><br />
			CCO makes no warranty that the App or the Output will meet users' requirements, that the App will be uninterrupted, timely, secure, or error free. Users understand and agree that any information or material and/or goods or services obtained through the App, including the Output itself, is obtained at the user's discretion and risk.
			<br /><br />
			CCO also assumes no responsibility, and shall not be liable for, any damage to, or virus that may infect, your computer equipment or other property on account of your access to, use of browsing in the App, or your downloading of any materials including the Output provided that we do not limit in any way our liability by law for death or personal injury caused by our negligence. Everything in the App is provided to you "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT provided that we do not limit in any way our liability by law for death or personal injury caused by our negligence.
			<br /><br />
			Please note that some jurisdictions may not allow the exclusion of implied warranties, so some of the above exclusions may not apply to you.
			<br /><br />
			CCO opposes the use of unwanted and unsolicited email which deteriorates the performance and availability of the App services. All forms of such email or postings which have the effect of promoting such effects are discouraged and will be removed from the App where detected. CCO prohibits the use of another internet service to send or post unsolicited emails to drive visitors to your site through the App, whether or not the messages were originated by you, or under your direction or by or under the direction of a related or unrelated third party, and may take any necessary action to stop such activity wherever detected.
			<br /><br />
			CCO has not reviewed any or all of the sites linked to the App and is not responsible for the content or the privacy policies of any off-site pages or any other sites linked to the App. CCO does not accept any liability or responsibility for any cookies, data or personal information collected by other linked sites accessed through this site, nor the linked sites.
			<br />
			<br />
			<br />
			</li>

			<li>
			Your liability to CCO
			<br />
			<br />
			You agree to defend, indemnify and hold harmless CCO, its Affiliates, and any other party involved in creating, producing or delivering the App, and their respective directors, officers, employees, agents, shareholders, licensors and representatives, from and against all claims, losses, costs and expenses (including without limitation attorneys' fees) arising out of (a) your use of, or activities in connection with, the App, the Output, the services, or the software; (b) any violation of these Terms of Use by you or through your Facebook registration; or (c) any allegation that any user content that you make available or create through the App, the services, or the software, including the Output, infringes or otherwise violates the copyright, trademark, trade secret, privacy or other intellectual property or other rights of any third party.
			<br />
			<br />
			<br />
			</li>

			<li>
			Ownership of Content and Intellectual Property
			<br />
			<br />
			All trade marks are protected from commercial use by third parties. You must not use any registered or unregistered trade marks on the App or the Output without either our prior written permission or the prior written permission of the relevant trade mark owner.
			<br /><br />
			You should assume that everything you see or read on the App and the Output is copyrighted unless otherwise noted and may not be used except as provided in these Terms of Use or in the text on the App without the written permission of CCO. CCO does not warrant or represent that your use of materials displayed on the App or the Output will not infringe rights of third parties.
			<br /><br />
			Except for the images that you upload in using the App, images of people or places displayed on the App and the Output are either the property of, or used with permission by CCO. The use of these images by you, or anyone else authorised by you, is prohibited unless specifically permitted by these Terms of Use or specific permission provided elsewhere on the App. Any unauthorised use of the images may violate copyright laws, trademark laws, the laws of privacy and publicity, and communications regulations and statutes.
			<br /><br />
			Any images that you upload while using this App must either be your own personal property or used with the permission of the image owner. You must obtain full consent from any person who is shown in any image that you upload to the App, prior to making use of that person's image. By uploading an image or creating Output, you grant to CCO of a royalty-free, transferable, assignable, irrevocable, perpetual, non-exclusive license to use, reproduce, modify, publish, create derivative works from, communicate to the public including transmission over the internet and display such image or the Output in whole or in part, on a worldwide basis, and to incorporate it into other works, in any form, media or technology now known or later developed for any purpose whatsoever, including for promotional or marketing purposes, without any inspection or approval by you or any attribution to you.
			<br /><br />
			You are also advised that CCO or its Affiliates, in their sole discretion, may choose to aggressively enforce its or its Affiliates' intellectual property rights to the fullest extent of the law, including the seeking of criminal prosecution or to refer violators to the appropriate authorities in the relevant jurisdiction.
			<br />
			<br />
			<br />
			</li>

			<li>
			Special Note for Parents
			<br /><br />
			CCO takes seriously its obligations under the applicable legislation and guidelines protecting the personal information of children under the age of 13. Facebook also prohibits the registration of users aged under 13. CCO therefore does not intend that this App should be used by children and does not encourage children to submit personal information to the App. Should CCO discover that the App has been used by users under the age of 13, or that Output contains images of users under the age of 13, CCO may delete or otherwise remove such images from the App, and may destroy the Output at its discretion.
			<br /><br />
			Parents should be aware that information that is voluntarily given by children or others in chat sessions, email exchanges, bulletin boards or the like may be used by other parties to generate unsolicited email or other contacts. CCO encourages all parents to instruct their children in the safe and responsible use of the Internet.
			<br />
			<br />
			<br />
			</li>

			<li>
			Breach of these Terms of Use
			<br /><br />
			CCO may, in its sole discretion, terminate your access to the App or the Facebook page or any part of it and remove and discard any content within the App, and may delete any content including the Output created through the App, or through the service provided by your Facebook registration, for any reason, including without limitation, if CCO believes that you have violated or acted inconsistently with the letter or spirit of these Terms of Use. CCO reserves the right to contact any authority or other relevant party and disclose any personal information and information regarding your conduct at its sole discretion.
			<br />
			<br />
			<br />
			</li>

		</ol>
<br />

</section>
</div>