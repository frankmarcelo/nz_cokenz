<div class="typography">

<h1>COCA-COLA ZERO All Blacks Promotion - in association with New World</h1>

	<section id="content">

		<h2>Terms and Conditions of Entry</h2>

		<ol>
			<h3>ENTRY</h3>
			<li>Entry into the competition constitutes acceptance of these
				terms and conditions.</li>
			<li>Information on how to enter and prizes form part of these
				terms and conditions. Any entry not complying with these terms and
				conditions is invalid.</li>
			<li>Entry is open to Fly Buys cardholders who are (a) not
				employees and their immediate families, of the Promoter (Coca-Cola
				Amatil (N.Z) Ltd), Loyalty New Zealand Limited (<span style="font-weight:bold;">Fly Buys</span>),
				Foodstuffs (Auckland) Limited, Foodstuffs (Wellington) Cooperative
				Society Limited, Foodstuffs South Island Limited, their respective
				related companies, its agencies, and anyone else professionally
				connected with the competition; (b) are not a spouse, de facto
				spouse, parent, child or sibling (whether natural or by adoption) of
				such an employee, and anyone else professionally connected with this
				competition; and (c) are not a director, shareholder, officer,
				employee, contractor or agent of any Prize Pack supplier
				participating in the competition.</li>
			<li>You automatically qualify to enter the competition when you
				purchase any product from the 'Coca-Cola', 'Coca-Cola Zero', 'Diet Coca-Cola', 'Vanilla Coca-Cola', 'Diet Coca-Cola Caffeine Free', 'Sprite', 'Sprite Zero', 'Fanta', 'Lift', 'Diet Lift' or 'L&P'  range of products, from any
				New World Supermarket, and swipe your Fly Buys card at the checkout.
				You may enter as many times as you wish during the competition
				period.</li>
			<li>These terms and conditions and this competition cover all
				New World stores across New Zealand.</li>

			<br />
			<h3>PRIZE</h3>
			<li>There is one prize to be won in each New World supermarket
				nationwide which consists of two tickets to an All Blacks game
				together with $500 cash (the <span style="font-weight:bold;">Prize</span>):
				<ul style="list-style-type: none;">
					<li>(a) Winners in the upper North Island region will win tickets
						to the All Blacks v Australia match at Eden Park in Auckland on
						Saturday 25 August 2012;</li>
					<li>(b) Winners in the lower North Island region will win tickets
						to the All Blacks v Argentina match at Westpac stadium in
						Wellington on 8 September 2012;</li>
					<li>(c) Winners in the South Island will win tickets to the All
						Blacks v South Africa match at the Forsyth Barr Stadium in Dunedin
						on 15 September 2012.</li>
				</ul>
			<li>There are 139 Prizes (50 upper North Island, 49 lower North
				Island and 40 South Island) in total based on the number of New
				World stores.</li>
			<li>The Prizes do not include travel or accommodation for the
				matches.</li>
			<li>Use of Match tickets are subject to the terms and conditions
				on each ticket and on <a href="www.allblacks.com">www.allblacks.com</a>.
			</li>
			<li>All Prizes are not transferrable, exchangeable or redeemable
				for cash.</li>


			<h3>PRIZE DRAW</h3>
			<li>The competition commences at 00.01am on 18 June 2012 and
				closes at 11.59pm on 30 July 2012.</li>
			<li>The Prizes will be drawn on 6 August 2012 by Fly Buys by
				random electronic selection from all valid entries. There will be
				one winner drawn from each participating New World store, each of
				whom will win a Prize.</li>
			<li>For your entry to be eligible to win, your Fly Buys account
				details must be complete and up to date (including a phone number).</li>
			<li>Fly Buys membership terms and conditions also apply and can
				be viewed at <a href="www.flybuys.co.nz">www.flybuys.co.nz</a>.
			</li>
			<li>The Promoter's decisions and the draw results are final and
				not subject to appeal.</li>
			<li>The Prize must not be sold or used, donated or given away as
				part of another competition.</li>
			<li>Match tickets and spending money will be sent out to the
				winners as soon as practicable following the draw date and after the
				Prize winner's details have been confirmed:
				<ul style="list-style-type: none;">
					<li>(a) If a Prize winner is under 18 years of age at the
						start of the competition, their prize will be transferred to their
						parent or guardian who is 18 years of age or older.</li>
					<li>(b) The match tickets will be couriered (using a track and
						trace courier) to each winner directly.</li>
					<li>(c) The winner must make contact with the Promoter in
						order to claim their $500 cash prize by 10 August for upper North
						Island winners and 17 August for lower North Island and South
						Island winners. The winner will be required to provide bank
						account details so the money can be placed directly into his/her
						account via direct debit.</li>
				</ul>
			<li>By accepting the Prize, the winner releases the Promoter and
				its related companies and each of their officer, directors,
				shareholders, employees, advisors, assignees, agents, licensees,
				representatives, advertising and promotional agencies from any and
				all liability in connection with this competition and/or the Prize.</li>
			<li>The Prize winner's take the Prize entirely at their own
				risk.</li>

			<h3>EXCLUSIONS AND LIABILITY</h3>
			<li>The Promoter will not be responsible for any incorrect or
				inaccurate information concerning this competition or any technical
				errors that may occur during administration of this competition.</li>
			<h3> USE OF PERSONAL INFORMATION</h3>
			<li>Under the Privacy Act, you have the right to access and
				correct any personal information held by Fly Buys.</li>
			<li>By entering the competition, Prize winners agree that the
				Promoter may use their name, image and photograph/s for publicity
				and promotion purposes, without compensation, and agrees that the
				Promoter will own copyright in any such images and photograph/s and
				in all material incorporating the photograph/s.</li>
			<h3>PROMOTERS DETAILS</h3>
			<li>This competition is being run in conjunction with Foodstuffs
				(Auckland) Limited.</li>
			<li>The Promoter is Coca-Cola Amatil (N.Z) Ltd of The Oasis, Mt
				Wellington, Auckland. For further assistance or queries regarding
				this competition please call 0800 505 123.</li>
		</ol>
		<br />

	</section>
</div>