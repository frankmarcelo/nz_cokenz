<?php $sf_response->setTitle('Privacy Policy');?>
<div class="typography">

       <h1>Privacy Policy</h1>

       <section id="content">

<p>
        This 'Privacy Policy' was updated 26 July 2011 and comprises the policies relating to the personal information you provide to use during your use of the www.coke.co.nz internet website.
</p>	
<p>
CCO: refers to Coca-Cola Oceania Limited, The Oasis Carbine Rd, Mt Wellington 1060, Auckland, New Zealand. PO Box 62042 Mt Wellington, 
0800 505 123.
</p>
<p>
Website: refers to this website, www.coke.co.nz
</p>
<p>
Affiliates: refers to any direct or indirect parent, subsidiaries, sponsors, or affiliated companies of CCO and shall include any authorised bottlers of 
The Coca-Cola Company.
</p>
<p>
1. Your Agreement to this Privacy Policy
</p>
<p>
By using this Website and submitting your personal information to us, you agree to the collection, use and disclosure of personal information by CCO or its Affiliates as set out in this Privacy Policy.
You must read this Privacy Policy before using our Website. Your use of this Website signifies that you agree with all terms of this Privacy Policy, so please do not use our Website if you disagree with any part of this Privacy Policy.
</p>
<p>
2. Your Personal Information is Important
</p>
<p>
We recognise that your privacy is very important to you and CCO is committed to protecting your privacy. This Privacy Policy sets out how we handle your personal information and safeguard your privacy in relation to this Website.
</p>
<p>
We will comply with the New Zealand Privacy Act 1993 and other requirements of New Zealand legislation to protect your privacy. Note that this Policy does not affect your rights under the New Zealand Privacy Act 1993, nor is it intended to limit or exclude your rights under that Act.
</p>
<p>
3. What Personal Information do we collect and why do we collect it?
</p>
<p>
CCO may collect personal information about you (such as your name, postal address, telephone number, email address, details of your family or household structure such as the number of children you have) that you provide to us voluntarily. We will use such information for the purpose for which it has been provided. This may include using your information for the following purposes:
</p>
<p>
1.For the administration of the Website;
</p>
<p>
2.To respond to and deal with questions comments enquries or complaints and other customer care related activities submitted through the use of the "Contact Us", "Comment" or any other features on the Website; and
</p>
<p>
3.All other general administrative and business purposes.
</p>
<p>
If you do not want your personal information collected, please do not submit it to us.
</p>
<p>
You ordinarily are not required to register or provide personal information in order to access this Website, although we require you to register on this Website if you wish to receive newsletter communications from us. By registering, you agree to receiving communications from CCO and the Website. You may also choose to receive further communications from The Coca-Cola Company through your registration. You may choose to opt out of receiving these communications and we will give you the opportunity to do that each time we contact you. If you want to opt out at any time from receiving newsletter communications, you may do so on your 'profile' page.
</p>
<p>
4. When do we disclose the personal information we collect?
</p>
<p>
Except as provided in this Privacy Policy, CCO will not now, and does not intend to, sell, transfer or rent personal information about you to unaffiliated third parties.
</p>
<p>
In addition to the purposes stated in section 3 above, we may disclose personal information you have provided through our websites to persons or companies that we retain to send communications or facilitate other activities for which you have registered or in which you have otherwise asked to participate. We also will disclose personal information if required by law, including compliance with warrants, subpoenas or other legal process.
</p>
<p>
CCO requires persons and companies to which it discloses personal information to restrict their use of such information to the purposes for which it has been provided by CCO and not to disclose that information to others. CCO cannot be responsible, however, for any damages caused by the failure of unaffiliated third parties to honour their privacy obligations to CCO. Similarly, CCO is not responsible for the privacy policies and practices of other websites that are linked to the Website.
</p>
<p>
In addition to these limited disclosures of personal information, CCO may provide its Affiliates or unaffiliated third parties with aggregate information about visitors to our sites. For example, we might disclose the median ages of visitors to our websites, or the numbers of visitors to our websites that come from different geographic areas. Such aggregate information will not include information of any individual visitors to our websites.
</p>
<p>
CCO may provide personal and other information to a purchaser or successor entity in connection with the sale of CCO, a subsidiary or line of business associated with CCO, or substantially all of the assets of CCO or one of its subsidiaries, Affiliates or lines of business.
</p>
<p>
5. Special Note for Parents
</p>
<p>
CCO takes seriously its obligations under the applicable legislation and guidelines protecting the personal information of children under the age of 13. CCO will specifically instruct children not to submit personal information nor attempt to register on this Website.
Parents should be aware that information that is voluntarily given by children - or others - in chat sessions, email exchanges, bulletin boards or the like may be used by other parties to generate unsolicited email or other contacts. CCO encourages all parents to instruct their children in the safe and responsible use of the Internet.
</p>
<p>
6. How do we protect your personal information?
</p>
<p>
CCO takes measures to prevent unauthorised intrusion in the Website and the alteration, acquisition or misuse of personal information by unauthorised persons. However, CCO cautions visitors to this Website that no network, including the Internet, is entirely secure. Accordingly, we cannot be responsible for loss, corruption or unauthorised acquisition of personal information provided to this Website, or for any damages resulting from such loss, corruption or unauthorised acquisition.
</p>
<p>
7. How do we maintain the integrity of your personal information?
</p>
<p>
CCO has procedures in place to keep your personal information accurate, complete and current for the purposes for which it is collected and used. You may review the information that you have provided to us and where appropriate you may request that it be corrected. If you wish to review your personal information please call our Customer Information Centre 0800 505 123 or send a request to:
</p>
<p>
Attn: Website Privacy Policy <br/>
Customer Information Centre<br/>
Coca-Cola Oceania Ltd<br/>
The Oasis Carbine Rd<br/>
Mt Wellington, 1060<br/>
PO Box 62042<br/>
Auckland, New Zealand
</p>
<p>
You also may obtain a copy of your personal information by contacting us by email.
</p>
<p>
8. What other information do we collect?
</p>
<p>
When you access and interact with a CCO website, we may collect certain information - often referred to as "clickstream data." Such data may include the type of Internet browser and operating system you are using, the pages and information you accessed on our site, the total time spent on our site, and the domain name of the website from which you linked to our site. Also, where you have previously identified yourself in the registration process as affiliated with a particular business or organization, we may record the fact that a representative of your business or organization visited the site, along with other clickstream data concerning that visit. Clickstream data will not be used to identify you personally and will not be associated or correlated with any site visitor in a personally-identifiable manner.
</p>
<p>
9. Information placed automatically on your computer - Cookies
</p>
<p>
When you view this Website, we may store certain information on your computer. This information may be in the form of a small text file called a "cookie" and can help us improve your experience with our websites in many ways. For example, cookies allow us to tailor a website to better match your interests and preferences. Notably, our websites use cookies to help understand which parts of our websites are most popular, where our visitors are going and how much time they spend there. Cookies are read only by the server that placed them, and are unable to execute any code or virus.
</p>
<p>
With most Internet browsers, you can erase cookies from your computer hard drive, block all cookies or receive a warning before a cookie is stored. Please refer to your browser instructions or help screen to learn more about these functions. Please be aware, however, that some features and programs of our websites may be unavailable to you if cookies are erased or blocked.
</p>
<p>
Some uses of cookies in connection with this Website may be under the control of unaffiliated entities that CCO retains for the management of certain programs and fulfillment of specific visitor/customer requests. CCO requires these entities to confine their use of cookies to the uses permitted by this Privacy Policy, but CCO cannot be responsible for third party uses of cookies.
</p>
<p>
10. How can you ask questions about our Privacy Policy and access your personal information?
</p>
<p>
If you have questions or concerns about this Privacy Policy, wish to access your personal information or request that we not use your personal information for a particular purpose, please follow the instructions posted at Contact Us.
</p>
<p>
11. Changes to this Policy
</p>
<p>
This Privacy Policy is the sole authorised statement of CCO's practices with respect to the collection of personal information through this Website and the subsequent use and disclosure of such information. Any summaries of this Privacy Policy generated by third party software or otherwise shall have no legal effect, are in no way binding upon CCO, shall not be relied upon in substitute for this Privacy Policy, and neither supersede nor modify this Privacy Policy.
</p>
<p>
CCO may revise this Privacy Policy from time to time. You should bookmark and periodically review this page to ensure that you are familiar with the most current version of this Privacy Policy. You can determine when this Privacy Policy was last revised by reviewing the statement at the top of the Privacy Policy.
</p>

       </section>

       </div>