<?php $sf_request->setAttribute('pageClass','contactus') ?>

<script src="/js/contact-form-script.js"></script>

<div class="cokebottle">
   <div class=" contentheader typography">
   	<h1>Contact Us</h1>
   </div>
    <div id="contactusmain">
    	<p class="bottomborder">
    		<noscript>
    			<h4>*Please enable javascript to display this page correctly. </h4>
    		</noscript>
        Have a question you'd like to ask about this site, or COCA-COLA? Or maybe you have a suggestion to improve this website? Enter your message below and we'll get back to you as soon as we can. Or alternatively you can call or write to contact us.
        <br/>
        <br/>Coca-Cola Amatil (NZ) Ltd.
        <br/>The Oasis, Oasis Lane, Mt Wellington
        <br/>Auckland 6, New Zealand
        <br/>
        <br/>Tel: 0800 505 123
        <br/>Fax: 0800 265 332
      </p>

    <?php if($underage): ?>
      <h4 class="msg">I'm sorry, we cannot accept your submission at this time.</h4>
    <?php else: ?>

    <form name="contactform" id="contactform" action="<?php echo url_for('@contact') ?>" method="post" >

    	<div class="bottomborder categories">
    		Please select your query from the categories below: <br/>

        <?php echo $form['type']->renderRow() ?>

    	</div>

    <div class="bottomborder" >
        <div id="maincontent">
          <?php if( $form->hasErrors() ): ?>
            <h4 class="err">* Please provide the required information</h4>
          <?php endif; ?>
	     		<h4>Contact Details</h4>
          <?php echo $form['first_name']->renderRow(array('class'=>'text')) ?>
          <?php echo $form['last_name']->renderRow(array('class'=>'text')) ?>
          <?php echo $form['address']->renderRow(array('class'=>'text')) ?>
          <?php echo $form['phone_number']->renderRow(array('class'=>'text')) ?>
          <?php echo $form['email']->renderRow(array('class'=>'text')) ?>
          <?php echo $form['date_of_birth']->renderLabel('*Date of Birth') ?>
          <div class="selects">
            <?php echo $form['date_of_birth']->render() ?>
          </div>
          <?php echo $form['date_of_birth']->renderError() ?>
        </div>

        <div id="prodcontent" class="extra" <?php echo ($form->getTaintedValue('type') == 'product') ? '' : 'style="display:none"' ?>>
          <h4>Which product does this relate to?</h4>
          <?php echo $form['product_name']->renderRow(array('class'=>'text')) ?>
          <?php echo $form['pack_size']->renderRow(array('class'=>'text')) ?>

          <h4>Product details</h4>
          <?php echo $form['best_before_date']->renderRow(array('class'=>'text')) ?>	 <a class="redlink">Where can I find this?<img src="/images/redarrow.jpg"/></a>
          <?php echo $form['production_code']->renderRow(array('class'=>'text')) ?>	 <a class="redlink">Where can I find this?<img src="/images/redarrow.jpg"/></a>

          <h4>Place of Purchase</h4>
          <?php echo $form['prod_store']->renderRow(array('class'=>'text')) ?>
          <?php echo $form['prod_suburb_town']->renderRow(array('class'=>'text'),'Suburb/Town') ?>
          <img id="wheretofind" style="display:none" src="/images/wheretofind.jpg"/>
        </div>

        <div id="promcontent" class="extra" <?php echo ($form->getTaintedValue('type') == 'promotion') ? '' : 'style="display:none"' ?>>
		     	<h4>Which promotion is this about?</h4>
          <?php echo $form['promotion_name']->renderRow(array('class'=>'text')) ?>
		     	<h4>Where did you purchase the promotional product?</h4>
          <?php echo $form['promo_store']->renderRow(array('class'=>'text')) ?>
          <?php echo $form['promo_suburb_town']->renderRow(array('class'=>'text'),'Suburb/Town') ?>
        </div>

        <div id="maincontent2">
	     		<h4>What is the nature of your enquiry?</h4>
           <?php echo $form['enquiry']->render() ?>
           <?php echo $form['enquiry']->renderError() ?>

        </div>
    </div>


    <div class="bottomborder">
        <?php echo $form['security_code']->renderRow(array('class'=>'text')) ?>
<div class="clearfix" style="margin-bottom: 20px"></div>
        <ul class="radio_list">
            <li>
                <?php echo $form['accept_pp']->renderError() ?>
                <?php echo $form['accept_pp']->render() ?>
                <?php echo $form['accept_pp']->renderLabel(sprintf('I have read and agree to the %s',link_to('Privacy Policy','@privacy_policy',array('class' => 'new_window')))) ?>
            </li>
        </ul>


    </div>
    <input class="submit" type="submit" value="Submit"/>	<a href="<?php echo url_for('@contact') ?>" id="clearform">Clear form</a>
      <?php echo $form->renderHiddenFields();  ?>
</form>


    <div id="footnote">* Mandatory</div>

    <?php endif; ?>

  </div>

</div>