<?php $sf_response->setTitle('Terms of Use');?>

<div class="typography">

<h1>Terms of Use</h1>

<section id="content">

<p>
         These 'Terms of Use' were updated on 26 July 2011 and comprise the terms on which you use the www.coke.co.nz internet website.
</p>
<p>
In these Terms of Use:
</p>
<p>
CCO: refers to Coca-Cola Oceania Ltd, The Oasis Carbine Rd, Mt Wellington 1060, PO Box 62042, Auckland, New Zealand
</p>
<p>
Website: refers to this website, www.coke.co.nz
</p>
<p>
Affiliates: refers to any direct or indirect parent, subsidiaries, sponsors, or affiliated companies of CCO and shall include any authorised bottlers of The Coca-Cola Company.
</p>
<p>
1. Your Agreement to these Terms and Your Use of this Website
</p>
<p>
Your use of this Website, including accessing or browsing this Website, is deemed to be your acceptance of these Terms of Use and the Privacy Policy. Please read both of them carefully. If you do not agree and accept, without limitation or qualification, these Terms of Use, please exit the Website immediately and refrain from using this Website in the future.
</p>
<p>
Your use of this Website is also subject to any other law or regulation that applies to this Website, the internet or the worldwide web.
</p>
<p>
This Website and its content are made available for your personal entertainment, information and communication only, and all content is provided for your non-commercial use. You may not distribute, modify, transmit, reuse, repost, or use the content of the Website for public or commercial purposes, including the text, images, audio and video without the written permission of CCO.
</p>
<p>
This Website is intended for use by residents of New Zealand only. These Terms of Use are governed by the laws of New Zealand. By using the Website you agree to submit to the exclusive jurisdiction of the New Zealand courts in the event of any dispute. If any of these Terms of Use are held to be invalid, unenforceable or illegal for any reason, the remaining Terms of Use will continue in full force.
</p>
<p>
CCO may at any time revise these Terms of Use by updating this posting. You are bound by any such revisions and should therefore periodically visit this page to review the then current Terms of Use to which you are bound. You will be bound by changes even if you do not re-visit this page to re-read this notice.
</p>
<p>
2. Registration
</p>
<p>
If you wish to receive newsletters and updates from this Website, you must register to use this Website. To register, you must provide a valid email address and you must be over 13 years old at the date of your registration. You are responsible for maintaining the confidentiality of your registrations and any passwords that you may set in order to obtain your registration. You are responsible for all uses of your registration, whether or not expressly authorised by you.
</p>
<p>
You may opt out of or unsubscribe from your registration, or from receiving information about other programs, at any time by clicking the unsubscribe link in the footer of this Website.
</p>
<p>
Any personal data (for example, your name, address, telephone number or e-mail address) you transmit to the Website by electronic mail or by direct entry or otherwise, will be used by CCO in accordance with the Website Privacy Policy. Any other communication or material you transmit to the Website, such as questions, comments, suggestions or the like, will be treated as non-confidential and non-proprietary.
</p>
<p>
CCO reserves the right to release your private information to authorities and your internet service provider if you conduct any illegal activities or otherwise breach these Terms of Use. Please click here to view CCO's Privacy Policy.
</p>
<p>
3. Conduct
</p>
<p>
You agree not to engage in any activity that is disruptive or is detrimental to the use of this Website.
</p>
<p>
Subject to your compliance with these Terms of Use, and solely for so long as you are permitted by CCO to access and use the Website, you may download and view one (1) copy of any content and software on the Website to which we provide you access or download, on any single computer, solely for your personal, non-commercial home use, provided that you keep intact all copyright and other proprietary notices. Except as otherwise expressly authorised in writing in advance by CCO, you agree not to reproduce, modify, rent, lease, loan, sell, distribute, adapt, translate, create derivative works based (whether in whole or in part) on, reverse engineer, decompile or disassemble any services or software, all or any part of the Website, or any materials made available through the Website. Certain software may be governed by an additional end user licence agreement to which you may be required to agree before using such software.
</p>
<p>
Although CCO may from time to time, monitor discussions, chats postings, transmissions, bulletin boards, and the like on the Website, CCO is under no obligation to do so and assumes no responsibility or liability arising from the content of any such locations nor for any error, defamation, omission, falsehood, obscenity, profanity, danger, or inaccuracy contained in any information within such locations on the Website.
You are prohibited from posting or transmitting any unlawful, threatening, defamatory, obscene, scandalous, inflammatory or profane material or any material that could constitute or encourage conduct that would be considered a criminal offence, give rise to civil liability, or otherwise violate any law. CCO will fully cooperate with any law enforcement authorities or court order requesting or directing CCO to disclose the identity of anyone posting any such information or materials and reserves the right to remove any such materials at any time from this Website.
</p>
<p>
Please note that other Website visitors may post messages or make statements in the services that are inaccurate, misleading, deceptive, or offensive. CCO and the affiliates entities neither endorse nor are responsible for any opinion, advice, information or statements made in any services by third parties. Without limitation, CCO and the affiliated entities are not responsible for any information or materials made available through the services (including without limitation errors or omissions in postings or links or images embedded in messages or profiles) or results obtained by using any such information or materials.
</p>
<p>
4. CCO's Liabilities to You
</p>
<p>
While CCO endeavours to include accurate and up to date information in the Website, CCO makes no warranties or representations as to its accuracy. CCO assumes no liability or responsibility for any errors or omissions in the content of the Website. CCO will use its best endeavours to correct any error or inaccuracy on the Website within a reasonable time of its being brought to our attention.
</p>
<p>
Your use of and browsing in the Website are at your risk. Neither CCO nor its Affiliates, nor any of its agencies, nor any other party involved in creating, producing, or delivering the Website, is liable for any direct, incidental, consequential, indirect, or punitive damages or losses arising out of your access to, or use of, the Website provided that we do not limit in any way our liability by law for death or personal injury caused by our negligence. You are advised that there are risks associated with activities related to the world wide web and incurred by using the internet and world wide web, and these risks arising from your use are borne by you.
</p>
<p>
CCO makes no warranty that the Website will meet users' requirements, that the Website will be uninterrupted, timely, secure, or error free. Users understand and agree that any information or material and/or goods or services obtained through the Website is obtained at the user's discretion and risk. 
CCO also assumes no responsibility, and shall not be liable for, any damage to, or virus that may infect, your computer equipment or other property on account of your access to, use of browsing in the Website, or your downloading of any materials provided that we do not limit in any way our liability by law for death or personal injury caused by our negligence. Everything on the Website is provided to you "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT provided that we do not limit in any way our liability by law for death or personal injury caused by our negligence.
</p>
<p>
Please note that some jurisdictions may not allow the exclusion of implied warranties, so some of the above exclusions may not apply to you.
</p>
<p>
The Website and CCO oppose the use of unwanted and unsolicited email which deteriorates the performance and availability of the Website services. All forms of such email which have the effect of promoting such emails are discouraged and will be removed from the Website where detected. CCO prohibits the use of another internet service to send or post unsolicited emails to drive visitors to your site through the Website, whether or not the messages were originated by you, or under your direction or by or under the direction of a related or unrelated third party, and may take any necessary action to stop such activity wherever detected.
</p>
<p>
CCO has not reviewed any or all of the sites linked to the Website and is not responsible for the content or the privacy policies of any off-site pages or any other sites linked to the Website. CCO does not accept any liability or responsibility for any cookies, data or personal information collected by other linked sites accessed through this site, nor the linked sites.
</p>
<p>
5. Your liability to CCO
</p>
<p>
You agree to defend, indemnify and hold harmless CCO, its Affiliates, and any other party involved in creating, producing or delivering the Website, and their respective directors, officers, employees, agents, shareholders, licensors and representatives, from and against all claims, losses, costs and expenses (including without limitation attorneys' fees) arising out of:
</p>
<p>
a.	your use of, or activities in connection with, the Website, the services, or the software; 
<br/>b.	any violation of these Terms of Use by you or through your registration; or 
<br/>c.	any allegation that any user content that you make available or create through the Website, the services, or the software infringes or otherwise violates the copyright, trademark, trade secret, privacy or other intellectual property or other rights of any third party.
</p>
<p>
6. Ownership of Content and Intellectual Property
</p>
<p>
All trade marks are protected from commercial use by third parties. You must not use any registered or unregistered trade marks on the Website without either our prior written permission or the prior written permission of the relevant trade mark owner.
</p>
<p>
You should assume that everything you see or read on the Website is copyrighted unless otherwise noted and may not be used except as provided in these Terms of Use or in the text on the Website without the written permission of CCO. CCO does not warrant or represent that your use of materials displayed on the Website will not infringe rights of third parties.
</p>
<p>
Images of people or places displayed on the Website are either the property of, or used with permission by CCO. The use of these images by you, or anyone else authorised by you, is prohibited unless specifically permitted by these Terms of Use or specific permission provided elsewhere on the Website. Any unauthorised use of the images may violate copyright laws, trademark laws, the laws of privacy and publicity, and communications regulations and statutes.
</p>
<p>
You are also advised that CCO or its Affiliates, in their sole discretion, may choose to aggressively enforce its or its Affiliates' intellectual property rights to the fullest extent of the law, including the seeking of criminal prosecution or to refer violators to the appropriate authorities in the relevant jurisdiction.
</p>
<p>
Breach of these Terms of Use
</p>
<p>
CCO may, in its sole discretion, terminate your registration or any part of it and remove and discard any content within the Website, or the service provided by your registration, for any reason, including without limitation, if CCO believes that you have violated or acted inconsistently with the letter or spirit of these Terms of Use. CCO reserves the right to contact any authority or other relevant party and disclose any personal information and information regarding your conduct at its sole discretion.
</p>
          
</section>
</div>