

<div id="promoBanner">

  <!-- PROMO BANNER - 950X300 -->
  <a href="http://www.facebook.com/CocaColaNZ/app_365327146872349" class="new_window" title="Get together and share a Coke">
     <img src="/images/banners/share-a-note.jpg" alt="Get together and share a Coke" /></a>
  <!-- /PROMO BANNER -->

</div>

<ul id="promoTiles">

  <li class="promoTile">

    <!-- PROMO TILE 1 - 230x240 -->
      <img src="/images/banners/tile-mobile-web.jpg" alt="Mobile website" />
    <!-- /PROMO TILE 1 -->

  </li>
  <li class="promoTile">

    <!-- PROMO TILE 1 - 230x240 -->
    <a href="https://www.facebook.com/CocaColaNZ" class="new_window" title="Coca-Cola on Facebook">
        <img src="/images/banners/tile-facebook.jpg" alt="Coca-Cola on Facebook" />
    </a>
    <!-- /PROMO TILE 1 -->

  </li>
  <li class="promoTile">

    <!-- PROMO TILE 3 - 230x240 -->
     <a href="http://www.livepositively.co.nz" class="new_window" title="Live Positively">
         <img src="/images/banners/tile-livepos.jpg" alt="Live Positively" />
     </a>
    <!-- /PROMO TILE 3 -->

  </li>
  <li class="promoTile last">

    <!-- PROMO TILE 4 - 230x240 -->
    <a href="https://www.facebook.com/CocaColaNZ/app_387898684580700" class="new_window" title="Morph Heroes">
        <img src="/images/banners/tile-morph-hero.jpg" alt="Morph Heroes" />
    </a>
    <!-- /PROMO TILE -->

  </li>

</ul>