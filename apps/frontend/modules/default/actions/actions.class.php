<?php

/**
 * default actions.
 *
 * @package    Coke NZ
 * @subpackage default
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class defaultActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request) {

    if( !isset($_COOKIE['show_desktop']) ) {

      switch( $request->getParameter('view','') ) {
        case 'once':
          $this->getResponse()->setCookie('show_desktop', 'once', time(), '/', $request->getHost(), false, true);
          break;

        case 'always':
          $this->getResponse()->setCookie('show_desktop', 'always', time() + 2592000, '/', $request->getHost(), false, true);
          break;

        default:
          //detect mobile browsers and redirect
          require_once( sfConfig::get('sf_lib_dir').'/vendor/mobile_device_detect.php' );
          mobile_device_detect(true,false,true,true,true,true,true, 'http://' . sfConfig::get('app_mobile_url')  );

      }

    }

  }
  public function executePrivacyPolicy(sfWebRequest $request) {}
  public function executeTermsOfUse(sfWebRequest $request) {}

  /**
   * @param sfWebRequest $request
   * To make a new promo terms page, create the partial in the modules/default/templates/_promo-terms directory
   * calling it the same as the url you want to have and passing that into the url_for function,
   * eg. url_for('promo_terms_page?page=XXXX')
   */

  public function executePromoTerms(sfWebRequest $request)
  {
    $this->page = $request->getParameter('page',null);
  }


  public function executeContact(sfWebRequest $request) {

    $form = new FrontendContactForm();
    $this->underage = false;

    if( isset($_COOKIE["underage"]) ) {
      $this->underage = true;
    }

    if( $request->isMethod('post') ){
      $form->bind($request->getParameter($form->getName()));
      if( $form->isValid() ){

        $dob = new DateTime($form->getValue('date_of_birth'));
        $now = new DateTime();
        $diff = $now->diff($dob);

        if( $diff->y >= sfConfig::get('app_min_age',14) ) {
          CokeMailer::sendContactEmail($this->getMailer(), $form);

          $this->forward('default','contactThanks');

        } else {
          $this->underage = true;
          setcookie('underage','true',time()+60*60,'/');
        }
      }
    }

    $this->form = $form;


  }
  public function executeContactThanks(sfWebRequest $request) {}
  public function executeFeature1(sfWebRequest $request) {}
  public function executeFeature2(sfWebRequest $request) {}

  public function executeError404(sfWebRequest $request) {
  }
}
