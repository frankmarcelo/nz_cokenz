<?php
use_javascript('swfobject.js');
$sf_request->setAttribute('pageClass','zerohero');
$type_config = sfConfig::get('app_zero_hero_type');
$sf_response->setTitle($log['name'].'\'s COKE ZERO '. $type_config[$log['type']]['title']);
?>

<div class="top">
    <img src="<?php echo image_path('zero_hero/title.png') ?>" />
</div>

<div class="content">
    <div class="video">
        <div id="zero"></div>
    </div>
    <div class="fb_like">
        <div class="fb-like" data-send="false" data-layout="box_count" data-width="55" data-show-faces="false"></div>
    </div>
</div>


<div class="bottom">
    <div class="inner">
        <a onclick='postToFeed(); return false;' style="cursor: pointer">
            <img src="<?php echo image_path('zero_hero/share.png') ?>" style="float: left;"/>
        </a>

        <a href="http://www.facebook.com/CocaColaNZ/app_387898684580700" class="new_window">
            <img src="<?php echo image_path('zero_hero/choose-another.png') ?>" style="float: right;"/>
        </a>
    </div>
</div>

<div id="fb-root"></div>
<script>
var flashvars = {
    type: "<?php echo $log['type'] ?>",
    name: "<?php echo $log['name'] ?>",
    image: "http://apps.coke.com.au<?php echo $log['image']  ?>",
    link: "<?php echo $log['link']  ?>",
    swflink: '/flash/zero_hero/zero.swf'
};

var params = { wmode:"transparent", menu:"false", AllowScriptAccess: "always" };

var attributes = { id: "zero"};

swfobject.embedSWF('/flash/zero_hero/loader.swf', 'zero', '640px', '436px', '10','/flash/expressInstall.swf', flashvars, params, attributes);

(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/all.js#xfbml=1&appId=<?php echo sfConfig::get('app_zero_hero_coke_nz_app_id') ?>";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

// Load the SDK Asynchronously
  (function(d){
     var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement('script'); js.id = id; js.async = true;
     js.src = "//connect.facebook.net/en_US/all.js";
     ref.parentNode.insertBefore(js, ref);
   }(document));

  window.fbAsyncInit = function() {
      FB.init({appId: "<?php echo sfConfig::get('app_zero_hero_coke_nz_app_id') ?>", status: true, cookie: true});
  }

  function postToFeed() {
    FB.ui( {
      method: 'feed',
      picture: '<?php echo sfConfig::get('app_zero_hero_type_img_base_url').$type_config[$log['type']]['img_url'] ?>',
      name: '<?php echo $log['name'] ?>\'s COKE ZERO <?php echo $type_config[$log['type']]['title'] ?>',
      link: window.location.href
    });
  }

</script>
