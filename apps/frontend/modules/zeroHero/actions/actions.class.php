<?php

class zeroHeroActions extends sfActions
{

  public function executeIndex(sfWebRequest $request)
  {
    $this->forward404Unless($id = $request->getParameter('id'));
    $token = sha1(sfConfig::get('app_zero_hero_auth_salt').$id);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, sfConfig::get('app_zero_hero_show_log_api_url').sprintf('?x=%s&token=%s', $id, $token));
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $curl_result = curl_exec($ch);
    curl_close($ch);

    $json = json_decode($curl_result, true);

//    error_log( ($curl_result ? $curl_result : 'false') , 1, 'nik@satellitemedia.co.nz');

    if ($json['status'] == 'ERROR' or !$curl_result) {
        return sfView::ERROR;
    }

    $this->log = $json['payload'];

  }


}
