<div class="cokebottle typography account">

  <h1>Your COCA-COLA Account Details</h1>

  <?php if( $sf_user->hasFlash('msg') ): ?>
  <div class="clearfloat">
    <h4 class="msg"><?php echo $sf_user->getFlash('msg') ?></h4>
  </div>
  <?php endif; ?>

  <section id="content" class="form">
    <div class="clearfloat">
      <div class="bottomborder">
        <div class="clearfloat">
          <label for="email">Email: </label>
          <span class="acctdeet"><?php echo $user->email_address ?></span>
        </div>
        <div class="clearfloat">
          <label for="dob">Date of Birth: </label>
          <div class="selects">
            <span class="acctdeet"><?php echo strftime( '%e %B %G', strtotime($user->Profile->dob)) ?></span>
          </div>
        </div>
      </div>

      <div class="bottomborder">
        <label for="">&nbsp;</label> <span class="acctdeet">I am a resident of New Zealand. </span>
      </div>

      <div class="bottomborder">
        <div class="clearfloat">
          <label for="fname">First name:</label>
          <span class="acctdeet"><?php echo $user->first_name ?></span>
        </div>
        <div class="clearfloat">
          <label for="lname">Last name:</label>
          <span class="acctdeet"><?php echo $user->last_name ?></span>
        </div>
        <div class="clearfloat">
          <label for="gender">Gender: </label>
          <div class="selects">
            <span class="acctdeet"><?php echo ucfirst($user->Profile->gender) ?></span>
          </div>
        </div>
      </div>
      <div class="bottomborder">
        <div class="clearfloat">
          <label for="num">Mobile number:</label>
          <span class="acctdeet"><?php echo $user->Profile->phone ?></span>
        </div>
        <div class="clearfloat">
          <label for="region">Region:</label>
          <div class="selects">
            <span class="acctdeet"><?php echo $user->Profile->region ?></span>
          </div>
        </div>
        <div class="clearfloat">
          <label for="post">Post code:</label>
          <span class="acctdeet"><?php echo $user->Profile->postcode ?></span>
        </div>
      </div>
      <div class="bottomborder">
        Sign me up to receive <strong>Email updates</strong> from Coca-Cola Oceania including information on promotions and community activities, which may include information on 3rd party partners
        <br/><br/>
        <label for="">&nbsp;</label> <span class="acctdeet"><?php echo $user->Profile->email_updates ?"Yes":"No"  ?></span>
      </div>
      <div class="bottomborder">
        Sign me up to receive <strong>Mobile updates</strong> from Coca-Cola Oceania including information on promotions and community activities, which may include information on 3rd party partners
        <br/><br/>
        <label for="">&nbsp;</label> <span class="acctdeet"><?php echo $user->Profile->mobile_updates ?"Yes":"No"  ?></span>
      </div>
      <div class="bottomborder">
        <label for="">&nbsp;</label><span class="acctdeet"> I agree to the <a href="<?php echo url_for('@privacy_policy') ?>" target="_blank">Privacy Policy </a></span>
      </div>
      <div class="clearfloat">
        <a href="<?php echo url_for('@user_account_change_password') ?>"><img src="/images/changebutton.png"/></a>
        <a href="<?php echo url_for('@user_account_edit') ?>"><img src="/images/updatebutton.png"/></a>
      </div>

    </div>
  </section>
</div>