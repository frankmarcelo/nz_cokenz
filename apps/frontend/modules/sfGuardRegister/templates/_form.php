<?php  if($form->hasErrors()):  ?>
  <p class="errors">Registration could not be completed as errors have occurred.</p>
<?php  endif; ?>

<?php  if($form->hasGlobalErrors()):  ?>
  <?php foreach($form->getGlobalErrors() as $error): ?>
    <p class="errors"> <?php  echo $error  ?> </p>
  <?php endforeach; ?>
<?php  endif; ?>



<form method="post" action="<?php echo ($form->isNew() ? url_for('@sf_guard_register') : url_for('@user_account_edit')) ?>">
  <div class="bottomborder">
    <div class="clearfloat">
      <?php echo $form['email_address']->renderRow(array('class' => 'text'),'Email *') ?>
    </div>

    <div class="clearfloat">
      <?php echo $form['Profile']['dob']->renderLabel('Date of Birth * ') ?>
      <div class="selects">
        <?php echo $form['Profile']['dob']->render() ?>
      </div>
      <?php echo $form['Profile']['dob']->renderError() ?>
    </div>
  </div>



  <div class="bottomborder">
    <div class="check">
      <?php echo $form['nz_resident']->render() ?>
      <?php echo $form['nz_resident']->renderLabel("* I am a resident of New Zealand") ?>
      <?php echo $form['nz_resident']->renderError() ?>
    </div>
  </div>

  <?php if($form->isNew()): ?>
  <div class="bottomborder">
    <div class="clearfloat">
      <?php echo $form['password']->renderRow(array('class' => 'text'),'Password * ') ?>
    </div>

    <div class="clearfloat">
      <?php echo $form['password_again']->renderRow(array('class' => 'text'),'Repeat Password * ') ?>
    </div>

    <div class="clearfloat">
      <p style="margin-left:120px;">Password must be at least 6 characters long</p>
    </div>
  </div>
  <?php endif; ?>

  <div class="bottomborder">
    <div class="clearfloat">
      <?php echo $form['first_name']->renderRow(array('class' => 'text'),'First name *') ?>
    </div>
    <div class="clearfloat">
      <?php echo $form['last_name']->renderRow(array('class' => 'text'),'Last name *') ?>
    </div>
    <div class="clearfloat">
      <?php echo $form['Profile']['gender']->renderLabel('Gender * ') ?>
      <div class="selects">
        <?php echo $form['Profile']['gender']->render() ?>
      </div>
      <?php echo $form['Profile']['gender']->renderError() ?>
    </div>
  </div>

  <div class="bottomborder">
    <div class="clearfloat">
      <?php echo $form['Profile']['phone']->renderRow(array('class' => 'text'),'Mobile number *') ?>
    </div>
    <div class="clearfloat">
      <?php echo $form['Profile']['region']->renderLabel('Region *') ?>
      <div class="selects">
        <?php echo $form['Profile']['region']->render() ?>
      </div>
      <?php echo $form['Profile']['region']->renderError() ?>
    </div>
    <div class="clearfloat">
      <?php echo $form['Profile']['postcode']->renderRow(array('class' => 'text'),'Post code *') ?>
    </div>
  </div>

  <div class="bottomborder">
    <div class="clearfloat check_wide">
      <?php echo $form['Profile']['email_updates']->render() ?>
      <label for="sf_guard_user_Profile_email_updates">Sign me up to receive <strong>Email updates</strong> from Coca-Cola Oceania including information on promotions and community activities, which may include information on 3rd party partners</label>
    </div>
    <div class="clearfloat check_wide" style="margin-top:12px;">
      <?php echo $form['Profile']['mobile_updates']->render() ?>
      <label for="sf_guard_user_Profile_mobile_updates">Sign me up to receive <strong>Mobile updates</strong> from Coca-Cola Oceania including information on promotions and community activities, which may include information on 3rd party partners</label>
    </div>
  </div>

  <div class="bottomborder">
    <div class="check">
      <?php echo $form['accept_privacy']->render() ?>
      <?php echo $form['accept_privacy']->renderLabel(sprintf('* I agree to the %s',link_to('Privacy Policy','@privacy_policy'), array('target' => '_blank'))) ?>
      <?php echo $form['accept_privacy']->renderError() ?>
    </div>
  </div>

  <?php if($form->isNew()): ?>
  <div class="bottomborder">
    <div class="clearfloat">
      <?php echo $form['security_code']->renderRow(array(),'Security Code *') ?>
    </div>
  </div>
  <?php endif; ?>

  <div class="clearfloat">
    <input class="submit" type="submit" value="Submit"/>
  </div>
  <?php echo $form->renderHiddenFields(true); ?>
</form>

<div id="footnote">* Mandatory</div>