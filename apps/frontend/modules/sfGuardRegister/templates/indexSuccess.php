<div class="cokebottle typography">
<h1>COCA-COLA Account Registration</h1>

  <section id="content" class="form">

    <?php  if($underage ) :  ?>
      <h4 class="msg">Thank you for your interest in our site, but we are unable to accept your registration at this time.</h4>
    <?php elseif($thanks): ?>
    <h4 class="msg">Thank you for your registration. Your COKEPass has been created successfully.</h4>
    <?php else: ?>
      <?php echo get_partial('sfGuardRegister/form', array('form' => $form)) ?>
    <?php endif;  ?>

  </section>

</div>


