<div class="cokebottle typography">
<h1>COCA-COLA Account Password Change</h1>

  <section id="content" class="form">

    <?php if($success): ?>

      <h3>Success</h3>
      <p>Your password has been updated successfully.</p>

    <?php else: ?>
      <?php  if($form->hasErrors()):  ?>
        <p class="errors">Registration could not be completed as errors have occurred.</p>
      <?php  endif; ?>

      <?php  if($form->hasGlobalErrors()):  ?>
        <?php foreach($form->getGlobalErrors() as $error): ?>
          <p class="errors"> <?php  echo $error  ?> </p>
        <?php endforeach; ?>
      <?php  endif; ?>

      <form method="post" action="<?php echo url_for('@user_account_change_password') ?>">
        <div class="clearfloat">
          <?php echo $form['current_password']->renderRow(array('class' => 'text'),'Current password * ') ?>
        </div>

        <div class="clearfloat">
          <?php echo $form['password']->renderRow(array('class' => 'text'),'New password * ') ?>
        </div>

        <div class="clearfloat">
          <?php echo $form['password_again']->renderRow(array('class' => 'text'),'Repeat new password * ') ?>
        </div>

        <div class="clearfloat">
          <p style="margin-left:120px;">Password must be at least 6 characters long</p>
        </div>


        <div class="clearfloat">
          <input class="submit" type="submit" value="Submit"/>
        </div>

        <?php echo $form->renderHiddenFields() ?>
      </form>
      <div id="footnote">* Mandatory</div>

    <?php endif; ?>

  </section>

</div>
