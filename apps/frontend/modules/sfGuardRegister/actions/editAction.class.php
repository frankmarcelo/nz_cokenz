<?php

class editAction extends sfAction
{
  /**
   * @param sfRequest $request The current sfRequest object
   *
   * @return mixed     A string containing the view name associated with this action
   */
  function execute($request)
  {
    $this->redirectUnless( $this->getUser()->isAuthenticated(), '@homepage' );

    $this->form = new UserRegisterForm($this->getUser()->getGuardUser());

    $this->form['nz_resident']->getWidget()->setAttributes(array('checked'=>'checked','disabled'=>'disabled'));
    $this->form['accept_privacy']->getWidget()->setAttributes(array('checked'=>'checked','disabled'=>'disabled'));

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $this->form->save();
        $this->redirect('@user_account_show');

      }

    }

  }

}
