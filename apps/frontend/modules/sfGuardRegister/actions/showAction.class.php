<?php

class showAction extends sfAction
{
  /**
   * @param sfRequest $request The current sfRequest object
   *
   * @return mixed     A string containing the view name associated with this action
   */
  function execute($request)
  {
    $this->redirectUnless( $this->getUser()->isAuthenticated(), '@homepage' );

    $this->user = $this->getUser()->getGuardUser();
  }
}
