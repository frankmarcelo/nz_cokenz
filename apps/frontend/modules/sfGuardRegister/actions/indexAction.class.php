<?php

class indexAction extends sfAction
{
  /**
   * @param sfRequest $request The current sfRequest object
   *
   * @return mixed     A string containing the view name associated with this action
   */
  function execute($request)
  {
    if ($this->getUser()->isAuthenticated())
    {
      $this->getUser()->setFlash('notice', 'You are already registered and signed in!');
      $this->redirect('@homepage');
    }
    $this->thanks = false;
    $this->form = new UserRegisterForm();
    $this->underage = false;
    if( isset($_COOKIE["underage"]) ) {
      $this->underage = true;
    }

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $Profile = $this->form->getValue('Profile');

        $dob = new DateTime($Profile['dob']);
        $now = new DateTime();
        $diff = $now->diff($dob);

        if( $diff->y >= sfConfig::get('app_min_age',14) ) {

          $user = $this->form->save();

          $user->getProfile()->setUser($user);
          $user->getProfile()->setRegistrationSource('desktop');
          $user->getProfile()->save();

          $this->getUser()->signIn($user);

          CokeMailer::sendRegoCompleteEmail($user->email_address,$user->first_name);

          $this->redirect('@sf_guard_register_thanks');

        } else {
          $this->underage = true;
          setcookie('underage','true',time()+60*60,'/');
        }

      }
    }


  }

}
