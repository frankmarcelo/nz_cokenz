<?php if(!isset($hide)){ $hide = false; } ?>

<div id="login_spacer">
 <div id="loginWidgetContainer" style="left:0px; <?php echo $hide ? "display:none;" : "display:block;" ?>" class="welcomeView">
  <div id="loginWidget">
    <div id="loggedInGreeting">

    <span id="intro">Hello <?php echo strlen($name) > 16 ? substr($name,0,15)."..." : $name ?> </span>
    </div>
    <div id="loggedInLinks">
      <a id="myAcctLink" href="<?php echo url_for('@user_account_show') ?>">My Account</a><a id="logoutLink" href="<?php echo url_for('@sf_guard_signout') ?>">Log out</a>
    </div>
    </div>
  </div>
</div>