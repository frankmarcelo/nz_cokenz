<?php
require_once(sfConfig::get('sf_plugins_dir').'/sfDoctrineGuardPlugin/modules/sfGuardAuth/lib/BasesfGuardAuthActions.class.php');
/**
 * userProfile actions.
 *
 * @package    powerade_nz
 * @subpackage userProfile
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardAuthActions extends BasesfGuardAuthActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */

  public function executeSignin($request)
  {


    parent::executeSignin($request);

  }

  public function executeAuthorised($request)
  {
    sfConfig::set('sf_web_debug',false);
    $this->setLayout('iframe');
  }

  public function executeUserPanel($request)
  {

    sfConfig::set('sf_web_debug',false);
  }

}
