<div class="cokebottle typography">

  <h1>Forgot Password?</h1>

  <section id="content" class="form">

    <?php if( $success ): ?>
    <h2 style="font-size: 16px;">Great! A password reminder has been sent to your registered email address.</h2>

    <?php else: ?>

    <p class="instructions">Enter your registered email address and the security code below to get a password reminder.</p>

    <?php  if($form->hasErrors()):  ?>
      <p class="errors">Registration could not be completed as errors have occurred.</p>
    <?php  endif; ?>

    <?php  if($sf_user->hasFlash('error')):  ?>
      <p class="errors"><?php echo $sf_user->getFlash('error') ?></p>
    <?php  endif; ?>

    <?php  if($form->hasGlobalErrors()):  ?>
      <?php foreach($form->getGlobalErrors() as $error): ?>
        <p class="errors"> <?php  echo $error  ?> </p>
      <?php endforeach; ?>
    <?php  endif; ?>


    <form method="post" action="<?php echo url_for('@sf_guard_forgot_password') ?>" autocomplete="off">
      <div class="bottomborder">
        <?php echo $form['email_address']->renderRow(array('class'=>'text')) ?>
        <br/>
        <?php echo $form['security_code']->renderRow() ?>
      </div>
      <input class="submit" type="submit" value="Submit"/>	<a href="<?php echo url_for('@sf_guard_forgot_password') ?>" id="clearform">Clear form</a>
      <?php echo $form->renderHiddenFields() ?>
    </form>

    <div id="footnote">* Mandatory  </div>
    <?php endif; ?>
  </section>
</div>