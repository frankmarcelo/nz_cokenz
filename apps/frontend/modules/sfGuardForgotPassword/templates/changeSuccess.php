<div class="cokebottle typography">

  <h1>Forgot Password?</h1>

  <section id="content" class="form">

    <?php if( $success ): ?>
      <h2 style="font-size: 16px;">Thanks! Your password has been updated successfully.</h2>

    <?php else: ?>

      <?php  if($form->hasErrors()):  ?>
        <p class="errors">Registration could not be completed as errors have occurred.</p>
      <?php  endif; ?>

      <?php  if($sf_user->hasFlash('error')):  ?>
        <p class="errors"><?php echo $sf_user->getFlash('error') ?></p>
      <?php  endif; ?>

      <?php  if($form->hasGlobalErrors()):  ?>
        <?php foreach($form->getGlobalErrors() as $error): ?>
          <p class="errors"> <?php  echo $error  ?> </p>
        <?php endforeach; ?>
      <?php  endif; ?>

      <p class="instructions"><?php echo sprintf('Hello %s', $user->getName()) ?></p>

      <p class="instructions">Enter your new password in the form below.</p>

      <form action="<?php echo url_for('@sf_guard_forgot_password_change?unique_key='.$sf_request->getParameter('unique_key')) ?>" method="POST">

        <div class="bottomborder">
          <?php echo $form['password']->renderRow(array('class' => 'text')) ?>
          <?php echo $form['password_again']->renderRow(array('class' => 'text')) ?>
          <div class="clearfloat">
            <p style="margin-left:120px;">Password must be at least 6 characters long</p>
          </div>
        </div>



        <?php echo $form->renderHiddenFields() ?>

        <input type="submit" class="submit" name="change" value="Submit" />

      </form>

    <?php endif; ?>
  </section>
</div>