<!DOCTYPE html>
<html>
<head>
  <?php include_title() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <?php include_stylesheets() ?>
  <?php include_javascripts() ?>
  <?php include_partial('global/ga') ?>
</head>
<body>

<div data-role="page" data-theme="a" data-back-btn="false">

    <div data-role="header"  data-id="header" class="coca-cola-header">
        <?php if (!($sf_request->getParameter('module') == 'article' and $sf_request->getParameter('action') == 'index') and $sf_request->getParameter('module') != 'myCokeCard'): ?>
          <img src="/images/mobile/btn-back.png" class="back-button" onclick="history.go(-1)">
        <?php endif; ?>
        <img src="/images/coca-cola-logo.png" class="coca-cola-logo float-center" />
    </div>

    <div data-role="navbar" role="navigation">
        <?php
        //New table based nav bar. Fixes all the alignment/padding issues.
        //tables > divs/ul/li for this sort of UI blocks with dynamic resizing-ness.
        ?>
        <table class="coca-cola-nav">
            <tr>
                <td><a data-role="none" href="<?php echo url_for('@homepage') ?>">HOME</a></td>
                <td><a data-role="none" href="<?php echo url_for('@play') ?>">PLAY</a></td>
                <td><a data-role="none" href="<?php echo url_for('@pass') ?>" data-ajax="false">&lsquo;COKE&rsquo; PASS</a></td>
                <td><a data-role="none" href="<?php echo url_for('@more') ?>">MORE</a></td>

            </tr>

        </table>
        <?php
        // the original nav bar, feel free to switch it if you need to.
        /*
      <ul class="coca-cola-nav">
        <li><a href="<?php echo url_for('@homepage') ?>">HOME</a></li>
        <li><a href="<?php echo url_for('@play') ?>">PLAY</a></li>
        <li><a href="<?php echo url_for('@pass') ?>" data-ajax="false">&lsquo;COKE&rsquo; PASS</a></li>
        <li><a href="<?php echo url_for('@more') ?>">MORE</a></li>
      </ul>
*/
        ?>
    </div>

    <?php echo $sf_content ?>

</div>

</body>
</html>
