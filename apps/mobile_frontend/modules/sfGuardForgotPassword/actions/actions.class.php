<?php

class sfGuardForgotPasswordActions extends sfActions
{
  /**
   * @param sfRequest $request The current sfRequest object
   *
   * @return mixed     A string containing the view name associated with this action
   */
  public function postExecute()
  {
    $this->setLayout('layout');
  }


  public function  executeIndex($request)
  {
    $this->form = new RequestForgotPasswordForm();
    $this->success = false;
    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $this->user = $this->form->user;
        $this->_deleteOldUserForgotPasswordRecords();

        $forgotPassword = new sfGuardForgotPassword();
        $forgotPassword->user_id = $this->form->user->id;
        $forgotPassword->unique_key = md5(rand() + time());
        $forgotPassword->expires_at = new Doctrine_Expression('NOW()');
        $forgotPassword->save();

        $message = Swift_Message::newInstance()
          ->setFrom(sfConfig::get('app_sf_guard_plugin_default_from_email', 'from@noreply.com'))
          ->setTo($this->form->user->email_address)
          ->setSubject('Forgot Password Request for '.$this->form->user->username)
          ->setBody($this->getPartial('sfGuardForgotPassword/send_request', array('user' => $this->form->user, 'forgot_password' => $forgotPassword)))
          ->setContentType('text/html')
        ;

        $this->getMailer()->send($message);

        $this->success = true;
      } else {
        $this->getUser()->setFlash('error', 'Invalid e-mail address!');
      }
    }

  }

  public function executeChange($request)
  {
    $this->forgotPassword = $this->getRoute()->getObject();
    $this->user = $this->forgotPassword->User;
    $this->form = new sfGuardChangeUserPasswordForm($this->user);
    $this->success = false;


    $this->form->getValidator('password')->setOption('min_length',6);
    $this->form->getValidator('password')->setMessage('min_length','Password is too short');
    $this->form->getValidator('password_again')->setOption('min_length',6);
    $this->form->getValidator('password_again')->setMessage('min_length','Password is too short');

    if ($request->isMethod('post'))
    {
      $this->form->bind($request->getParameter($this->form->getName()));
      if ($this->form->isValid())
      {
        $this->form->save();

        $this->_deleteOldUserForgotPasswordRecords();

        $this->success = true;
      }
    }
  }


  private function _deleteOldUserForgotPasswordRecords()
  {
    Doctrine_Core::getTable('sfGuardForgotPassword')
      ->createQuery('p')
      ->delete()
      ->where('p.user_id = ?', $this->user->id)
      ->execute();
  }

}
