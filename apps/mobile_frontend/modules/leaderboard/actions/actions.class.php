<?php

class leaderboardActions extends sfActions
{
  public function executeSubmit(sfWebRequest $request)
  {
    $form = new SubmitGameScoreForm(null, null, false); // the third parameter disables CSRF attack prevention
    
    $values = array(
      'score' => $request->getParameter('score'),
      'game_id' => $request->getParameter('game_id'),
      'user_id' => $this->getUser()->getGuardUser()->getId()
    );
    
    if (!$form->bindAndSave($values))
    {
      $messages = array();
      
      foreach ($form->getErrorSchema() as $fieldName => $error)
      {
        $messages[$fieldName] = $error->getMessage();
      }
      
      $this->messages = $messages;
      
      return sfView::ERROR;
    }
  }
}
