<?php

/**
 * olympics actions.
 *
 * @package    Coke NZ
 * @subpackage olympics
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class olympicsActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */

  public function postExecute()
  {
    $this->setLayout('layout');
  }


  public function executeIndex(sfWebRequest $request)
  {

  }

  public function executeAnywhereInTheWorld(sfWebRequest $request)
  {
  }

  public function executeNzAthletes(sfWebRequest $request)
  {
  }

  public function executeDownloadTrack($request)
  {
  }

  public function executeDownloadMP3File(sfWebRequest $request)
  {
    sfConfig::set('sf_web_debug', false);

    $file = sfConfig::get('sf_web_dir').'/audio/Mark_Ronson_Katy_B-Anywhere_In_The_World.mp3';

    $this->setLayout(false);
    $this->getResponse()->setHttpHeader('Content-Description','File Transfer');
    $this->getResponse()->setContentType('application/octet-stream');
    $this->getResponse()->setHttpHeader('Content-Disposition', 'attachment;filename="'.basename($file).'"');
    $this->getResponse()->setHttpHeader('Content-Transfer-Encoding','binary');
    $this->getResponse()->setHttpHeader('Expires','0');
    $this->getResponse()->setHttpHeader('Cache-Control','must-revalidate');
    $this->getResponse()->setHttpHeader('Pragma','public');
    $this->getResponse()->setHttpHeader('Content-Length',filesize($file));
    $this->getResponse()->setContent(file_get_contents($file));

    return sfView::NONE;

  }
}
