  <div data-role="content" style="text-align:center;">
  <h1>&lsquo;Anywhere in the World&rsquo;</h1>
  <p>The &lsquo;Coca-Cola&rsquo; song for the London <br>2012 Olympic Games, created by <br>&lsquo;Coca-Cola&rsquo; DJ Mark Ronson &amp; Katy B <br>from the sounds of top athletes.</p>
  <p>
    <a href="<?php echo url_for('olympics',array('action' => 'downloadMP3File')) ?>" data-ajax="false" onclick="_gaq.push(['_trackEvent', 'Downloads', 'Audio', 'Mark_Ronson_Katy_B-Anywhere_In_The_World.mp3']);">
      <img src="/images/mobile/download-track-btn.png" alt="" style="width: 70%; display: block; margin: 0 auto">
    </a>
  </p>
  <p style="font-style: italic; font-size: 80%">You are subject to carrier charges for <br>downloading this content. This will consume <br>approx. 3 MB of your data allowance.</p>
  <p>&nbsp;</p>
</div>