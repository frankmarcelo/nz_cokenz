<?php use_helper('Mobile') ?>
<div data-role="content" id="olympics_content">
  <?php include_partial('head') ?>

  <ul data-role="listview" class="coca-cola-articles" data-theme="none">

    <li data-article-type="list" data-icon="false">
      <a href="<?php echo youtube_link('DcNKq0A2wGQ') ?>">
        <img src="/images/mobile/olympic-video-thumbs/Glenn_s_thumb1.jpg" class="coca-cola-article-thumb">
        <h4>NZ Swimmer Glenn Synders</h4>
        <p>COKE is helping the world Move To The Beat of the London 2012 Olympic Games. To celebrate, we caught up with some NZ Olympians to see exactly what role music plays in their lives. Let’s meet swimmer Glenn Snyders.</p>
      </a>
    </li>

    <li data-article-type="list" data-icon="false">
      <a href="<?php echo youtube_link('doXJ6UuAeqk') ?>">
        <img src="/images/mobile/olympic-video-thumbs/hockey_thumb1.jpg" class="coca-cola-article-thumb">
        <h4>NZ Hockey Players Phil Burrows and Shea McAleese</h4>
        <p>COKE is helping the world Move To The Beat of the London 2012 Olympic Games. To celebrate, we caught up with some NZ Olympians to see exactly what role music plays in their lives. Meet NZ Hockey Players Phil Burrows and Shea McAleese.</p>
      </a>
    </li>

    <li data-article-type="list" data-icon="false">
      <a href="<?php echo youtube_link('h13tqEEU6i8') ?>">
        <img src="/images/mobile/olympic-video-thumbs/Lisa_k_thumb.jpg" class="coca-cola-article-thumb">
        <h4>NZ Kayaker Lisa Carrington</h4>
        <p>COKE is helping the world Move To The Beat of the London 2012 Olympic Games. To celebrate, we caught up with some NZ Olympians to see exactly what role music plays in their lives.  Lets meet kayaker Lisa Carrington.</p>
      </a>
    </li>

  </ul>

</div>

