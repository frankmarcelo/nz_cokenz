<?php use_helper('Mobile') ?>
<ul data-role="listview" class="coca-cola-articles" data-theme="none">

  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('ZA8onDcDACg') ?>">
    <img src="/images/mobile/olympic-video-thumbs/beat-1.jpg" class="coca-cola-article-thumb">
    <h4>Beat 1: The Rhythm and Mark Ronson</h4>
    <p>Move to the Beat of London 2012 – ‘Coca-Cola’ helps  Mark Ronson, the famous London based DJ, find the sound of the Olympics for 2012. </p>
    </a>
  </li>

  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('bz-HSPnz4bA') ?>">
    <img src="/images/mobile/olympic-video-thumbs/beat-2.jpg" class="coca-cola-article-thumb">
    <h4>Beat 2:  Move to the Beat</h4>
    <p>Move to the Beat of London 2012 - ‘Coca-Cola’ and the Sounds of the Olympics help fans move to the beat of sport.</p>
    </a>
  </li>

  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('RfRode53kHU') ?>">
    <img src="/images/mobile/olympic-video-thumbs/beat-3.jpg" class="coca-cola-article-thumb">
    <h4>Beat 3: Sounds of Sport</h4>
    <p>Move to the Beat of London 2012 – The sound of the Olympic Games and ‘Coca-Cola’ can be found anywhere in the world.</p>
    </a>
  </li>

  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('c7aDGsHEaXw') ?>">
    <img src="/images/mobile/olympic-video-thumbs/bol-1.jpg" class="coca-cola-article-thumb">
    <h4>The Beat of London 1: Where is all began</h4>
    <p>In the first video of the ‘Coca-Cola’ series, DJ Mark Ronson makes his preparations to record the ‘Coca-Cola’ song for the London 2012 Olympic Games.</p>
    </a>
      
  </li>
  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('gpNUL2cnOsg') ?>">
    <img src="/images/mobile/olympic-video-thumbs/bol-2.jpg" class="coca-cola-article-thumb">
    <h4>The Beat of London 2: Abdul Dayyan</h4>
    <p>Eagle-eyed Singaporean archery competitor Abdul Dayyan says his bow is his musical instrument. Abual works with Mark R to create the ‘Coca-Cola’ song for the London 2012 Olympic Games. </p>
    </a>
  </li>
  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('QQ0nkSGYfw8') ?>">
    <img src="/images/mobile/olympic-video-thumbs/bol-3.jpg" class="coca-cola-article-thumb">
    <h4>The Beat of London 3: Darius Knight</h4>
    <p>Darius Knight (English table tennis player) says the rhythm of table tennis is just like dancing. When teamed up with ‘Coca-Cola’ and Mark Ronson, Darius moves to the beat.</p>
    </a>
  </li>
  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('4DTuurlkFO0') ?>">
    <img src="/images/mobile/olympic-video-thumbs/bol-4.jpg" class="coca-cola-article-thumb">
    <h4>The Beat of London 4: Maria Espinoza</h4>
    <p>Maria Espinoza, the high-kicking Taekwondo Mexican champ unleashes her deadly feet on a Mark Ronson Piñata to create the ‘Coca-Cola’s’ London 2012 Olympics Games soundtrack. </p>
    </a>
  </li>
  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('UWT01nRrlYU') ?>">
    <img src="/images/mobile/olympic-video-thumbs/bol-5.jpg" class="coca-cola-article-thumb">
    <h4>The Beat of London 5: David Oliver</h4>
    <p>American track &amp; field Bronze medallist David Oliver does what he does best and becomes part of the ‘Coca-Cola’ soundtrack for the Olympic Games.</p>
    </a>
  </li>
  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('tKtQ5vOe92g') ?>">
    <img src="/images/mobile/olympic-video-thumbs/bol-6.jpg" class="coca-cola-article-thumb">
    <h4>The Beat of London 6:  Ksenyia Vdovina</h4>
    <p>From London to Moscow the beat goes on. Mark takes something as subtle as Russian track star Ksenyia’s heartbeat and makes it the backbone of the ‘Coca-Cola’ London 2012 Olympic soundtrack. </p>
    </a>
  </li>
  <li data-article-type="list" data-icon="false">
    <a href="<?php echo youtube_link('1HtJZUT32mU') ?>">
    <img src="/images/mobile/olympic-video-thumbs/bol-7.jpg" class="coca-cola-article-thumb">
    <h4>The Beat of London 7: The SHOW</h4>
    <p>See and hear an incredible orchestration of athletes and their sounds as Mark and Katy B’s soundtrack ‘Anywhere in the World’ receives its debut on the ‘Coca-Cola’ stage in London. </p>
    </a>
  </li>




</ul>