<a href="<?php echo url_for('olympics/downloadTrack') ?>">
  <img src="/images/mobile/download-track.jpg" alt="Download 'Anywhere in the World' free">
</a>

<a href="<?php echo url_for('olympics') ?>">
  <?php if($sf_request->getParameter('action') == 'index'): ?>
    <img src="/images/mobile/btn-marks-journey-active.png" alt="Mark's Journey">
  <?php else: ?>
    <img src="/images/mobile/btn-marks-journey-inactive.png" alt="Mark's Journey">
  <?php endif; ?>
</a>

<a href="<?php echo url_for('olympics',array('action'=>'anywhereInTheWorld')) ?>">
  <?php if($sf_request->getParameter('action') == 'anywhereInTheWorld'): ?>
    <img src="/images/mobile/btn-anywhere-in-the-world-active.png" alt="Anywhere in the world">
  <?php else: ?>
    <img src="/images/mobile/btn-anywhere-in-the-world-inactive.png" alt="Anywhere in the world">
  <?php endif; ?>
</a>

<a href="<?php echo url_for('olympics',array('action'=>'nzAthletes')) ?>">
  <?php if($sf_request->getParameter('action') == 'nzAthletes'): ?>
    <img src="/images/mobile/btn-nz-athletes-active.png" alt="New Zealand Athletes">
  <?php else: ?>
    <img src="/images/mobile/btn-nz-athletes-inactive.png" alt="New Zealand Athletes">
  <?php endif; ?>
</a>