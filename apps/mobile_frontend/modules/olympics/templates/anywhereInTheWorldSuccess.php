<?php use_helper('Mobile') ?>
<div data-role="content" id="olympics_content">
  <?php include_partial('head') ?>

  <ul data-role="listview" class="coca-cola-articles" data-theme="none">

    <li data-article-type="list" data-icon="false">
      <a href="<?php echo youtube_link('YZ88gEXc7Kg') ?>">
        <img src="/images/mobile/olympic-video-thumbs/anywhere-video.jpg" class="coca-cola-article-thumb">
        <h4>Anywhere in the World 30 sec </h4>
        <p>Anywhere in the World is the ‘Coca-Cola’ song for the London 2012 Olympic Games, created by DJ Mark Ronson and Katy B from the sounds of top athletes. </p>
      </a>
    </li>

    <li data-article-type="list" data-icon="false">
      <a href="<?php echo youtube_link('YVB1r4D8QH0') ?>">
        <img src="/images/mobile/olympic-video-thumbs/anywhereTVC.jpg" class="coca-cola-article-thumb">
        <h4>Anywhere in the World TVC: 60 sec</h4>
        <p>Anywhere in the World is the ‘Coca-Cola’ song for the London 2012 Olympic Games, created by DJ Mark Ronson and Katy B from the sounds of top athletes.</p>
      </a>
    </li>

  </ul>

</div>