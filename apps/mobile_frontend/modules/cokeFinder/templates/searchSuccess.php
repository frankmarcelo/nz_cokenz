<div class="ui-bar ui-bar-b coca-cola-article-title">
    <h3>FIND A COKE</h3>
</div>
<div data-role="content" class="coke_finder">
  <div class="message">
    <?php echo $message ?>
    Please enter your address below to find your nearest COKE.
  </div>

  <form method="post" action="<?php echo url_for('@coke_finder_search') ?>">
    <?php echo $form ?>
    <input type="submit" value="SEARCH">
  </form>

</div>