<?php use_javascript('//maps.googleapis.com/maps/api/js?key=AIzaSyA6ERTCfnxpHx3vFzWYhOkO83oMQZ73SCE&sensor=true','first') ?>
<?php use_javascript('coke_finder.js','last') ?>

<div class="ui-bar ui-bar-b coca-cola-article-title">
    <h3>FIND A COKE</h3>
</div>
<div data-role="content" class="coke_finder">

  <div data-role="navbar" data-mini="true">
    <ul>
      <li><a href="<?php echo url_for('@coke_finder') ?>" class="ui-btn-active" data-theme="c">MAP VIEW</a></li>
      <li><a id="list_view_link" href="<?php echo url_for('@coke_finder_list') ?>" data-theme="c">LIST VIEW</a></li>
    </ul>
  </div>

  <div id="info" class="lightbox"></div>
  <div id="map_canvas">Please wait...</div>

</div>

<script type="text/javascript">
  $(document).ready(function(){
    var map_height = $('.ui-page').height() - $('.ui-header').height() - $('.ui-footer').height() - $('.ui-navbar').height() - $('.coca-cola-article-title').height();

    $('#map_canvas').css('height',map_height+'px');
    initialiseMap();
  });
</script>
