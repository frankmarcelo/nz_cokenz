<?php if( count($locations) ): ?>
  <?php include_partial('locations',array('locations' => $locations)) ?>
<?php else: ?>
  <p id="info">Sorry. No locations found</p>
<?php endif; ?>