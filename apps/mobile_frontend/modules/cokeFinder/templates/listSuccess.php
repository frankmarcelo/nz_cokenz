<?php use_javascript('//maps.googleapis.com/maps/api/js?key=AIzaSyA6ERTCfnxpHx3vFzWYhOkO83oMQZ73SCE&sensor=true','first') ?>
<?php use_javascript('coke_finder.js','last') ?>

<div class="ui-bar ui-bar-b coca-cola-article-title">
    <h3>FIND A COKE</h3>
</div>
<div data-role="content" class="coke_finder">
  <div data-role="navbar" data-mini="true">
    <ul>
      <li><a href="<?php echo url_for('@coke_finder') ?>" data-theme="c">MAP VIEW</a></li>
      <li><a href="<?php echo url_for('@coke_finder_list') ?>" class="ui-btn-active" data-theme="c">LIST VIEW</a></li>
    </ul>
  </div>

  <div id="location_list">
    <?php if( count($locations) ): ?>
      <?php include_partial('locations',array('locations' => $locations)) ?>
    <?php else: ?>
      <p id="info">Please wait...</p>
      <script type="text/javascript">
        initialiseList();
      </script>
    <?php endif; ?>
  </div>
</div>