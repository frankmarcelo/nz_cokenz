<ul class="locations">
<?php foreach($locations as $location): ?>
  <li>
    <div class="icon icon_<?php echo $location['type_slug'] ?>"></div>
    <div class="location">
      <span class="name"><?php echo $location['name'] ?></span><br>
      <span class="address"><?php echo $location['address'] ?><br/><?php echo $location['suburb'] ?></span>
    </div>
    <div class="distance">
      <?php echo round($location['distance'],1) ?> km
    </div>
  </li>
<?php endforeach; ?>
</ul>