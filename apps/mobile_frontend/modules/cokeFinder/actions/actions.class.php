<?php

/**
 * cokeFinder actions.
 *
 * @package    Coke NZ
 * @subpackage cokeFinder
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cokeFinderActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */

  public function preExecute()
  {

  }

  public function executeIndex(sfWebRequest $request)
  {
    $this->setLayout('layout');
  }

  public function executeList(sfWebRequest $request)
  {
    $locations = array();

    if ($request->getParameter('lat') != 0 and $request->getParameter('long') != 0) {
      $range = 10;
      do {
        $locations = Doctrine::getTable('CokeLocation')->getNearbyLocations($request->getParameter('lat'), $request->getParameter('long'), $range);
        $range += 10;
      } while (count($locations) == 0);

      //Load the helper containing the slugify function
      sfContext::getInstance()->getConfiguration()->loadHelpers('Mobile');

      for ($i = 0; $i < count($locations); $i++) {
        $locations[$i] = array_change_key_case($locations[$i],CASE_LOWER);
        $locations[$i]['type_slug'] = slugify($locations[$i]['type']);
        $locations[$i]['distance'] = round($locations[$i]['distance'],1);
      }

      $this->range = $range;
    }

    $this->locations = $locations;

    if ($request->getParameter('sf_format') == 'html' && $request->isXmlHttpRequest()) {
      $this->setTemplate('locationList');
    }

    if ($request->getParameter('sf_format') == 'html' && !$request->isXmlHttpRequest()) {
      $this->setLayout('layout');
    }

  }

  public function executeSearch(sfWebRequest $request)
  {
    $form = $this->searchForm();

    $this->message = $request->getParameter('message','Your location cannot be detected.');

    if ($request->isMethod('post')) {
      $form->bind($request->getParameter($form->getName()));

      if ($form->isValid()) {
        $this->redirect('@coke_finder?lat='.$form->getValue('latitude').'&long='.$form->getValue('longitude'));
      }
    }

    $this->form = $form;

  }

  private function searchForm()
  {
    $form = new sfForm();
    $form->setWidgets(array(
      'address'   => new sfWidgetFormInputText(),
      'city'      => new sfWidgetFormInputText(),
      'postcode'  => new sfWidgetFormInputText(),
    ));
    $form->setValidators(array(
      'address'   => new sfValidatorString(),
      'city'      => new sfValidatorString(),
      'postcode'  => new sfValidatorString(array('required' => false))
    ));
    $form->getValidatorSchema()->setPostValidator( new validatorPhysicalAddress() );
    $form->getWidgetSchema()->setNameFormat('search[%s]');

    return $form;
  }

}
