<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3>Contact Us</h3>
</div>

<div data-role="content">

<?php if($underage): ?>
<div id="underage" >
  <p>I'm sorry, but we can not accept your submission at this time.</p>
</div>
<?php else: ?>

<form action="<?php echo url_for('more/contact') ?>" method="post" class="page contact_form">

  <?php if ($form->hasErrors()): ?>
    <p class="form_errors">There was an error submitting the form. Please correct the below problems and re-submit the form.</p>
  <?php endif; ?>

  <p>Have a question you&rsquo;d like to ask about this site, or &lsquo;Coca-Cola&rsquo;?
    Or maybe you have a suggestion to improve this website? Enter your message below and we&rsquo;ll get back to you as soon as we can.
    Or alternatively you can call <strong class="brand"><a href="tel://0800-505-123">0800 505 123</a> </strong> to contact us.</p>

  <hr/>

  <div class="form_row">
    <?php echo $form['type']->renderError() ?>
    <?php echo $form['type']->renderLabel('Please select your query from the categories below:',array('class' => 'ui-input-text', 'style'=>'width:100%;')) ?>
    <?php echo $form['type']->render(array('data-theme' => 'c')) ?>
  </div>

  <hr/>

  <h2 class="brand">Contact Details</h2>

  <?php echo $form['first_name']->renderRow() ?>

  <?php echo $form['last_name']->renderRow() ?>

  <?php echo $form['address']->renderRow() ?>

  <?php echo $form['phone_number']->renderRow() ?>

  <?php echo $form['email']->renderRow() ?>

  <div data-role="fieldcontain" style="border: none">
    <fieldset data-role="controlgroup" data-type="horizontal" id="fieldset_dob">
      <legend>*Date of Birth:  <?php echo $form['date_of_birth']->renderError() ?></legend>
      <div class="clear"></div>
      <?php echo $form['date_of_birth']->render(array('data-theme' => 'c','id' => 'date_of_birth')); ?>
    </fieldset>
  </div>

  <div class="form_row">
    <?php echo $form['enquiry']->renderError() ?>
    <?php echo $form['enquiry']->renderLabel(null, array('class' => 'brand', 'style' => 'width:100%')) ?>
    <?php echo $form['enquiry']->render(array('style' => 'height: 10em;')) ?>
  </div>
  <div class="form_row coca-cola-contact-checkbox">
    <?php echo $form['accept_pp']->renderError() ?>
    <?php echo $form['accept_pp']->render(array('data-theme' => 'd')) ?>
    <?php echo $form['accept_pp']->renderLabel('I have read and agree to the Privacy Policy') ?>
  </div>

  <div class="form_row">
    <?php echo $form['security_code']->renderError() ?>
    <?php echo $form['security_code']->renderLabel() ?>
    <?php echo $form['security_code']->render() ?>
    <div style="font-size: 80%; display: inline;">Touch for a new code</div>
  </div>

  <div class="form_row">
    <button type="submit" class="button">Submit</button>
    <?php echo link_to('Read the Privacy Policy','more/privacy',array('data-role' => "button")) ?>
    <button type="reset" class="link">Clear Form</button>
  </div>

  <?php echo $form->renderHiddenFields() ?>

</form>
  <?php endif; ?>
  </div>