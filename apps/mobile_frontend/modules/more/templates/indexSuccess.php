<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3>MORE</h3>
</div>

<div data-role="content" style="padding: 15px;">

  <ul data-role="listview" data-theme="c" data-inset="true">
  	<li><a href="<?php echo url_for('more/contact') ?>" data-ajax="false">Contact Us</a></li>
  	<li><a href="<?php echo url_for('more/privacy') ?>">Privacy Policy</a></li>
  	<li><a href="<?php echo url_for('more/terms') ?>">Terms of Use</a></li>
  </ul>



</div>