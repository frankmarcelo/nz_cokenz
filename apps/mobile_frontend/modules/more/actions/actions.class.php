<?php

/**
 * more actions.
 *
 * @package    Coke NZ
 * @subpackage more
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class moreActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */

  public function postExecute()
  {
    $this->setLayout('layout');
  }

  public function executeIndex(sfWebRequest $request)
  {}
  public function executeContact(sfWebRequest $request)
  {
    $form = new MobileContactForm();

    $this->underage = false;
    if( isset($_COOKIE['underage']) ){
      $this->underage = true;
    }

    if($this->getUser()->isAuthenticated()){
      $user = $this->getUser()->getGuardUser();

      $form->setDefaults(array(
        'first_name'  => $user->first_name,
        'last_name'   => $user->last_name,
        'phone_number'=> $user->Profile->phone,
        'email'       => $user->email_address,
        'date_of_birth' => $user->Profile->dob
      ));
    }

    if( $request->isMethod('post') ){
      $form->bind($request->getParameter($form->getName()));
      if( $form->isValid() ){
        $dob = new DateTime($form->getValue('date_of_birth'));
        $now = new DateTime();
        $diff = $now->diff($dob);

        if( $diff->y >= sfConfig::get('app_min_age',14) ) {
          CokeMailer::sendContactEmail($this->getMailer(), $form);
          $this->forward('more','thanks');
        } else {
          $this->underage = true;
          setcookie('underage','true',time()+(60*60*24) );
        }
      }
    }

    $this->form = $form;

  }

  public function executeThanks(sfWebRequest $request)
  {
//    $this->redirectUnless($this->getUser()->hasFlash('contact_success'), 'more/contact');
  }

  public function executeTerms(sfWebRequest $request)
  {}
  public function executePrivacy(sfWebRequest $request)
  {}
}
