<?php

/**
 * article actions.
 *
 * @package    coke_nz
 * @subpackage article
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class articleActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    // force layout for AJAX requests
    $this->setLayout('layout');
//sfConfig::set('sf_web_debug',false);
//    $this->coca_cola_articles = Doctrine_Core::getTable('CocaColaArticle')->findAll();
  }

  public function executeShow(sfWebRequest $request)
  {
    // force layout for AJAX requests
    $this->setLayout('layout');

    $this->coca_cola_article = Doctrine_Core::getTable('CocaColaArticle')->find(array($request->getParameter('id')));
    $this->forward404Unless($this->coca_cola_article);
  }

}
