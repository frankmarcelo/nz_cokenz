<?php foreach ($coca_cola_articles as $coca_cola_article): ?>
<?php
  if($coca_cola_article->article_type == 'article') {
    $link = url_for('article_show', array('id' => $coca_cola_article->id, 'slug' => $coca_cola_article->slug) );
  } else {
    if( strstr($coca_cola_article->getArticleContent(),'@') ){
      $link = url_for($coca_cola_article->getArticleContent());
    } else {
      $link = $coca_cola_article->getArticleContent();
    }
  }
?>
<li data-coca-cola-article-href="<?php echo $link ?>" data-article-type="<?php echo $coca_cola_article->getArticleType() ?>">
  <img src="<?php echo '/uploads/articles/'. $coca_cola_article->getThumbImage() ?>" class="coca-cola-article-thumb"/>
  <h4><?php echo $coca_cola_article->getTitle() ?></h4>
  <p><?php echo strip_tags(html_entity_decode($coca_cola_article->getShortDescription())); ?></p>
</li>

<?php endforeach; ?>