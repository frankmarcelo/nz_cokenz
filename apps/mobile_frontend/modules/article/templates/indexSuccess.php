<?php use_helper('Mobile') ?>
<?php use_javascript('add2home.min.js') ?>

<div data-role="content">

  <div class="coca-cola-media-header">
    <div class="coca-cola-home-banner">
      <a href="<?php echo url_for('@shareacoke') ?>"><img src="/images/mobile/shareacoke/main-home-banner2.jpg" class="float-left" id="home-banner"/></a>
    </div>
    <div class="casual-home-images">
      <div class="center-wrap">
        <a href="<?php echo url_for('@play') ?>"><img src="/images/mobile/playgames.jpg" class="float-left"/></a>
        <a href="<?php echo url_for('@coke_finder') ?>"><img src="/images/mobile/btn-find-a-coke.jpg" class="float-left" style="margin:0;"/></a>
      </div>
    </div>
  </div>


  <ul data-role="listview" class="coca-cola-articles" data-theme="none">

    <?php if(sfConfig::get('sf_environment') == 'niks' or sfConfig::get('sf_environment') == 'staging'): ?>
      <li data-icon="false">
        <a href="<?php echo url_for('@coke_finder') ?>">Coke Finder</a>
      </li>
      <li data-icon="false">
        <a href="<?php echo url_for('@coke_card') ?>">Loyalty Card</a>
      </li>
    <?php endif; ?>
    <li data-article-type="link" data-icon="false">
      <a href="<?php echo url_for('@launch_game?game=targets') ?>" data-ajax="false">
        <img src="/images/mobile/hmpage-thumbs/targets.jpg" class="coca-cola-article-thumb"/>
        <h4>Unlock the latest COKE game!</h4>
        <p>Test your precision in Targets</p>
      </a>
    </li>
    <li data-article-type="link" data-icon="false">
      <a href="http://itunes.apple.com/us/app/crabs-and-penguins/id529717659?ls=1&mt=8">
        <img src="/images/mobile/hmpage-thumbs/crabs.jpg" class="coca-cola-article-thumb"/>
        <h4>Crabs and Penguins</h4>
        <p>A state of the art mobile app game from &lsquo;Coca-Cola&rsquo;. Download from iTunes</p>
      </a>
    </li>
    <li data-article-type="link" data-icon="false">
      <a href="<?php echo url_for('@launch_game?game=bounce') ?>" data-ajax="false">
        <img src="/images/mobile/hmpage-thumbs/bounce.jpg" class="coca-cola-article-thumb"/>
        <h4>Make sure you don&rsquo;t freeze!</h4>
        <p>Get powered up in &lsquo;Bounce&rsquo;</p>
      </a>
    </li>
    <li data-article-type="link" data-icon="false">
      <a href="<?php echo url_for('@user_register') ?>" data-ajax="false">
        <img src="/images/mobile/hmpage-thumbs/cokePass.jpg" class="coca-cola-article-thumb"/>
        <h4>Sign up for a COKE Pass.</h4>
        <p>Stay up to date with all the latest COKE news & promotions</p>
      </a>
    </li>

  </ul>

  <p class="view_links"><a data-ajax="false" href="http://<?php echo sfConfig::get('app_desktop_url') ?>/?view=once">View Desktop version</a></p>
</div>