<?php use_helper('Date'); ?>

<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3><?php echo $coca_cola_article->getTitle(); ?></h3>
</div>

<?php if ($coca_cola_article->getTitleImage()) :?>
<div class="coca-cola-media-header">
  <img src="/uploads/articles/<?php echo $coca_cola_article->getTitleImage(); ?>" class="coca-cola-article-img float-center" />
</div>
<?php endif;?>

<div data-role="content">

  <div class="coca-cola-article-date"><?php echo format_date($coca_cola_article->getArticleDate(), 'MMMM d, yyyy'); ?></div>

  <?php echo html_entity_decode($coca_cola_article->getBody()); ?>

</div>
