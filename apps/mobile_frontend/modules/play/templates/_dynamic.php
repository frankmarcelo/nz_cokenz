<?php foreach ($coca_cola_games as $coca_cola_game): ?>
<a class="coca-cola-app-button" data-role="button" data-inline="true" data-ajax="false" data-iconpos="top"
   href="/mobile-games/<?php echo $coca_cola_game->slug.'/'.$coca_cola_game->directory_index; ?>"
   data-icon="<?php echo $coca_cola_game->getIconClass(); ?>"><?php echo $coca_cola_game->getTitle(); ?></a>
<?php endforeach; ?>