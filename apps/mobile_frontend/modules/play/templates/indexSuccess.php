<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3>PLAY</h3>
</div>

<div data-role="content">
  <div class="coca-cola-play-wrap">

    <a class="coca-cola-app-button" data-role="button" data-inline="true" data-ajax="false" data-iconpos="top"
           href="/mobile-games/bounce/index.html" data-icon="d5741da169f152c2671392e496b1886df04fa799">BOUNCE</a>
    <a class="coca-cola-app-button" data-role="button" data-inline="true" data-ajax="false" data-iconpos="top"
           href="/mobile-games/targets/index.html" data-icon="4a600c8db9308be471c31695c4c2dc77fe56ef10">TARGETS</a>

    <div data-role="button" data-theme="b" data-inline="true" class="coca-cola-app-button coming_soon">COMING SOON</div>
    <div data-role="button" data-theme="b" data-inline="true" class="coca-cola-app-button coming_soon">COMING SOON</div>
    <div data-role="button" data-theme="b" data-inline="true" class="coca-cola-app-button coming_soon">COMING SOON</div>
    <div data-role="button" data-theme="b" data-inline="true" class="coca-cola-app-button coming_soon">COMING SOON</div>

  </div>
</div>

