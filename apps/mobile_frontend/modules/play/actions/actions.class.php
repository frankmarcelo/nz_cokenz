<?php

/**
 * play actions.
 *
 * @package    coke_nz
 * @subpackage play
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class playActions extends sfActions
{
  public function executeIndex(sfWebRequest $request)
  {
    // force layout for AJAX requests
    $this->setLayout('layout');

//    $this->coca_cola_games = Doctrine_Core::getTable('CocaColaGame')->getActiveGames();
  }


  public function executeLaunchGame(sfWebRequest $request)
  {
    $this->forward404Unless($request->hasParameter('game'));
    $this->redirect( '/mobile-games/'.$request->getParameter('game').'/index.html');
  }

}
