<div class="coke-card-header med">
<a href="<?php echo url_for('@coke_card') ?>">
    <img src="/images/mobile/coke-card/coke-card-logo-lge.png" alt="My COKE Card">
</a>
</div>
<div data-role="content" class="coke_card_content">
  <p>Enter the unique redeem code below to receive your free beverage. Once redeemed, your MyCoke Card will be reset.</p>
  <form method="post" action="<?php echo url_for('@coke_card_do_redeem_free') ?>" id="redeem_free_form" data-ajax="false">

    <?php echo $form['code']->renderRow() ?>
    <?php echo $form->renderHiddenFields() ?>
    <?php echo $form->renderGlobalErrors() ?>

    <input type="submit" value="GET FREE DRINK">
    <a href="<?php echo url_for('coke_card_location',$card->UserLocation) ?>" data-role="button">BACK</a>
  </form>
</div>

<a href="#error_dialog_invalid" id="link_error_dialog_invalid" data-rel="dialog" data-transition="pop" style="display: none;"></a>
<a href="#error_dialog_min_time_period" id="link_error_dialog_min_time_period" data-rel="dialog" data-transition="pop" style="display: none;"></a>

<div id="error_dialog_required" style="display: none;" >
  <h3>PLEASE ENTER A CODE</h3>
  <a rel='close' data-role='button' href='#'>Close</a>
</div>

<div id="error_dialog_invalid" style="display: none;" >
  <div style="text-align: center">
    <h3>SORRY, WE DON'T RECOGNISE THAT CODE, PLEASE TRY AGAIN.</h3>

    <p>If you continue to have issues, <br>please call 0800 505 123.</p>
    <a rel='close' data-role='button' href='#'>CLOSE</a>
  </div>
</div>

<div id="error_dialog_insufficient_redeems" style="display: none;">
  <div style="text-align: center">
    <h3>SORRY, YOU HAVEN'T REDEEMED ENOUGH CODES TO BE ELIGIBLE FOR A FREE DRINK.</h3>

    <p>If you continue to have issues, <br>please call 0800 505 123.</p>

    <a rel='close' data-role='button' href='#'>CLOSE</a>
  </div>
</div>

<div id="success_dialog_reset" style="display: none;">
  <div style="text-align: center">
    <h3>CONGRATULATIONS!<br>
    YOUR FREE DRINK HAS BEEN REWARDED
    </h3>

    <p>YOUR CARD HAS NOW BEEN RESET.</p>

    <a data-role='button' href='<?php echo url_for('@coke_card') ?>'>RETURN TO MyCoke CARD</a>
  </div>
</div>






