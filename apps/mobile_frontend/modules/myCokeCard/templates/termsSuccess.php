<div class="coke-card-header med">
    <a href="<?php echo url_for('@coke_card') ?>">
            <img src="/images/mobile/coke-card/coke-card-logo-lge.png" alt="My COKE Card">
        </a>
</div>
<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3>MyCoke Card Terms &amp; Conditions</h3>
</div>

<div data-role="content" class="coke_card_content">
<p>These Terms &amp; Conditions apply to the MyCoke Card that may be accessed from the MyCoke Mobile App or by scanning a QR code on advertising materials in participating outlets.  All information about the MyCoke Card including information in the App and on advertising materials in participating outlets is part of these Terms &amp; Conditions.  By accessing and using the MyCoke Card, You accept these Terms and Conditions.</p>

<h4>Introduction</h4>
<p>MyCoke Card is a store based rewards program that is specific to the retail outlet in which You first access the MyCoke Card. You access the store MyCoke Card via point of sale in the retail outlet or via the MyCoke Mobile App installed on your smartphone (Mobile) and allowing for GPS to &lsquo;find the user&rsquo;. (You). Once You have accessed the MyCoke Card and purchased a participating product, You will receive a scratch card with a unique code from the outlet.  Enter this unique code when requested within the MyCoke Card application on your mobile phone. After You have made the qualifying number of purchases You will be rewarded with a free 600mL product from the TCCC Soft Drink Range as outlined in the eligibility section.</p>

<h4>Eligibility</h4>
<p>Simply purchase a 355mL can or 420mL/600mL bottle of &ldquo;COCA-COLA&rdquo;, &ldquo;DIET COKE&rdquo;, &ldquo;FANTA&rdquo;, &ldquo;SPRITE&rdquo; or &ldquo;SPRITE ZERO&rdquo; and receive a scratch card from participating retailers. Offer is only open to purchases made in participating stores. Free reward can only be redeemed from the outlet where all the qualifying purchases have been made.  You must be aged 13 or over to be eligible to participate.</p>

<h4>Participation</h4>
<p>Activate the MyCoke card specific to the retailer and enter the unique code from the MyCoke Card Scratch card. A maximum of one unique code per day can be entered. Once 5 codes have been entered for the retailer MyCoke Card, You will qualify for the reward.</p>

<h4>Reward</h4>
<p>The reward for 5 purchases/unique codes from a participating outlet is one Free 600mL bottle of &ldquo;COCA-COLA&rdquo; or a &ldquo;DIET COKE&rdquo; or a &ldquo;FANTA&rdquo; or a &ldquo;SPRITE&rdquo; or a &ldquo;SPRITE ZERO&rdquo;. On receipt of your reward You will scan a QR code supplied by the retailer which will return the MyCoke Card balance back to zero.</p>

<h4>General</h4>
<p>Offer valid until 31 December 2012.</p>
<p>Any costs associated with downloading and using the MyCoke Card are at your sole cost and is dependent on your mobile phone plan.  The promoter takes no responsibility should the user not have the software, hardware or mobile phone plan that would permit access to the MyCoke Card.</p>
<p>If You have any queries please contact Coca-Cola on 0800 505 123. The offer is only open to residents of New Zealand. Promoter is Coca-Cola Oceania Ltd, The Oasis, Mount Wellington, Auckland.</p>



<p><a href="<?php echo url_for('@coke_card') ?>" data-role="button">ACTIVATE CARD</a></p>
</div>