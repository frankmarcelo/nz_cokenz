<div class="coke-card-header med">
    <a href="<?php echo url_for('@coke_card') ?>">
    <img src="/images/mobile/coke-card/coke-card-logo-lge.png" alt="My COKE Card">
    </a>
</div>

<div data-role="content" class="coke_card_content">
  <h3>ABOUT MyCoke CARD</h3>

  <p>MyCoke is a digital loyalty card. Every time you purchase a 355mL/420mL/600mL ‘COCA-COLA’, ‘DIET COKE’, 'COKE ZERO’, ‘SPRITE’, ‘SPRITE ZERO’ or ‘FANTA’ beverage from a participating outlet, we’ll give you a code to activate a stamp for your MyCoke Card. Once you reach 5 stamps, redeem them for a FREE 355mL ‘COCA-COLA’, ‘DIET COKE’, 'COKE ZERO’, ‘SPRITE’, ‘SPRITE ZERO’ or ‘FANTA’ beverage!</p>

  <h4>How to get a MyCoke Card</h4>
  <p>Click 'Activate Card' below and select an outlet to activate your card.</p>

  <h4>Where to use your MyCoke Card</h4>
  <p>At any participating outlet. You can have more than one outlet registered to your MyCoke Card and get stamps for each. For example, you can have a MyCoke Card for your local dairy and the shop next to your work.</p>

  <h4>How to get stamps</h4>
  <p>The outlet where you make a purchase will give you a card with a unique code. Visit MyCoke at m.coke.co.nz, select that outlet and enter the code to get a stamp.</p>

  <h4>Which drinks apply</h4>
  <p>Any 355mL/420mL/600mL ‘COCA-COLA’, ‘DIET COKE’, ‘COKE ZERO’, ‘SPRITE’, ‘SPRITE ZERO’ or ‘FANTA’. View full details.</p>

  <h4>How to get your free beverage</h4>
  <p>Once you have 5 stamps for one outlet on your MyCoke Card, visit the outlet and show your MyCoke Card to the owner. You'll receive a unique "code" to enter. Once entered, you'll receive your free beverage and your MyCoke Card will be reset.</p>

  <p><?php echo link_to('View full Terms &amp; Conditions.','@coke_card?action=terms') ?></p>

  <a href="<?php echo url_for('@coke_card') ?>" data-role="button">ACTIVATE CARD</a>
</div>