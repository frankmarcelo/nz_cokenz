<div class="coke-card-header med">
    <a href="<?php echo url_for('@coke_card') ?>">
            <img src="/images/mobile/coke-card/coke-card-logo-lge.png" alt="My COKE Card">
        </a>
</div>
<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3><?php echo $location->Location->getLocationLabel() ?></h3>
</div>

<div data-role="content" class="coke_card_content">

  <div class="stamp_card">
    <?php if( $redeemed ): for($i=1;$i<=$redeemed;$i++): ?>
      <div class="stamp redeemed">
        <div class="icon"></div>
      </div>
    <?php endfor; endif; ?>

    <?php for($j=($redeemed+1); $j < 6; $j++): ?>
      <a href="<?php echo url_for('coke_card_redeem',$location_card) ?>">
        <div class="stamp remaining">
          <div class="icon"></div>
          <div class="black_overlay"></div>
          <div class="numbers-sprite numbers-number-<?php echo $j ?>"></div>
        </div>
      </a>
    <?php endfor; ?>

    <?php if($redeemed == 5): ?>
      <a href="<?php echo url_for('coke_card_redeem_free',$location_card) ?>">
        <div class="stamp free">
          <div class="text" style="text-decoration: none; border: none;">FREE COKE</div>
        </div>
      </a>
    <?php else: ?>
      <div class="stamp free">
        <div class="text">FREE COKE</div>
        <div class="black_overlay"></div>
        <div class="numbers-sprite numbers-number-6"></div>
      </div>
    <?php endif; ?>
  </div>

  <div class="content_box_red">
    <p>Purchase a 355mL/420mL/600mL &lsquo;COCA-COLA&rsquo;, &lsquo;DIET COKE&rsquo;, &lsquo;COKE ZERO&rsquo;, &lsquo;SPRITE&rsquo;, &lsquo;SPRITE ZERO&rsquo; or &lsquo;FANTA&rsquo; beverage from this outlet, receive an activation code and redeem it to get a stamp for your MyCoke Card. Once you reach 5 stamps, redeem them for a free drink! Terms &amp; Conditions apply</p>
  </div>
<?php if ($redeemed == 5): ?>
  <a data-theme="b" data-role="button" href="<?php echo url_for('coke_card_redeem_free',$location_card) ?>">REDEEM YOUR FREE COKE</a>
<?php else: ?>
  <a data-theme="b" data-role="button" href="<?php echo url_for('coke_card_redeem',$location_card) ?>">ENTER ACTIVATION CODE</a>
<?php endif; ?>
</div>