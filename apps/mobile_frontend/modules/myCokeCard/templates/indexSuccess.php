<?php if($user_locations->count()): ?>
  <div class="coke-card-header med">
    <a href="<?php echo url_for('@coke_card') ?>">
        <img src="/images/mobile/coke-card/coke-card-logo-lge.png" alt="My COKE Card">
    </a>
  </div>
  <div class="ui-bar ui-bar-b coca-cola-article-title">
    <h3>MyCoke CARD ACCOUNT</h3>
  </div>
<?php else: ?>
  <div class="coke-card-header large">
    <img src="/images/mobile/coke-card/coke-card-logo-lge.png" alt="MyCoke Card">
  </div>
<?php endif; ?>

<div data-role="content"  class="coke_card_content">

  <?php if($user_locations->count()): ?>
    <div class="coke_card_locations">
    <?php foreach($user_locations as $loc): ?>
      <p><a href="<?php echo url_for('coke_card_location',$loc) ?>" data-theme="b" data-role="button" data-mini="true" data-reloadPage="true"><?php echo $loc->Location->getLocationLabel() ?></a></p>
    <?php endforeach; ?>
    </div>

    <p>Activate a MyCoke Card for another outlet:</p>
  <?php else: ?>
    <p>Select an outlet to activate your MyCoke Card</p>
  <?php endif; ?>
  <form method="post" action="<?php echo url_for('@coke_card?action=activate') ?>" data-ajax="false">
    <?php echo $form ?>
    <input type="submit" value="ACTIVATE CARD" >
  </form>
<p>&nbsp;</p>
<p>&nbsp;</p>
  <a data-theme="b" data-role="button" href="<?php echo url_for('@coke_card?action=about') ?>">What is MyCoke Card?</a>
</div>