<div class="coke-card-header med">
    <a href="<?php echo url_for('@coke_card') ?>">
            <img src="/images/mobile/coke-card/coke-card-logo-lge.png" alt="My COKE Card">
        </a>
</div>
<div data-role="content" class="coke_card_content">
  <p>Enter your code and press Activate</p>
  <form method="post" action="<?php echo url_for('@coke_card_redeem_code') ?>" id="redeem_form" data-ajax="false">

    <?php echo $form['code']->renderRow() ?>
    <?php echo $form->renderHiddenFields() ?>
    <?php echo $form->renderGlobalErrors() ?>

    <input type="submit" value="ACTIVATE">
    <a href="<?php echo url_for('coke_card_location',$card->UserLocation) ?>" data-role="button">BACK</a>
  </form>
</div>

<a href="#error_dialog_invalid" id="link_error_dialog_invalid" data-rel="dialog" data-transition="pop" style="display: none;"></a>
<a href="#error_dialog_min_time_period" id="link_error_dialog_min_time_period" data-rel="dialog" data-transition="pop" style="display: none;"></a>

<div id="error_dialog_required" style="display: none;" >
  <h3>PLEASE ENTER A CODE</h3>
  <a rel='close' data-role='button' href='#'>Close</a>
</div>

<div id="error_dialog_invalid" style="display: none;" >
  <div style="text-align: center">
    <h3>SORRY, WE DON'T RECOGNISE THAT CODE, PLEASE TRY AGAIN.</h3>

    <p>If you continue to have issues, <br>please call 0800 505 123.</p>
    <a rel='close' data-role='button' href='#'>Close</a>
  </div>
</div>

<div id="error_dialog_min_time_period" style="display: none;">
  <div style="text-align: center">
    <h3>SORRY, YOU CAN ONLY REDEEM ONE ACTIVATION CODE PER DAY.</h3>

    <h4>SEE YOU TOMORROW!</h4>
    <p>If you continue to have issues, <br>please call 0800 505 123.</p>

    <a rel='close' data-role='button' href='#'>Close</a>
  </div>
</div>


