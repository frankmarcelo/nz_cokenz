<?php

/**
 * myCokeCard actions.
 *
 * @package    Coke NZ
 * @subpackage myCokeCard
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class myCokeCardActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */

  public function preExecute()
  {
    $this->setLayout('layout');
  }


  public function executeIndex(sfWebRequest $request)
  {
    $this->user_locations = $this->getUser()->getGuardUser()->getUserCardLocations();
    $this->setLayout('layout');
    $this->form = new SelectLocationForm();
  }


  public function executeLocationCard(sfWebRequest $request)
  {
    $this->location = $this->getRoute()->getObject();
    $this->location_card = Doctrine::getTable('CardUserCard')->getActiveCard($this->location->id);
    $this->redeemed = $this->location_card->Redemptions->count();
    $this->remaining = 6 - $this->redeemed;
  }

  public function executeLocationCardRedeem(sfWebRequest $request)
  {
    $this->card = $this->getRoute()->getObject();

    $this->form = new RedeemCardCodeForm( array('user_card_id' => $this->card->id) );

  }

  public function executeLocationCardRedeemFree(sfWebRequest $request)
  {
    $this->card = $this->getRoute()->getObject();

    $this->form = new RedeemFreeDrinkCodeForm( array('user_card_id' => $this->card->id) );

  }

  public function executeDoRedeem(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
    sfConfig::set('sf_web_debug',false);
    $form = new RedeemCardCodeForm();
    $form->bind($request->getParameter($form->getName()));

    if( $form->isValid() ) {

      $card = Doctrine::getTable('CardUserCard')->find($form->getValue('user_card_id'));

      $redeem = new CardUserRedemption();
      $redeem->setCard($card);

      $code = Doctrine::getTable('CardCode')->findOneCodeCaseInsensitive($form->getValue('code'));
      $redeem->setCode($code);
      $redeem->save();

      $code->is_used = true;
      $code->save();

    }

    if( !isset($card) ) {
      $tv = $form->getTaintedValues();
      $card = Doctrine::getTable('CardUserCard')->find($tv['user_card_id']);
    }

    $this->renderText(json_encode( $this->getRedemptionErrors($form,$card) ));
    
    return sfView::NONE;
  }

  public function executeDoFreeRedeem(sfWebRequest $request)
  {
    $this->forward404Unless($request->isMethod('post'));
    sfConfig::set('sf_web_debug',false);

    $form = new RedeemFreeDrinkCodeForm();
    $form->bind($request->getParameter($form->getName()));

    if( $form->isValid() ) {

      $card = Doctrine::getTable('CardUserCard')->find($form->getValue('user_card_id'));

      $card->is_active = false;
      $card->redemption_date = new Doctrine_Expression('NOW()');
      $card->save();

      $new_card = new CardUserCard();
      $new_card->user_location_id = $card->user_location_id;
      $new_card->save();

    }

    if( !isset($card) ) {
      $tv = $form->getTaintedValues();
      $card = Doctrine::getTable('CardUserCard')->find($tv['user_card_id']);
    }

    $this->renderText(json_encode( $this->getRedemptionErrors($form,$card) ));

    return sfView::NONE;
  }

  private function getRedemptionErrors($form, $card)
  {

    $ge = array();
    foreach ($form->getGlobalErrors() as $e) {
      $ge[$e->getCode()] = $e->getMessage();
    }

    return array(
      'success'    => $form->isValid() && !$form->hasGlobalErrors(),
      'message'    => array(
        'global'        => $ge,
        'code'          => ( $form['code']->hasError() ? $form['code']->getError()->getMessageFormat() : false ),
      ),
      'target_url' => (empty($request['_target_path'])) ? $this->generateUrl('coke_card_location', $card->UserLocation )
                                                        : $this->generateUrl($request['_target_path'])
    );


  }



  public function executeActivate(sfWebRequest $request)
  {

    $form = new SelectLocationForm();
    $form->bind($request->getParameter($form->getName()));
    if($form->isValid())
    {
      //Add the location
      $user_location = new CardUserLocation();
      $user_location->setUser($this->getUser()->getGuardUser());
      $user_location->setLocationId( $form->getValue('location') );
      $user_location->save();

      //Create the first card
      $card = new CardUserCard();
      $card->setUserLocation($user_location);
      $card->save();

      $this->redirect('@coke_card');

    }

    $this->form = $form;

    $this->setTemplate('index');

  }

  public function executeAbout($request)
  {
  }
  public function executeTerms($request)
  {
  }

}
