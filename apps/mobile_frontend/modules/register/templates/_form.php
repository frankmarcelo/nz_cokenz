<form action="<?php echo url_for('register/do'); ?>" method="post" data-ajax="false" id="register_form">

    <label for="first_name">First Name: <span class="error"></span></label>
    <?php echo $form['first_name']->render(array('data-theme' => 'c', 'id' => 'first_name')); ?>

    <label for="last_name">Last Name: <span class="error"></span></label>
    <?php echo $form['last_name']->render(array('data-theme' => 'c', 'id' => 'last_name')); ?>

    <label for="email">E-mail: <span class="error"></span></label>
    <?php echo $form['email_address']->render(array('data-theme' => 'c', 'id' => 'email')); ?>

    <?php if(!$edit): ?>
    <label for="password">Password: <span class="error"></span></label>
    <?php echo $form['password']->render(array(
        'data-theme'  => 'c',
        'placeholder' => 'Minimum 6 characters',
        'autocomplete' => 'off',
        'id'          => 'password'));
    ?>
    <label for="password_again">Confirm Password: <span class="error"></span></label>
    <?php echo $form['password_again']->render(array(
        'data-theme'  => 'c',
        'placeholder' => 'Minimum 6 characters',
        'autocomplete' => 'off',
        'id'          => 'password_again'));
    ?>
    <?php endif; ?>

    <fieldset data-role="controlgroup" data-type="horizontal" data-theme="c" id="fieldset_gender">
      <legend>Gender: <span class="error"></span></legend>
      <?php echo $form['Profile']['gender']->render(array('data-theme' => 'c', 'data-width' => '100%')); ?>
    </fieldset>

    <div data-role="fieldcontain">
      <fieldset data-role="controlgroup" data-type="horizontal" id="fieldset_dob">
        <legend>Date of Birth: <span class="error"></span></legend>
        <div class="clear"></div>
        <?php echo $form['Profile']['dob']->render(array('data-theme' => 'c','id' => 'dob')); ?>
      </fieldset>
    </div>

    <label for="phone">Mobile Number: <span class="error"></span></label>
    <?php echo $form['Profile']['phone']->render(array('data-theme' => 'c', 'id' => 'phone')); ?>

    <label for="region">Region: <span class="error"></span></label>
    <?php echo $form['Profile']['region']->render(array('data-theme' => 'c', 'id' => 'region')); ?>

    <label for="postcode">Postcode: <span class="error"></span></label>
    <?php echo $form['Profile']['postcode']->render(array('data-theme' => 'c', 'id' => 'postcode')); ?>

    <div class="clear" style="height: 20px"></div>

    <?php if(!$edit): ?>
    <div data-role="fieldcontain" class="coca-cola-register-checkbox">
      <?php echo $form['nz_resident']->render(array('data-theme' => 'd', 'data-mini' => 'true')); ?>
      <?php echo $form['nz_resident']->renderLabel("I am a resident of New Zealand <br><span class=\"error\"></span>"); ?>
    </div>
    <?php endif; ?>

    <div data-role="fieldcontain" class="coca-cola-register-checkbox">
      <?php echo $form['Profile']['email_updates']->render(array('data-theme' => 'd', 'data-mini' => 'true')); ?>
      <?php echo $form['Profile']['email_updates']->renderLabel() ?>
    </div>

    <div data-role="fieldcontain" class="coca-cola-register-checkbox">
      <?php echo $form['Profile']['mobile_updates']->render(array('data-theme' => 'd', 'data-mini' => 'true')); ?>
      <?php echo $form['Profile']['mobile_updates']->renderLabel(); ?>
    </div>

    <?php if(!$edit): ?>
    <div data-role="fieldcontain" class="coca-cola-register-checkbox">
      <?php echo $form['accept_privacy']->render(array('data-theme' => 'd', 'id' => 'accept_privacy', 'data-mini' => 'true')); ?>
      <label for="accept_privacy">I have read and agree with the <a data-rel="dialog" data-transition="pop" href="<?php echo url_for('more/privacy') ?>">Privacy Policy</a>&emsp;<br><span class="error"></span></label>
    </div>
    <?php endif; ?>

    <?php echo $form->renderHiddenFields(true) ?>

    <?php if(!$form->isNew()): ?>
      <input type="hidden" name="sf_method" value="put">
      <input type="submit" value="SAVE" />
    <?php else: ?>
      <input type="submit" value="REGISTER" />
    <?php endif; ?>


    <label for="global"><span class="error"></span></label>

  </form>