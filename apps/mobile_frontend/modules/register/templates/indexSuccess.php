<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3>REGISTER FOR A COKE PASS</h3>
</div>

<div data-role="content">

    <?php if( !$underage ) {
      include_partial('form',array('form' => $form, 'edit' => false));
    } ?>

    <div id="underage" style="display: <?php echo ( $underage ? 'block' : 'none') ?>" >
      <p>I'm sorry, but we can not accept your registration at this time.</p>
    </div>

</div>