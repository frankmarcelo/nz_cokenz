<?php

class registerActions extends sfActions
{
  public function executeIndex()
  {
    // force layout for AJAX requests
    $this->setLayout('layout');

    $this->forwardIf($this->getUser()->isAuthenticated(),'cokePass','index');

    $this->form = new MobileUserRegisterForm();
    $this->underage = false;

    if( isset($_COOKIE['underage']) ){
      $this->underage = true;
    }
  }

  public function executeEdit(sfWebRequest $request)
  {
    $this->redirectUnless($this->getUser()->isAuthenticated(),'@sf_guard_signin');
    $this->form = new MobileUserRegisterForm($this->getUser()->getGuardUser());

    $this->setLayout('layout');
  }

  public function executeDo(sfWebRequest $request)
  {
    // Mobile version of registration form
    $edit = null;
    if($request->isMethod('put')){
      $edit = $this->getUser()->getGuardUser();
    }
    $this->form = new MobileUserRegisterForm($edit);
    $this->underage = false;

    if ($request->isMethod('post') or $request->isMethod('put'))
    {
      $this->getResponse()->setHttpHeader('Content-type', 'application/json');

      $this->form->bind( $request->getParameter($this->form->getName()) );

      if ($this->form->isValid())
      {
        $Profile = $this->form->getValue('Profile');

        $dob = new DateTime($Profile['dob']);
        $now = new DateTime();
        $diff = $now->diff($dob);

        if( $diff->y >= sfConfig::get('app_min_age',14) ) {
          $user = $this->form->save();
          $user->getProfile()->setUser($user);
          $user->getProfile()->setRegistrationSource('mobile');
          $user->getProfile()->save();

          if($this->form->isNew()){
            CokeMailer::sendRegoCompleteEmail($user->email_address,$user->first_name);
            $this->getUser()->signin($user);
          }

        } else {
          $this->underage = true;
          setcookie('underage','true',time()+(60*60*24) );
        }

      }

      // General JSON response based on result
      $this->renderText(json_encode(
        array(
          'success'    => $this->form->isValid() && !$this->form->hasGlobalErrors() && !$this->underage,
          'underage'   => $this->underage,
          'message'    => array(
            'global'        => $this->form->renderGlobalErrors(),
            'first_name'    => $this->form['first_name']->renderError(),
            'last_name'     => $this->form['last_name']->renderError(),
            'email'         => $this->form['email_address']->renderError(),
            'password'      => ( isset($this->form['password']) ? $this->form['password']->renderError() : ''),
            'dob'           => $this->form['Profile']['dob']->renderError(),
            'gender'        => $this->form['Profile']['gender']->renderError(),
            'phone'         => $this->form['Profile']['phone']->renderError(),
            'region'        => $this->form['Profile']['region']->renderError(),
            'postcode'      => $this->form['Profile']['postcode']->renderError(),
            'accept_privacy' => ( isset($this->form['accept_privacy']) ? $this->form['accept_privacy']->renderError() : ''),
            'sf_guard_user_nz_resident'   => ( isset($this->form['nz_resident']) ? $this->form['nz_resident']->renderError() : '')
          ),
          // not really sure if options are necessary here, however lets provide this feature
          'target_url' => (empty($request['_target_path'])) ? $this->generateUrl('user_register_success')
                                                            : $this->generateUrl($request['_target_path'])
        )
      ));

    }
    else
    {
      $this->redirect('@homepage');
    }

    return sfView::NONE;
  }

  public function executeComplete()
  {
    // force layout for AJAX requests
    $this->setLayout('layout');

    // check if user is already authenticated
    $this->redirectIf($this->getUser()->isAuthenticated(), '@homepage');
  }


}