<?php

require_once(sfConfig::get('sf_plugins_dir').'/sfDoctrineGuardPlugin/modules/sfGuardAuth/lib/BasesfGuardAuthActions.class.php');

class sfGuardAuthActions extends BasesfGuardAuthActions
{
  public function executeLogin(sfWebRequest $request)
  {
    // force layout for AJAX requests
    if ($request->isXmlHttpRequest())
    {
      $this->setLayout('layout');
    }

    // check if user is already authenticated
    $this->redirectIf($this->getUser()->isAuthenticated(), '@pass'  );

    $this->form = new MobileSigninForm();
    $this->form->setDefault('_target_path', $request->getReferer());


  }

  public function executeProtected(sfWebRequest $request)
  {
    // force layout for AJAX requests
    if ($request->isXmlHttpRequest())
    {
      $this->setLayout('layout');
    }

  }

  /*
   * Proxy method to provide JSON interface, mere copy of plugin action
   */
  public function executeDo(sfWebRequest $request)
  {
    // check if user is already authenticated
    $user = $this->getUser();

    if ($user->isAuthenticated())
    {
      $this->getResponse()->setHttpHeader('Content-type', 'application/json');
      $this->renderText(json_encode(
        array(
          'success'    => true,
          'message'    => array(),
          'target_url' => $this->generateUrl('homepage')
        )
      ));
    }

    // Mobile version of sign in form
    $this->form = new MobileSigninForm();

    if ($request->isMethod('post'))
    {
      $this->getResponse()->setHttpHeader('Content-type', 'application/json');

      $this->form->bind($request->getParameter('signin'));
      if ($this->form->isValid())
      {
        $values = $this->form->getValues();
        $this->getUser()->signin($values['user'], false);
      }
      $target_path = $this->form->getValue('_target_path');
      // General JSON response based on result
      $this->renderText(json_encode(
        array(
          'success'    => $this->form->isValid() && !$this->form->hasGlobalErrors(),
          'message'    => array(
            'global'   => $this->form->renderGlobalErrors(),
            'username' => $this->form['username']->renderError()
          ),
          'target_url' => (empty( $target_path )) ? $this->generateUrl('homepage') : $target_path
        )
      ));

    }
    else
    {

      $this->redirect( ($request->getReferer() ? $request->getReferer() : '@homepage')  );
    }

    return sfView::NONE;
  }
}