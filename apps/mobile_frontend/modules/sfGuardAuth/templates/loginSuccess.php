<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3>COKE PASS LOG IN</h3>
</div>

<div data-role="content" class="coke_pass">

  <form action="<?php echo url_for('login/do'); ?>" method="post" data-ajax="false" id="login_form">

    <?php echo $form['username']->render(array(
      'placeholder' => 'enter email...',
      'class'       => 'float-center',
    ));
    ?>
    <br />
    <?php echo $form['password']->render(array(
      'placeholder' => 'enter password...',
      'class'       => 'float-center',
    ));
    ?>

    <?php echo $form->renderHiddenFields() ?>

    <input type="submit" value="LOG IN" data-theme="a" />
  </form>
  <br />

  <div class="coca-cola-error-box">
    <ul class="client"></ul>
  </div>

  <p class="text-center">
    Forgot your password? <a href="<?php echo url_for('@sf_guard_forgot_password') ?>">Reset your password now.</a>
  </p>
  <p class="text-center">
    No COKE Pass login? Register now...
  </p>
  <a href="<?php echo url_for('register/index'); ?>" data-role="button" data-ajax="false">REGISTER</a>

</div>