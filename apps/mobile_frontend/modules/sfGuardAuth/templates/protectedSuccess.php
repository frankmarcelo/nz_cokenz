<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3>&nbsp;</h3>
</div>

<div data-role="content" class="coke_pass">
  <div class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-c">
    <span style="white-space: normal; font-size: 134%; margin:30px 0;" class="ui-btn-inner ui-btn-corner-all">TO ACCESS THIS CONTENT YOU NEED TO LOG IN WITH YOUR COKE PASS.</span>
  </div>

  <a href="<?php echo url_for('@user_login'); ?>" data-role="button" data-theme="a" data-ajax="false">LOG IN</a>
  <a href="<?php echo url_for('register/index'); ?>" data-role="button" data-theme="a" data-ajax="false">GET A COKE PASS</a>
  <a href="<?php echo url_for('@homepage'); ?>" data-role="button" data-theme="b">CANCEL</a>
</div>