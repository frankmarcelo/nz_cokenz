<?php

class shareacokeActions extends sfActions
{

    public function postExecute()
    {
    $this->setLayout('layout');
    }


    public function executeIndex(sfWebRequest $request)
    {

    }

    public function executeNominate(sfWebRequest $request)
    {
        if ($request->getParameter('complete', false)) {
            $this->complete = true;

        } else {
            $form = new SACNominateNameForm();
            $this->complete = false;

            if ($request->isMethod('post')) {
                if ($form->bindAndSave($request->getParameter($form->getName()))) {
                    $this->redirect('@shareacoke_nominate_name_complete');
                }
            }

            $this->form = $form;
        }
    }

    public function executeNominateFormSent(sfWebRequest $request)
    {

    }
    public function executeShareYourStory(sfWebRequest $request)
    {
        $form = new SACStoryForm();

        $towns = ShareACoke::getNZTownList();
        $this->town_list = json_encode($towns);

        $this->submitted = false;
        $this->is_ios = false;

        $user_agent       = $_SERVER['HTTP_USER_AGENT'];
        if( preg_match('/ipod/i',$user_agent) or preg_match('/iphone/i',$user_agent)) {
            $this->is_ios = true;
        }

        if ($request->isMethod('post')) {

            if ($form->bindAndSave($request->getParameter($form->getName()), $request->getFiles($form->getName()))) {
                $this->submitted = true;

                $fileName = $form->getObject()->getPic();
                if ($fileName) {
                    $thumbnail = new sfThumbnail(150, 150);
                    $thumbnail->loadFile(sfConfig::get('sf_upload_dir').'/app/shareacoke/stories/'.$fileName);

                    if (!file_exists(sfConfig::get('sf_upload_dir').'/app/shareacoke/stories/thumb/')) {
                        mkdir(sfConfig::get('sf_upload_dir').'/app/shareacoke/stories/thumb/');
                    }
                    $thumbnail->save(sfConfig::get('sf_upload_dir').'/app/shareacoke/stories/thumb/'.$fileName);
                }
            }
        }
        $form->getWidgetSchema()->setFormFormatterName('mobileShareACoke');
        $this->form = $form;
    }

    public function execute150Names(sfWebRequest $request)
    {
        $this->cokename = $request->getParameter("title");
        $names = sfConfig::get('app_shareacoke_names150');
        $this->allnames = $names['names'];

    }

    public function executeVoteForm(sfWebRequest $request)
    {
        $this->cokename = $request->getParameter("title");
        //TODO: update the names list;
        $names = sfConfig::get('app_shareacoke_names150');
        $this->allnames = $names['names'];

    }
    public function executeVoteFormSent(sfWebRequest $request)
    {

    }



    public function executeTerms(sfWebRequest $request)
    {

    }

    public function executeAnnounceNamesList(sfWebRequest $request)
    {

        $this->cokename = $request->getParameter("title");
        //TODO: update the names list;
        $names = sfConfig::get('app_shareacoke_names150');
        $this->allnames = $names['names'];


    }

    public function executeCustomiseCan(sfWebRequest $request)
    {

    }
    public function executeTvc(sfWebRequest $request)
    {

    }


}