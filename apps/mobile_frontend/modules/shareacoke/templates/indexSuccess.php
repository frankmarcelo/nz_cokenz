<?php
    use_javascript('apps/shareacoke/cufon1.09.js');
    use_javascript('apps/shareacoke/gotham.cufon.js');
    use_javascript('mobile-shareacoke.js');
?>

<div>
<?php include_component('shareacoke', 'nav') ?>

    <div class="banner">
        <?php echo image_tag('mobile/shareacoke/home-banner.jpg') ?>
    </div>

    <?php if (time() < mktime(0,0,0,9,24,2012)): ?>
    <div class="banner_footer">
        <table>
            <tr>
                <td>
                    <a href="<?php echo url_for('@shareacoke_150names') ?>">
                        <div class="banner_footer_left you">150 Names</div>
                    </a>
                </td>
                <td>
                    <a href="<?php echo url_for('@shareacoke_nominate_name') ?>">
                        <div class="banner_footer_right you">Nominate a Name</div>
                    </a>
                </td>
            </tr>
        </table>
    </div>
    <?php endif; ?>
    <div class="home_content_btns">
        <div>
            <a class="pointy_btn" href="<?php echo url_for('@shareacoke_150names') ?>">
                <img src="<?php echo image_path('mobile/shareacoke/thumb-150names_03.jpg') ?>" /><span class="you">150 Names</span></a>
        </div>

        <div>
            <a class="pointy_btn" href="<?php echo url_for('@shareacoke_story') ?>">
                <img src="<?php echo image_path('mobile/shareacoke/thumb-shareastory.jpg') ?>" /><span class="you">Share your Story</span></a>
        </div>

        <div>
            <a class="pointy_btn" href="<?php echo url_for('@shareacoke_tvc') ?>">
                <img src="<?php echo image_path('/images/mobile/shareacoke/thumb-tvc.jpg') ?>" /><span class="you">Watch the TV ads</span>
            </a>
        </div>

    </div>
</div>