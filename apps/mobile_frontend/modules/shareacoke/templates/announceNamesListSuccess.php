<?php
use_javascript('apps/shareacoke/cufon1.09.js');
use_javascript('apps/shareacoke/gotham.cufon.js');
use_javascript('mobile-shareacoke.js');

?>
<div class="one_fifty_names">
    <?php include_component('shareacoke', 'nav') ?>

    <div class="title_1 gotham">50 New Names Announced</div>


    <div class="names_list">

        <ul data-role="listview" data-filter="true" data-theme="a" data-filter-placeholder="search name...">
            <?php foreach($allnames as $name): ?>
            <li class="you"><?php echo $name ?></li>
            <?php endforeach; ?>

        </ul>
    </div>


</div>