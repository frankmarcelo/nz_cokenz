<?php
use_javascript('apps/shareacoke/cufon1.09.js');
use_javascript('apps/shareacoke/gotham.cufon.js');
use_javascript('mobile-shareacoke.js');

?>
<div class="one_fifty_names vote" xmlns="http://www.w3.org/1999/html">
    <?php include_component('shareacoke', 'nav') ?>

    <div class="title_1 gotham">Vote for the top 50 New Names</div>

    <div class="subtle_text_1">
        <p>Search or scroll through the 100 below and vote for the names of the people you would most like to share a <b>Coke</b> with.</p>
        <p>You can vote for upto ten names.</p>
    </div>
    <div class="nominate_name_form_wrapper">
        <form>
            <div class="form_block error_text">
                <input type="text" name="name" placeholder="Your Name:"  />
            </div>
            <div class="form_block">
                <input type="email" name="email" placeholder="Your Email:"  />
            </div>


            <div class="names_list">

                <ul data-role="listview" data-filter="true" data-theme="a" data-filter-placeholder="search name...">
                    <?php foreach($allnames as $name): ?>
                    <li class="you"><?php echo $name ?> <button name="<?php echo $name ?>" class="vote_btn" style="width: 100px;">Vote</button></li>
                    <?php endforeach; ?>

                </ul>
            </div>

            <div class="form_block checkbox">

                <input type="checkbox" name="terms_agree" id="checkbox-mini-0" data-mini="true" data-theme="c" style="display: none;"/>
                <label for="checkbox-mini-0">I have read &amp; agree to the Terms &amp; Conditions</label>
            </div>

            <div class="form_block submit">
                <div class='simple_pointy_btn'>
                    <a onclick="javascript: submitVotes();">Submit</a>
                </div>
            </div>


        </form>
    </div>
    <script type="text/javascript">
        maxVotesCount = 10;
        maxVotesReachedError = "You have already selected the maximum amount of names you can vote for, " + maxVotesCount + " names. Please deselect some names if you wish to change the selection.";

        buttonDownClassName = "btn_down"; //this is hardcoded in CSS (.btn_down)

        $(document).ready(function(){
            $('.nominate_name_form_wrapper .names_list .vote_btn').click(function(){
                downButtons = $('.nominate_name_form_wrapper .names_list .vote_btn.btn_down');
                if(downButtons.length >= maxVotesCount && !$(this).hasClass(buttonDownClassName)){
                    alert(maxVotesReachedError);
                    return false;
                }

                //alert($(this).attr('name'));
                $(this).toggleClass(buttonDownClassName);
                $(this).parent().toggleClass(buttonDownClassName);

                return false;
                //$(this).toggleClass("down");
            });
        });
        function submitVotes(){
            //just going to display the selected users for now.
            str = '';
            downButtons = $('.nominate_name_form_wrapper .names_list .vote_btn.' + buttonDownClassName);

            for(i=0; i< downButtons.length; i++){
                str += $(downButtons[i]).attr('name') + ", ";

            }
            alert("User wants to vote for: " + str);

        }


    </script>


</div>