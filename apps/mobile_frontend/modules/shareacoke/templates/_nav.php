
<div class="share_coke_nav">
<table>
    <tr>
        <td<?php echo $current == 'index' ? ' class="selected"':'' ?> id="share_coke_nav_button_home">
            <a href="<?php echo url_for('@shareacoke') ?>"><div class="outer"><div class="inner"><img src="<?php echo image_path('mobile/shareacoke/icon-home.png') ?>"></div></div></a>
        </td>
        <td<?php echo $current == '150Names' ? ' class="selected"':'' ?> id="share_coke_nav_button_150names">
            <a href="<?php echo url_for('@shareacoke_150names') ?>"><div class="outer"><div class="inner you">150 Names</div></div></a>
        </td>
        <td<?php echo $current == 'customiseCan' ? ' class="selected"':'' ?> id="share_coke_nav_button_customise">
            <a href="<?php echo url_for('@shareacoke_customise') ?>"><div class="outer"><div class="inner you">Customise a can or bottle</div></div></a>
        </td>
        <td<?php echo $current == 'shareYourStory' ? ' class="selected"':'' ?> id="share_coke_nav_button_story">
            <a href="<?php echo url_for('@shareacoke_story') ?>"><div class="outer"><div class="inner you">Share your Story</div></div></a>
        </td>
        <td></td>
        <?php /* ?>

        <td></td>

        <td<?php echo $current == 'terms' ? ' class="selected"':'' ?> id="share_coke_nav_button_tcs">
        <a href="<?php echo url_for('@shareacoke?action=terms') ?>"><div class="outer"><div class="inner you">T&amp;Cs</div></div></a>
        </td> <?php */ ?>
    </tr>
</table>
</div>