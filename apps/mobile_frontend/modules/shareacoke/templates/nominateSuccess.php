<div>
    <?php include_component('shareacoke', 'nav') ?>

    <div class="title_1">Nominate a Name</div>

<?php if ($complete): ?>

    <div class="title_2">Thank you!</div>

    <div class="subtle_text_1">
        Your nomination has been sent.
    </div>

<?php else: ?>

    <div class="subtle_text_1">
        Simply enter your name, your email address and the first name of the person you would like to share a <b>Coke</b> with.
    </div>

    <?php if ($form->hasErrors()): ?>
    <div class="subtle_error_1">
        Sorry, please complete all the required fields to continue. A valid email address is required.
    </div>
    <?php endif; ?>

    <div class="nominate_name_form_wrapper">
        <form method="post" action="<?php echo url_for('@shareacoke_nominate_name') ?>">
            <div class="form_block <?php echo $form['name']->hasError() ? 'error_text' : '' ?>">
                <?php echo $form['name']->renderLabel('Your Name:') ?>
                <?php echo $form['name']->render() ?>
            </div>
            <div class="form_block <?php echo $form['email']->hasError() ? 'error_text' : '' ?>">
                <?php echo $form['email']->renderLabel('Your Email:') ?>
                <?php echo $form['email']->render() ?>
            </div>
            <div class="form_block <?php echo $form['nominate_name']->hasError() ? 'error_text' : '' ?>">
                <?php echo $form['nominate_name']->renderLabel('Name you&rsquo;d like to see on a <strong>Coke</strong> bottle:') ?>
                <?php echo $form['nominate_name']->render() ?>
            </div>
            <div class="form_block <?php echo $form['nominate_reason']->hasError() ? 'error_text' : '' ?>">
                <?php echo $form['nominate_reason']->renderLabel('Tell us why you&rsquo;d like to share a <strong>Coke</strong> with that person.<br> We&rsquo;re looking for the most creative reasons:') ?>
                <?php echo $form['nominate_reason']->render(array('rows' => 3)) ?>
            </div>

            <div class="form_copy">
                <ul>
                <li>Go to shareacoke.co.nz between 12:00 noon on 14th September 2012 and midnight on 23rd September 2012 to enter the name you would like to nominate.</li>
                <li>Enter your name, email, and the name you would like to nominate.</li>
                <li>Names must be a maximum of 12 characters, and cannot feature spaces, numbers, or special characters.</li>
                <li>A final shortlist of 100 names will be displayed on shareacoke.co.nz. You may vote by clicking “Vote” once on a name or range of names, up to a maximum of 10 names, between 09:00am on 8th October 2012 and the midnight of 12th October 2012.</li>
                <li>The 50 names with the highest number of Votes at midnight on 12th October 2012 will be printed on COCA-COLA bottles and will be available for purchase around 17th November 2012.</li>
                <li>We will not print names that have already been printed in the first batch of 150 names.</li>
                </ul>
            </div>

            <div class="form_block checkbox <?php echo $form['agree_tc']->hasError() ? 'error_text' : '' ?>">
                <?php echo $form['agree_tc']->render(array('data-theme' => 'd', 'data-mini' => 'true', 'style' => 'display:none')) ?>
                <?php echo $form['agree_tc']->renderLabel('I have read and agree to the terms &amp; conditions');//sprintf('', link_to('terms &amp; conditions', '@terms?p=nominate',  array('id' => 'terms_link', 'data-fancybox-type' => 'ajax')))) ?>
            </div>

            <a href="#" data-role="button" id="view_terms">View Terms &amp; Conditions</a>

            <div class="form_block submit">
                <button type="submit">SUBMIT</button>
            </div>
            <?php echo $form->renderHiddenFields() ?>

        </form>
    </div>
<?php endif; ?>
</div>

<div class="terms" style="display: none;">
    <?php include_partial('shareacoke/terms/nominate') ?>
</div>
<script type="text/javascript">
    $('#view_terms').click(function(){
        $('<div>').simpledialog2({
            mode: 'blank',
            headerText: 'Terms & Conditions',
            headerClose: true,
            cleanOnClose: true,
            dialogAllow: true,
            dialogForce: true,
            blankContent : $('.terms').html()
        });
    });
</script>