<?php
use_javascript('apps/shareacoke/cufon1.09.js');
use_javascript('apps/shareacoke/gotham.cufon.js');
use_javascript('mobile-shareacoke.js');

?>

<div class="customise_can">
    <?php include_component('shareacoke', 'nav') ?>

    <div class="banner">
        <?php echo image_tag('mobile/shareacoke/home-banner.jpg') ?>
    </div>





<!--
    <div class="banner_footer_2">
        <select name="location" id="select_location" data-theme="c">
            <option value="choose-one" data-placeholder="true">Choose your location</option>

            <option value="Auckland">Auckland</option>
            <option value="Tauranga">Tauranga</option>
            <option value="Hamilton">Hamilton</option>
            <option value="Wellington">Wellington</option>
            <option value="Christchurch">Christchurch</option>
            <option value="Dunedin">Dunedin</option>
            <option value="Palmerston North">Palmerston North</option>

        </select>
    </div>
-->
    <div id="slideleft" class="slide">
        <div class="inner">
            <div class="step_1">
                <div>
                    <a class="pointy_btn" href="">
                        <img src="<?php echo image_path('mobile/shareacoke/coke_can_thumb.jpg') ?>" /><span class="you">200ml COKE cans</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="">
                        <img src="<?php echo image_path('mobile/shareacoke/coke_bottle_thumb.jpg') ?>" /><span class="you">600ml COKE  bottles</span></a>
                </div>
            </div>



        </div>

        <div class="inner_2" style="left: -1000px; display: none;">
            <div class="step_2 step_2_200ml">
                <div>
                    <a class="pointy_btn" href="" title="Auckland">
                        <span class="you">Auckland</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Tauranga">
                        <span class="you">Tauranga</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Hamilton">
                        <span class="you">Hamilton</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Wellington">
                        <span class="you">Wellington</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Christchurch">
                        <span class="you">Christchurch</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Dunedin">
                        <span class="you">Dunedin</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Palmerston North">
                        <span class="you">Palmerston North</span></a>
                </div>
            </div>
            <div class="step_2 step_2_600ml">
                <div>
                    <a class="pointy_btn" href="" title="Auckland">
                        <span class="you">Auckland</span></a>
                </div>

                <div>
                    <a class="pointy_btn" href="" title="Hamilton">
                        <span class="you">Hamilton</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Wellington">
                        <span class="you">Wellington</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Christchurch">
                        <span class="you">Christchurch</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Dunedin">
                        <span class="you">Dunedin</span></a>
                </div>
                <div>
                    <a class="pointy_btn" href="" title="Palmerston North">
                        <span class="you">Palmerston North</span></a>
                </div>
            </div>
        </div>

        <div class="inner_3" style="left: -1000px; display: none;">
            <div class="step_3">
            <div class="event_info_wrapper">


            <div id="200ml_locations" class="location_list">
                <div class="title_2" style="display: none;">200ml <b>Coke</b> can locations</div>

                <div>

                    <div class="city gotham-b">Auckland</div>
                    <table class="city_locations">
                        <thead>
                        <tr>
                            <th>Date:</th>
                            <th>Location:</th>
                            <th>Address:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="date">19, 20, 21 Oct 2012</td>
                            <td class="location">Sylvia Park</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/A4UM9" target="_blank">286 Mt Wellington Highway</a></span></td>
                        </tr>
                        <tr>
                            <td class="date">2, 3, 4 Nov 2012</td>
                            <td class="location">Westfield Manukau</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/u49OH" target="_blank">Cnr Gt South &amp; Wiri Station Rd</a></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="date">9, 10, 11 Nov 2012</td>
                            <td class="location">Botany Town Centre</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/a5dfB" target="_blank">Cnr Te Irirangi &amp; Ti Rakau Dr</a></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="date">9, 10, 11 Nov 2012</td>
                            <td class="location">Westfield St Lukes</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/kvZk6" target="_blank">80 St Lukes Rd, Mt Albert</a></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="date">16, 17, 18 Nov 2012</td>
                            <td class="location">Westfield WestCity</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/hEzxR" target="_blank">7 Catherine St, Henderson</a></span>
                            </td>
                        </tr>
                        <tr>
                            <td class="date">16, 17, 18 Nov 2012</td>
                            <td class="location">Westfield Albany</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/jNtu0" target="_blank">219 Don McKinnon Dr, Albany</a></span>
                            </td>
                        </tr>
                        </tbody>
                    </table>

                </div>

                <div>

                    <div class="city gotham-b">Tauranga</div>
                    <table class="city_locations">
                        <thead>
                        <tr>
                            <th>Date:</th>
                            <th>Location:</th>
                            <th>Address:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="date">2, 3, 4 Nov 2012</td>
                            <td class="location">Bayfair Mall</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/F3cGN" target="_blank">Cnr Maunganui &amp; Girven Rd</a></span>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </div>

                <div>

                    <div class="city gotham-b">Hamilton</div>
                    <table class="city_locations">
                        <thead>
                        <tr>
                            <th>Date:</th>
                            <th>Location:</th>
                            <th>Address:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="date">26, 27, 28 Oct 2012</td>
                            <td class="location">Westfield Chartwell</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/TlDMz" target="_blank">Chartwell Square, Hamilton</a></span>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </div>

                <div>

                    <div class="city gotham-b">Wellington</div>
                    <table class="city_locations">
                        <thead>
                        <tr>
                            <th>Date:</th>
                            <th>Location:</th>
                            <th>Address:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="date">19, 20, 21 Oct 2012</td>
                            <td class="location">Westfield Queensgate</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/nIAE2" target="_blank">Cnr Queens Dr &amp; Bunny St, Pipitea</a></span>
                            </td>
                        </tr>
                        </tbody>
                    </table>


                </div>

                <div>

                    <div class="city gotham-b">Christchurch</div>
                    <table class="city_locations">
                        <thead>
                        <tr>
                            <th>Date:</th>
                            <th>Location:</th>
                            <th>Address:</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="date">26, 27, 28 Oct 2012</td>
                            <td class="location">Westfield Riccarton</td>
                            <td class="address">
                                <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                                <span><a href="http://goo.gl/maps/1Z2o3" target="_blank">129 Riccarton Road, Riccarton</a></span>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>


            </div>

            <div id="600ml_locations" class="location_list" style="">
            <div class="title_2" style="display: none;">600ml <b>Coke</b> bottle locations</div>
            <div>

                <div class="city gotham-b">Auckland</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date">15 &amp; 16 Sept</td>
                        <td class="location">Countdown Newmarket</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/JbQyW" target="_blank">277 Broadway, Newmarket</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">15 &amp; 16 Sept</td>
                        <td class="location">Countdown Quay St</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/lfyUg" target="_blank">76 Quay St, Auckland CBD</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">22 &amp; 23 Sept</td>
                        <td class="location">PaknSave Sylvia Park</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/A4UM9" target="_blank">286 Mt Wellington Highway</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">22 &amp; 23 Sept</td>
                        <td class="location">New World Vic Park	</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/MEq18" target="_blank">2 College Hill, Freemans Bay</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">29 &amp; 30 Sept</td>
                        <td class="location">Countdown Manukau</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/uuzzZ" target="_blank">4 Ronwood Ave, Manukau</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">29 &amp; 30 Sept</td>
                        <td class="location">Countdown Westgate</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/bzEsV" target="_blank">Fernhill Dr, Massey North</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">16 &amp; 17 Oct</td>
                        <td class="location">New World Botany</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/hrHwi" target="_blank">588 Chapel Road, Dannemora</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">16 &amp; 17 Oct	</td>
                        <td class="location">PNS Mt Albert</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/2C9e0" target="_blank">1177 New North Road, Mt Albert</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">23 &amp; 24 Oct</td>
                        <td class="location">Countdown Silverdale</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/s4taK" target="_blank">54 Hibiscus Coast Highway</a></span>
                        </td>
                    </tr>

                    </tbody>
                </table>

            </div>
            <div>


                <div class="city gotham-b">Wellington</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date">3 &amp; 5 Oct</td>
                        <td class="location">New World Railway</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/04bvF" target="_blank">Bunny St, Pipitea, Wellington</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">6 &amp; 7 Oct</td>
                        <td class="location">New World Porirua</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/JJQ6z" target="_blank">Cnr Walton Leigh &amp; Lyttelton Ave</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">13 &amp; 14 Oct</td>
                        <td class="location">Countdown Johnsonville</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/fXspZ" target="_blank">31 Johnsonville Rd, Johnsonville</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">13 &amp; 14 Oct</td>
                        <td class="location">Countdown Kilbirnie</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/UgZSG" target="_blank">47 Bay Road, Kilbirnie</a></span>
                        </td>
                    </tr>

                    </tbody>
                </table>


            </div>
            <div>



                <div class="city gotham-b">Christchurch</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date">6 &amp; 7 Oct</td>
                        <td class="location">New World St Martins</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/nyEU5" target="_blank">96 Wilsons Road, St Martins</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">13 &amp; 14 Oct</td>
                        <td class="location">PaknSave Moorhouse</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/Isa5K" target="_blank">297 Moorhouse Ave, Sydenham</a></span>
                        </td>
                    </tr>
                    <td class="date">24 &amp; 25 Nov</td>
                    <td class="location">Countdown Church Corner</td>
                    <td class="address">
                        <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                        <span><a href="http://goo.gl/maps/lb0aS" target="_blank">361 Riccarton Road, Riccarton</a></span>
                    </td>
                    </tr>
                    <td class="date">24 &amp; 25 Nov	</td>
                    <td class="location">Countdown Northlands</td>
                    <td class="address">
                        <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                        <span><a href="http://goo.gl/maps/gi42a" target="_blank">85 Main North Road, Papanui</a></span>
                    </td>
                    </tr>
                    </tbody>
                </table>



            </div>
            <div>



                <div class="city gotham-b">Dunedin</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date">4 Oct</td>
                        <td class="location">Golden Centre Mall</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/kJEz3" target="_blank">86 Hillside Rd, South Dunedin</a></span>
                        </td>
                    </tr>
                     <tr>
                        <td class="date">6 &amp; 7 Oct</td>
                        <td class="location">PaknSave Dunedin</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/kJEz3" target="_blank">86 Hillside Rd, South Dunedin</a></span>
                        </td>
                    </tr>
                    </tbody>
                </table>


            </div>
            <div>



                <div class="city gotham-b">Universities in Auckland</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date">13-Sep</td>
                        <td class="location">Auckland Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/EYJHD" target="_blank">Princes St, CBD</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">20-Sep</td>
                        <td class="location">AUT Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/nn79r" target="_blank">Wellesley St East, CBD</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">27-Sep</td>
                        <td class="location">Massey Uni Albany</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/vxzBQ" target="_blank">Albany Highway, Albany</a></span>
                        </td>
                    </tr>

                    </tbody>
                </table>



            </div>
            <div>


                <div class="city gotham-b">Universities in Hamilton</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date">24-Oct		</td>
                        <td class="location">Waikato Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/7WMiZ" target="_blank">University of Waikato, Knighton Rd</a></span>
                        </td>
                    </tr>
                    </tbody>
                </table>


            </div>
            <div>


                <div class="city gotham-b">Universities in Palmerston North</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date">4-Oct		</td>
                        <td class="location">Massey University P. North</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/vuxc4" target="_blank">Tennent Drive, Palmerston North</a></span>
                        </td>
                    </tr>
                    </tbody>
                </table>



            </div>
            <div>


                <div class="city gotham-b">Universities in Wellington</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date">27-Sep	</td>
                        <td class="location">Victoria Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/jLeZC" target="_blank">Kelburn Parade, Wellington</a></span>
                        </td>
                    </tr>
                    </tbody>
                </table>


            </div>
            <div>


                <div class="city gotham-b">Universities in Christchurch</div>
                <table class="city_locations">
                    <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="date"> 11-Oct</td>
                        <td class="location">Canterbury Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/iH7PG" target="_blank">University Dr, Ilam</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">11-Oct</td>
                        <td class="location">Lincoln Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/QHdqn" target="_blank">Ellesmere Rd, Lincoln</a></span>
                        </td>
                    </tr>

                    </tbody>
                </table>


            </div>





            </div>


            </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var customiseCanCity = "";
        var customiseCanType = "";

        $(document).ready(function() {
            $('#slideleft .step_1 .pointy_btn').click(function() {
                //window.location.hash = 'step1';
                //alert($(this).text());
                if( $(this).text().indexOf('200ml') != -1 ){
                    customiseCanType = "200ml";
                    $("#slideleft .inner_2 .step_2_600ml").css("display", "none");
                    $("#slideleft .inner_2 .step_2_200ml").css("display", "block");
                }else{
                    customiseCanType = "600ml";
                    $("#slideleft .inner_2 .step_2_600ml").css("display", "block");
                    $("#slideleft .inner_2 .step_2_200ml").css("display", "none");
                }


                var $hide =  $("#slideleft .inner");
                $($hide).css('display', $hide.outerWidth());
                $hide.animate({
                    left: -($hide.outerWidth() + 1000)
                });

                var $show =  $("#slideleft .inner_2");
                $($show).css('display', 'block');
                $($show).css('display', $show.outerWidth());

                $show.animate({
                    left: 0
                });

                $($hide).css('display', 'none');
                return false;
            });

            $('#slideleft .step_2 .pointy_btn').click(function() {
                //window.location.hash = 'step2';

                customiseCanCity = $(this).attr('title');
                if(customiseCanCity == ''){
                    customiseCanCity = $(this).text();
                }

                cityList = $('.event_info_wrapper .location_list .city');
                for(i=0; i< cityList.length; i++){
                    $(cityList[i]).parent().css('display', 'none');
                }

                if( customiseCanType == "200ml" ){
                    $('#200ml_locations .title_2').css('display', 'block');
                    $('#600ml_locations .title_2').css('display', 'none');

                    $('#200ml_locations').css('display', 'block');
                    $('#600ml_locations').css('display', 'none');

                }else{
                    $('#200ml_locations .title_2').css('display', 'none');
                    $('#600ml_locations .title_2').css('display', 'block');

                    $('#200ml_locations').css('display', 'none');
                    $('#600ml_locations').css('display', 'block');
                }



                    cityList = $('.event_info_wrapper .location_list .city');
                    count = 0;
                    for(i=0; i< cityList.length; i++){
                        count++;
                        $(cityList[i]).parent().css('display', 'none');
                        //console.log($(cityList[i]).text());
                        //console.log(customiseCanCity);
                        if( $(cityList[i]).text().indexOf(customiseCanCity) != -1 ){
                            //alert('found a city');
                            $(cityList[i]).parent().css('display', 'block');

                        }
                    }

                //alert($(this).text());

                var $hide =  $("#slideleft .inner_2");
                $($hide).css('display', $hide.outerWidth());
                $hide.animate({
                    left: -($hide.outerWidth() + 1000)
                });

                var $show =  $("#slideleft .inner_3");
                $($show).css('display', 'block');
                $($show).css('display', $show.outerWidth());

                $show.animate({
                    left: 0
                });

                $($hide).css('display', 'none');
                return false;
            });
        });



    </script>





            <script type="text/javascript">
                cityList = $('.event_info_wrapper .location_list .city');
                for(i=0; i< cityList.length; i++){
                    $(cityList[i]).parent().css('display', 'none');
                }


                $("#select_location").change(function () {
                    $('#200ml_locations .title_2').css('display', 'none');
                    $('#600ml_locations .title_2').css('display', 'none');

                    cityList = $('.event_info_wrapper .location_list .city');
                    count = 0;
                    for(i=0; i< cityList.length; i++){
                        count++;
                        $(cityList[i]).parent().css('display', 'none');
                        if( $(cityList[i]).text().indexOf($(this).val()) != -1 ){
                            $(cityList[i]).parent().css('display', 'block');
                            if($(cityList[i]).parent().parent().attr('id') == '200ml_locations'){
                                $('#200ml_locations .title_2').css('display', 'block');
                            }
                            if($(cityList[i]).parent().parent().attr('id') == '600ml_locations'){
                                $('#600ml_locations .title_2').css('display', 'block');
                            }

                        }
                    }

                })

            </script>


</div>