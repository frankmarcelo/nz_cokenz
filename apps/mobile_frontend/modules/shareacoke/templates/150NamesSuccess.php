<?php
use_javascript('apps/shareacoke/cufon1.09.js');
use_javascript('apps/shareacoke/new_gotham.cufon.js');
use_javascript('apps/shareacoke/init.js');
use_javascript('mobile-shareacoke.js');

?>
<div class="one_fifty_names">
    <?php include_component('shareacoke', 'nav') ?>

    <div class="title_1 gotham">If you know an Amelia,</div>
    <div class="title_1 gotham">then share a <img src="<?php echo image_path('app/shareacoke/label-cokelogo.jpg') ?>" alt="" style="height: 18px; width: auto;"></div>
    <div class="title_1 gotham">with an Amelia</div>

    <div class="subtle_text_1">
       Do you know a Chris, an Anna, a George, a Katie, or an Amelia? Now you can share a real <b>Coke</b> with their name on it! <b>Coke</b> bottles with 150 different names on them are now on sale everywhere. To see them all check out the list below.
    </div>
    <div class="names_list">

      <ul data-role="listview" data-filter="true" data-theme="a" data-filter-placeholder="search name...">
        <?php foreach($allnames as $name): ?>
            <li class="you"><?php echo $name ?></li>
        <?php endforeach; ?>
      </ul>
    </div>


</div>