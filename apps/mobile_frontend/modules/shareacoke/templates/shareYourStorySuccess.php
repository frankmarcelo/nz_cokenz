<?php
    use_javascript('apps/shareacoke/bootstrap.typeahead.min.js');
    use_javascript('apps/shareacoke/cufon1.09.js');
    use_javascript('apps/shareacoke/gotham.cufon.js');
    use_javascript('mobile-shareacoke.js');
?>
<div class="share_your_story_form">
    <?php include_component('shareacoke', 'nav') ?>

    <?php if ($submitted) : ?>
        <div class="title_1 gotham">
            <p>Thank you!<br/>Your story has been sent.</p>
        <?php if ($is_ios):  ?>
            <p style="font-size: 15px;">Don’t forget to email your story's pic to shareacoke@cokemail.co.nz. Please include your name in the subject line.</p>
        <?php endif; ?>
        </div>

    <?php else: ?>

    <div class="title_1 gotham">Shared a <img src="<?php echo image_path('app/shareacoke/label-cokelogo.jpg') ?>" alt="" style="height: 22px; width: auto; vertical-align: baseline;">?</div>
    <div class="title_1 gotham">Tell us about it!</div>

    <?php if ($form->hasErrors()): ?>
    <div class="subtle_error_1">
        Sorry, please complete all the required fields to continue.
    </div>
    <?php endif; ?>


    <div class="share_your_story_form_form_wrapper">
        <form method="post" action="<?php echo url_for('@shareacoke?action=shareYourStory') ?>" enctype="multipart/form-data">
            <?php echo $form['name']->renderRow(array('placeholder' => 'Your Name'), false) ?>
            <?php echo $form['email']->renderRow(array('placeholder' => 'Your Email', 'type' => 'email'), false) ?>
            <?php echo $form['phone']->renderRow(array('placeholder' => 'Your Phone', 'type' => 'tel'), false) ?>

            <?php if ($is_ios):  ?>
                <p style="color: #f40009;">We have detected you are using an iOS device. Please email your story&rsquo;s &lsquo;pic&rsquo; by following the instructions on the next screen.</p>
            <?php else: ?>
                <?php echo $form['pic']->renderRow(array(), 'Upload a Pic') ?>
            <?php endif; ?>

            <?php echo $form['location']->renderRow(array('placeholder' => 'Story location', 'autocomplete' => 'off'), false) ?>
            <?php echo $form['story']->renderRow(array(), 'Your story') ?>

            <ul class="styled">
                <li>The story must refer to yourself or to a friend who you know would like to be
                    included in this story and posted on Facebook.
                </li>
                <li>When you submit your story, you authorise us to publish it.</li>
                <li>We will not publish a story that is not fit to be published or uses 3rd party
                    intellectual property. We decide when this condition applies.
                </li>
            </ul>

            <div data-role="fieldcontain" class="form_row checkbox">
                <?php echo $form['accept_tc']->render(array('data-theme' => 'd', 'data-mini' => 'true')); ?>
                <?php echo $form['accept_tc']->renderLabel('I have read and agree to the Terms &amp; Conditions') ?>
            </div>

            <a href="#" data-role="button" id="view_terms">View Terms &amp; Conditions</a>

            <button type="submit">SUBMIT</button>

            <?php echo $form->renderHiddenFields() ?>
        </form>
    </div>
    <?php endif; ?>
</div>

<div class="terms" style="display: none;">
    <?php include_partial('shareacoke/terms/story') ?>
</div>
<script type="text/javascript">
    $('#view_terms').click(function(){
        $('<div>').simpledialog2({
            mode: 'blank',
            headerText: 'Terms & Conditions',
            headerClose: true,
            cleanOnClose: true,
            dialogAllow: true,
            dialogForce: true,
            blankContent : $('.terms').html()
        });
    });
    $('document').ready(function () {
        $('#share_a_coke_location').typeahead({
            source: <?php echo $sf_data->getRaw('town_list') ?>
        });
    });
</script>
