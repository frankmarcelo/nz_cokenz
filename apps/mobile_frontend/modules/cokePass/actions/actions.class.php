<?php

/**
 * cokePass actions.
 *
 * @package    Coke NZ
 * @subpackage cokePass
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class cokePassActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $this->redirectUnless($this->getUser()->isAuthenticated(), '@user_login');
    $this->setLayout('layout');
  }
}
