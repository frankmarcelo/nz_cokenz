<div class="ui-bar ui-bar-b coca-cola-article-title">
  <h3>PAGE NOT FOUND</h3>
</div>

<div data-role="content">
  <div class="coca-cola-note-box">
    Oops! Page Not Found...
  </div>

  <a href="<?php echo url_for('@homepage') ?>" data-theme="b" data-role="button" data-rel="back">Back to previous page</a>
  <a href="<?php echo url_for('@homepage') ?>" data-theme="b" data-role="button">Go to Homepage</a>

</div>
