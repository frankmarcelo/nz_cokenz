<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
      
    <?php include_stylesheets() ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <?php include_partial('global/ga') ?>
  </head>
  <body>
    <div id="fb-root"></div>
    <div class="container">
        <div class="heading"></div>

        <?php echo $sf_content ?>

        <div class="footer">
            <div class="barb"></div>
            <div class="like_count">
                <script>(function(d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s); js.id = id;
                js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=140292872746980";
                fjs.parentNode.insertBefore(js, fjs);
                }(document, 'script', 'facebook-jssdk'));</script>
                
                <div class="fb-like" data-href="https://www.facebook.com/CocaColaNZ" data-send="false" data-width="342" data-show-faces="false"></div>
            </div>
            
            <div class="other_footer_links">
                <?php if ($sf_request->getParameter('action') == 'customise'): ?>
                    <a class="terms_link" href="<?php echo url_for("@terms?p=".$sf_request->getParameter('action')) ?>">Terms and Conditions</a>
                <?php endif; ?>
                <?php if (in_array($sf_request->getParameter('action'), array('sharecan', 'canfinish'))): ?>
                    <a class="terms_link" href="<?php echo url_for("@terms?p=shareacan") ?>">Terms and Conditions</a>
                <?php endif; ?>
                <?php if (in_array($sf_request->getParameter('action'), array('storyshare'))): ?>
                    <a class="terms_link" href="<?php echo url_for("@terms?p=shareastory") ?>">Terms and Conditions</a>
                <?php endif; ?>
                <?php if (in_array($sf_request->getParameter('action'), array('note', 'note_name', 'note_name_type', 'note_complete'))): ?>
                    <a class="terms_link" href="<?php echo url_for("@terms?p=note") ?>">Terms and Conditions</a>
                <?php endif; ?>

                &nbsp;&nbsp;|&nbsp;&nbsp; <a class="terms_link" href="<?php echo url_for("@privacy") ?>">Privacy Policy</a>
            </div>
            
            
            <br class="clear" />
            <div class="legal_line">
                &copy; 2012 The Coca-Cola Company. 'Coca-Cola', 'Coke', 'Open Happiness', the Dynamic Ribbon device and the 'Grip & Go' bottle<br />are registered trade marks of the Coca-Cola Company.
            </div>
            
            <a href="http://www.coke.co.nz" target="_blank"><div class="mini_logo"></div></a>
        </div>
    </div>
    
    <?php include_javascripts() ?>

    <script type="text/javascript"> Cufon.now(); </script>

  </body>
</html>
