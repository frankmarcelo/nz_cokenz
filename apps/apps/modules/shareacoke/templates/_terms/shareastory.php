<h2 class="gotham-b">&lsquo;SHARE YOUR STORY&rsquo;</h2>

<p>The Share your Story Application on the Coca-Cola New Zealand Facebook page allows a user to share their
    stories and images with Coca-Cola Oceania and its wider Facebook audience.</p>

<h3>&nbsp;Steps for sharing a story</h3>
<ol>
    <li>The user clicks the &lsquo;Share Your Story&rsquo; button</li>
    <li>The user fills out the template provided with their name, email address, phone number and then upload a
        picture and enter their story.
    </li>
    <li>Once the user has accepted the terms and conditions and privacy policy they can submit their story</li>
    <li>The story and or picture will then be submitted and checked for compliance with the terms and
        conditions.
    </li>
    <li>Once approved the story will be posted on the Coca-Cola New Zealand Share a COKE Facebook hub.</li>
</ol>


<h3>Conditions</h3>
<ul>
    <li>You must be aged 13 years or older to participate</li>
    <li>You must only refer to people who are know to you, who are Facebook &lsquo;friends&rsquo; and who you
        could reasonably expect would like to be referred to in your published story. When you complete the
        process above, you authorise us to publish your story and imagery on the Coca-Cola New Zealand Facebook
        page.
    </li>
    <li>We will not publish a story that is illegal, obscene, derogatory, threatening, violent, scandalous,
        inflammatory, discriminatory (on any grounds), or would give rise to or encourage conduct which is
        inappropriate or illegal or which is otherwise unfit to be published.&nbsp; We won&rsquo;t publish a
        name which is a third party&rsquo;s intellectual property or which is clearly not yours or your friend&rsquo;s,
        such as a celebrity&rsquo;s name.&nbsp; You must not submit a story that does not comply with this
        condition.&nbsp; We decide when this condition applies. 
    </li>
    <li>We accept no liability at all for any loss (including claims, damages, injury, costs or expenses) which
        is suffered by you or anyone else for any reason in connection with this promotion, to the fullest
        extent permitted by law.
    </li>
    <li>By accepting these terms and conditions you as the user allow us to take the information provided
        including the full story, your name and details and also any pictures and use royalty free in any
        communication medium for our own use without notification or payment of any kind.  You are hereby
        licensing the use of the story and imagery submitted to Coca-Cola Oceania Ltd. 
    </li>
    <li>If you do not comply with these conditions, we may remove the story from the Facebook page, take any
        items from you that you have created in breach of these conditions, and otherwise take action to protect
        our rights in our intellectual property and products.
    </li>
    <li>For further information on this campaign please go to shareacoke.co.nz. We are Coca-Cola Oceania Ltd.
        These conditions also apply to our related party, Coca-Cola Amatil (NZ) Pty Limited.
    </li>
</ul>

<p>&nbsp;</p>