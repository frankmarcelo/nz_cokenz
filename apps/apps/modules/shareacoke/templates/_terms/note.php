<h2 class="gotham-b">&lsquo;SHARE A COKE, SHARE A NOTE&rsquo;</h2>
<p>The Share a COKE Share a Note Application on the Coca-Cola New Zealand Facebook page allows a user to create a personalised note with a Facebook friend&rsquo;s name on it. This virtual note can then be shared with the friend.</p>

<h3>Steps for creating a Share a COKE, Share a Note</h3>
<ol>
  <li> The user must give permission for &lsquo;Coca-Cola&rsquo; to install the application and to access your profile&rsquo;s basic information, write to your profile&rsquo;s Wall, access to post items in your News Feed, access to your profile information and access to your friends&rsquo; information.<br />
    </li>
  <li>The user may then select a Facebook friend to share a note with.<br />
    </li>
  <li> Once a user has selected a friend they then click &lsquo;choose your note&rsquo; and select the themed note you want to send.<br />
    </li>
  <li> Once a user has clicked on the themed note they want they then select options from the drop down boxes to customize their note.<br />
    </li>
  <li> The user then has the option to preview what the note looks like or go back and edit the note.<br />
    </li>
  <li> Once the user is happy with the note they click the &ldquo;share it&rdquo; button and get presented with a pre written Facebook post which they can add their own copy too.<br />
    </li>
  <li> The user then clicks the share link button and it posts to their friends wall.&nbsp;</li>
</ol>

<h3>Conditions:</h3>
<ul>
  <li>You must be aged 13 years or older to participate.</li>
  <li>The note you create must be for a real person, who is known to you, and who you could reasonably expect would like you to send a Note with their name on it including the sentiments expressed in it.&nbsp; When you complete the process above, you authorise us to create a Note with that story and name on it, and a virtual COKE bottle with that name.</li>
  <li>We will not print a name that is illegal, obscene, derogatory, threatening, violent, scandalous, inflammatory, discriminatory (on any grounds), or would give rise to or encourage conduct which is inappropriate or illegal or which is otherwise unfit to be printed.&nbsp; We won&rsquo;t print a name which is a 3rd party&rsquo;s intellectual property or which is clearly not yours or your friend&rsquo;s, such as a celebrity&rsquo;s name.&nbsp; You must not use name that does not comply with this condition.&nbsp; We decide when this condition applies.</li>
  <li>We are giving you the Note for your own personal use, or to virtually share with a friend or loved one for their own personal use.&nbsp; The Note is not provided for any form of commercial use, whatsoever.&nbsp; No Note recipient may sell the Note or the image on the Note.&nbsp; No Note recipient is granted rights in any of the intellectual property on the Note, including the rendering of the name.&nbsp; Just enjoy it privately.</li>
  <li>We accept no liability at all for any loss (including claims, damages, injury, costs or expenses) which is suffered by you or anyone else for any reason in connection with this promotion, to the fullest extent permitted by law. By accepting these terms and conditions you as the user allow us to take the information provided and the Note and use royalty free in any communication medium for our own use without notification or payment of any kind.  You are hereby handing over the ownership of the Note created to Coca-Cola Oceania Ltd.  If you do not comply with these conditions, we may take the Note from you, take any items from you that you have created in breach of these conditions, and otherwise take action to protect our rights in our intellectual property and products.</li>
  <li>For further information on this campaign please go to <u>www.shareacoke.co.nz</u> .  We are Coca-Cola Oceania Ltd. These conditions also apply to our related party, Coca Cola Amatil (NZ) Pty Limited.</li>
</ul>