<h2 class="gotham-b">'Buy a COKE for a Mate'</h2>

<div class="txt">
    <p>The Buy a COKE for a Mate can promotion is accessed via the shareacoke.co.nz website and allows registered user of the website to order COKE cans for a mate.&nbsp; These terms and conditions (&ldquo;Promotion Conditions&rdquo;) include all information on how to participate and purchase the cans displayed on the Promotion Website or in other advertising materials.</p>
    <p>&nbsp;</p>
    <p>Steps for requesting a Buy a COKE for a Mate:</p>
    <p>1)You must visit the www.shareacoke.co.nz website (&ldquo;Promotion Website&rdquo;) and follow the directions to register your purchase.</p>
    <p>2)To register, you must provide an email address, first name, last name and phone number. You must also provide your mate&rsquo;s full name and mobile phone number.</p>
    <p>3)&nbsp;Your order must comply with these Promotional Conditions.</p>
    <p>4)You are required to pay for the cans you order using a credit card or debit card. The cost of your order is stated on the Promotion Website. All costs are stated in New Zealand dollars.</p>
    <p>&nbsp;</p>
    <p>By ordering cans you accept the Promotional Conditions, including, but not limited to the following:&nbsp;</p>
    <p>a.The promotion is open from 00:01am New Zealand Daylight Savings Time on 8 October 2012 until 24:00 pm New Zealand Daylight Savings Time on 22 October 2012.</p>
    <p>b. You must be aged 18 years or older to participate and be a resident of New Zealand.&nbsp;</p>
    <p>c. You may order the COKE cans via the Promotion Website only.&nbsp; We do not accept phone or email orders. Once your order is placed and your credit card has been charged, the order cannot be changed or revised in any manner.</p>
    <p>d. Any cost associated with accessing the Promotion Website is your responsibility and is dependent on the internet service provider you use.&nbsp; We will not accept orders made using automated entry software or any other mechanical or electronic means.</p>
    <p>e. We are providing this promotion to enable you to give to a friend or loved one for their personal use.&nbsp; The can is not provided for any form of commercial use, whatsoever.&nbsp; No can recipient may sell the can or the image on the can.&nbsp; No can recipient is granted rights in any of the intellectual property on the can, including the rendering of the name and the image of the can.&nbsp; Just enjoy it privately.</p>
    <p>f. We recommend you consume the contents of the COKE can on or before the &lsquo;best before&rsquo; date shown on the can.&nbsp; If you want to keep it after the &lsquo;best before&rsquo; date, we recommend you empty the can (preferably by enjoying it!), and wash it out.</p>
    <p>g. Your can order is not transferable, exchangeable, nor redeemable for cash. Neither the can, nor any image or the can, may be sold at any cost for any reason.&nbsp;</p>
    <p>h. We accept no responsibility for any orders which are not received for any reason. We are not responsible for lost, late or misdirected entries. You must check all details entered on the Promotional Website carefully to ensure they are correct. We are not liable for orders which are stolen, damaged or tampered with in any way before they reach you. We are not responsible for any incorrect or inaccurate information, whether caused by the Promotional Website; users; or by any of the equipment or programming used in association with the Promotion.</p>
    <p>i. We accept no liability at all for any loss (including claims, damages, injury, costs or expenses) which is suffered by you or anyone else for any reason in connection with this promotion, to the fullest extent permitted by law.</p>
    <p>j. If you do not comply with these Promotional Conditions, we may take any items from you that you have created in breach of these conditions, and otherwise take action to protect our rights in our intellectual property and products.</p>
    <p>k. We will be collecting your personal information and your mate&rsquo;s personal information to provide the can(s) to you. We will not retain any personal information after the promotion concludes and we have refunded any unclaimed transactions. Check the Promotion Website for our Privacy Policy.</p>
    <p>l. We are Coca-Cola Oceania of The Oasis, Mt Wellington, Auckland, New Zealand. These conditions also apply to our related party, Coca Cola Amatil (NZ) Pty Limited.</p>
    <p>&nbsp;</p>
</div>