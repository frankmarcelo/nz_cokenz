<h2 class="gotham-b">&lsquo;Share a COKE moment&rsquo;</h2>
<p>The Share a COKE moment Application on the Coca-Cola New Zealand Facebook page allows a user to create a personalised mate picture with one of their own photos. This note can then be shared with the friend on Facebook.</p>

<h3>Steps for creating a share a COKE moment picture</h3>
<ol>
  <li>The user must give permission for &lsquo;Coca-Cola&rsquo; to install the application and to access your profile&rsquo;s basic information, write to your profile&rsquo;s Wall, access to post items in your News Feed, access to your profile information and access to your friends&rsquo; information.<br>
  </li>
  <li> The user may then select a photo from their computer files that they want to create the new picture out of.<br>
    </li>
  <li> Once a user has selected the image that they want to use they then scroll through the drop down box and choose one of the options of &lsquo;mates&rsquo; that they want to have on that picture.<br>
    </li>
  <li> The user then has the option to tag their friends that feature in that picture and send a message to them to notify them that the picture has been created.<br>
    </li>
  <li> Once the user shares the final picture it will appear on their friends Facebook page.</li>
</ol>
<h3>Conditions</h3>
<p>You must be aged 13 years or older to participate.</p>
<p>The picture you create must be for a real person, who is known to you, and who you could reasonably expect would like you to send a picture with their name tagged on it including the sentiment expressed in it.&nbsp; When you complete the process above, you authorise us to create a picutre with that &lsquo;mate&rsquo; text and with your friends tagged.</p>
<p>We will not print a picture that is illegal, obscene, derogatory, threatening, violent, scandalous, inflammatory, discriminatory (on any grounds), or would give rise to or encourage conduct which is inappropriate or illegal or which is otherwise unfit to be printed.&nbsp; We decide when this condition applies.</p>
<p>We are giving you the picture tool for your own personal use, or to virtually share with a friend or loved one for their own personal use.&nbsp; The picture is not provided for any form of commercial use, whatsoever.&nbsp; No picture recipient may sell the picture.&nbsp; No picture recipient is granted rights in any of the intellectual property on the picture.  Just enjoy it privately.</p>
<p>We accept no liability at all for any loss (including claims, damages, injury, costs or expenses) which is suffered by you or anyone else for any reason in connection with this promotion, to the fullest extent permitted by law. By accepting these terms and conditions you as the user allow us to take the information provided and the picture and use royalty free in any communication medium for our own use without notification or payment of any kind.  You are hereby handing over the ownership of the picture created to Coca-Cola Oceania Ltd.  If you do not comply with these conditions, we may take the picture from you, take any items from you that you have created in breach of these conditions, and otherwise take action to protect our rights in our intellectual property and products.</p>
For further information on this campaign please go to <u>www.shareacoke.co.nz</u> .  We are Coca-Cola Oceania Ltd. These conditions also apply to our related party, Coca Cola Amatil (NZ) Pty Limited.