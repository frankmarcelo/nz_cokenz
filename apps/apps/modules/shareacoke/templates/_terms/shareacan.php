<h2 class="gotham-b">&lsquo;SHARE A VIRTUAL COKE&rsquo;</h2>

<p>The Share a Virtual COKE Application on the Coca-Cola New Zealand Facebook page allows a user to create a
COKE can with a Facebook friend&rsquo;s name on it. This virtual COKE can may then be shared with that
friend.</p>

<h3>Steps for creating a virtual COKE can</h3>
<ol>
    <li>The user must give permission for &lsquo;Coca-Cola&rsquo; to install the application and to access their
                profile&rsquo;s basic information, write to their profile&rsquo;s Wall, access to post items in their News
                Feed, access to their profile information and access to their friends&rsquo; information.</li>
    <li>The user may then select a friend to share a virtual COKE can with.</li>
    <li>Clicking the &lsquo;Create&rsquo; button will initiate an animation of the creation of the COKE can with
                the friend&rsquo;s name on it.</li>
    <li>The user may share the image of the COKE can with the friend&rsquo;s name on it with that friend.
                Clicking the &lsquo;Share with Name&rsquo; button opens the share functionality in a new window, enabling
                the user to post the image to their friend&rsquo;s wall, as well as adding in an optional message for the
                friend.</li>
</ol>

<h3>Conditions</h3>
<ul>
<li>You must be aged 13 years or older to participate.</li>

<li>The name you use must be the name of a real person, who is known to you, and who you could reasonably expect
would like you to give them a virtual COKE can with their name on it. When you complete the process above,
you authorise us to create a virtual can with that name.</li>

<li>We will not print a name that is illegal, obscene, derogatory, threatening, violent, scandalous,
inflammatory, discriminatory (on any grounds), or would give rise to or encourage conduct which is
inappropriate or illegal or which is otherwise unfit to be printed. We won&rsquo;t print a name which is a
third party&rsquo;s intellectual property or which is clearly not yours or your friend&rsquo;s, such as a
celebrity&rsquo;s name. You must not use a name that does not comply with this condition. We decide when
this condition applies.</li>

<li>We are giving you the virtual COKE can for your own personal use, or to virtually share with a friend or
loved one for their own personal use. The virtual COKE can is not provided for any form of commercial use,
whatsoever. No virtual COKE can recipient may sell the COKE can or the image on the COKE can. No virtual
COKE can recipient is granted rights in any of the intellectual property on the COKE can, including the
rendering of the name. Just enjoy it privately.</li>

<li>We accept no liability at all for any loss (including claims, damages, injury, costs or expenses) which is
suffered by you or anyone else for any reason in connection with this promotion, to the fullest extent
permitted by law. By accepting these terms and conditions you as the user allow us to take the information
provided and the virtual can and use royalty free in any communication medium for our own use without
notification or payment of any kind. You are hereby handing over the ownership of the can created to
Coca-Cola Oceania Ltd. If you do not comply with these conditions, we may take the virtual COKE can from
you, take any items from you that you have created in breach of these conditions, and otherwise take action
to protect our rights in our intellectual property and products.</li>

<li>For further information on this campaign please go to shareacoke.co.nz. We are Coca-Cola Oceania Ltd of New
Zealand. These conditions also apply to our related party, Coca-Cola Amatil (NZ) Pty Limited.</li>
</ul>
<p>&nbsp;</p>