<?php include_partial('nav', array('pagename'=>$pagename)); ?>
<br class="clear"/>
<div class="buy_for_mate_page">

    <div class="page intro buy_for_mate" style="">
        <div class="subintro" style="padding-right: 300px; height: 250px; background: url(/images/app/shareacoke/cokecan-mate.jpg) no-repeat 400px 0;">
            <div class="header">
                <div class="instruction gotham" style="width: 340px">Buy a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>
                <div class="instruction gotham">for a Mate!</div>
            </div>
            <div class="content_txt">
                <div class="txt">
                    Want to share a <strong>Coke</strong> with a mate, but can't get together? Buy your mate a 440ml. <strong>Coke</strong> can for pickup at a selected vending machine. Get started by following the prompts below.

                </div>
            </div>




        </div>

    </div>

    <div class="overview_guide">
    <div class="title_1 gotham" style="width: 340px">How it works...</div>
        <div class="instructions">
            <div class="steps">
                <table>
                    <tr>
                        <td><img class="step" src="<?php echo image_path("app/shareacoke/vending/step_1.jpg") ?>" /></td>
                        <td><div class="txt">Select a friend from Facebook</div></td>
                    </tr>

                    <tr>
                        <td><img class="step" src="<?php echo image_path("app/shareacoke/vending/step_2.jpg") ?>" /></td>
                        <td><div class="txt">Enter your mate's details including mobile number</div></td>
                    </tr>


                    <tr>
                        <td><img class="step" src="<?php echo image_path("app/shareacoke/vending/step_3.jpg") ?>" /></td>
                        <td><div class="txt">Choose your vending machine location for pick-up</div></td>
                    </tr>


                    <tr>
                        <td><img class="step" src="<?php echo image_path("app/shareacoke/vending/step_4.jpg") ?>" /></td>
                        <td><div class="txt">Use a credit or debit card to pay $3 for the <strong>Coke</strong> can</div></td>
                    </tr>

                </table>

            </div>
            <div class="map">
                <img id="location_map" width="150px" src="<?php echo image_path("app/shareacoke/vending/nz-map.png") ?>" usemap="#location_map_map"/>


                <map name="location_map_map">
                    <area class="map_area" id="palmy" shape="rect" coords="104,81,115,96" alt="Albany" >
                    <area class="map_area" id="auckland" shape="rect" coords="88,12,99,30" alt="Auckland" >
                    <area class="map_area" id="hamilton" shape="rect" coords="90,30,107,49" alt="Hamilton" >
                    <area class="map_area" id="wellington" shape="rect" coords="93,96,105,113" alt="Wellington" >
                    <area class="map_area" id="chch" shape="rect" coords="65,138,79,156" alt="Christchurch" >
                    <area class="map_area" id="dunedin" shape="rect" coords="37,173,53,189" alt="Dunedin" >
                </map>
                <div id="location_hover">Location</div>
            </div>
        </div>

    </div>
    <div id="debug_div"></div>
    <div id="debug_div2" style="color: red;"></div>

    <script type="text/javascript">
        $(".map_area").mousemove(function(e){
           // $('#debug_div').html($(this).attr('alt'));
            $("#location_hover").html($(this).attr('alt'));

            topVal = (parseInt(e.pageY) - 10);

            left = (parseInt(e.pageX) + 10);
            $("#location_hover").css("display", "inline");
            $("#location_hover").css("top",  topVal + "px");
            $("#location_hover").css("left", left + "px");
            $("#location_hover").css("display", "inline");


        });



</script>
    <div class="footer_btn">
        <a  href="<?php echo url_for('@buyforamate_select') ?>"><img src="<?php echo image_path("app/shareacoke/vending/select_a_friend.png") ?>"/>
        </a>


    </div>

</div>
<?php include_partial('fb_default', array('app_id' => $app_id)); ?>
