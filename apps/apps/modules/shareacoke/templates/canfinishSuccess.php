<?php include_partial('nav', array('pagename'=>$pagename)); ?>


<br class="clear" />

<div class="page sharecan_page">
    
   <div class="sharecan_instructions" style="background: url(/images/app/shareacoke/cokecan-mate.jpg) no-repeat top right; height:227px;">
        <div class="instruction gotham" style="width: 340px">Create a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>
        <div class="instruction gotham">can especially</div>
        <div class="instruction gotham">for a friend</div>

    </div>
    
    
    <br class="clear" />
    
    <?php if(isset($error) && $error): ?>
        <div class="final_name_error_container gotham">
            <?php if ($error == 1): ?>
                <!-- Profanity List -->
            Sorry, we are unable to process that name.<br /><br />Please <a class="gotham-b" href="<?php echo url_for("@sharecan"); ?>">select another friend</a> or <br />view  <a class="gotham-b" href="<?php echo url_for("@terms?p=shareacan"); ?>">Terms &amp; Conditions</a> for more info.
                
            <?php elseif ($error == 2): ?>
                <!-- Invalid Symbols -->
            Sorry, we are unable to process that name.<br /><br />Please <a class="gotham-b" href="<?php echo url_for("@sharecan"); ?>">select another friend</a> or <br>view <a class="gotham-b" href="<?php echo url_for("@terms?p=shareacan"); ?>">Terms &amp; Conditions</a> for more info.
            <?php endif; ?>
        </div>
    <?php else: ?>
        <div class="final_name_container">
            <img src="<?php echo image_path('app/shareacoke/coke-mini-logo.jpg') ?>" class="logo_handle" />

            <div class="empty_can">
                <div class="empty_can_container">
                    <span class="you"><?php echo $cokename ?></span>
                </div>
            </div>
        </div>
    <?php endif; ?>
    
    <div class="share_success gotham black">
        Your <strong>'Coke'</strong> can has been shared! Click 'Go Back' to select another friend!
    </div>
    
    <a href="<?php echo url_for("@sharecan"); ?>"><div class="control_back"></div></a>

    <?php if(isset($error) && $error): ?>
    
    <?php else: ?>
        <div class="control_share">
            <span class="gotham-b"><?php echo strtoupper($cokename) ?></span>
        </div>
    <?php endif; ?>
    
</div>

<br class="clear" />

<?php include_partial('fb_canfinish', array('app_id' => $app_id, 'cokeid' => $cokeid, 'cokename' => $cokename, 'tab_url' => $tab_url)); ?>