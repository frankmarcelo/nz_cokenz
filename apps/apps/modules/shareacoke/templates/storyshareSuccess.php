<?php use_javascript('apps/shareacoke/bootstrap.typeahead.min.js') ?>
<?php include_partial('nav', array('pagename' => $pagename)); ?>


<br class="clear"/>

<div class="story_page">

    <div class="container" style="background: url(/images/app/shareacoke/cokecan-mate.jpg) no-repeat top right">
        <div class="story_intro">
            <div class="introtext gotham" style="width: 425px">Shared a <img class="floating_coke_label"
                                                                             src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>"/>?
            </div>
            <div class="introtext gotham">Tell us about it!</div>


        </div>

        <br class="clear"/>

        <div class="form_content" <?php echo !isset($show_success) ? 'style="visibility: hidden;"':''?>>

            <div class="left">
                <?php /*                 <a href="http://www.coke.co.nz/" target="_blank"><img src="<?php echo image_path('app/shareacoke/banner-win-something.jpg'); ?>" /></a> */ ?>
            </div>

            <div class="form right">

                <?php if (isset($show_success) and $show_success): ?>
                <div class="gotham large">
                    Thank you!<br/>
                    Your story has been sent.
                </div>
                <?php else: ?>
                <form method="post" enctype="multipart/form-data" action="<?php echo url_for("@sharestory") ?>"
                      onsubmit="return checkFields(); return false;">

                    <?php echo $shareform['_csrf_token']->render(); ?>

                    <table width="100%" id="form_container">
                        <tr>
                            <td><?php echo $shareform['name']->renderLabel('Your Name'); ?></td>
                            <td width="340" class="inner_data">
                                <?php echo $shareform['name']; ?>
                                <?php echo $shareform['name']->renderError(); ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?php echo $shareform['email']->renderLabel('Your Email'); ?></td>
                            <td width="340" class="inner_data">
                                <?php echo $shareform['email']; ?>
                                <?php echo $shareform['email']->renderError(); ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?php echo $shareform['phone']->renderLabel('Your Phone'); ?></td>
                            <td width="340" class="inner_data">
                                <?php echo $shareform['phone']; ?>
                                <?php echo $shareform['phone']->renderError(); ?>
                            </td>
                        </tr>
                        <tr>
                            <td><?php echo $shareform['location']->renderLabel('Story location'); ?></td>
                            <td width="340" class="inner_data">
                                <?php echo $shareform['location']->render(array('autocomplete' => 'off')); ?>
                                <?php echo $shareform['location']->renderError(); ?>
                            </td>
                        </tr>

                        <tr>
                            <td><?php echo $shareform['pic']->renderLabel('Upload a Pic'); ?></td>
                            <td width="340">
                                <?php echo $shareform['pic']; ?>
                                <?php echo $shareform['pic']->renderError(); ?>
                            </td>
                        </tr>
                        <tr valign="top">
                            <td valign="top"
                                style="vertical-align: top"><?php echo $shareform['story']->renderLabel('Your Story'); ?></td>
                            <td width="340" class="inner_data">
                                <?php echo $shareform['story']; ?><br/>

                                <div class="mini_note"><span id="total_count">0</span>/500 characters</div>
                                <?php echo $shareform['story']->renderError(); ?>
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td width="340" class="inner">
                                <ul class="styled">
                                    <li>The story must refer to yourself or to a friend who you know would like to be
                                        included in this story and posted on Facebook.
                                    </li>
                                    <li>When you submit your story, you authorise us to publish it.</li>
                                    <li>We will not publish a story that is not fit to be published or uses 3rd party
                                        intellectual property. We decide when this condition applies.
                                    </li>
                                </ul>

                                <br/>

                                <div id="container_terms_agree">
                                    <?php echo $shareform['accept_tc']->render(); ?>
                                    <?php echo $shareform['accept_tc']->renderLabel(sprintf('I have read and agree to the %s', link_to1('Terms &amp; Conditions', '@terms?p=shareastory', array('id' => 'terms_link', 'data-fancybox-type' => 'ajax')))) ?>
                                </div>
                            </td>
                        </tr>

                        <?php if (isset($completion_server_error) and $completion_server_error): ?>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="error">
                                <br/><br/>
                                Sorry, please complete all the form by filling all the fields.
                            </td>
                        </tr>
                        <?php endif; ?>

                        <tr id="form_error_incomplete" style="display: none">
                            <td>&nbsp;</td>
                            <td class="error">
                                <br/><br/>
                                Sorry, there are some errors with your form, highlighted in red above. Please complete
                                all required fields and agree to the terms and conditions before submitting.
                            </td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td width="340">
                                <br/><br/>
                                <input type="image" src="<?php echo image_path('app/shareacoke/button-submit-small.jpg') ?>">
                            </td>
                        </tr>
                    </table>

                </form>
                <?php endif; ?>
            </div>
        </div>

        <br class="clear"/>
    </div>

</div>

<br class="clear"/>

<script type="text/javascript">
    $('document').ready(function () {
        $('#sac_story_location').typeahead({
            source: <?php echo $sf_data->getRaw('town_list') ?>
        });
    });

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '<?php echo $app_id ?>', // App ID
            channelUrl : '//<?php echo $_SERVER['SERVER_NAME'] ?>/channel.html', // Channel File
            status     : true, // check login status
            cookie     : true, // enable cookies to allow the server to access the session
            xfbml      : true  // parse XFBML
        });

        // Auto resize yuck
        FB.Canvas.setAutoGrow(91);
        FB.Canvas.scrollTo(0,0);

        FB.login(function(response) {
            if (response.status === 'connected') {
                FB.api('/me', function(api_response) {
                    $('#sac_story_name').val(api_response.name);
                    $('#sac_story_email').val(api_response.email);
                });
                $('.form_content').css('visibility', 'visible');
            }
        }, { scope: '<?php echo sfConfig::get('app_facebook_permissions') ?>'});
    };

    // Load the SDK Asynchronously
    (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

</script>