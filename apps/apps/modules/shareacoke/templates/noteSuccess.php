<?php include_partial('nav', array('pagename'=> 'shareanote')); ?>
<?php use_javascript('jquery.roundabout.min.js') ?>
<?php use_javascript('jquery.roundabout-shapes.min.js') ?>
<?php use_javascript('jquery.selectBox.min.js') ?>

<br class="clear" />

<div class="page note_page">
    
    <div class="sharecan_instructions" >
        <div class="instruction gotham" style="width: 317px">Share a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>
        <div class="instruction gotham">Share a Note</div>

        <?php if ($is_preview): ?>
        <?php elseif (!empty($selected_note)): ?>
        <div class="subintro">
            Scroll through the dropdown boxes below to<br>
            customise your note.
        </div>
        <?php elseif (empty($selected_name) and empty($selected_id)): ?>
        <div class="subintro">
            Choose a friend that you want to share a  <strong>Coke</strong><br>
            and share a note with.
        </div>
        <?php elseif (!empty($selected_name) and !empty($selected_id)): ?>
        <div class="subintro">
            Choose the theme of your note.
        </div>
        <?php endif; ?>

    </div>

    <?php if (!empty($selected_note)): ?>
    <form method="post" action="<?php echo url_for('note_complete', array('name' => $selected_name, 'id' => $selected_id, 'type' => $selected_note)) ?>">
    <div class="note_create" style="margin-bottom:110px;">
        <div class="the_note" id="note_<?php echo $selected_note ?>">
            <?php
                $font_size = 37;
                if (strlen($selected_name) > 6) {
                    $diff = strlen($selected_name) - 6;

                    switch (true) {
                        case $diff <= 2:
                            $font_size = 35;
                            break;
                        case $diff <= 4:
                            $font_size = 28;
                            break;
                        case $diff <= 6:
                            $font_size = 24;
                            break;
                        case $diff <= 8:
                            $font_size = 22;
                            break;
                        default:
                            $font_size = 20;
                            break;
                    }
                }

                echo strtr(vsprintf($sf_data->getRaw('pattern'), $dropdowns->getRawValue()), array(
                    '$$COKE$$' => image_tag('app/shareacoke/label-cokelogo.jpg'),
                    '$$NAME$$' => content_tag('div', $selected_name, array('class' => 'note_name', 'style' => 'font-size:'.$font_size.'px; '))
                ));
            ?>
        </div>
    </div>

    <div class="share_success gotham black" style="padding-bottom:130px;">
        <p><span style="font-size: 42px;">Thank you!</span><br> Your note has been shared!</p>
        <p><a href="<?php echo url_for('@note') ?>">
            <img src="/images/app/shareacoke/button-select-another-note.png" alt="Select another friend to share a note">
        </a></p>
    </div>

    <?php if ($is_preview): ?>
        <a href="<?php echo url_for('note_name_type', array('name' => $selected_name, 'id' => $selected_id, 'type' => $selected_note)); ?>"><div class="control_back"></div></a>
        <div class="control_share">
            <span class="gotham-b"><?php echo strtoupper($selected_name) ?></span>
        </div>


    <?php else: ?>
        <a href="<?php echo url_for('note_name', array('name' => $selected_name, 'id' => $selected_id)) ?>"><div class="control_back"></div></a>
        <button type="submit" class="preview_button"></button>
        <input type="hidden" name="preview" value="true">
    <?php endif; ?>

    </form>

    <?php elseif (!empty($selected_name) and !empty($selected_id) and empty($selected_note)): ?>
    <div class="note_carousel">
        <div class="arrow prev"></div>
        <ul>
            <li>
                <a href="<?php echo url_for('note_name_type', array('name' => $selected_name, 'id' => $selected_id, 'type' => 'love')) ?>">
                <div class="share_a"><img src="/images/app/shareacoke/notes/love-note.png" alt="Share a love note"></div>
                </a>
            </li>
            <li>
                <a href="<?php echo url_for('note_name_type', array('name' => $selected_name, 'id' => $selected_id, 'type' => 'best-friend')) ?>">
                <div class="share_a"><img src="/images/app/shareacoke/notes/best-friend.png" alt="Share a best friend note"></div>
                </a>
            </li>
            <li>
                <a href="<?php echo url_for('note_name_type', array('name' => $selected_name, 'id' => $selected_id, 'type' => 'miss-you')) ?>">
                <div class="share_a"><img src="/images/app/shareacoke/notes/miss-you.png" alt="Share a miss you note"></div>
                </a>
            </li>
            <li>
                <a href="<?php echo url_for('note_name_type', array('name' => $selected_name, 'id' => $selected_id, 'type' => 'siblings')) ?>">
                <div class="share_a"><img src="/images/app/shareacoke/notes/sibilings.png" alt="Share a sibilings note"></div>
                </a>
            </li>
            <li>
                <a href="<?php echo url_for('note_name_type', array('name' => $selected_name, 'id' => $selected_id, 'type' => 'birthday')) ?>">
                <div class="share_a"><img src="/images/app/shareacoke/notes/birthday.png" alt="Share a birthday note"></div>
                </a>
            </li>

        </ul>
        <div class="arrow next"></div>
    </div>

    <?php elseif (empty($selected_name)): ?>
    <a href="#" id="link_to_connect"><div class="connect_facebook"></div></a>

    <div class="friends_container">
        <div class="create_coke_label"></div>

        <div class="searchfriend">
            <input name="search_friend" id="search_friend" />
        </div>

        <br class="clear" />

        <ul class="friendslist">

        </ul>

    </div>
    <?php endif; ?>
    
    <a id="nextpage" class="tTip"><div class="create_button"></div></a>
    
</div>

<br class="clear" />


<script>

    $(document).ready(function() {
        $('.note_carousel ul').roundabout({
            minOpacity: 1,
            minScale: 0.4,
            btnNext: '.next',
            btnPrev: '.prev',
            shape: 'square'
        });

        $('select').selectBox().change(function(){
            Cufon.refresh();
        });
    });
        var selectedname = "";

        window.fbAsyncInit = function() {
            FB.init({
                appId      : '<?php echo $app_id ?>', // App ID
                channelUrl : '//<?php echo $_SERVER['SERVER_NAME'] ?>/channel.html', // Channel File
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                xfbml      : false  // parse XFBML
            });

            // Auto resize yuck
            FB.Canvas.setAutoGrow(91);
            FB.Canvas.scrollTo(0,0);
            <?php if (empty($selected_name)): ?>
            FB.login(function(response) {
                if (response.status === 'connected') {

                    // Access token for API Calls
                    //var accessToken = response.authResponse.accessToken;

                    $(".friends_container").css("visibility", "visible");
                    $(".note_page").css("padding-bottom", "140px");

                    $(".create_button").css("display", "block");

                    FB.api('/me/friends', function(response) {

                        $.each(response.data, function(i, val){
                            $(".friendslist").append('<li rel="'+val.id+'" title="' + val.name + '" class="name highlight_friend"><img src="https://graph.facebook.com/' + val.id + '/picture">' + val.name + '</li>');
                        });

                        // scroller's autoReinitialise flickers too much, so had to use manual call mixed with typewatch to avoid delays
                        $('ul.friendslist').jScrollPane({
                            showArrows: 'true'
                        });

                        // Search Filter
                        var typewatch = (function(){
                            var timer = 0;
                            return function(callback, ms){
                                clearTimeout (timer);
                                timer = setTimeout(callback, ms);
                            }
                        })();

                        $("#search_friend").keyup(function(event) {
                            var input_value = $("#search_friend").val();

                            $(".friendslist li").css('display', 'none')

                            $(".friendslist li:regex(title, .*" + input_value + ".*)").css('display', 'block');

                            typewatch(function () {

                                // scroller's autoReinitialise flickers too much, so had to use manual call mixed with typewatch to avoid delays
                                $('ul.friendslist').jScrollPane({
                                    showArrows: 'true'
                                });
                            }, 500);
                        });

                        // apply selection lock
                        $(".highlight_friend").click(function() {

                            selectedname = $(this).attr("title");
                            selectedid = $(this).attr("rel");

                            $('.friendslist .name').each(function(index) {
                                $(this).removeClass("active");
                            });

                            /* Add active class on click */
                            $(this).addClass("active");

                            var selected_first_name = selectedname.split(' ');

                            $("#nextpage").attr("href",'<?php echo url_for("@note"); ?>/' + selected_first_name[0] + '/' + selectedid);

                        });
                    });

                } else if (response.status === 'not_authorized') {

                    $("a#link_to_connect").click(function() {
                        FB.ui({
                            method: 'oauth',
                            client_id: '<?php echo $app_id ?>',
                            redirect_uri: '<?php echo $tab_url ?>',
                            scope: '<?php echo sfConfig::get('app_facebook_permissions') ?>'
                        }, function(data) {
                            window.location = '<?php echo url_for("@note"); ?>';
                        });
                        return false;
                    });
                    $(".connect_facebook").css("display", "inline");

                    $(".note_page").css("padding-bottom", "0");
                    $(".note_page").css("height", "440px");
                }
            }, { scope: '<?php echo sfConfig::get('app_facebook_permissions') ?>'});

            <?php elseif ($is_preview): ?>

            $(".control_share").click(function() {
                FB.getLoginStatus(function(response) {

                    if (response.status == "connected") {

                        FB.ui({
                            method: 'feed',
                            name: "This 'Coke' is unlike any other 'Coke' - It's got a special note with it.",
                            link: '<?php echo $share_link ?>',
                            picture: 'http://<?php echo $_SERVER['SERVER_NAME'] ?>/images/app/shareacoke/note-fb-share.jpg',
                            description: '<?php echo $message ?> <?php echo $selected_name ?>',
                            to: '<?php echo $selected_id ?>'
                        }, feed_callback );

                    } else {
                        FB.ui({
                            method: 'oauth',
                            client_id: '<?php echo $app_id ?>',
                            redirect_uri: '<?php echo $tab_url ?>',
                            scope: '<?php echo sfConfig::get('app_facebook_permissions') ?>'
                        });
                    }
                });
            });


            function feed_callback() {
                _gaq.push(['_trackSocial', 'Facebook', 'Share', 'Share a note']);
                $(".control_share").hide();
                $(".note_create").hide();
                $(".control_back").hide();
                $(".share_success").show();
                window.top;
                // Reposition Back Button
                $(".control_back").css("left", "340px");
                $(".control_back").css("bottom", "100px");
            }

        <?php endif; ?>
        };

        // Load the SDK Asynchronously
        (function(d){
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));

</script>