<?php include_partial('nav', array('pagename'=>$pagename)); ?>

<?php use_javascript('jquery.selectBox.min.js') ?>
<br class="clear"/>
<div class="share_moment share_moment_tag">



    <div class="page intro moment_intro" style="">
        <div class="subintro" style="padding-right: 300px; height: 250px; background: url(/images/app/shareacoke/cokecan-mate.jpg) no-repeat 400px 0;">
            <div class="header">
                <div class="instruction gotham" style="width: 340px">Share a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>
                <div class="instruction gotham">Moment with a Mate</div>
            </div>
            <div class="content_txt">
                <div class="txt">
                    <p>
                        Scroll through the options below and choose the type of mate that goes with your picture.

                    </p>

                </div>
            </div>




        </div>

    </div>
    <div class="img_canvas_wrapper white">
        <div class="color_picker"><div class="black" onclick="javascript: changeColor('black');"></div>
            <div class="white" onclick="javascript: changeColor('white');"></div></div>

        <img id="image_to_tag" class="selected_img" src="<?php echo image_path("app/shareacoke/moment/placeholder.jpg") ?>" />
        <div class="text_wrapper">
            <div class="selecting_txt">
            <select class="selectBox" id="mate_type_select">
                <option value='' disabled selected style='display:none;'></option>
                <option value='FLAT'>FLAT</option>
                <option value='UNI'>UNI</option>
                <option value='WORK' >WORK</option>

                <option value='TEAM' >TEAM</option>

                <option value='OLD' >OLD</option>


            </select> MATES
            </div>
            <div class="tagging_txt">
            </div>
        </div>
        <img class="mate_can_img" src="<?php echo image_path("app/shareacoke/moment/mate_can.png") ?>" />
        <?php
        /*var_dump( $data);
        var_dump( $printArr);
        var_dump( $data2);
        var_dump( $datatag);
          */


        ?>

    </div>
    <div id="tag_holder">

        <div class="friends_container">
            <div class="create_coke_label"></div>

            <div class="searchfriend">
                <input name="search_friend" id="search_friend" />
            </div>

            <br class="clear" />

            <div class="friendslist">

            </div>

        </div>

    </div>



        <div class="footer_btn" method="post">

            <div id="share_btn_footer">
                <a href="#" id="back_link"><img src="<?php echo image_path("app/shareacoke/moment/back.png") ?>" />
                </a>
                <a href="#" id="share_photo_link"><img src="<?php echo image_path("app/shareacoke/moment/share.png") ?>" />
                </a></div>

            <div id="tag_btn_footer"><a href="#" id="tag_photo_link"><img src="<?php echo image_path("app/shareacoke/moment/tag_photo.png") ?>" />
            </a></div>



        </div>
    <script type="text/javascript">
        var activeColor = "white";
        var txtToDisplay = "MATE";
        function changeColor(color){
            $(".img_canvas_wrapper").removeClass("white");
            $(".img_canvas_wrapper").removeClass("black");

            $(".img_canvas_wrapper").addClass(color);
            activeColor = color;

        }
        function shareImg(){
            console.log("List Of Tags: ", tagList);//list of tags. relativeX/Y is the one you actually need.
            console.log("Text on Image: ", txtToDisplay);//Tect that needs to be put on the image.
            alert("shareImg method has all the data you need to submit to the backend, check console");
        }

        var activeTag = null;
        var tagList = new Array();

        function TagObject (id, x, y) {
            this.x = (x!=null && x!= '') ? parseInt(x) : 0;
            this.y = (y!=null && y!= '') ? parseInt(y) : 0;

            this.relativeY = y - parseInt($("#image_to_tag").position().top);
            if(this.relativeY < 0){ this.relativeY = 0;}
            this.relativeX = x - 105;
            if(this.relativeY < 0){ this.relativeY = 0;}


            this.name = null;
            this.fbID = null;
            this.id = id;


            this.submit = function() {

                tagList[tagList.length] = this;
                $(".friends_container").css("display", "none");
                $("#" + this.id+ " div").html("" + this.name);
                console.log(tagList);
            };
        }





        $(document).ready(function() {
           $('select').selectBox().change(function(){

                //Cufon.refresh();
            });
        });


        var pageMode = "select";
        $("#tag_photo_link").click(function() {
            if(pageMode == "select"){
                taggingTxt = $(".share_moment_tag .tagging_txt");
                selectTxt = $(".share_moment_tag .selecting_txt");
                txtToDisplay = "MATE";
                if($("#mate_type_select").val() != null && $("#mate_type_select").val() != ''){
                    txtToDisplay = $("#mate_type_select").val() + " MATES";
                }
                $(taggingTxt).html(txtToDisplay);
                $("#tag_btn_footer").css("display", "none");
                $("#share_btn_footer").css("display", "block");


                $(taggingTxt).css("display", "block");
                $(selectTxt).css("display", "none");
                pageMode = "tag";
            }

           return false;

        });
        tagNumber = 0;
        $(".img_canvas_wrapper").click(function(e){
            if(pageMode == "tag"){
            imageLeftoffset = 105; //canvas == 810. Image (centered) = 600. left/right offset == (810-600)/2
            clickX = e.pageX;
            clickY = e.pageY;

            if(clickY >= $("#image_to_tag").position().top && clickY <= $("#image_to_tag").position().top+600 && clickX >= 105 && clickX <= 600 + 105){
                if(clickX < imageLeftoffset + 50){
                    clickX = imageLeftoffset + 50;
                }
                if(clickX > imageLeftoffset + 600 - 50){
                    clickX = imageLeftoffset + 600 - 50;
                }

                if(clickY < $("#image_to_tag").position().top + 50){
                    clickY = $("#image_to_tag").position().top + 50;
                }
                if(clickY > $("#image_to_tag").position().top + 600 - 50){
                    clickY = $("#image_to_tag").position().top + 600 - 50;
                }

                //alert("correct height/width");
                $("#tag_holder");
                var tagDiv = $( document.createElement('div') );
                tagNumber ++;
                $(tagDiv).attr("id", "tag_" + tagNumber );
                $(tagDiv).attr("class", "tag_pos_div");

                $(tagDiv).css("top", (clickY - 50) + "px");
                $(tagDiv).css("left", (clickX - 50) + "px");
                $(tagDiv).html("<div></div>");

                if(activeTag!=null){
                    console.log(activeTag);
                    console.log( $("#"+ activeTag.id));
                    $("#"+ activeTag.id).remove();
                }
                activeTag = new TagObject("tag_" + tagNumber, clickX, clickY);



                $(".friends_container").css("left", (clickX - 50) + "px");
                $(".friends_container").css("top", (clickY + 53) + "px");
                $(".friends_container").css("display", "block");

                $(".img_canvas_wrapper").append(tagDiv);
            }

            //console.log(e);//pageY: 544 == correct absolute from top  clientY gives current Y as you see on screen, not taking scrolling into account.
            //console.log($("#image_to_tag").position());//.top is top of the img, left is 0 because theres margins... grr...
            //Cufon.refresh();
            }

           });
        $("#share_photo_link").click(function() {
            shareImg();
            return false;
        });
        $("#back_link").click(function() {
            if(pageMode == "tag"){ //back button functionaility;
                taggingTxt = $(".share_moment_tag .tagging_txt");
                selectTxt = $(".share_moment_tag .selecting_txt");
                $("#tag_btn_footer").css("display", "block");
                $("#share_btn_footer").css("display", "none");

                $(taggingTxt).css("display", "none");
                $(selectTxt).css("display", "");
                pageMode = "select";
                tagList_ = $(".tag_pos_div");
                for(i=0; i< tagList_.length; i++){
                    $(tagList_[i]).remove();

                }
                tagList = new Array();
                activeTag = null;


            }
            return false;
        });





    </script>


</div>


<?php include_partial('fb_momenttag', array('app_id' => $app_id, 'tab_url' => $tab_url)); ?>