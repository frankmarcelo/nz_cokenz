<?php include_partial('nav', array('pagename' => $pagename)); ?>
<?php
    if (!@$partial) {
        $partial = 'shareacoke';
    }
?>

<br class="clear" xmlns="http://www.w3.org/1999/html"/>

<div class="page sharecan_page" style="padding-bottom: 40px; background: none; height: auto;">

    <div class="sharecan_instructions">
        <div class="instruction gotham">Terms and Conditions</div>
    </div>

    <div class="misc_content">

    <?php include_partial('shareacoke/terms/'.$partial) ?>

    </div>

</div>

<br class="clear"/>

<?php include_partial('fb_canfinish', array('app_id' => $app_id)); ?>