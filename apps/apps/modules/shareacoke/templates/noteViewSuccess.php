<!DOCTYPE html>
<html lang="en" style="width:auto;">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <title>A note for <?php echo $selected_name ?></title>

    <link rel="stylesheet" type="text/css" href="/css/apps/shareacoke/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/apps/shareacoke/style.css?1234">
    <link rel="stylesheet" type="text/css" href="/plugins/fancybox/jquery.fancybox.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <?php include_partial('global/ga') ?>
  </head>
  <body class="note_view">
    <div id="fb-root"></div>
    <div class="container">

        <div class="note_create">
            <div class="the_note" id="note_<?php echo $selected_note ?>">
                <?php
                    $font_size = 37;
                    if (strlen($selected_name) > 6) {
                        $diff = strlen($selected_name) - 6;

                        switch (true) {
                            case $diff <= 2:
                                $font_size = 35;
                                break;
                            case $diff <= 4:
                                $font_size = 28;
                                break;
                            case $diff <= 6:
                                $font_size = 23;
                                break;
                            case $diff <= 8:
                                $font_size = 20;
                                break;
                            default:
                                $font_size = 20;
                                break;
                        }
                    }

                    echo strtr(vsprintf($sf_data->getRaw('pattern'), $selections->getRawValue()), array(
                        '$$COKE$$' => image_tag('app/shareacoke/label-cokelogo.jpg'),
                        '$$NAME$$' => content_tag('div', $selected_name, array('class' => 'note_name', 'style' => 'font-size:'.$font_size.'px; '))
                    ));
                ?>
            </div>
        </div>




        <div class="footer">
            <div class="barb"></div>

            <div class="other_footer_links">
                <a href="<?php echo url_for("@terms?p=note") ?>">Terms and Conditions</a>
            </div>

            <div class="legal_line">
                &copy; 2012 The Coca-Cola Company. 'Coca-Cola', 'Coke', 'Open Happiness', the Dynamic Ribbon device and the 'Grip & Go' bottle<br />are registered trade marks of the Coca-Cola Company.
            </div>

            <a href="http://www.coke.co.nz" target="_blank"><div class="mini_logo"></div></a>
        </div>
    </div>

    <script type="text/javascript" src="/plugins/fancybox/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/js/apps/shareacoke/cufon1.09.js"></script>
    <script type="text/javascript" src="/js/apps/shareacoke/gotham.cufon.js"></script>
    <script type="text/javascript">
        $('.other_footer_links a').fancybox({
                type:'ajax',
                afterShow: function(){
                    Cufon.refresh();
                }
            });
        /* Cufon Font Replacements*/
        Cufon('.gotham', {
            fontFamily: 'Gotham-Book'
        });

        Cufon('.gotham-b', {
            fontFamily: 'Gotham-Bold'
        });

        Cufon.now();
    </script>

  </body>
</html>
