<?php include_partial('nav', array('pagename'=>$pagename)); ?>


<br class="clear" />

<div class="nominate_name_page">

    <div class="pageintro">
        <div class="introtext gotham">Nominate the name of someone</div>
        <div class="introtext gotham">you'd like to share a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /> with</div>

        <?php if (!$complete): ?>
        <div class="subintro">
            Simply enter your name, your email address, and the<br>
            first name of the person you would like to share a <br>
            <strong>Coke</strong> with.
        </div>
        <?php endif; ?>
    </div>





    <br class="clear" />
    <?php if ($complete): ?>
    <div class="thanks">
        <h3 class="gotham">Thank you!</h3>
        <p class="gotham">Your nomination has been sent.</p>
    </div>


    <?php else: ?>
    <div class="form_content" style="visibility: hidden;">

        <div class="form">
            <form method="post" action="<?php echo url_for('@nominate_name') ?>">

                <div class="inner_data">
                    <?php echo $form['name']->renderRow(array(), 'Your Name') ?>
                </div>
                <div class="inner_data">
                    <?php echo $form['email']->renderRow(array(), 'Your Email') ?>
                </div>
                <div class="inner_data">
                    <?php echo $form['nominate_name']->renderRow(array(), 'Name you&rsquo;d like to see on a <strong>Coke</strong> bottle') ?>
                </div>
                <div class="inner_data textarea">
                    <?php echo $form['nominate_reason']->renderRow(array(), 'Tell us why you&rsquo;d like to share a <strong>Coke</strong> with that person.<br> We&rsquo;re looking for the most creative reasons') ?>
                    <br><span class="help">Maximum 50 words</span>
                </div>

                <div class="form_copy">
                    <ul>
                    <li>Go to shareacoke.co.nz between 12:00 noon on 14th September 2012 and midnight on 23rd September 2012 to enter the name you would like to nominate.</li>
                    <li>Enter your name, email, and the name you would like to nominate.</li>
                    <li>Names must be a maximum of 12 characters, and cannot feature spaces, numbers, or special characters.</li>
                    <li> A final shortlist of 100 names will be displayed on shareacoke.co.nz. You may vote by clicking “Vote” once on a name or range of names, up to a maximum of 10 names, between 09:00am on 8th October 2012 and the midnight of 12th October 2012.</li>
                    <li>The 50 names with the highest number of Votes at midnight on 12th October 2012 will be printed on COCA-COLA bottles and will be available for purchase around 17th November 2012.</li>
                    <li>We will not print names that have already been printed in the first batch of 150 names.</li>
                    </ul>
                </div>

                <div class="inner_data checkbox">
                    <?php echo $form['agree_tc'] ?>
                    <?php echo $form['agree_tc']->renderLabel(sprintf('I have read and agree to the %s', link_to('terms &amp; conditions', '@terms?p=nominate',  array('id' => 'terms_link', 'data-fancybox-type' => 'ajax')))) ?>
                    <?php echo $form['agree_tc']->renderError() ?>
                </div>
                <br class="clear" />
                <input type="image" src="<?php echo image_path('app/shareacoke/button-submit-small.jpg') ?>">
                <?php echo $form->renderHiddenFields() ?>
            </form>
        </div>

    </div>

<?php endif; ?>

    <div class="cokebottle_tall">
        <div class="coke_name ff-you">
            <span>
                Kelly
            </span>
        </div>
    </div>


</div>


<br class="clear" />

<script type="text/javascript">

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '<?php echo $app_id ?>', // App ID
            channelUrl : '//<?php echo $_SERVER['SERVER_NAME'] ?>/channel.html', // Channel File
            status     : true, // check login status
            cookie     : true, // enable cookies to allow the server to access the session
            xfbml      : true  // parse XFBML
        });

        // Auto resize yuck
        FB.Canvas.setAutoGrow(91);
        FB.Canvas.scrollTo(0,0);

        <?php if (!$complete): ?>
        FB.login(function(response) {
            if (response.status === 'connected') {
                FB.api('/me', function(api_response) {
                    $('#sac_nominate_name_name').val(api_response.name);
                    $('#sac_nominate_name_email').val(api_response.email);
                });
                $('.form_content').css('visibility', 'visible');
            }
        }, { scope: '<?php echo sfConfig::get('app_facebook_permissions') ?>'});
        <?php endif; ?>
    };

    // Load the SDK Asynchronously
    (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

</script>