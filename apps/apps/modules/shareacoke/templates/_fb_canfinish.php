<script>
    
        var selectedname = "";
        
        window.fbAsyncInit = function() {
            FB.init({
                appId      : <?php echo $app_id ?>, // App ID
                channelUrl : '//<?php echo $_SERVER['SERVER_NAME'] ?>/channel.html', // Channel File
                status     : true, // check login status
                cookie     : true, // enable cookies to allow the server to access the session
                xfbml      : true  // parse XFBML
            });

            // Auto resize yuck
            FB.Canvas.setAutoGrow(91);
            FB.Canvas.scrollTo(0,0);

            FB.getLoginStatus(function(response) {

                $(".control_share").click(function() {
                   // calling the API ...
                    FB.ui({
                        method: 'feed',
                        name: "<?php echo $cokename ?>, this 'Coke' is unlike any other 'Coke' can - it's got your name on it!",
                        link: '<?php echo $tab_url; ?>',
                        picture: 'http://<?php echo $_SERVER['SERVER_NAME'] ?>/uploads/app/shareacoke/<?php echo $cokename ?>.jpg',
                        caption: 'You can also create a \'Coke\' can especially for a friend. Simply pick a friend, watch their virtual \'Coke\' can being made in the special digital can factory and share it on their wall!',
                        description: '',
                        to: '<?php echo $cokeid ?>'
                    }, callback );
                    
                    function callback() {
                        _gaq.push(['_trackSocial', 'Facebook', 'Share', 'Share a virtual can']);
                        $(".control_share").hide();
                        $(".final_name_container").hide();
                        $(".share_success").show();

                        // Reposition Back Button
                        $(".control_back").css("left", "340px");
                        $(".control_back").css("bottom", "100px");
                    }
                });
            }); 
        };

        $(document).ready(function(){
            //Preload the image into the user's local cache.
            $.get('http://<?php echo $_SERVER['SERVER_NAME'] ?>/uploads/app/shareacoke/<?php echo $cokename ?>.jpg');
        });

        // Load the SDK Asynchronously
        (function(d){
            var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement('script'); js.id = id; js.async = true;
            js.src = "//connect.facebook.net/en_US/all.js";
            ref.parentNode.insertBefore(js, ref);
        }(document));

</script>