<?php include_partial('nav', array('pagename'=>$pagename)); ?>


<br class="clear" />

<div class="page sharecan_page">
    
   <div class="sharecan_instructions">
        <div class="instruction gotham" style="width: 340px">Create a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>
        <div class="instruction gotham">can especially</div>
        <div class="instruction gotham">for a friend</div>
        
        <div class="subintro">
            Share a very special <strong>Coke</strong> can with a good friend,<br />
            an old friend, or maybe even a new friend. Simply select a<br/>
            friend below to get started. Conditions apply.
        </div>
        
        <div class="floating_mate_can"></div>
    </div>
    
    
    <br class="clear" />
    
    <a href="#" id="link_to_connect"><div class="connect_facebook"></div></a>
    
    <div class="friends_container">
        <div class="create_coke_label"></div>
        
        <div class="searchfriend">
            <input name="search_friend" id="search_friend" />
        </div>
        
        <br class="clear" />
        
        <ul class="friendslist">

        </ul>
        
    </div>
    
    <a id="linktocanpage_withname" class="tTip"><div class="create_button"></div></a>
    
</div>

<br class="clear" />

<?php include_partial('fb_sharecan', array('app_id' => $app_id, 'tab_url' => $tab_url)); ?>