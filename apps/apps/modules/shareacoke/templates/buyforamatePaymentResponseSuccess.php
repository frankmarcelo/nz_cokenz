<?php include_partial('nav', array('pagename'=>$pagename)); ?>

<br class="clear"/>
<div class="buy_for_mate_page buy_for_mate_payment_res">

    <?php if ($payment_successful): ?>
    <div class="success">
        <div class="title_1 gotham">Thanks!</div>
        <div class="txt">
            <p>We've sent an SMS and confirmation email to your mate with instructions on how to pick up their <strong>COKE</strong>.<br>If your mate is on the Telecom network, they will only get an email.</p>
        </div>
        <div class="submit_btn_wrapper">
            <a href="<?php echo url_for("@buyforamate") ?>" id="buy_another"><img src="/images/app/shareacoke/vending/buy_another.png" alt=""></a>
        </div>
    </div>

    <?php else: ?>
    <div class="success">
        <div class="title_1 gotham">Oops!</div>
        <div class="txt">
            <p>
                We are experiencing some problems processing your payment.
            </p>
        </div>
        <div class="submit_btn_wrapper">
            <a href="<?php echo url_for("@buyforamate") ?>" id="buy_another"><img src="/images/app/shareacoke/vending/buy_another.png" alt=""></a>
        </div>
    </div>
    <?php endif; ?>

</div>
<?php include_partial('fb_default', array('app_id' => $app_id)); ?>

