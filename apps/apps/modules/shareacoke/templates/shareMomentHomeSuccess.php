<?php include_partial('nav', array('pagename' => $pagename)); ?>
<br class="clear"/>
<div class="share_moment">


    <div class="page intro moment_intro" style="">
        <div class="subintro"
             style="padding-right: 300px; height: 250px; background: url(/images/app/shareacoke/cokecan-mate.jpg) no-repeat 400px 0;">
            <div class="header">
                <div class="instruction gotham" style="width: 340px">
                    Share a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>"/>
                </div>
                <div class="instruction gotham">Moment with a Mate</div>
            </div>
            <div class="content_txt">
                <div class="txt">
                    <p>Create a special picture with you and your flat mates, uni mates, work mates, team mates, old
                        mates, new mates, best mates, soul mate...you get the idea. <br/>
                        Click below to choose the image of you and your mate and get started.
                    </p>

                </div>
            </div>


        </div>

    </div>
    <form id="img_form" action="<?php echo url_for('@sharemoment_submit') ?>" enctype="multipart/form-data" method="post">

        <div class="overview_guide">
            <ul>
                <li>
                    The picture you have created must be for a person who is known to you and who you could reasonably
                    expect to send a picture with your friends name tagged within it.
                </li>
                <li>
                    When you submit the picture you create, you are authorizing us to publish it.
                </li>
                <li>
                    We will not publish a picture that is illegal, obscene, derogatory, threatening, violent,
                    scandalous, inflammatory, discriminatory (on any grounds), or would give rise to or encourage
                    conduct which is inappropriate or illegal or which is unfit to be published. We decide when this
                    condition applies.
                </li>
            </ul>
            <?php echo $form['original_image']->render(array('id' => 'img_select')) ?>
            <?php echo $form['_csrf_token']->render() ?>

            <input type="checkbox" id="terms_accept">
            <label for="terms_accept"  id="terms_accept_label">I have read and agreed to the <?php echo link_to('terms and conditions', '@terms?p=moment', array('class' => 'terms_link')) ?></label>

            <div class="error"></div>
        </div>

        <div class="footer_btn" method="post">

            <a href="#" id="file_upload_click">
                <img src="<?php echo image_path("app/shareacoke/moment/get_started.png") ?>"/>
            </a>


        </div>
    </form>
    <script type="text/javascript">
        String.prototype.endsWith = function (suffix) {
            return this.toLowerCase().indexOf(suffix.toLowerCase(), this.length - suffix.length) !== -1;
        };

        $('#img_select').change(function () {
            err = $(".share_moment .overview_guide .error");
            imgStr = $('#img_select').val();
            if (imgStr.endsWith(".png") || imgStr.endsWith(".jpg") || imgStr.endsWith(".gif") || imgStr.endsWith(".bmp") || imgStr.endsWith(".jpeg")) {

                $(err).css("display", "none");
                $('#img_form').submit();

            } else {

                $(err).html("Please select a jpg/png/gif image.");
                $(err).css("display", "block");

            }


        });

        $("#file_upload_click").click(function () {
            err = $(".share_moment .overview_guide .error");
            if ($('#terms_accept').is(':checked')) {
                $(err).html("");
                $(err).css("display", "none");
                $("#img_select").click();
                return false;
            } else {

                $(err).html("To create a picture, you must accept the terms and conditions by clicking the checkbox above.");
                $(err).css("display", "block");
                return false;
            }
        });


    </script>


</div>
<?php include_partial('fb_default', array('app_id' => $app_id)); ?>
