<?php include_partial('nav', array('pagename'=>$pagename)); ?>


<br class="clear" />

<div class="page sharecan_page buy_for_mate_page buy_for_mate_share_page">

    <div class="intro buy_for_mate" style="">
        <div class="subintro" style="padding-right: 300px; height: 250px; background: url(/images/app/shareacoke/cokecan-mate.jpg) no-repeat 400px 0;">
            <div class="header">
                <div class="instruction gotham" style="width: 340px">Buy a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>
                <div class="instruction gotham">for a Mate!</div>
            </div>
            <div class="content_txt">
                <div class="txt">
                    Want to buy a <strong>Coke</strong> for a mate, but cant get together? <br />
                    We're giving you the chance to buy your mate a 440ml <strong>Coke</strong> can for pick up at a selected vending machine. Get started by following the prompts below.
                </div>
            </div>
        </div>
    </div>


    <br class="clear" />

    <a href="#" id="link_to_connect"><div class="connect_facebook"></div></a>

    <div class="friends_container">
        <div class="create_coke_label"></div>

        <div class="searchfriend">
            <input name="search_friend" id="search_friend" />
        </div>

        <br class="clear" />

        <ul class="friendslist">

        </ul>

    </div>

    <a id="linktocanpage_withname" class="tTip"><div class="create_button"></div></a>

</div>

<br class="clear" />

<?php include_partial('fb_buyforamateselect', array('app_id' => $app_id, 'tab_url' => $tab_url)); ?>