<?php include_partial('nav', array('pagename'=>$pagename)); ?>
<?php use_javascript('jquery.selectBox.min.js') ?>

<br class="clear"/>
<div class="buy_for_mate_page buy_for_mate_details_page">
    <div class="your_details">
        <form id="continue_to_payment_form" method="post" action="<?php echo url_for('@buyforamate_details_submit') ?>">

        <div class="title_1 gotham">Enter your details</div>
        <table class="form_content">
            <tr class="user_first_name inner_data <?php echo $form['user_first_name']->hasError()? "required" : ''?>">
                <td><?php echo $form['user_first_name']->renderLabel("Your First Name:"); ?></td>

                <td><?php echo $form['user_first_name']->render(array('class'=>'field')); ?>
                    </td>
            </tr>
            <tr class="user_last_name inner_data <?php echo $form['user_last_name']->hasError()? "required" : ''?>">
                <td><?php echo $form['user_last_name']->renderLabel("Your Last Name:"); ?></td>

                <td><?php echo $form['user_last_name']->render(array('class'=>'field')); ?>
                    </td>
            </tr>
            <tr class="user_email inner_data <?php echo $form['user_email']->hasError()? "required" : ''?>">
                <td><?php echo $form['user_email']->renderLabel("Your Email:"); ?></td>

                <td><?php echo $form['user_email']->render(array('class'=>'field')); ?>
                    </td>
            </tr>
            <tr class="user_phone inner_data <?php echo $form['user_phone']->hasError()? "required" : ''?>">
                <td><?php echo $form['user_phone']->renderLabel("Your Phone:"); ?></td>

                <td><?php echo $form['user_phone']->render(array('class'=>'field')); ?>
                    </td>
            </tr>
            <tr class="recipient_mobile_info ">
                <td></td>
                <td>Enter a valid New Zealand mobile number eg. 0211231234. <br />
                    No spaces, dashes or brackets.
                </td>
            </tr>

        </table>
        <div class="title_1 gotham">Enter your Mate's details</div>

        <table class="form_content">
            <tr class="recipient_first_name inner_data <?php echo $form['recipient_first_name']->hasError()? "required" : ''?>">
                <td><?php echo $form['recipient_first_name']->renderLabel("Mate's First Name:"); ?></td>

                <td><?php echo $form['recipient_first_name']->render(array('class'=>'field'));  ?>
                    </td>
            </tr>
            <tr class="recipient_last_name inner_data <?php echo $form['recipient_last_name']->hasError()? "required" : ''?>">
                <td><?php echo $form['recipient_last_name']->renderLabel("Mate's Last Name:"); ?></td>

                <td><?php echo $form['recipient_last_name']->render(array('class'=>'field')); ?>
                    </td>
            </tr>

            <tr class="recipient_email inner_data <?php echo $form['recipient_email']->hasError()? "required" : ''?>">
                <td><?php echo $form['recipient_email']->renderLabel("Mate's Email:"); ?></td>

                <td><?php echo $form['recipient_email']->render(array('class'=>'field')); ?>
                   </td>
            </tr>
            <tr class="recipient_mobile inner_data <?php echo $form['recipient_mobile']->hasError()? "required" : ''?>">
                <td><?php echo $form['recipient_mobile']->renderLabel("Mate's Mobile Number:"); ?></td>

                <td><?php echo $form['recipient_mobile']->render(array('class'=>'field')); ?>
                    </td>
            </tr>
            <tr class="recipient_mobile_info ">
                <td></td>

                <td>Enter a valid New Zealand mobile number eg. 0211231234. <br />
                    No spaces, dashes or brackets.
                </td>
            </tr>
            <tr class="location inner_data <?php echo $form['location']->hasError()? "required" : ''?>">
                <td></td>

                <td>
                    <?php echo $form['location']->render(array('class'=>'selectBox')) ?>

                </td>
            </tr>

            <tr class="terms_small inner_data">
                <td></td>

                <td>
                 <p>
                     *Your Mate will be sent an SMS to redeem their <strong>Coke</strong> can. Please double check your mate's mobile number before continuing.
                 </p>
                    <ul>
                        <li>
                            You must be 18 years or older to enter.
                        </li>
                        <li>
                            Promotion us open from 8th of October until 11.59pm on Sunday 22nd October 2012
                        </li>
                        <li>
                            Your Credit or Debit card will be charged for the cost of this product.
                        </li>
                        <li>
                            We accept no responsibility for any orders which are not received for any reason.
                        </li>
                        <li>
                            Should your <strong>Coke</strong> can not be redeemed by your mate, you will receive an automatic refund of your purchase at the conclusion of the promotion.
                        </li>

                    </ul>
                </td>
            </tr>
            <tr class="terms_accept_row inner_data">
                <td></td>

                <td>
                    <input type="checkbox" id="terms_checkbox" class="terms_check">
                    <label for="terms_checkbox">I have read and agreed to the <a target="_blank" class="terms_link" href="<?php echo url_for("@terms?p=buyforamate") ?>">terms and conditions</a>.</label>

                </td>
            </tr>

        </table>
        <?php echo $form->renderHiddenFields() ?>

            <div class="submit_btn_wrapper">
                <a href="#" id="continue_to_payment"><img src="/images/app/shareacoke/vending/continue_to_payment.png" alt=""></a>
            </div>

        </form>

    </div>



<script type="text/javascript">
    $(document).ready(function() {
        $('#continue_to_payment').click(function() {
            if($('#terms_checkbox').is(':checked')){
                $('#continue_to_payment_form').submit();
            }else{
                $('label[for=terms_checkbox]').css('color', '#F40009').css('font-weight', 'bold');
            }
            return false;
        });


        $('select').selectBox();
    });

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '<?php echo $app_id ?>', // App ID
            channelUrl : '//<?php echo $_SERVER['SERVER_NAME'] ?>/channel.html', // Channel File
            status     : true, // check login status
            cookie     : true, // enable cookies to allow the server to access the session
            xfbml      : true  // parse XFBML
        });

        FB.getLoginStatus(function(response) {
            if (response.status == "connected") {
                FB.api('/me', function(api_response) {
                    $('#sac_buy_user_first_name').val(api_response.first_name);
                    $('#sac_buy_user_last_name').val(api_response.last_name);
                    $('#sac_buy_user_email').val(api_response.email);
                });
            }
        });

        FB.Canvas.setAutoGrow(91);
        FB.Canvas.scrollTo(0,0);
    };

    // Load the SDK Asynchronously
    (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

</script>

</div>