<?php include_partial('nav', array('pagename'=>$pagename)); ?>


<br class="clear" />

<div class="names_150_page">
    
    
    <!-- 150 Names Page Intro  -->

    <div class="pageintro">
        <div class="introtext gotham">If you know an Amelia,</div>
        <div class="introtext gotham" style="width: 425px">then share a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>
        <div class="introtext gotham">with an Amelia</div>
        
        <div class="subintro">
            Do you know a Chris, an Anna, a George, a Kate, or an Amelia?<br />
            Now you can share a real <strong>Coke</strong> with their name on it! <strong>Coke</strong><br />
            bottles with 150 different names on them are now on sale<br />
            everywhere. To see them all check out the list below.
        </div>
    </div>




    <!-- Nmaes List -->
    <br class="clear" />

    <div class="sharecoke_names">
        <div class="label_share_coke"></div>

        <div class="names_container">
            <div class="searchname">
                <input name="search_text" id="search_text" />
            </div>

            <ul class="nameslist">
                <?php foreach($allnames as $name): ?>
                    <li title="<?php echo $name?>" class="highlight_bottle name ff-you <?php if ($cokename == $name) {echo "active"; } ?>"><?php echo $name ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>


    <div class="cokebottle_tall">
        <div class="coke_name ff-you">
            <span>
                <?php echo ucfirst($cokename); ?>
            </span>
        </div>
    </div>

    
</div>


<br class="clear" />

<?php include_partial('fb_default', array('app_id' => $app_id)); ?>