<script>

    var selectedname = "";

    window.fbAsyncInit = function() {
        FB.init({
            appId      : '<?php echo $app_id ?>', // App ID
            channelUrl : '//<?php echo $_SERVER['SERVER_NAME'] ?>/channel.html', // Channel File
            status     : true, // check login status
            cookie     : true, // enable cookies to allow the server to access the session
            xfbml      : false  // parse XFBML
        });

        // Auto resize yuck
        FB.Canvas.setAutoGrow(91);
        FB.Canvas.scrollTo(0,0);
        FB.login(function(response) {
            if (response.status === 'connected') {

                // Access token for API Calls
                //var accessToken = response.authResponse.accessToken;

                $(".friends_container").css("visibility", "visible");
                //$(".sharecan_page").css("padding-bottom", "140px");

                $(".create_button").css("display", "block");

                FB.api('/me/friends', function(response) {

                    $.each(response.data, function(i, val){
                        $(".friendslist").append('<div rel="'+val.id+'" title="' + val.name + '" class="name highlight_friend"><img src="https://graph.facebook.com/' + val.id + '/picture"><div>' + val.name + '</div></div>');
                    });

                    // scroller's autoReinitialise flickers too much, so had to use manual call mixed with typewatch to avoid delays
                    $('ul.friendslist').jScrollPane({
                        showArrows: 'true'
                    });

                    // Search Filter
                    var typewatch = (function(){
                        var timer = 0;
                        return function(callback, ms){
                            clearTimeout (timer);
                            timer = setTimeout(callback, ms);
                        }
                    })();

                    $("#search_friend").keyup(function(event) {
                        var input_value = $("#search_friend").val();

                        $(".friendslist div.name").css('display', 'none')

                        $(".friendslist div.name:regex(title, .*" + input_value + ".*)").css('display', 'block');

                        typewatch(function () {

                            // scroller's autoReinitialise flickers too much, so had to use manual call mixed with typewatch to avoid delays
                            $('ul.friendslist').jScrollPane({
                                showArrows: 'true'
                            });
                        }, 500);
                    });

                    // apply selection lock
                    $(".highlight_friend").click(function() {

                        selectedname = $(this).attr("title");
                        selectedid = $(this).attr("rel");

                        activeTag.name = selectedname;
                        activeTag.fbID = selectedid;
                        activeTag.submit();
                        activeTag = null;
                        $(".friends_container").css("display", "none"); //hide friends list

                        $('.friendslist .name').each(function(index) {
                            $(this).removeClass("active");
                        });

                        /* Add active class on click */
                        //$(this).addClass("active");

                        var selected_first_name = selectedname.split(' ');
                        fname = '';
                        lname = '';
                        if(selected_first_name.length >= 1){
                            fname = selected_first_name[0];
                        }
                        if(selected_first_name.length >= 2){
                            lname = selected_first_name[selected_first_name.length - 1];
                        }

                        $("#linktocanpage_withname").attr("href",'<?php echo url_for("@buyforamate_details"); ?>/' + fname + '/' + lname);

                    });
                });

            } else if (response.status === 'not_authorized') {

                $("a#link_to_connect").click(function() {
                    FB.ui({
                        method: 'oauth',
                        client_id: '<?php echo $app_id ?>',

                        //redirect_uri: '<?php echo $tab_url ?>',

                        scope: '<?php echo sfConfig::get('app_facebook_permissions') ?>'
                    }, function(data) {
                        window.location = '<?php echo url_for("sharecan"); ?>';
                    });
                    return false;
                });
                $(".connect_facebook").css("display", "inline");

                $(".sharecan_page").css("padding-bottom", "0");
                $(".sharecan_page").css("height", "440px");
            }
        }, { scope: '<?php echo sfConfig::get('app_facebook_permissions') ?>'});
    };

    // Load the SDK Asynchronously
    (function(d){
        var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement('script'); js.id = id; js.async = true;
        js.src = "//connect.facebook.net/en_US/all.js";
        ref.parentNode.insertBefore(js, ref);
    }(document));

</script>