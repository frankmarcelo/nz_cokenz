<ul class="nav">
    <li>
        <a href="<?php echo url_for("@homepage") ?>">
            <div class="home <?php if($pagename == 'home') {echo 'active'; } ?>">Home</div>
        </a>
    </li>

    <li>
        <a href="<?php echo url_for("@names150") ?>">
            <div class="names150 <?php if($pagename == 'names150') {echo 'active'; } ?>">150 Names</div>
        </a>
    </li>

    <li>
        <a href="<?php echo url_for("@sharecan") ?>">
            <div class="sharecan <?php if($pagename == 'sharecan') {echo 'active'; } ?>">Share a virtual can</div>
        </a>
    </li>

    <li>
        <a href="<?php echo url_for("@note") ?>">
            <div class="shareanote <?php if($pagename == 'shareanote') {echo 'active'; } ?>">Share a COKE, Share a note</div>
        </a>
    </li>

    <li>
        <a href="<?php echo url_for("@sharestory") ?>">
            <div class="sharestory <?php if($pagename == 'sharestory') {echo 'active'; } ?>">Share a COKE, Share your story</div>
        </a>
    </li>

    <li>
        <a href="<?php echo url_for("@customise") ?>">
            <div class="customise <?php if($pagename == 'customise') {echo 'active'; } ?>">Customise a can</div>
        </a>
    </li>
    <?php
        /*
<li>
    <a href="<?php echo url_for("@sharemoment_tag") ?>">
        <div class="customise <?php if($pagename == 'customise') {echo 'active'; } ?>">Customise a can</div>
    </a>
</li> */
    ?>
</ul>