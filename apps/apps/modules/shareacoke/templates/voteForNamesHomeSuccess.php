<?php include_partial('nav', array('pagename'=>$pagename)); ?>


<br class="clear" />

<div class="names_150_page">


    <!-- 150 Names Page Intro  -->
    <div class="pageintro">
        <div class="introtext gotham">Vote for the name of someone</div>
        <div class="introtext gotham" style="width: 600px">you'd like to share a <img class="floating_coke_label" width="95px" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" />with </div>
        

        <div class="subintro">
            
            Search or scroll through the 100 names below and vote for the<br />
            names of the people you would most like to share a <strong>Coke</strong> with<br />
            You can vote for up to ten names.
        </div>
    </div>
    <!-- Nmaes List -->
    <br class="clear" />
     
   <div class="sharecoke_names">
        <!--<div class="label_share_coke"></div>-->
        
        <?php if (isset($show_success) and $show_success): ?>
        <div class="gotham large">
            Thank you!<br/>
            Your story has been sent.
        </div>
        <?php else: ?>
        <div class="form_content">
        <form method="post" action="<?php echo url_for("@sharestory") ?>"
                onsubmit="return checkFields(); return false;">

            <?php echo $votesform['_csrf_token']->render(); ?>

            <table width="100%" id="form_container">
                <tr>
                    <td><?php echo $votesform['name']->renderLabel('Your Name'); ?></td>
                    <td width="340" class="inner_data">
                        <?php echo $votesform['name']; ?>
                        <?php echo $votesform['name']->renderError(); ?>
                    </td>
                </tr>
                <tr>
                    <td><?php echo $votesform['email']->renderLabel('Your Email'); ?></td>
                    <td width="340" class="inner_data">
                        <?php echo $votesform['email']; ?>
                        <?php echo $votesform['email']->renderError(); ?>
                    </td>
                </tr>
                <tr><td colspan="2"><div class="names_container">
                        <div class="searchname">
                            <input name="search_text" id="search_text" />
                        </div>

                        <ul class="nameslist">
                            <?php foreach($allnames as $name): ?>
                            <li title="<?php echo $name['name']?>" class="highlight_bottle name ff-you <?php if ($cokename == $name['name']) {echo "active"; } ?>"><label for="vote_for_name_id_<?php echo intval($name['id']);?>"><?php echo $name['name']?></label><input type="checkbox" name="sac_votes[vote_for_name_id][]" id="vote_for_name_id_<?php echo intval($name['id']);?>" /></li>
                            <?php endforeach; ?>
                        </ul>
                    </div></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td width="340">
                        <br/><br/>
                        <input type="image" src="<?php echo image_path('app/shareacoke/button-submit-small.jpg') ?>">
                    </td>
                </tr>
            </table> 
        </form>
        </div>
        <?php endif; ?>
    </div>
    <div class="cokebottle_tall">
        <div class="coke_name ff-you">
            <span>
                <?php echo ucfirst($cokename); ?>
            </span>
        </div>
    </div>
</div>
<br class="clear" />
<?php include_partial('fb_default', array('app_id' => $app_id)); ?>