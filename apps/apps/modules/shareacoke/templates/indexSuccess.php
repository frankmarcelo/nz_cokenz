<?php include_partial('nav', array('pagename' => $pagename)); ?>

<br class="clear"/>

<div class="home_page" style="height: 850px">
    <!-- Buy for mate Intro -->
    <!--
    <div class="page intro buy_for_mate" style="">
        <div class="subintro" style="padding-right: 300px; height: 250px; background: url('/images/app/shareacoke/cokecan-mate.jpg') no-repeat 400px 0;">
            <div class="header">
                <div class="instruction gotham" style="width: 340px">Buy a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>
                <div class="instruction gotham">for a Mate!</div>
            </div>
            <div class="content_txt">
                <div class="txt">
                    Want to share a <strong>Coke</strong> with a mate, but can't get together? Buy your mate a 440ml. <strong>Coke</strong> can for pickup at a selected vending machine.

                </div>
                <div class="btn"> <a href="<?php echo url_for('@buyforamate') ?>" style=""><img style="width: 150px;" src="<?php echo image_path('app/shareacoke/vending/get_started.png') ?>" alt="Share a virtual can" /></a></div>

            </div>




        </div>

    </div>
     -->
    <!--
    <div class="page intro buy_for_mate" style="">
        <div class="subintro" style="padding-right: 200px; height: 250px; background: url('/images/app/shareacoke/new-names-bottles.png') no-repeat 530px 0;">
            <div class="header">
                <div class="instruction gotham" style="width: 590px">Vote for the name of someone you'd like to share a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /> with</div>

            </div>
            <div class="content_txt">
                <div class="btn"><a href="<?php echo url_for('@votenames_home') ?>" style="color: red; font-size: 30px;"><div class="instruction gotham" style="width: 540px">Click here to vote ></div></a>
                 </div>

            </div>




        </div>

    </div>
        -->
    <!-- Home Page Intro  -->
    <?php if (time() > mktime(0,0,0,9,24,2012)): ?>
    <div class="page intro" style="height:353px; padding-bottom:100px;">
        <div class="subintro" style="padding-right: 400px; height: 250px; background: url(/images/app/shareacoke/cokecan-mate.jpg) no-repeat 400px 0;">
            <strong>Coke</strong> has always been a part of people coming together. And now, for the first time ever,
            we're giving New Zealanders the chance to find, create and share cans of <strong>Coke</strong> customised
            with the names of the people who matter to them.

            <a href="<?php echo url_for('@sharecan') ?>" style="float: left; margin: 40px 0 0 -10px;">
                <img src="<?php echo image_path('app/shareacoke/Virtual_Can_Btn.png') ?>" alt="Share a virtual can">
            </a>
        </div>

    </div>

    <?php else: ?>

    <div class="pageintro intro">
        <div class="introtext gotham">Nominate the name of someone</div>
        <div class="introtext gotham">you'd like to share a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /> with</div>

        <div class="gotham introtext" style="margin-top:30px;">
            <a href="<?php echo url_for('@nominate_name') ?>">Submit a nomination here > </a>
        </div>


        <a href="<?php echo url_for('@nominate_name') ?>" class="nominate_bottles">
            <img src="<?php echo image_path('app/shareacoke/new-names-bottles.png') ?>" alt="Share a virtual can">
        </a>

    </div>
    <?php endif; ?>
    <!-- Nmaes List -->
    <br class="clear"/>
<?php /* ?>
    <div class="pageintro names_title">
        <div class="introtext gotham">If you know <?php echo $an_or_a ?> <?php echo $random_name ?>,</div>
        <div class="introtext gotham" style="width: 425px; font-size: 42px;">then share a
            <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>"/>
        </div>
        <div class="introtext gotham">with <?php echo $an_or_a ?> <?php echo $random_name ?></div>

        <div class="subintro">
            Do you know a Chris, an Anna, a George, a Kate, or an Amelia?<br/>
            Now you can share a real <strong>Coke</strong> with their name on it! <strong>Coke</strong><br/>
            bottles with 150 different names on them are now on sale<br/>
            everywhere. To see them all check out the list below.
        </div>
    </div>
<?php */ ?>
    <div class="sharecoke_names">

        <div class="label_share_coke"></div>

        <div class="names_container">
            <div class="searchname">
                <input name="search_text" id="search_text"/>
            </div>

            <ul class="nameslist">
                <?php foreach ($allnames as $name): ?>
                <li title="<?php echo $name?>"
                    class="highlight_bottle name ff-you <?php if (isset($cokename) && $cokename == $name) {
                        echo "active";
                    } ?>"><?php echo $name ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>


    <!-- Video Ads -->
    <div class="new_ads">
        <img src="<?php echo image_path('app/shareacoke/label-checkout.jpg') ?>"/>

        <a href="http://www.youtube.com/watch?v=EyAQnQI56Vw" rel="prettyPhoto" title="" id="video">
            <img src="<?php echo image_path('app/shareacoke/tvc-preview-josh.jpg') ?>" style="margin-bottom: 5px;"/>
        </a>

        <div class="josh left active" data-video-url="http://www.youtube.com/watch?v=EyAQnQI56Vw" data-image-url="<?php echo image_path('app/shareacoke/tvc-preview-josh.jpg') ?>">
        </div>
        <div class="jess right" data-video-url="http://www.youtube.com/watch?v=z8-WBjFZQnM" data-image-url="<?php echo image_path('app/shareacoke/tvc-preview-jess.jpg') ?>"></div>
    </div>



</div>

<br class="clear"/>

<?php include_partial('fb_default', array('app_id' => $app_id)); ?>

<script type="text/javascript">
    $('.josh, .jess').click(function(){
        $('.josh, .jess').removeClass('active');
        $(this).addClass('active');
        $('#video').attr('href', $(this).data('video-url'));
        $('#video img').attr('src', $(this).data('image-url'));
    });
</script>