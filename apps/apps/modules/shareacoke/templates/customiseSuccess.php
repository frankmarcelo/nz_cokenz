<?php include_partial('nav', array('pagename'=>$pagename)); ?>

<br class="clear" />

<div class="story_page customise_page">

    <div class="container">
        <div class="story_intro">
            <div class="introtext gotham" style="width: 425px">Get together<br>and share a <img class="floating_coke_label" src="<?php echo image_path("app/shareacoke/label-cokelogo.jpg") ?>" /></div>

            <div class="subintro">
                <p>We&rsquo;ve put 150 names on <strong>Coke</strong> bottles, but what if your friend&rsquo;s a &lsquo;Sharni&rsquo;, a &lsquo;Malcolm&rsquo;, or a &lsquo;Kelly&rsquo;?</p>

                <p>Don&rsquo;t worry, we haven&rsquo;t forgotten about them.</p>

                <p>Simply get down to one of the locations listed below  and the friendly <strong>Coke</strong> team will put your friend&rsquo;s name on either a 200mL <strong>Coke</strong> can or 600mL <strong>Coke</strong> bottle - for real!</p>
                <p>There are more locations to come and we’ll add them to this page, so check back again soon!</p>
            </div>



        </div>

        <br class="clear" />

        <div class="tabs">
            <div style="float:left;" class="tab gotham active" data-target="200ml_locations">200ml <span class="gotham-b">Coke</span> can locations</div>
            <div style="float: right;" class="tab gotham" data-target="600ml_locations">600ml <span class="gotham-b">Coke</span> bottle locations*</div>
        </div>


        <div id="200ml_locations" class="location_list">
            <div class="gotham-b introtext" style="">We will be at the following mall locations around New Zealand:</div>
            <br class="clear" />
            <div class="city gotham-b">Auckland</div>
            <table class="city_locations">
                <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="date">19, 20, 21 Oct</td>
                        <td class="location">Sylvia Park</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/A4UM9" target="_blank">286 Mt Wellington Highway</a></span></td>
                    </tr>
                    <tr>
                        <td class="date">2, 3, 4 Nov</td>
                        <td class="location">Westfield Manukau</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/u49OH" target="_blank">Cnr Gt South &amp; Wiri Station Rd</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">9, 10, 11 Nov</td>
                        <td class="location">Botany Town Centre</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/a5dfB" target="_blank">Cnr Te Irirangi &amp; Ti Rakau Dr</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">9, 10, 11 Nov</td>
                        <td class="location">Westfield St Lukes</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/kvZk6" target="_blank">80 St Lukes Rd, Mt Albert</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">16, 17, 18 Nov</td>
                        <td class="location">Westfield WestCity</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/hEzxR" target="_blank">7 Catherine St, Henderson</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">16, 17, 18 Nov</td>
                        <td class="location">Westfield Albany</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/jNtu0" target="_blank">219 Don McKinnon Dr, Albany</a></span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="divider"></div>

            <div class="city gotham-b">Tauranga</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">2, 3, 4 Nov</td>
                        <td class="location">Bayfair Mall</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/F3cGN" target="_blank">Cnr Maunganui &amp; Girven Rd</a></span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="divider"></div>

            <div class="city gotham-b">Hamilton</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">26, 27, 28 Oct</td>
                        <td class="location">Westfield Chartwell</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/TlDMz" target="_blank">Chartwell Square, Hamilton</a></span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="divider"></div>

            <div class="city gotham-b">Wellington</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">19, 20, 21 Oct</td>
                        <td class="location">Westfield Queensgate</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/nIAE2" target="_blank">Cnr Queens Dr &amp; Bunny St, Pipitea</a></span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="divider"></div>

            <div class="city gotham-b">Christchurch</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">26, 27, 28 Oct</td>
                        <td class="location">Westfield Riccarton</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/1Z2o3" target="_blank">129 Riccarton Road, Riccarton</a></span>
                        </td>
                    </tr>
                </tbody>
            </table>

        </div>

        <div id="600ml_locations" class="location_list" style="display:none;">
        <p style="margin:15px 0; width:60%; color:#999;">* You will be required to purchase your 600mL bottle of <strong>Coke</strong>.
        </p>
            <div class="gotham-b introtext" style="">We will be at the following locations around New Zealand:</div>
            <br class="clear" />
            <div class="city gotham-b">Auckland</div>
            <table class="city_locations">
                <thead>
                    <tr>
                        <th>Date:</th>
                        <th>Time:</th>
                        <th>Location:</th>
                        <th>Address:</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="date">15 &amp; 16 Sept</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">Countdown Newmarket</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/JbQyW" target="_blank">277 Broadway, Newmarket</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">15 &amp; 16 Sept</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">Countdown Quay St</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/lfyUg" target="_blank">76 Quay St, Auckland CBD</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">22 &amp; 23 Sept</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">PaknSave Sylvia Park</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/A4UM9" target="_blank">286 Mt Wellington Highway</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">22 &amp; 23 Sept</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">New World Vic Park	</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/MEq18" target="_blank">2 College Hill, Freemans Bay</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">29 &amp; 30 Sept</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">Countdown Manukau Mall</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/uuzzZ" target="_blank">4 Ronwood Ave, Manukau</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">29 &amp; 30 Sept</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">Countdown Westgate</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/bzEsV" target="_blank">Fernhill Dr, Massey North</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">16 &amp; 17 Oct</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">New World Botany</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/hrHwi" target="_blank">588 Chapel Road, Dannemora</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">16 &amp; 17 Oct	</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">PaknSave Mt Albert</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/2C9e0" target="_blank">1177 New North Road, Mt Albert</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">23 &amp; 24 Oct</td>
                        <td class="time">&nbsp;</td>
                        <td class="location">Countdown Silverdale</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/s4taK" target="_blank">54 Hibiscus Coast Highway</a></span>
                                                    </td>
                    </tr>

                </tbody>
            </table>

            <div class="divider"></div>

            <div class="city gotham-b">Wellington</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">3 &amp; 5 Oct</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">New World Railway</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/04bvF" target="_blank">Bunny St, Pipitea, Wellington</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">6 &amp; 7 Oct</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">New World Porirua</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/JJQ6z" target="_blank">Cnr Walton Leigh &amp; Lyttelton Ave</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">13 &amp; 14 Oct</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">Countdown Johnsonville</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/fXspZ" target="_blank">31 Johnsonville Rd, Johnsonville</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">13 &amp; 14 Oct</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">Countdown Kilbirnie</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/UgZSG" target="_blank">47 Bay Road, Kilbirnie</a></span>
                                                    </td>
                    </tr>

                </tbody>
            </table>

            <div class="divider"></div>


            <div class="city gotham-b">Christchurch</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">6 &amp; 7 Oct</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">New World St Martins</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/nyEU5" target="_blank">96 Wilsons Road, St Martins</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">13 &amp; 14 Oct</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">PaknSave Moorhouse</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/Isa5K" target="_blank">297 Moorhouse Ave, Sydenham</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">24 &amp; 25 Nov</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">Countdown Church Corner</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/lb0aS" target="_blank">361 Riccarton Road, Riccarton</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">24 &amp; 25 Nov	</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">Countdown Northlands</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/gi42a" target="_blank">85 Main North Road, Papanui</a></span>
                        </td>
                    </tr>
                </tbody>
            </table>


            <div class="divider"></div>


            <div class="city gotham-b">Dunedin</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">4 Oct</td>
                        <td class="time">11am &mdash; 4pm</td>
                        <td class="location">Golden Centre Mall</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/UeCYk" target="_blank">251 George St, Dunedin</a></span>
                        </td>
                    </tr>
                    <tr>
                        <td class="date">6 &amp; 7 Oct</td>
                        <td class="time">12pm &mdash; 5pm</td>
                        <td class="location">PaknSave Dunedin</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/kJEz3" target="_blank">86 Hillside Rd, South Dunedin</a></span>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="divider"></div>


            <div class="city gotham-b">Universities in Auckland</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">13-Sep</td>
                        <td class="time">11am &mdash; 4pm</td>
                        <td class="location">Auckland Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/EYJHD" target="_blank">Princes St, CBD</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">20-Sep</td>
                        <td class="time">9.30am &mdash; 2.30pm</td>
                        <td class="location">AUT Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/nn79r" target="_blank">Wellesley St East, CBD</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">27-Sep</td>
                        <td class="time">11am &mdash; 4pm</td>
                        <td class="location">Massey Uni Albany</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/vxzBQ" target="_blank">Albany Highway, Albany</a></span>
                                                    </td>
                    </tr>

                </tbody>
            </table>


            <div class="divider"></div>

               <div class="city gotham-b">Universities in Hamilton</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">24-Oct		</td>
                        <td class="time">11am &mdash; 4pm</td>
                        <td class="location">Waikato Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/7WMiZ" target="_blank">University of Waikato, Knighton Rd</a></span>
                                                    </td>
                    </tr>
                </tbody>
            </table>

            <div class="divider"></div>

            <div class="city gotham-b">Universities in Palmerston North</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">4-Oct		</td>
                        <td class="time">11am &mdash; 4pm</td>
                        <td class="location">Massey University P. North</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/vuxc4" target="_blank">Tennent Drive, Palmerston North</a></span>
                                                    </td>
                    </tr>
                </tbody>
            </table>


            <div class="divider"></div>

            <div class="city gotham-b">Universities in Wellington</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date">27-Sep	</td>
                        <td class="time">11am &mdash; 4pm</td>
                        <td class="location">Victoria Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/jLeZC" target="_blank">Kelburn Parade, Wellington</a></span>
                                                    </td>
                    </tr>
                </tbody>
            </table>

            <div class="divider"></div>

            <div class="city gotham-b">Universities in Christchurch</div>
            <table class="city_locations">
                <tbody>
                    <tr>
                        <td class="date"> 11-Oct</td>
                        <td class="time">11am &mdash; 4pm</td>
                        <td class="location">Canterbury Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/iH7PG" target="_blank">University Dr, Ilam</a></span>
                                                    </td>
                    </tr>
                    <tr>
                        <td class="date">11-Oct</td>
                        <td class="time">11am &mdash; 4pm</td>
                        <td class="location">Lincoln Uni</td>
                        <td class="address">
                            <img src="/images/app/shareacoke/map-marker.jpg" class="marker">
                            <span><a href="http://goo.gl/maps/QHdqn" target="_blank">Ellesmere Rd, Lincoln</a></span>
                                                    </td>
                    </tr>

                </tbody>
            </table>

        </div>


    </div>
</div>
<?php include_partial('fb_default', array('app_id' => $app_id)); ?>