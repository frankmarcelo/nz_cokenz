<?php

/**
 * home actions.
 *
 * @package    symfony
 * @subpackage home
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class shareacokeActions extends sfActions
{

    /*
     * @var Facebook $facebook
     */
    private $facebook,
            $facebookUser;

    public function preExecute()
    {
        $this->pagename = "home";
    }

    public function postExecute()
    {
        $this->app_id = sfConfig::get('app_facebook_appid');
        $this->tab_url = sfConfig::get('app_facebook_tab_url');
    }

    public function executeIndex(sfWebRequest $request)
    {
        $c = sfConfig::get('app_shareacoke_names150');
        $this->allnames = $c['names'];
        $locations = array_merge(array('' => ''), $this->getNZTownList());

        $s = new sfWidgetFormSelect(array('choices' => array_combine($locations, $locations)));
        $this->locationDropdown = $s->render('location', null, array('id' => 'location_select'));

    }

    public function executeNames150(sfWebRequest $request)
    {
        $this->pagename = "names150";
        $this->cokename = $request->getParameter("title");

        $c = sfConfig::get('app_shareacoke_names150');
        $this->allnames = $c['names'];
    }

    public function executeSharecan(sfWebRequest $request)
    {
        $this->pagename = "sharecan";
    }
    public function executeVoteForNamesHome(sfWebRequest $request)
    {   
        $this->pagename = "voteForNamesHome";    
        $this->votesform = new SACVotesForm();
        
        if ($request->isMethod('post')) {

            // employ basic server side validation
            if (!$request->getParameter('name') ||
                !$request->getParameter('email') 
            ) {
                $this->completion_server_error = true;
            }

            $this->votesform->bind($request->getParameter($this->shareform->getName()), $request->getFiles($this->shareform->getName()));
            if ($this->votesform->isValid()) {
                $this->votesform->save();
                $this->votesform->getObject()->SACUser = $this->getSACUser();
                $this->votesform->getObject()->save();
                $this->show_success = true;

                $fileName = $this->votesform->getObject()->getPic();
                echo 'redirect';die;
                //$this->redirect('@sharestorythanks');
            }
        }
        
        
        /*
        if ($request->isMethod('post')) {

            $form->bind($request->getParameter($form->getName()));

            if ($form->isValid()) {
                $form->save();

            }
        }
        
        
        */
        
        //using names 150 for now..
        $this->cokename = $request->getParameter("title");
        //$c = sfConfig::get('app_shareacoke_names150');
        $this->allnames = Doctrine::getTable('SACVoteForName')->findAll();
    }
    
    public function executeShareMomentHome(sfWebRequest $request)
    {
        $this->form = new SACMomentForm();
        $this->pagename = "shareMomentHome";
    }

    public function executeShareMomentUpload(sfWebRequest $request)
    {
        $o = new SACMoment();
        $o->SACUser = $this->getSACUser();
        $form = new SACMomentForm($o);
        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $form->save();

            $this->redirect('sharemoment_tag', $form->getObject());
        }

        $this->form = $form;
        $this->setTemplate('shareMomentHome');
    }

    public function executeShareMomentTag(sfWebRequest $request)
    {
        $moment = $this->getRoute()->getObject();
var_dump($moment);exit;
        $this->pagename = "shareMomentTag";

       $file= "images/app/shareacoke/moment/placeholder.jpg";
        $this->initFacebook();

        /*$this->facebook->setFileUploadSupport(true);
        $args = array('message' => 'Photo Caption');
        $args['image'] = '@' . realpath($file);
         */

        //this next section is supposed to let you tag people. But not working...
        /*$args['tags'] = array(
            array(
                'tag_uid'=> "100003202984916",
                'x'      => '235',
                'y'      => '366',
            ),



        );
        $args['tags'] = json_encode($args['tags']);
        */

        /*
        $this->data = $this->facebook->api('/me/photos?access_token='.$this->facebook->getAccessToken(), 'post', $args);
        */


    }
    public function executeBuyforamate(sfWebRequest $request)
    {
        $this->pagename = "buyforamate";
    }

    public function executeBuyforamateselect(sfWebRequest $request)
    {
        $this->pagename = "buyforamateselect";
    }
    public function executeBuyforamateterms(sfWebRequest $request)
    {
        $this->pagename = "buyforamateterms";
    }
    public function executeBuyforamatefaq(sfWebRequest $request)
    {
        $this->pagename = "buyforamatefaq";
    }

    public function executeBuyforamatedetails(sfWebRequest $request)
    {
        $this->pagename = "buyforamatedetails";

        $this->fname = $request->getParameter("fname");
        $this->lname = $request->getParameter("lname");

        $o = new SACBuy();
        $o->SACUser = $this->getSACUser();
        $o->RedemptionCode = null;
        $form = new SACBuyForm($o);

        $form->getWidgetSchema()->setDefaults(array(
                    'recipient_first_name' => $request->getParameter("fname"),
                    'recipient_last_name' => $request->getParameter("lname"),
                    'recipient_mobile' => '02',
                    'user_phone' => '02'
                ));

        if ($request->isMethod('post')) {

            $form->bind($request->getParameter($form->getName()));

            if ($form->isValid()) {
                $form->save();

                $payment = new ShareACokePayment(true);
                $url = $payment->createRequest(
                    $form->getObject()->id,
                    $this->generateUrl('buyforamate_payment_return', array(), true)
                );
                $this->redirect($url);
            }
        }
        $this->form = $form;
    }

    public function executeBuyforamatePaymentReturn(sfWebRequest $request)
    {
        $this->pagename = "buyforamate_payment_reponse";

        $this->redirectUnless($request->hasParameter('result'), 'shareacoke/buyforamatePaymentFailed');

        $dev = in_array(sfConfig::get('sf_environment'), array('niks','this','staging','web1'));
        $payment = new ShareACokePayment($dev);
        $rsp = $payment->getResponse();

        $id = $rsp->getMerchantReference();

        $this->redirectUnless(is_numeric($id), 'shareacoke/buyforamatePaymentFailed');

        $purchase = Doctrine::getTable('SACBuy')->find($id);

        $this->redirectUnless($purchase, 'shareacoke/buyforamatePaymentFailed');

        if ($payment->isSuccessful() and $purchase->payment_status == SACBuyTable::PAYMENT_STATUS_PENDING) {
            $purchase->payment_status = SACBuyTable::PAYMENT_STATUS_SUCCESS;
            $code = Doctrine::getTable('SACBuyRedemptionCode')->getUnallocatedCodeForLocation($purchase->location);

            if ($code) {

                $purchase->RedemptionCode = $code;

                $code = $purchase->RedemptionCode->getCode();
                $consumer_name = $purchase->SACUser->first_name;
                $recipient_name = $purchase->recipient_first_name;

                //Build the email body
                $body = strtr(sfConfig::get('app_shareacoke_email_message_pattern'), array(
                    '$$recipient_name$$' => $recipient_name,
                    '$$consumer_name$$' => $consumer_name,
                    '$$location$$'  => $purchase->getLongLocation(),
                    '$$uniqueID$$' => $code
                ));

                //Generate and send the email
                $message = Swift_Message::newInstance()
                    ->setFrom(sfConfig::get('app_shareacoke_vending_from_email'), sfConfig::get('app_shareacoke_vending_from_name'))
                    ->setTo($purchase->recipient_email)
                    ->setSubject(sfConfig::get('app_shareacoke_vending_email_subject'))
                    ->setBody($body);
                $this->getMailer()->send($message);

                //Send the SMS
                $sms = new ShareACokeSMS();
                $sms->sendMessage(
                    $purchase->recipient_mobile,
                    $code,
                    $purchase->getLocationFullName(),
                    $consumer_name,
                    $recipient_name
                );

                $purchase->save();
                $this->redirect('shareacoke/buyforamatePaymentSuccess');
            } else {
                error_log("Code was unable to be issued to:\n\n".print_r($purchase->getData(), true), 1, 'nik@satellitemedia.co.nz');
                $this->redirect('shareacoke/buyforamatePaymentFailed');
            }
        } else {
            //Don't change it from original status if user reloads page
            if ($purchase->payment_status == SACBuyTable::PAYMENT_STATUS_PENDING) {
                $purchase->payment_status = SACBuyTable::PAYMENT_STATUS_FAILED;
                $purchase->save();
            }
            $this->redirect('shareacoke/buyforamatePaymentFailed');
        }


    }

    public function executeBuyforamatePaymentSuccess(sfWebRequest $request)
    {
        $this->pagename = "buyforamate_payment_reponse";
        $this->payment_successful = true;
        $this->setTemplate('buyforamatePaymentResponse');
    }

    public function executeBuyforamatePaymentFailed(sfWebRequest $request)
    {
        $this->pagename = "buyforamate_payment_reponse";
        $this->payment_successful = false;
        $this->setTemplate('buyforamatePaymentResponse');
    }


    public function executeVendingApi(sfWebRequest $request)
    {
        $this->forward404Unless($request->hasParameter('query'));
        $this->setTemplate('json');
        $payload = array();
        try {

            switch ($request->getParameter('query')) {
                case 'names':
                    $this->forward404Unless($request->hasParameter('code'));
                    $code = $request->getParameter('code');
                    $o = Doctrine::getTable('SACBuy')->findOneByCode($code);

                    if (!$o) {
                        throw new sfException('Record for code does not exist', 500);
                    }

                    $payload = array(
                        'recipient' => $o->recipient_first_name,
                        'consumer' => $o->SACUser->first_name
                    );
                    $o->redemption_date = new Doctrine_Expression('NOW()');
                    $o->RedemptionCode->used = true;
                    $o->redemption_status = SACBuyTable::REDEMPTION_STATUS_COMPLETE;
                    $o->save();
                    break;

                case 'delayed-update':
                    $this->forward404Unless($request->isMethod('post'));
                    $code = $request->getParameter('code');
                    $o = Doctrine::getTable('SACBuy')->findOneByCode($code);

                    if (!$o) {
                        throw new sfException('Record for code does not exist', 500);
                    }

                    $o->RedemptionCode->used = true;
                    $o->redemption_status = SACBuyTable::REDEMPTION_STATUS_COMPLETE;
                    $o->redemption_date = urldecode($request->getParameter('timestamp'));
                    $o->save();

                    break;

                case 'heartbeat':
                    break;
            }

        } catch (Exception $e) {
            $this->message = array(
                'message' => $e->getMessage(),
                'code'    => $e->getCode()
            );
            return sfView::ERROR;
        }

        $this->result = $payload;
    }

    public function executeCanfinish(sfWebRequest $request)
    {

        $this->pagename = "sharecan";

        $selected_name = strtok($request->getParameter("title"), " ");
        $selected_name = substr($selected_name, 0, 12);

        // Check profanity list
        if (in_array(strtolower($selected_name), $this->getProfanityList())) {
            $this->error = 1;
        }

        if (!ctype_alpha(strtolower($selected_name))) {

            if (strspn($selected_name, sfConfig::get('app_allowed_characters')) !== strlen($selected_name)) {
                $this->error = 2;
            }

        }

        $this->cokename = $selected_name;

        $this->cokeid = $request->getParameter("id");

        // Determin Text Positioning for GD
        $left = 65;
        if (strlen($selected_name) >= 6 & strlen($selected_name) <= 8) {
            $left = 45;
        } elseif (strlen($selected_name) >= 9 & strlen($selected_name) <= 10) {
            $left = 40;
        } elseif (strlen($selected_name) > 10) {
            $left = 30;
        }

        // Create text
        $image = imagecreatefromjpeg(sfConfig::get('sf_web_dir') . "/images/app/shareacoke/coke-share.jpg");

        $textCol = imagecolorallocate($image, 255, 255, 255);

        $textX = $left;
        $textY = 117;
        $font = sfConfig::get('sf_web_dir') . "/images/app/font/you.ttf";

        imagettftext($image, 20, 0, $textX, $textY, $textCol, $font, $selected_name);
        if (is_dir(sfConfig::get('sf_upload_dir') . "/app/shareacoke/")) {
            imagejpeg($image, sfConfig::get('sf_upload_dir') . "/app/shareacoke/" . $selected_name . ".jpg", 95);
        } else {
            echo "Directory not found";
            die();
        }

        $can = new SACVirtualCan();
        $can->SACUser = $this->getSACUser();
        $can->recipient_name = $selected_name;
        $can->recipient_id = $request->getParameter("id");
        $can->save();

    }

    public function executeStoryshare(sfWebRequest $request)
    {
        $this->town_list = json_encode($this->getNZTownList());

        $this->pagename = "sharestory";

        $this->shareform = new SACStoryForm();

        if ($request->isMethod('post')) {

            // employ basic server side validation
            if (!$request->getParameter('name') ||
                !$request->getParameter('email') ||
                !$request->getParameter('phone') ||
                !$request->getParameter('story')
            ) {
                $this->completion_server_error = true;
            }

            $this->shareform->bind($request->getParameter($this->shareform->getName()), $request->getFiles($this->shareform->getName()));
            if ($this->shareform->isValid()) {
                $this->shareform->save();
                $this->shareform->getObject()->SACUser = $this->getSACUser();
                $this->shareform->getObject()->save();
                $this->show_success = true;

                $fileName = $this->shareform->getObject()->getPic();
                if ($fileName) {
                    //Make the small thumb
                    $thumbnail = new sfThumbnail(150, 150);
                    $thumbnail->loadFile(sfConfig::get('sf_upload_dir') . '/app/shareacoke/stories/' . $fileName);

                    if (!file_exists(sfConfig::get('sf_upload_dir') . '/app/shareacoke/stories/thumb/')) {
                        mkdir(sfConfig::get('sf_upload_dir') . '/app/shareacoke/stories/thumb/');
                    }
                    $thumbnail->save(sfConfig::get('sf_upload_dir') . '/app/shareacoke/stories/thumb/' . $fileName);

                    //Make the large preview
                    $thumbnail = new sfThumbnail(800, 800);
                    $thumbnail->loadFile(sfConfig::get('sf_upload_dir') . '/app/shareacoke/stories/' . $fileName);

                    if (!file_exists(sfConfig::get('sf_upload_dir') . '/app/shareacoke/stories/preview/')) {
                        mkdir(sfConfig::get('sf_upload_dir') . '/app/shareacoke/stories/preview/');
                    }
                    $thumbnail->save(sfConfig::get('sf_upload_dir') . '/app/shareacoke/stories/preview/' . $fileName);
                }

                $this->redirect('@sharestorythanks');
            }
        }
    }

    public function executeStorysharethanks(sfWebRequest $request)
    {
        $this->show_success = true;
        $this->setTemplate('storyshare');
    }

    public function executePrivacy(sfWebRequest $request){}

    public function executeTerms(sfWebRequest $request)
    {
        $this->partial = $request->getParameter('p');

        if ($request->isXmlHttpRequest()) {
            $this->setTemplate('termspopup');
        }
    }

    public function executeNote(sfWebRequest $request)
    {
        $this->pagename = 'note';
        $this->tab_url = sfConfig::get('app_facebook_tab_url');

        $this->selected_name = $request->getParameter('name', null);
        $this->selected_id = $request->getParameter('id', null);
        $this->selected_note = $request->getParameter('type', null);
        $this->is_preview = $request->hasParameter('preview');

        if ($this->selected_note) {
            $note_config = sfConfig::get('app_share_a_note_' . $this->selected_note, array());

            $exploded = explode("\n", $note_config['pattern']);
            $this->pattern = '';
            $c = count($exploded) - 1;
            $i = 1;
            foreach ($exploded as $line) {
                if (strlen($line)) {
                    $first = ($i == 1 ? 'first' : '');
                    $last = ($i == $c ? 'last' : '');
                    $this->pattern .= "<div class=\"line gotham $first $last\">$line</div>";
                    $i++;
                }
            }
            $this->pattern .= '$$NAME$$';


            if ($request->isMethod('post') and $request->hasParameter('preview')) {

                $selections = array_map('htmlspecialchars', $request->getParameter('selection'));

                $this->dropdowns =  $selections;

                $message = vsprintf($note_config['pattern'], $selections);
                $this->message = str_replace('i ', 'I ', str_replace('$$coke$$', "'COKE'", str_replace("\n", " ", ucfirst(strtolower($message)))));

                $data = base64_encode(serialize(array(
                        'name' => $request->getParameter('name', null),
                        'type' => $request->getParameter('type', null),
                        'selections' => $selections
                )));
                $this->share_link = $this->generateUrl('note_view', array('data' => $data), true);

                $note = new SACNote();
                $note->SACUser = $this->getSACUser();
                $note->recipient_id = $this->selected_id;
                $note->recipient_name = $this->selected_name;
                $note->note_type = $this->selected_note;
                $note->note_selections = $this->dropdowns;
                $note->save();

            } else {

                $this->dropdowns = array();
                $i = 1;
                foreach ($note_config['options'] as $option) {
                    $choices = array_combine(array_values($option), array_values($option));
                    $select = new sfWidgetFormSelect(array('choices' => $choices));
                    $this->dropdowns[] = $select->render('selection[]', null, array('id' => 'selection_' . $i));
                    $i++;
                }
            }

        }

    }



    public function executeNoteView(sfWebRequest $request)
    {
        $this->forward404Unless($request->hasParameter('data'));

        $data = base64_decode($request->getParameter('data'));
        $this->forward404Unless($data);

        $data = @unserialize($data);
        $this->forward404Unless($data);
        $this->forward404Unless(is_array($data));

        $this->selected_name = $data['name'];
        $this->selected_note = $data['type'];
        $this->selections = $data['selections'];

        $note_config = sfConfig::get('app_share_a_note_' . $this->selected_note, array());

        $exploded = explode("\n", $note_config['pattern']);
        $this->pattern = '';
        $c = count($exploded) - 1;
        $i = 1;
        foreach ($exploded as $line) {
           if (strlen($line)) {
               $first = ($i == 1 ? 'first' : '');
               $last = ($i == $c ? 'last' : '');
               $this->pattern .= "<div class=\"line gotham $first $last\">$line</div>";
               $i++;
           }
        }
        $this->pattern .= '$$NAME$$';

        $this->setLayout(false);
    }

    public function executeCustomise(sfWebRequest $request)
    {
        $this->pagename = "customise";

    }

    public function executeNominate(sfWebRequest $request)
    {
        if ($request->getParameter('complete', false)) {
            $this->complete = true;

        } else {
            $form = new SACNominateNameForm();
            $this->complete = false;

            if ($request->isMethod('post')) {
                if ($form->bindAndSave($request->getParameter($form->getName()))) {
                    $this->redirect('@nominate_name_complete');
                }
            }

            $this->form = $form;
        }
    }

    public function executeMapApi(sfWebRequest $request)
    {
        $this->forward404Unless($request->hasParameter('query'));
        $friends_only = $request->getParameter('friendsOnly', false);

        if ($friends_only) {
            $this->initFacebook();

            $friends = $this->facebook->api('/me/friends?fields=id');

            $friends_list = array();
            foreach ($friends['data'] as $friend) {
                $friends_list[] = $friend['id'];
            }
        }

        $query = null;
        switch ($request->getParameter('query')) {
            case 'story' :
                $query = Doctrine::getTable('SACStory')->createQuery('s')
                    ->leftJoin('s.SACUser u');

                if ($friends_only) {
                    $query->whereIn('u.facebook_id', $friends_list)
                        ->orWhere('u.facebook_id = ?', $this->facebook->getUser());
                }
                break;

            case 'can' :
                $query = Doctrine::getTable('SACVirtualCan')->createQuery('c')
                    ->leftJoin('c.SACUser u');

                if ($friends_only) {
                    $query->whereIn('u.facebook_id', $friends_list)
                        ->orWhereIn('c.recipient_id', $friends_list)
                        ->orWhere('u.facebook_id = ?', $this->facebook->getUser());
                }
                break;

            case 'note' :
                $query = Doctrine::getTable('SACNote')->createQuery('n')
                    ->leftJoin('n.SACUser u');

                if ($friends_only) {
                    $query->whereIn('u.facebook_id', $friends_list)
                        ->orWhereIn('n.recipient_id', $friends_list)
                        ->orWhere('u.facebook_id = ?', $this->facebook->getUser());
                }
                break;
        }

        $this->forward404If($query === null);

        $this->result = $query->fetchArray();

        if ($request->getParameter('query') == 'note') {
            for ($i = 0; $i < count($this->result); $i++) {
                $note_config = sfConfig::get('app_share_a_note_' . $this->result[$i]['note_type'], array());
                $message = vsprintf($note_config['pattern'], $this->result[$i]['note_selections']);
                $this->result[$i]['full_message'] = str_replace('i ', 'I ', str_replace('$$coke$$', "'COKE'", str_replace("\n", " ", ucfirst(strtolower($message)))));
            }
        }

        $this->setTemplate('json');
    }


    public function getNZTownList()
    {
        return self::getCSVFileList('nz_city_list.csv');
    }

    public function getProfanityList()
    {
        return self::getCSVFileList('profanity_list.csv');
    }

    public static function getCSVFileList($filename)
    {
        $list = array();
        if (($file = fopen(sfConfig::get('sf_data_dir') . '/'.$filename, "r")) !== false) {
            while (($data = fgetcsv($file, 100, ",")) !== false) {
                $list[] = $data[0];
            }
            fclose($file);
        }
        return $list;
    }

    private function initFacebook()
    {
        require_once(sfConfig::get('sf_lib_dir').'/vendor/facebook-php-sdk/src/facebook.php');

        $this->facebook = new Facebook(array(
            'appId'  => sfConfig::get('app_facebook_appid'),
            'secret' => sfConfig::get('app_facebook_app_secret'),
            'status' => true,
            'cookie' => true
        ));

        $this->facebookUser = $this->facebook->api('/me');
    }

    private function getSACUser()
    {
        $this->initFacebook();
        $user = Doctrine::getTable('SACUser')->findOneBy('facebook_id', $this->facebook->getUser());

        //If the user doesn't exist, create a record
        if (!$user) {
            $user = new SACUser();
            $user->first_name = $this->facebookUser['first_name'];
            $user->last_name = $this->facebookUser['last_name'];
            $user->email = $this->facebookUser['email'];
            $user->facebook_id = $this->facebookUser['id'];
            $user->facebook_token = $this->facebook->getAccessToken();

            if (isset($this->facebookUser['location'])) {
                $user->location = $this->facebookUser['location']['name'];
                $location = explode(', ', $this->facebookUser['location']['name']);
                $geo = new Geocoder();
                $coords = $geo->coordinates(null, @$location[0], null, @$location[1]);
                $user->latitude = $coords['lat'];
                $user->longitude = $coords['lng'];
            }

            $user->save();
        }

        return $user;
    }

    public static function isInsideFacebook()
    {
        return isset($_POST['signed_request']);
    }

    public function executeSmsReceived(sfWebRequest $request)
    {
        return sfView::NONE;
    }
}

