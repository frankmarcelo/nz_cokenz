<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN""http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="/favicon.ico"/>
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
</head>
<body>
<?php if ($sf_user->isAuthenticated() and ($sf_user->isSuperAdmin() or $sf_user->hasPermission('admin'))): ?>
<div class="navbar">
    <div class="navbar-inner">
        <div class="container-fluid">
            <a class="brand" href="<?php echo url_for('@homepage') ?>">
                COCA-COLA New Zealand
            </a>

            <ul class="nav">
                <?php if ($sf_user->isSuperAdmin()): ?>
                <li class="dropdown">
                    <a href="<?php echo url_for('sf_guard_user') ?>" class="dropdown" class="dropdown-toggle"
                       data-toggle="dropdown">Users <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo url_for('sf_guard_user') ?>">Browse</a></li>
                        <li><a href="<?php echo url_for('sf_guard_group') ?>">Groups</a></li>
                        <li><a href="<?php echo url_for('sf_guard_permission') ?>">Permissions</a></li>
                        <li><a href="<?php echo url_for('@import_user') ?>">Import</a></li>
                        <li><a href="<?php echo url_for('@user_reporting') ?>">Reporting</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="<?php echo url_for('coke_location') ?>" class="dropdown" class="dropdown-toggle"
                       data-toggle="dropdown">Locations<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo url_for('coke_location') ?>">Stores</a></li>
                        <li><a href="<?php echo url_for('coke_location_type') ?>">Types</a></li>
                        <li><a href="<?php echo url_for('@import_location') ?>">Import Stores</a></li>
                    </ul>
                </li>
                <li class="dropdown">
                    <a href="<?php echo url_for('card_code') ?>" class="dropdown" class="dropdown-toggle"
                       data-toggle="dropdown">Loyalty Card<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo url_for('card_code') ?>">Codes</a></li>
                    </ul>
                </li>
                <?php endif; ?>

                <?php if ($sf_user->isSuperAdmin() or $sf_user->hasPermission('shareacoke')): ?>
                <li class="dropdown">
                    <a href="#" class="dropdown" class="dropdown-toggle"
                       data-toggle="dropdown">Share a Coke<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="<?php echo url_for('sac_user') ?>">Users</a></li>
                        <li><a href="<?php echo url_for('sac_story') ?>">Stories</a></li>
                        <li><a href="<?php echo url_for('sac_note') ?>">Notes</a></li>
                        <li><a href="<?php echo url_for('sac_virtual_can') ?>">Virtual Cans</a></li>
                        <li><a href="<?php echo url_for('sac_nominate_name') ?>">Nominated Names</a></li>
                        <li><a href="<?php echo url_for('sac_buy') ?>">Vending</a></li>
                        <li><a href="<?php echo url_for('sac_buy_redemption_code') ?>">Vending Codes</a></li>
                    </ul>
                </li>

                <?php endif; ?>
            </ul>

            <ul class="nav pull-right">

                <?php if ($sf_user->isSuperAdmin()): ?>
                <li><a href="<?php echo url_for(
                    'sf_guard_user_edit',
                    $sf_user->getGuardUser()
                ) ?>"><?php echo $sf_user->getGuardUser()->getName() ?></a></li>
                <li class="divider-vertical"></li>
                <?php endif; ?>

                <li><a href="<?php echo url_for('sf_guard_signout') ?>">Signout</a></li>

            </ul>

        </div>
    </div>
</div>
    <?php endif ?>

<div class="container-fluid">
    <?php echo $sf_content ?>
</div>
</body>
</html>
