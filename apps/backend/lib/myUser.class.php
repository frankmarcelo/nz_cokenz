<?php

class myUser extends sfGuardSecurityUser
{
    public function hasCredential($credential, $useAnd = true)
    {
        if ($this->isSuperAdmin()) {
            return true;
        }

        return parent::hasCredential($credential, $useAnd);
    }

}
