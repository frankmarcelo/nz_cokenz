<?php

require_once dirname(__FILE__).'/../lib/sac_storyGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sac_storyGeneratorHelper.class.php';

/**
 * sac_story actions.
 *
 * @package    Coke NZ
 * @subpackage sac_story
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sac_storyActions extends autoSac_storyActions
{
    public function executeApprove(sfWebRequest $request)
    {
        $obj = $this->getRoute()->getObject();
        $obj->setStatus(SACStory::STATUS_APPROVED);
        $obj->save();
        $this->redirect('sac_story');
    }

    public function executeDecline(sfWebRequest $request)
    {
        $obj = $this->getRoute()->getObject();
        $obj->setStatus(SACStory::STATUS_DECLINED);
        $obj->save();
        $this->redirect('sac_story');
    }
}
