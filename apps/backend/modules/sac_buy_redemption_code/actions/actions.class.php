<?php

require_once dirname(__FILE__).'/../lib/sac_buy_redemption_codeGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sac_buy_redemption_codeGeneratorHelper.class.php';

/**
 * sac_buy_redemption_code actions.
 *
 * @package    Coke NZ
 * @subpackage sac_buy_redemption_code
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sac_buy_redemption_codeActions extends autoSac_buy_redemption_codeActions
{
}
