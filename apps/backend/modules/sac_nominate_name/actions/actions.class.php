<?php

require_once dirname(__FILE__).'/../lib/sac_nominate_nameGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sac_nominate_nameGeneratorHelper.class.php';

/**
 * sac_nominate_name actions.
 *
 * @package    Coke NZ
 * @subpackage sac_nominate_name
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sac_nominate_nameActions extends autoSac_nominate_nameActions
{
}
