<?php

/**
 * coke_location module configuration.
 *
 * @package    Coke NZ
 * @subpackage coke_location
 * @author     Nik Spijkerman
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class coke_locationGeneratorConfiguration extends BaseCoke_locationGeneratorConfiguration
{
}
