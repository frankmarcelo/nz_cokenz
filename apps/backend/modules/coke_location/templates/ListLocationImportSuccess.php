
<div class="page-header">
    <h1>Import locations</h1>
</div>

<p><strong>Fields need to be (case sensitive!);</strong><br>
    name, address, suburb, city, postcode, phone, latitude (optional), longitude (optional)</p>
<hr>

<?php echo form_tag('coke_location/ListLocationImport', array('multipart' => true, 'class' => 'form-horizontal')); ?>
<fieldset>
    <div class="control-group <?php echo $form['location_type']->hasError() ? 'error' : ''; ?>">
        <?php echo $form['location_type']->renderLabel(null, array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form['location_type']->render(); ?>
            <span class="help-inline"><?php echo $form['location_type']->renderError(); ?></span>
        </div>
    </div>

    <div class="control-group <?php echo $form['data']->hasError() ? 'error' : ''; ?>">
        <?php echo $form['data']->renderLabel(null, array('class' => 'control-label')); ?>
        <div class="controls">
            <?php echo $form['data']->render(); ?>
            <span class="help-inline"><?php echo $form['data']->renderError(); ?></span>
        </div>
    </div>

<?php echo $form->renderHiddenFields() ?>
</fieldset>
<div class="form-actions">
    <div class="btn-group">
        <a class="btn" href="/backend.php/coke_location">Back to list</a>
        <button type="submit" class="btn btn-primary">Save</button>
    </div>
</div>

</form>

