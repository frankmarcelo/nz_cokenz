<?php

require_once dirname(__FILE__) . '/../lib/coke_locationGeneratorConfiguration.class.php';
require_once dirname(__FILE__) . '/../lib/coke_locationGeneratorHelper.class.php';

/**
 * coke_location actions.
 *
 * @package    Coke NZ
 * @subpackage coke_location
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class coke_locationActions extends autoCoke_locationActions
{
    public function executeListLocationImport(sfWebRequest $request)
    {
        $form = new LocationImportForm();

        if ($request->isMethod('post')) {

            $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));

            if ($form->isValid()) {

                $csv = $form->getValue('data');

                $row = 1;
                $handle = fopen($csv->getTempName(), "r");

                while (($data = fgetcsv($handle)) !== false) {

                    try {

                        if ($row == 1) {
                            $blank = array_search("", $data, true);

                            if (count($data) > 10) {
                                throw new sfException('CSV file is not formatted correctly. There may be an extra column?');
                            }

                            if ($blank != false) {
                                unset($data[$blank]);
                            }
                            foreach ($data as $d) {
                                $keys[] = strtolower($d);
                            }

                        } else {

                            $insert = @array_combine($keys, $data);

                            if (!$insert) {
                                throw new sfException('CSV file is not formatted correctly. There may be an extra column?');
                            }

                            if (!isset($insert['latitude']) or !isset($insert['longitude'])) {
                                $geo = new Geocoder();
                                $geo->coordinates(
                                    $insert['address'] . ', ' . $insert['suburb'],
                                    $insert['city'],
                                    $insert['postcode'],
                                    'New Zealand'
                                );
                                $insert['latitude'] = @$geo->coordinates['lat'];
                                $insert['longitude'] = @$geo->coordinates['lng'];

                                $pc = trim($insert['postcode']);
                                if (empty($pc)) {
                                    if ($geo->postal_code) {
                                    $insert['postcode'] = $geo->postal_code;
                                    } else {
                                        $insert['postcode'] = '0';
                                    }
                                }

                            }

                            $location = new CokeLocation();
                            $location->fromArray($insert);
                            $location->setShowInFinder(true);
                            $location->setTypeId($form->getValue('location_type'));
                            $location->save();
                        }


                    } catch (sfDoctrineException $e) {
                        $flash = $this->getUser()->getFlash('error');

                        if (empty($flash)) {
                            $flash = "The following record(s) could not be saved:<br>";
                        }
                        $flash .= "Record #" . $row . ": " . $insert['name'] . ". Error: " . $e->getMessage() . "<br>";
                        $this->getUser()->setFlash('error', $flash);
                        continue;

                    } catch (sfException $e) {
                        $this->getUser()->setFlash('error', $e->getMessage());
                    }

                    $row++;

                }
                fclose($handle);

                $this->getUser()->setFlash('notice', "Upload complete. $row rows inserted.");

                $this->redirect('coke_location/index');
            }
        }
        $this->form = $form;

    }

}
