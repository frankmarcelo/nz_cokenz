<?php

require_once dirname(__FILE__).'/../lib/sac_noteGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sac_noteGeneratorHelper.class.php';

/**
 * sac_note actions.
 *
 * @package    Coke NZ
 * @subpackage sac_note
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sac_noteActions extends autoSac_noteActions
{
}
