<?php

require_once dirname(__FILE__).'/../lib/card_codeGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/card_codeGeneratorHelper.class.php';

/**
 * card_code actions.
 *
 * @package    Coke NZ
 * @subpackage card_code
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class card_codeActions extends autoCard_codeActions
{
}
