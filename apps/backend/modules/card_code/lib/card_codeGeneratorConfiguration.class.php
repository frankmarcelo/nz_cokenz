<?php

/**
 * card_code module configuration.
 *
 * @package    Coke NZ
 * @subpackage card_code
 * @author     Nik Spijkerman
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class card_codeGeneratorConfiguration extends BaseCard_codeGeneratorConfiguration
{
}
