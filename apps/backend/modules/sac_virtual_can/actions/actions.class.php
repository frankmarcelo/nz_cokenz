<?php

require_once dirname(__FILE__).'/../lib/sac_virtual_canGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sac_virtual_canGeneratorHelper.class.php';

/**
 * sac_virtual_can actions.
 *
 * @package    Coke NZ
 * @subpackage sac_virtual_can
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sac_virtual_canActions extends autoSac_virtual_canActions
{
}
