<?php

/**
 * promo_reporting actions.
 *
 * @package    powerade_nz
 * @subpackage promo_reporting
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class user_reportingActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $filtered = false;
    $filterForm = new sfForm();
    $filterForm->setWidgets(array(
      'date_from' => new sfWidgetFormDate(array('format' => '%day%/%month%/%year%','default' => date('m/d/Y')),array('class'=>'input-small')),
      'date_to' => new sfWidgetFormDate(array('format' => '%day%/%month%/%year%','default' => date('m/d/Y')),array('class'=>'input-small'))
    ));
    $filterForm->setValidators(array(
      'date_from' => new sfValidatorDate(),
      'date_to' => new sfValidatorDate()
    ));
    $filterForm->getWidgetSchema()->setNameFormat('filter[%s]');


    if($request->isMethod('post') and !$request->getParameter('reset') == 'reset'){
      $filterForm->bind($request->getParameter($filterForm->getName()));
      if($filterForm->isValid()){
        $filtered = true;
      }
    }

    $conn = strtolower(Doctrine_Manager::connection()->getDriverName());

    //    Region breakdown
    $regions_query = Doctrine::getTable('sfGuardUserProfile')->createQuery('p')
                      ->select('p.region, count(u.id) count')
                      ->leftJoin('p.User u')
                      ->groupBy('p.region')
                      ->orderBy('p.region');
    if($filtered) {
      $regions_query->where('created_at >= ?', $filterForm->getValue('date_from'))->andWhere('created_at <= ?', $filterForm->getValue('date_to'));
    }

    $this->regions = $regions_query->execute(array(),Doctrine::HYDRATE_ARRAY_SHALLOW);


    //Unique users
    $users_query = Doctrine::getTable('sfGuardUser')->createQuery('u')->leftJoin('u.Profile p');
    if($filtered) {
      $users_query->where('created_at >= ?', $filterForm->getValue('date_from'))->andWhere('created_at <= ?', $filterForm->getValue('date_to'));
    }
    $this->total_users = count($users_query->fetchArray());
    $this->mobile_users = count($users_query->where('p.registration_source = ?', 'mobile')->fetchArray() );
    $this->desktop_users = count($users_query->where('p.registration_source = ?', 'desktop')->fetchArray() );

    //Age breakdown
    $ages_query = Doctrine::getTable('sfGuardUser')->createQuery('u')
              ->distinct()
              ->leftJoin('u.Profile p')
              ->orderBy('p.dob desc');
    if($filtered) {
      $ages_query->where('created_at >= ?', $filterForm->getValue('date_from'))->andWhere('created_at <= ?', $filterForm->getValue('date_to'));
    }

    $ages_query_result = $ages_query->execute();

    $ages = array();
    foreach($ages_query_result as $user){
      $user_age = $user->Profile->getAge();
      if( !isset($ages[$user_age]) ){
        $ages[$user_age] = 1;
      } else {
        $ages[$user_age]++;
      }
    }

    $ranges = array('14-19','20-24','25-29','30-34','35-39','40-44','45-50','50-60','60-70','70+');
    $this->ages = array();
    foreach($ranges as $range){
      $min = intval(substr($range,0,2));
      $max = intval(substr($range,3,2));
      if($max == false){
        $max = 130;
      }
      $c = 0;
      for( $i = $min; $i <= $max; $i++ ){
        if(isset( $ages[$i])){
          $c += $ages[$i];
        }
      }

      $this->ages[$range] = $c;
    }

    $this->filter_form = $filterForm;

  }
}
