<?php use_stylesheet('/bootstrapAdminThemePlugin/css/bootstrap.min.css') ?>
<div class="container-fluid">

  <form action="<?php echo url_for('@user_reporting') ?>" method="post" class="well form-inline">
    <?php echo $filter_form ?>
    <button type="submit" class="btn">Go</button>
    <button type="submit" class="btn" value="reset" name="reset">Reset</button>
  </form>

  <div style="float:left; width:50%">
    <h2>Unique users</h2>
    <table class="table table-bordered table-striped" style="width: auto;">
      <tr>
        <th>Desktop</th>
        <td><?php echo $desktop_users ?></td>
        </tr>
        <tr>
        <th>Mobile</th>
        <td><?php echo $mobile_users ?></td>
        </tr>
        <tr>
            <th>Total</th>
            <td><?php echo $total_users ?></td>
        </tr>

    </table>



      <h2>Age breakdown</h2>
      <table class="table table-bordered table-striped" style="width: auto;">
        <thead>
          <tr>
            <th>Age</th>
            <th>Count</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach($ages as $key => $val): ?>
          <tr>
            <td><?php echo $key ?></td>
            <td><?php echo $val ?></td>
          </tr>
      <?php endforeach; ?>
        </tbody>
      </table>

  </div>
  <div style="float:left; width:50%">
    <h2>Region breakdown</h2>
      <table class="table table-bordered table-striped" style="width: auto;">
        <thead>
          <tr>
            <th>Region</th>
            <th>Count</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach($regions as $region): ?>
          <tr>
            <td><?php echo $region['region'] ?></td>
            <td><?php echo $region['count'] ?></td>
          </tr>
        <?php endforeach; ?>

        </tbody>
      </table>


  </div>







  


</div>
