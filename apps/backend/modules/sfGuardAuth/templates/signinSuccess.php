<div style="margin: 100px 0 0 0;">
  <div class="row">
    <div class="span4 offset6" id="backendSigninForm">
      <h1 style="margin: 6px">Login</h1>

      <?php echo get_partial('sfGuardAuth/signin_form', array('form' => $form)) ?>
    </div>
  </div>
</div>
