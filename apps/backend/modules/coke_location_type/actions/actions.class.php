<?php

require_once dirname(__FILE__).'/../lib/coke_location_typeGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/coke_location_typeGeneratorHelper.class.php';

/**
 * coke_location_type actions.
 *
 * @package    Coke NZ
 * @subpackage coke_location_type
 * @author     Nik Spijkerman
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class coke_location_typeActions extends autoCoke_location_typeActions
{
}
