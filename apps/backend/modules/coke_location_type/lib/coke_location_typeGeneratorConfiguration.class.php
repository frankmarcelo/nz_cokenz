<?php

/**
 * coke_location_type module configuration.
 *
 * @package    Coke NZ
 * @subpackage coke_location_type
 * @author     Nik Spijkerman
 * @version    SVN: $Id: configuration.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class coke_location_typeGeneratorConfiguration extends BaseCoke_location_typeGeneratorConfiguration
{
}
