<?php

require_once dirname(__FILE__).'/../lib/sac_userGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sac_userGeneratorHelper.class.php';

/**
 * sac_user actions.
 *
 * @package    Coke NZ
 * @subpackage sac_user
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sac_userActions extends autoSac_userActions
{
}
