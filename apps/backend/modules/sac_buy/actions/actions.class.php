<?php

require_once dirname(__FILE__).'/../lib/sac_buyGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sac_buyGeneratorHelper.class.php';

/**
 * sac_buy actions.
 *
 * @package    Coke NZ
 * @subpackage sac_buy
 * @author     
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sac_buyActions extends autoSac_buyActions
{
}
