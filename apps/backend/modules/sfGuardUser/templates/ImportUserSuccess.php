
<?php echo $form->renderGlobalErrors() ?>
<form method="post" action="<?php echo url_for('@import_user') ?>" enctype="multipart/form-data">
  <p><?php echo $form['user_list']->renderRow() ?></p>
  <button class="btn btn-primary" type="submit" name="submit">Import</button>
  <?php echo $form->renderHiddenFields() ?>
</form>
