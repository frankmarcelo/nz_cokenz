<?php foreach( $form['Profile'] as $field ):  //var_dump($field);exit; ?>

<?php if ( $field->isHidden() ) continue ?>
<div class="control-group <?php $field->hasError() and print ' error' ?>">
    <?php echo $field->renderLabel(null, array('class' => 'control-label')) ?>

    <div class="controls">
      <?php echo $field->render() ?>
      <?php if ( $field->hasError()): ?>
        <p class="help-inline"><?php echo $field->renderError() ?></p>
      <?php endif; ?>

      <?php if ( $field->renderHelp()): ?>
        <p class="help-block"><?php echo $field->renderHelp() ?></p>
      <?php endif; ?>
    </div>
</div>
<?php endforeach; ?>

<?php echo  $form['Profile']->renderHiddenFields() ?>

