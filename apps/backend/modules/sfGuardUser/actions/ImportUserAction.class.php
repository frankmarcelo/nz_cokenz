<?php

class ImportUserAction extends sfAction
{
  public function execute($request)
  {
    $this->form = new sfForm();
    $this->form->setWidget('user_list', new sfWidgetFormInputFile());
    $this->form->setValidator('user_list', new sfValidatorFile(array(
    	  'required' => true,
    	  'mime_types' => array('text/csv', 'text/plain'),
        'max_size' => (1024 * 1024 * 10),
    	  'path' => sfConfig::get('sf_upload_dir')
    )));

    $this->form->getWidgetSchema()->setNameFormat('upload[%s]');


    if( $request->isMethod('post')){

      $this->form->bind( $request->getParameter($this->form->getName()), $request->getFiles($this->form->getName()) );

      if($this->form->isValid()){

        try {
          $csv = $this->form->getValue('user_list');

          $row = 1;
          $keys = array();
          $handle = fopen($csv->getTempName(), "r");
          while (($data = fgetcsv($handle)) !== FALSE) {

            if($row == 1)
            {
              $blank = array_search("",$data,true);
              if( $blank != false ){
                unset($data[$blank]);
              }
              foreach($data as $d){
                $keys[] = strtolower($d);
              }

            } else {

              $insert = @array_combine($keys,$data);

              if(!$insert){
                throw new sfException('CSV file is not formatted correctly. There may be an extra column?');
              }

              $not_saved = 0;
              try {

                $user = new sfGuardUser();
                $user->first_name = $insert['first_name'];
                $user->last_name = $insert['last_name'];
                $user->email_address = strtolower($insert['email_address']);
                $user->username = strtolower($insert['email_address']);
                $user->setPassword($insert['password']);
                $user->is_active = true;

                $user->Profile->email_updates = ( $insert['email_updates'] ? true : false );

                $dob = new DateTime($insert['date_of_birth']);
                if($dob->getTimestamp() > time() ) {
                  $dob = $dob->sub(new DateInterval('P100Y'));
                }

                $user->Profile->dob = $dob->format('Y-m-d');
                $user->Profile->gender = ( strtolower($insert['gender']) == 'm' ? 'male' : 'female' );
                $user->Profile->phone = $insert['phone'];
                $user->Profile->postcode = ( strlen($insert['postcode']) !== 4 ? str_pad($insert['email_updates'],4,'0',STR_PAD_LEFT ) : $insert['postcode'] );
                $user->Profile->region = $insert['region'];
                $user->save();

              } catch ( Exception $e) {
                $not_saved++;
                continue;
              }


            }

            $row++;

          }
          fclose($handle);

          $this->getUser()->setFlash('notice',"Upload complete. $row rows inserted. $not_saved were not saved.");

        } catch( sfException $e) {
          $this->getUser()->setFlash('error',$e->getMessage());
        }
        $this->redirect('sfGuardUser/index');
      }





    }





  }
}
