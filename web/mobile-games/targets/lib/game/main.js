ig.module( 
	'game.main' 
)
.requires(
	'impact.game'
	,'impact.font'
	,'game.entities.cap'
	,'game.entities.captrail'
	,'game.entities.chain1'
	,'game.entities.chain2'
	,'game.entities.chain3'
	,'game.entities.target'
	,'game.entities.targetsub'
	,'game.entities.slingshot'
	,'game.levels.map'
	,'game.menus'
)
.defines(function(){

MyGame = ig.Game.extend({
	
	// Load a font
	font: new ig.Font( 'media/arialfont.png' ),
	font2: new ig.Font( 'media/bigfont.png' ),
	imgtime: new ig.Image( 'media/Time.png' ),
	imgscore: new ig.Image( 'media/score.png' ),
	imglives: new ig.Image( 'media/lives.png' ),
	imgdistance: new ig.Image( 'media/distance.png' ),
	imginfo: new ig.Image( 'media/instructions.png' ),
	imglifeGO: new ig.Image( 'media/gameover.png' ),
	imgtimeGO: new ig.Image( 'media/gameovertime.png' ),
	imgloselife: new ig.Image( 'media/loselife.png' ),
	Score: 0,
	ScoreGained: 0,
	Lives: 3,
	GameTime: 120,
	distance:0,
	ScoreTimer:1,
	PanTimer:1,
	fromx:0,
	fromy:0,
	
	game_over: false,
	send_post: false,
	
	menu: null,
	start_background: new ig.Image('media/start_bg.jpg'),
	
		
	init: function() {
		// Initialize your game here; bind keys etc.
		ig.input.bind(ig.KEY.MOUSE1, 'click');
		this.loadLevel(LevelMap)
		var cap = ig.game.getEntitiesByType( EntityCap )[0];
		cap.GameState = 1;
		
		this.setTitleMenu();

	},
	
	update: function() {
		// Update all entities and backgroundMaps
		this.parent();
		var cap = ig.game.getEntitiesByType( EntityCap )[0];
		var captrail = ig.game.getEntitiesByType( EntityCaptrail )[0];
		var target = ig.game.getEntitiesByType( EntityTarget )[0];
		var targetsub = ig.game.getEntitiesByType( EntityTargetsub )[0];
		
		AA = cap.pos.x-target.pos.x;
		if(AA<0) AA *= -1;
		BB = cap.pos.y-target.pos.y;
		if(BB<0) BB *= -1;
		this.distance = (Math.sqrt((AA*AA)+(BB*BB))*2);
		
		if(cap.GameState == 1)
		{ //gamestart
			/*if(ig.input.released('click'))
			{
				cap.GameState = 3;
				cap.kill();
				ig.game.spawnEntity( EntityCap);
			}*/
		}else if(cap.GameState == 7)
		{ //pan to new target
			if(this.PanTimer > 0.5)
			{
				this.PanTimer -= ig.system.tick / ((this.PanTimer*2)); //ease in
			}else
			{
				this.PanTimer -= ig.system.tick / (1/(this.PanTimer*2)); //ease out
			}
			ig.game.screen.x = fromx - 160 + ((target.pos.x - fromx) * (1-this.PanTimer));
			ig.game.screen.y = fromy - 208 + ((target.pos.y - fromy) * (1-this.PanTimer));
			target.currentAnim.alpha = 1 - this.PanTimer;
			targetsub.currentAnim.alpha = this.PanTimer;
			if(this.PanTimer <0.01)
			{
				cap.GameState = 3;
				cap.kill();
				ig.game.spawnEntity( EntityCap);
			}	
		}else if(cap.GameState == 6)
		{ //showscore
			this.ScoreTimer -= ig.system.tick;
				
			if(this.ScoreTimer < 0)
			{
				targetsub.pos.x = target.pos.x;
				targetsub.pos.y = target.pos.y;
				targetsub.currentAnim.alpha = 1;
				while(true)
				{
					target.currentAnim.alpha = 0;
					target.pos.x = Math.random() * 1536 + 256;
					target.pos.y = Math.random() * 1536 + 256;
					if(((target.pos.x < 736)||(target.pos.x > 1376)) && ((target.pos.y < 1376)||(target.pos.y > 1440))) break;
				}
				
				fromx = ig.game.screen.x + 160;
				fromy = ig.game.screen.y + 208;
				this.PanTimer = 1;
				cap.GameState = 7;
				
			}
			
		}else if(cap.GameState == 2)
		{ //lose a life
			
			this.setLifeMenu();
			
			/*if(ig.input.released('click'))
			{
				cap.kill()
				ig.game.spawnEntity( EntityCap);
			}*/
			
		} else if((cap.GameState == 5)||(cap.GameState == 4))
		{ //game over
			if(cap.GameState == 5) {
				this.setTimeoutMenu();
			} else if(cap.GameState == 4) {
				this.setOverMenu();
			}
			
			
			if(this.game_over == false) {
				this.game_over = true;
			}
			
			/*if(ig.input.released('click'))
			{
				cap.GameState = 1;
				this.Score = 0;
				this.GameTime = 120;
				this.Lives =3;
				
			}*/
			
		} else if(cap.GameState ==3)
		{ //game on
		
			this.GameTime -= ig.system.tick;
			if(this.GameTime < 0 )
			{ //out of time
				this.PanTimer = 1;
				cap.GameState = 5;
			}
			
			if(cap.LaunchState)
			{
				ig.game.spawnEntity(EntityCaptrail,cap.pos.x,cap.pos.y);
			}
			if(cap.EndState)
			{
				score = 500 - this.distance;
				if(score > 0)
				{
					if(this.distance < 512) this.ScoreGained = 50;
					if(this.distance < 256) this.ScoreGained = 100;
					if(this.distance < 100) this.ScoreGained = 500;
					this.Score += this.ScoreGained;
					this.ScoreTimer =1;
					cap.GameState = 6;  // score time
				}
				else
				{
					this.Lives -= 1;
					this.PanTimer = 1;
					cap.GameState = 2;
					if(this.Lives < 1) cap.GameState = 4; 
				}
				
				cap.EndState = false;
				
				
				if(cap.GameState == 3)
				{
					cap.kill()
					ig.game.spawnEntity( EntityCap);
				} 
			}
		}
	},
	
	draw: function() {
		// Draw all entities and backgroundMaps
		this.parent();
		var cap = ig.game.getEntitiesByType( EntityCap )[0];
		var w = ig.system.realWidth;
		var h = ig.system.realHeight;
		ig.system.context.fillStyle = '#000000';
		
		//debug show gamestate
		//this.font.draw( cap.GameState.toFixed(0), 130, ig.system.realHeight -30, ig.Font.ALIGN.LEFT );
		
		if(cap.GameState == 1)
		{//show instructions
			//this.imginfo.draw(  0, 0 );
			this.start_background.draw(0, 0);
			
		} else if(cap.GameState == 6)
		{//show score pop up
			this.font2.draw( this.ScoreGained.toFixed(0), w/2, (h/2)-120 + (this.ScoreTimer*80), ig.Font.ALIGN.CENTER );
		}
		
		if(cap.GameState !=1)
		{
			// Add your own drawing code here
			this.imgtime.draw(  10, 10 );
			this.imgscore.draw(  10, 40 );
			this.imglives.draw(  10, 70 );
			this.imgdistance.draw(  10, ig.system.realHeight -60 );
					
			this.font.draw( this.GameTime.toFixed(0), 90, 12, ig.Font.ALIGN.LEFT );
			this.font.draw( this.Score.toFixed(0), 90, 42, ig.Font.ALIGN.LEFT );
			this.font.draw( this.Lives.toFixed(0), 90, 72, ig.Font.ALIGN.LEFT );
			this.font.draw( this.distance.toFixed(0), 10, h -30, ig.Font.ALIGN.LEFT );
		}
	},
		
	setTitleMenu: function () {
		this.menu = new TitleMenu();
		this.menu.show();
	},
	
	setLifeMenu: function() {
		this.menu = new LifeMenu();
		this.menu.show();
	},
	
	setTimeoutMenu: function() {
		this.menu = new TimeoutMenu();
		this.menu.show();
	},
	
	setOverMenu: function() {
		this.menu = new OverMenu();
		this.menu.show();
	},
	
	startGame: function() {
		var cap = ig.game.getEntitiesByType( EntityCap )[0];
		cap.kill();
		ig.game.spawnEntity( EntityCap);
		var cap = ig.game.getEntitiesByType( EntityCap )[0];
		cap.GameState = 3;
		this.send_post = false;
		this.menu.hide();
	},
	continueGame: function() {
		var cap = ig.game.getEntitiesByType( EntityCap )[0];
		cap.kill()
		ig.game.spawnEntity( EntityCap);
		this.menu.hide();
	},
	submitGame: function() {
		if(this.send_post == false) {
			// POST TO LEADERBOARD API
			$.post('coke', {game_id: 1, score: this.Score});
			
			this.send_post = true;
			this.restartGame();
		}
	},
	restartGame: function() {
		var cap = ig.game.getEntitiesByType( EntityCap )[0];
		cap.GameState = 1;
		this.Score = 0;
		this.GameTime = 120;
		this.Lives = 3;
		this.game_over = false;
		this.menu.hide();
		this.menu = new TitleMenu();
		this.menu.show();
	}
});


// Start the Game with 60fps, a resolution of 320x240, scaled
// up by a factor of 2
ig.main( '#canvas', MyGame, 60, 320, 416, 1 );

});
