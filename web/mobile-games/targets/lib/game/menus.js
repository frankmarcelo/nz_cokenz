ig.baked = true;
ig.module(
	'game.menus'
)
.requires(
	'impact.game',
	'impact.entity',
	'impact.font'
)
.defines(function () {
    MenuItem = ig.Class.extend({
		selector: null,
		on_click: function() {},
		init: function() {
			var this_item = this;
			$(this.selector).click(function(e) {
				e.preventDefault();
				this_item.on_click();
			});	
		}
    });
    Menu = ig.Class.extend({
        items: [],
        init: function () {
			this.items = [];
            for (var i = 0; i < this.itemClasses.length; i++) {
                this.items.push(new this.itemClasses[i]());
            }
        },
        draw: function () {
           
        },
		show: function() {
			$('#menus').show();
			$(this.selector).show();
		},
		hide: function() {
			$('#menus').hide();
			$(this.selector).hide();
		}
		
    });
	
	
	MenuPlay = MenuItem.extend({
		selector: '.play_btn',
        on_click: function () {
			ig.game.startGame();
        }
    });
	ContinuePlay = MenuItem.extend({
		selector: '.continue_btn',
        on_click: function () {
			ig.game.continueGame();
        }
    });

	RestartButton = MenuItem.extend({
		selector: '.restart_btn',
        on_click: function () {
			ig.game.restartGame();
        }
    });
	
    TitleMenu = Menu.extend({
		selector: '.title_menu',
        itemClasses: [MenuPlay]
    });
	
    LifeMenu = Menu.extend({
		selector: '.life_menu',
        itemClasses: [ContinuePlay]
    });
	
    TimeoutMenu = Menu.extend({
		selector: '.timeout_menu',
        itemClasses: [RestartButton]
    });
	
    OverMenu = Menu.extend({
		selector: '.over_menu',
        itemClasses: [RestartButton]
    });
	
});