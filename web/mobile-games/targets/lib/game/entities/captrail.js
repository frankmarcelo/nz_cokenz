ig.module(
	'game.entities.captrail'
)
.requires(
	'impact.entity'
	,'game.entities.target'
	
	
)
.defines(function(){

    EntityCaptrail = ig.Entity.extend
    ({
        
        size: {x:34, y:34},
        collides: ig.Entity.COLLIDES.NEVER,
        offset: {x: 22, y: 22},
        type: ig.Entity.TYPE.A,
        animSheet: new ig.AnimationSheet( 'media/puck.png', 44, 44 ),
	bounciness: 1,
        maxVel: {x: 500, y: 500},
	startVel: {x: 10, y: 10},
	
	zIndex: 19,
        life:0.3,
        
        init:function (x,y,settings)
        {
            this.parent(x,y,settings);
            this.addAnim( 'idle', 0.1, [0]);
        },
        update:function()
        {
            
            this.parent();
            this.life -= ig.system.tick;
            this.currentAnim.alpha = this.life ;
            if(this.life < 0 ) this.kill();
            
        }
            
    });
}
);