ig.module(
	'game.entities.cap'
)
.requires(
	'impact.entity'
	,'game.entities.target'
	
)
.defines(function(){

    EntityCap = ig.Entity.extend
    ({
        
        size: {x:34, y:34},
        collides: ig.Entity.COLLIDES.NEVER,
        offset: {x: 22, y: 22},
        type: ig.Entity.TYPE.A,
        animSheet: new ig.AnimationSheet( 'media/puck.png', 44, 44 ),
	bounciness: 1,
        maxVel: {x: 500, y: 500},
	startVel: {x: 10, y: 10},
	
	zIndex: 10,
	
        SpeedX:0,
	SpeedY:0,
	Friction:1.3,
	
	GameState:3,
	LaunchState : false,
	StartState:true,
	StartPanTimer : 1,
	EndState:false,
	Detach: false,
	
        init:function (x,y,settings)
        {
            this.parent(x,y,settings);
            this.addAnim( 'idle', 0.1, [0] );
	    this.StartState = true;
	    this.Detach = false;
	    this.LauchState = false;
	    this.StartPanTimer=1;
	    this.EndState = false;
            this.pos.x = 1056;
            this.pos.y = 1056;
        },
	
	update: function()
	{
	    this.parent();
	    if(this.GameState == 3)
	    {
		if(this.StartState)
		{
		    var target = ig.game.getEntitiesByType( EntityTarget )[0];
		    if(this.StartPanTimer > 0.5)
		    {
			this.StartPanTimer -= ig.system.tick / ((this.StartPanTimer*2));
		    }else
		    {
			this.StartPanTimer -= ig.system.tick / (1/(this.StartPanTimer*2));
		    }
		    
		    ig.game.screen.x = target.pos.x - 160 - ((target.pos.x - 1056) * (1 - this.StartPanTimer));
		    ig.game.screen.y = target.pos.y - 208 - ((target.pos.y - 1056) * (1 - this.StartPanTimer));;
		    if(this.StartPanTimer < 0.01) this.StartState=false;
		}else
		{
		    if(ig.input.state("click") && !this.LaunchState) //&& this.inFocus()
		    {
			    nX = ig.input.mouse.x + ig.game.screen.x ;
			    nY = ig.input.mouse.y + ig.game.screen.y ;
			    this.pos.x=nX;
			    this.pos.y=nY;
			    
		    }
		    if(ig.input.released('click') && !this.LaunchState)
		    {
			this.LaunchState = true;
			SpeedX = -(this.pos.x - 1056);
			SpeedY = -(this.pos.y - 1056);
			pantimer=1;
		    }
		    
		    if(this.LaunchState)
		    {
			if(pantimer > 0.01)
			{
			    pantimer = pantimer - ig.system.tick * pantimer *3;
			    OffsetX = (this.pos.x - 1056) * pantimer;
			    OffsetY = (this.pos.y - 1056) * pantimer;
			    if(pantimer < 0.6) this.Detach = true;
			}else
			{
			    OffsetX =0;
			    OffsetY =0;
			}
			
			ig.game.screen.x = this.pos.x - 160 - OffsetX;
			ig.game.screen.y = this.pos.y - 208 - OffsetY;
			this.pos.x += SpeedX * ig.system.tick * 10;
			this.pos.y += SpeedY * ig.system.tick * 10;
			if(((SpeedX < 1) && (SpeedX > -1)) && ((SpeedY < 1) && (SpeedY > -1)))
			{
			    SpeedX = 0;
			    SpeedY = 0;
			    this.EndState = true;
			} else
			{
			    SpeedX -=  SpeedX * this.Friction * ig.system.tick;
			    SpeedY -=  SpeedY * this.Friction * ig.system.tick;
			}
		    }
		}
	    }
	}
    });
}
);