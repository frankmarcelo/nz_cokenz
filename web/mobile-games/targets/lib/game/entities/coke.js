ig.module(
	'game.entities.coke'
)
.requires(
	'impact.entity',
        'game.entities.cap'
)
.defines(function(){

EntityCoke = ig.Entity.extend
(
{
    
    size: {x:192, y:192},
    collides: ig.Entity.COLLIDES.NEVER,
    offset: {x: 96, y: 96},
    type: ig.Entity.TYPE.A,
    animSheet: new ig.AnimationSheet( 'media/coke.png', 192  , 192 ),
    zIndex: 1,
    
    init:function (x,y,settings)
    {
        this.parent(x,y,settings);
        this.addAnim( 'idle', 0.1, [0] );
        this.pos.x = 1056;
        this.pos.y = 1056;
	//this.currentAnim.alpha = 0.9;
    },
    
    update:function()
    {
	
    }

}
);
}
);
