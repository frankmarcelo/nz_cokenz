ig.module(
	'game.entities.chain1'
)
.requires(
	'impact.entity',
        'game.entities.cap'
)
.defines(function(){

EntityChain1 = ig.Entity.extend
(
{
    
    size: {x:200, y:32},
    collides: ig.Entity.COLLIDES.NEVER,
    offset: {x: 100, y: 16},
    type: ig.Entity.TYPE.A,
    animSheet: new ig.AnimationSheet( 'media/chain1.png', 200, 32 ),
    zIndex: 5,


    init:function (x,y,settings)
    {
        this.parent(x,y,settings);
        this.addAnim( 'idle', 0.1, [0] );
        this.pos.x = 1056;
        this.pos.y = 1056;
    },
    update:function()
    {
	var cap = ig.game.getEntitiesByType( EntityCap )[0];
	this.currentAnim.angle = Math.atan((cap.pos.y-1056)/(cap.pos.x-1056)) + 1.57079633;
	if(!cap.Detach)
	{
	    this.pos.x = cap.pos.x - ((cap.pos.x-1056)*0.75);
	    this.pos.y = cap.pos.y - ((cap.pos.y-1056)*0.75);
	}
    }
}
);
}
);
