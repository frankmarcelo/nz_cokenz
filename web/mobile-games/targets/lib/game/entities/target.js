ig.module(
	'game.entities.target'
)
.requires(
	'impact.entity'
)
.defines(function(){

EntityTarget = ig.Entity.extend
(
{
    
    size: {x:512, y:512},
    collides: ig.Entity.COLLIDES.NEVER,
    offset: {x: 256, y: 256},
    type: ig.Entity.TYPE.A,
    animSheet: new ig.AnimationSheet( 'media/target.png', 512, 512 ),
    zIndex: 1,

    init:function (x,y,settings)
    {
        this.parent(x,y,settings);
        this.addAnim( 'idle', 0.1, [0]);
	/*
	while(true)
	{
	    this.pos.x = Math.random() * 1536 + 256;
	    this.pos.y = Math.random() * 1536 + 256;
	    if(((this.pos.x < 736)||(this.pos.x > 1376)) && ((this.pos.y < 1376)||(this.pos.y > 1440))) break;
	}
	*/
    }

}
);
}
);
