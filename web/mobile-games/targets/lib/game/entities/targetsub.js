ig.module(
	'game.entities.targetsub'
)
.requires(
	'impact.entity'
)
.defines(function()
{

    EntityTargetsub = ig.Entity.extend
    (
    {
    
        size: {x:512, y:512},
        collides: ig.Entity.COLLIDES.NEVER,
        offset: {x: 256, y: 256},
        type: ig.Entity.TYPE.A,
        animSheet: new ig.AnimationSheet( 'media/target.png', 512, 512 ),
        zIndex: 1,
    
        init:function (x,y,settings)
        {
            this.parent(x,y,settings);
            this.addAnim( 'idle', 0.1, [0]);
            this.pos.x = 6000;
            this.pos.y = 6000;
        }

    }
    );
}
);