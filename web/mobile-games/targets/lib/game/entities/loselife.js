ig.module(
	'game.entities.loselife'
)
.requires(
	'impact.entity',
        'game.entities.cap'
)
.defines(function(){

EntityLoselife = ig.Entity.extend
(
{
    
    size: {x:640, y:960},
    collides: ig.Entity.COLLIDES.NEVER,
    offset: {x: 0, y: 0},
    type: ig.Entity.TYPE.A,
    animSheet: new ig.AnimationSheet( 'media/MenuBG.jpg', 640  , 960 ),
    zIndex: 20,
    
    init:function (x,y,settings)
    {
        this.parent(x,y,settings);
        this.addAnim( 'idle', 0.1, [0] );
        this.pos.x = 3056;
        this.pos.y = 3056;
    },
    
    update:function()
    {
	if(ig.input.released('click') && !this.LaunchState)
	{
            
        }
    }

}
);
}
);
