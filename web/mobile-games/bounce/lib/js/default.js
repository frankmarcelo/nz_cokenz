function init() {
	window.scroll(0,1);
	window.document.addEventListener('touchmove',preventDefaultScroll, false);
}

function preventDefaultScroll(event) {
	event.preventDefault();
	window.scroll(0,1);
	return false;
};
    
	
