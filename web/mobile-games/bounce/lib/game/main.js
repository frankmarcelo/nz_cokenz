ig.module(
	'game.main' 
)
.requires(
	'impact.game',
	'impact.font',
	
	'game.entities.puck',
	'game.entities.paddle-cpu',
	'game.entities.paddle-player',
	'game.entities.pause',
	'game.entities.powerup',
	
	'game.menus',
	
	'game.levels.main',
	'game.levels.home'
)
.defines(function(){
	
	MyGame = ig.Game.extend({
		
		font: new ig.Font( 'media/font_gotham.png' ),
		
		start_background: new ig.Image('media/home_bg.jpg'),
		game_background: new ig.Image('media/bg.gif'),
		
		scored: false,
		score_player: 0,
		score_cpu: 0,
		current_level: 1,
		
		startPopup: 0,
		paused: 0,
		
        menu: null,
        mode: 1,
		
		timer: null,
		
		init: function() {
			ig.input.bind(ig.KEY.MOUSE1, 'click');
			
			this.setTitleMenu();
		},
		
		update: function() {
			this.parent();
			
			var puck = ig.game.getEntitiesByType( EntityPuck )[0];
			var paddle = ig.game.getEntitiesByType( EntityPaddlePlayer )[0];
			var cpu_paddle = ig.game.getEntitiesByType( EntityPaddleCpu )[0];
			
			if(puck) {
				var puck_pos = puck.pos;
				var puck_y = puck_pos.y;
				var puck_x = puck_pos.x;
				if(puck_y <= 0 || puck_y > ig.system.height) {
					if(puck_y <= 0 && ig.game.scored == false) {
						this.score_player += 1;
						this.scored = true;
					} else if(puck_y > ig.system.height && ig.game.scored == false) {
						this.score_cpu += 1;
						this.scored = true;
					} 
					this.current_level += 0.1;
					
					if(cpu_paddle.frozen == true) {
						cpu_paddle.unFreeze();
					}
					if(cpu_paddle.puck_fire == true) {
						cpu_paddle.resetCollide();
					}
					if (paddle.double == true) {
						paddle.unDouble();
					}
					
					puck.kill();
					
					if(this.score_player >= 11 || this.score_cpu >= 11) {
						this.setGameOver();
					} else {
						this.reset_puck();
					}
					
				}
			}
			
           /* if (this.menu) {
                this.menu.update();
            }*/
			
		},
		
		draw: function() {
			this.parent();
			 
			if (this.mode == 0) {
                this.drawGame();
            } else if (this.mode == 1) {
                this.drawTitle();
            }
			
           /* if (this.menu) {
                this.menu.draw();
            }*/
		},
        drawGame: function () {
        	this.font.draw(this.score_player, 39, 200, ig.Font.ALIGN.CENTER);
			this.font.draw(this.score_cpu, 60, 200, ig.Font.ALIGN.CENTER);
			
			var powerup = ig.game.getEntitiesByType( EntityPowerup )[0];
			if(!powerup) {
				if(this.paused != 1 && this.time) {
					if (this.time.delta() > 15){
						ig.game.spawnEntity( EntityPowerup);
						this.time.reset();
					}
				}
			}
			
			if(this.entities.length == 0) {
				this.loadLevel(LevelMain);
				
			} /*else {
				if(this.startPopup == 0) {
					this.menu = new StartMenu();
					this.menu.show();
					this.pauseGame();
				}
			}*/
			
			
        },
		
        drawTitle: function () {
			this.start_background.draw(0, 0);
        },
		
		loadLevel: function( data ) {
			this.parent( data );
			this.spawnEntity(EntityPause, 278, 5);
			this.sortEntitiesDeferred();
			this.time = new ig.Timer();
			
		},
		
		
        setTitleMenu: function () {
            this.reset_game();
            this.mode = 1;
           	this.menu = new TitleMenu();
			this.unPauseGame();
			this.menu.show();
        },
		
		setGame: function () {
            this.reset_game();
			this.menu.hide();
            //this.menu = new PauseMenu();
			this.menu = new StartMenu();
			this.menu.show();
            this.mode = 0;
			this.pauseGame();
        },
		
		setGameOver: function () {
            this.menu = new GameOverMenu();
			var text = this.score_player >= 11 ? 'YOU WIN' : 'YOU LOSE';
			$('h1',this.menu.selector).html(text);
  			this.pauseGame();
		},
		
		pauseGame: function () {
			this.paused = 1;
 			this.menu.show();
 			ig.system.stopRunLoop.call(ig.system);
		},
		
		unPauseGame: function () {
 			this.menu.hide();
			this.paused = 0;
			ig.system.startRunLoop.call(ig.system);
        },
		
		reset_game: function() {
			this.entities = [];
			this.scored = false;
			this.score_player = 0;
			this.score_cpu = 0;
			this.current_level = 1;
		},
	
		reset_puck: function() {
			ig.game.spawnEntity(EntityPuck,200,200);
			this.scored = false;
		},
	});
		
	
	MyLoader = ig.Loader.extend({


		draw: function() {
			/*var background = new ig.Image( 'media/load_bg.jpg' );
			background.draw(0, 0);*/
			
			var w = ig.system.realWidth;
			var h = ig.system.realHeight;
			ig.system.context.fillStyle = '#000000';
			ig.system.context.fillRect( 0, 0, w, h );
	
			var percentage = (this.status * 100).round() + '%';
			ig.system.context.font = '20px sans-serif';;
			ig.system.context.fillStyle = '#ffffff';
			ig.system.context.fillText( percentage, w/2,  h/2 );
		}
	});
	
	ig.main( '#canvas', MyGame, 60, 320, 416, 1 );
	
	/*if( ig.ua.iPhone4 ) {
		ig.main( '#canvas', MyGame, 60, 640, 832, 1, MyLoader );
	} else if( ig.ua.mobile ) {
		ig.main( '#canvas', MyGame, 60, 640, 832, 0.5, MyLoader );
	} else {
		ig.main( '#canvas', MyGame, 60, 640, 832, 1, MyLoader );
	}*/
	
});
