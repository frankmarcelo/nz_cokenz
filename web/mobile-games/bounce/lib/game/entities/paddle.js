ig.module(
	'game.entities.paddle'
)
.requires(
	'impact.entity'
)
.defines(function(){

EntityPaddle = ig.Entity.extend({
	
	size: {x:80, y:18},
	collides: ig.Entity.COLLIDES.FIXED,
	
	animSheet: new ig.AnimationSheet( 'media/paddle_cpu.png', 80, 18 ),
    freezeAnimSheet: new ig.AnimationSheet( 'media/paddle_freeze.png', 80, 18 ),
    doubleAnimSheet: new ig.AnimationSheet( 'media/paddle_double.png', 154, 18 ),
	
	zIndex: 10,
	
	timer: null,
	
	frozen: false,
	double: false,
	
	init: function( x, y, settings ) {
		this.parent( x, y, settings );
		
		this.addAnim( 'idle', 1, [0] );
		
		var freeze = new ig.Animation( this.freezeAnimSheet, 0.1, [0] );
		this.anims['freeze'] = freeze;
		
		var double = new ig.Animation( this.doubleAnimSheet, 0.1, [0] );
		this.anims['double'] = double;
	},
	
	update: function() {
		
		if(this.pos.x <= 0) {
			this.pos.x = 0;	
		} else if(this.pos.x >= ig.system.width - this.size.x) {
			this.pos.x = ig.system.width - this.size.x;	
		}
		
		this.parent();
	},
	
	setFreeze: function() {
		this.currentAnim = this.anims.freeze;
		this.frozen = true;
	},
	
	setDouble: function() {
		this.currentAnim = this.anims.double;
		this.size = {x:154, y:18};
		this.double = true;
	},
	
	unFreeze: function() {
		this.currentAnim = this.anims.idle;
		this.frozen = false;
	},
	
	unDouble: function() {
		this.currentAnim = this.anims.idle;
		this.size = {x:80, y:18};
		this.double = false;
	}
});

});