ig.module(
	'game.entities.powerup'
)
.requires(
	'impact.entity'
)
.defines(function(){

	EntityPowerup = ig.Entity.extend({
		
		size: {x:16, y:16},
		collides: ig.Entity.COLLIDES.LITE,
		
		animSheet: null,
		
		zindex: 5,
		
		mode: 0,
		powerup_images: ['media/powerup_flame.png','media/powerup_freeze.png','media/powerup_paddle.png'],
		
		collidedWith: null,
		
		init: function( x, y, settings ) {
			this.parent( x, y, settings );
			
			this.mode = Math.floor(Math.random() * 3);
			//this.animSheet = new ig.AnimationSheet('media/powerup.png', 16, 16 );
			this.animSheet = new ig.AnimationSheet(this.powerup_images[this.mode], 20, 20 );
			
			this.addAnim( 'idle', 0.1, [0] );
			
			var min_x = 40;
			var max_x = ig.system.width-40;
			var min_y = 80;
			var max_y = ig.system.height-80;

			var random_x = Math.floor(Math.random() * (max_x - min_x + 1)) + min_x;//Math.random() * (ig.system.width-150) + 150;
			var random_y = Math.floor(Math.random() * (max_y - min_y + 1)) + min_y;//Math.random() * (ig.system.height-150) + 150;
			
			this.pos.x = random_x;
			this.pos.y = random_y;
			
			
		},
		
		
		collideWith: function(other,axis) {
			var powerUp = new Powerup(this.mode);
			this.kill();
		}
		
	});
	
    Powerup = ig.Class.extend({
		
		init: function(mode) {
			var puck = ig.game.getEntitiesByType( EntityPuck )[0];
			var paddle = ig.game.getEntitiesByType( EntityPaddlePlayer )[0];
			var cpu_paddle = ig.game.getEntitiesByType( EntityPaddleCpu )[0];
			if(puck) {
				if(mode == 0) {
					this.flame(puck,cpu_paddle);
				} else if(mode == 1)  {
					this.freeze(cpu_paddle);
				} else if(mode == 2)  {
					this.double(paddle);
				}
			}
		},
		
		flame: function(puck,cpu_paddle) {
			puck.setFlame(cpu_paddle);
		},
		
		freeze: function(paddle) {
			paddle.setFreeze();
		},
		
		double: function(paddle) {
			paddle.setDouble();
			
		}
		
    });

});