ig.module(
	'game.entities.pause'
)
.requires(
	'impact.entity'
)
.defines(function(){

EntityPause = ig.Entity.extend({
	
	size: {x:35, y:34},
	collides: ig.Entity.COLLIDES.NEVER,
	checkAgainst: ig.Entity.TYPE.A,
	
	animSheet: new ig.AnimationSheet( 'media/btn_pause.gif', 35, 34 ),
	
	zIndex: 1,
	
	init: function( x, y, settings ) {
		this.parent( x, y, settings );
		
		this.addAnim( 'idle', 1, [0] );
		
	},
	
	draw: function() {
		this.parent();
		
		if(ig.game.paused == 0) {
			if(ig.input.pressed("click") && this.inFocus()) {
				ig.game.pauseGame();
			}
		}
	},
		 
	inFocus: function() {
		return (
			(this.pos.x <= (ig.input.mouse.x + ig.game.screen.x)) &&
			((ig.input.mouse.x + ig.game.screen.x) <= this.pos.x + this.size.x) &&
			(this.pos.y <= (ig.input.mouse.y + ig.game.screen.y)) &&
			((ig.input.mouse.y + ig.game.screen.y) <= this.pos.y + this.size.y)
		);
	}
});

});