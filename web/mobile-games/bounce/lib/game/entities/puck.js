ig.module(
	'game.entities.puck'
)
.requires(
	'impact.entity'
)
.defines(function(){

EntityPuck = ig.Entity.extend({
	
	size: {x:34, y:34},
	collides: ig.Entity.COLLIDES.ACTIVE,
	offset: {x: 5, y: 5},
	type: ig.Entity.TYPE.A,
	
	animSheet: new ig.AnimationSheet( 'media/puck.png', 44, 44 ),
    fireAnimSheet: new ig.AnimationSheet( 'media/puck_fire.png', 44, 66 ),
	
	bounciness: 1,
	
	maxVel: {x: 800, y: 800},
	startVel: {x: 0, y: 0},
	
	zIndex: 10,
	
	animationDirection: 0.01,
	
	speed: 400,
	curspeed: 250,
	
	flame: false,
	angle: null,
	
	just_bounced: false,
		
	init: function( x, y, settings ) {
		this.parent( x, y, settings );
		
		this.addAnim( 'idle', 0.1, [0] );
		var fire = new ig.Animation( this.fireAnimSheet, 0.1, [0] );
		this.anims['fire'] = fire;

		this.pos.x = ig.system.width/2 - this.size.x/2;
		this.pos.y = ig.system.height/2 - this.size.y/2;
		
		var ax = this.getRandAngle();
		
		var random_sin = Math.sin(ax)*this.speed;
		var random_cos = Math.cos(ax)*this.speed;
		
		this.startVel.x = random_sin;
		this.startVel.y = random_cos;
		
		this.vel.x = this.startVel.x;
		this.vel.y = this.startVel.y; 
		
		if(this.vel.x > 150) this.vel.x = 150;
		if(this.vel.x < -150) this.vel.x = -150;
		if(this.vel.y > 0)
			{
				if(this.vel.x > 0)
					this.vel.y = this.speed - this.vel.x;
				else
					this.vel.y = this.speed + this.vel.x;
			}else
			{
				if(this.vel.x > 0)
					this.vel.y = -this.speed + this.vel.x;
				else
					this.vel.y = -this.speed - this.vel.x;
			}
		this.curspeed=this.speed;
	},
	
	getRandAngle: function() {
		var ax = Math.random() * 6.28;
		if((ax > 4 && ax < 5.5) || (ax > 0.8 && ax < 2.3)) {
			ax = this.getRandAngle();
		}
		return ax;
	},
	
	update: function() {
        this.parent();
		
		if(this.flame != true) {
			this.currentAnim.angle += this.animationDirection;
		}
		
		var puck_pos = this.pos;
		
		var puck_x = puck_pos.x - ig.game.screen.x;
        if( puck_x >= (ig.system.width - this.size.x)){
			this.pos.x = ig.system.width - this.size.x;
			this.vel.x *= -1;
        } else if(puck_x <= 0) {
			this.pos.x = 0;
			this.vel.x *= -1;
		}
		
		if(this.flame != true && ((puck_pos.y <= 17 && (puck_pos.x <= 61 || puck_pos.x + this.size.x >= 259)) || (puck_pos.y + this.size.y >= 399 && (puck_pos.x <= 61 || puck_pos.x + this.size.x >= 259)))) {
			if(this.just_bounced == false) {
				this.vel.y *= -1;
			}
			this.just_bounced = true;
		} else {
			this.just_bounced = false;
		}
		
		/*if( this.pos.y < 57 || this.pos.y > 359 ) {
			this.collides = ig.Entity.COLLIDES.NEVER;
		}*/
		
	},
	
	draw: function() {
		this.parent();
		
		if(this.flame == true) {
			//this.vel.x *= 1.10;
			this.vel.y *= 1.10;
		}
	},
	
	setFlame: function(paddle) {
		this.angle = this.angleTo(paddle);
		//this.angle = Math.atan2((ig.system.height/2) - (this.pos.y + this.size.y/2),(ig.system.width/2) - (this.pos.x + this.size.x/2));
		this.vel.x = Math.cos(this.angle)*20;
		this.vel.y = Math.sin(this.angle)*20;
		
		paddle.collides = ig.Entity.COLLIDES.NEVER;
		paddle.puck_fire = true;	
		this.flame = true;
		
		this.currentAnim = this.anims.fire;
		this.currentAnim.angle = 0;
	},
		
	collideWith: function(other,axis) {
		if(axis == 'y') {
			
			/*this.curspeed *= 1.05;
			//this.vel.x *= 1.05;
			//this.vel.y *= 1.05;
			
			var paddle_middle = other.pos.x+(other.size.x/2);
			var puck_middle =  this.pos.x+(this.size.x/2);
			
			this.vel.x -= ( paddle_middle - puck_middle)*1.5;
			if(this.vel.x > 150) this.vel.x = 150;
			if(this.vel.x < -150) this.vel.x = -150;
			if(this.vel.y > 0)
			{
				if(this.vel.x > 0)
					this.vel.y = this.curspeed - this.vel.x;
				else
					this.vel.y = this.curspeed + this.vel.x;
			}else
			{
				if(this.vel.x > 0)
					this.vel.y = -this.curspeed + this.vel.x;
				else
					this.vel.y = -this.curspeed - this.vel.x;
			}	*/		
		}
		
		
	}
});

});