ig.module(
	'game.entities.paddle-player'
)
.requires(
	'game.entities.paddle'
)
.defines(function(){

EntityPaddlePlayer = EntityPaddle.extend({
	
	animSheet: new ig.AnimationSheet( 'media/paddle_player.png', 80, 18 ),
	
	maxVel: {x: 1000, y: 1000},
	
	update: function() {
		
		if(ig.input.state("click") && this.inFocus()) {
			nX = ig.input.mouse.x - this.size.x/2;
			this.pos.x=nX;
		}
		
		this.parent();
	},
		 
	inFocus: function() {
		return (
			(this.pos.x-100 <= (ig.input.mouse.x + ig.game.screen.x)) &&
			((ig.input.mouse.x + ig.game.screen.x) <= this.pos.x + this.size.x+100) &&
			(this.pos.y-20 <= (ig.input.mouse.y + ig.game.screen.y)) &&
			((ig.input.mouse.y + ig.game.screen.y) <= this.pos.y + this.size.y+40)
		);
	}
});

});