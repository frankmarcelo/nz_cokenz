ig.module(
	'game.entities.paddle-cpu'
)
.requires(
	'game.entities.paddle',
	'game.entities.puck'
)
.defines(function(){

EntityPaddleCpu = EntityPaddle.extend({
	
	maxVel: {x: 400, y: 400},
	
	puck_fire: false,
	
	update: function() {
		
		var puck = ig.game.getEntitiesByType( EntityPuck )[0];
		
		if(puck) {
			if(!this.frozen) {
				var dist = (puck.pos.x+puck.size.x/2) - (this.pos.x + this.size.x/2);
				this.vel.x = dist*ig.game.current_level;
			} else {
				this.vel.x = 0;
			}
		} 
		this.parent();
	}, 
	
	resetCollide: function() {
		this.collides = ig.Entity.COLLIDES.FIXED;
		this.puck_fire = false;
	}
});

});