var play_home_url;
if (window.location.host == '64.14.225.195') {
    play_home_url = '/mobile.php/play';
} else {
    play_home_url = '/play';
}

ig.baked = true;
ig.module(
	'game.menus'
)
.requires(
	'impact.game',
	'impact.entity',
	'impact.font'
)
.defines(function () {
    MenuItem = ig.Class.extend({
		selector: null,
		on_click: function() {},
		init: function() {
			var this_item = this;
			$(this.selector).click(function(e) {
				e.preventDefault();
				this_item.on_click();
			});	
		}
    });
    Menu = ig.Class.extend({
        items: [],
        init: function () {
			this.items = [];
            for (var i = 0; i < this.itemClasses.length; i++) {
                this.items.push(new this.itemClasses[i]());
            }
			//console.log(this.items);
			
        },
//        update: function () {
//			for (var i = 0; i < this.items.length; i++) {
//				var this_item = this.items[i];
//				var selector = this_item.selector;
//				/*if (ig.input.pressed('click')) {
//					console.log(selector);
//					this_item.on_click();
//                }*/
//				/*if(selector) {
//					$(selector).click(function() {
//						console.log(selector);
//						//console.log($(selector).html());
//						this_item.on_click();
//					});
//				}*/
//			}
//        },
        draw: function () {
           
        },
		show: function() {
			$('#menus').show();
			$(this.selector).show();
		},
		hide: function() {
			$('#menus').hide();
			$(this.selector).hide();
		}
		
    });
	
	
	MenuTap = MenuItem.extend({
		selector: '.tap_btn',
        on_click: function () {
			ig.game.menu.hide();
			ig.game.startPopup = 0;
 			ig.game.unPauseGame();
            ig.game.menu = new PauseMenu();
       }
    });
	MenuPlay = MenuItem.extend({
		selector: '.play_btn',
        on_click: function () {
			ig.game.setGame();
        }
    });
	MenuClose = MenuItem.extend({
		selector: '.close_btn',
        on_click: function () {
			ig.game.unPauseGame();
        }
    });
	MenuRestart = MenuItem.extend({
		selector: '.restart_btn',
        on_click: function () {
			ig.game.setGame();
        }
    });
	MenuHome = MenuItem.extend({
		selector: '.home_btn',
        on_click: function () {
			window.location = play_home_url;
        }
    });
	MenuHowTo = MenuItem.extend({
		selector: '.howto_btn',
        on_click: function () {
			ig.game.menu.hide();
			ig.game.setTitleMenu();
		}
    });
	MenuAgain = MenuItem.extend({
		selector: '.again_btn',
        on_click: function () {
			ig.game.setGame();
        }
    });
	
    TitleMenu = Menu.extend({
		selector: '.title_menu',
        itemClasses: [MenuPlay]
    });
	
    PauseMenu = Menu.extend({
		selector: '.pause_menu',
        itemClasses: [MenuClose,MenuRestart,MenuHome,MenuHowTo]
    });
	
    StartMenu = Menu.extend({
		selector: '.start_menu',
        itemClasses: [MenuTap]
    });
	
    GameOverMenu = Menu.extend({
		selector: '.over_menu',
        itemClasses: [MenuAgain, MenuHome]
    });
});