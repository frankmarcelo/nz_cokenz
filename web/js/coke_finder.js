var map;
var markers = [];
var infoWindow;

function initialiseMap() {
  var latlng = new google.maps.LatLng(-41.178654,174.287109);
  var myOptions = {
    zoom: 5,
    center: latlng,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    disableDefaultUI: true
  }
  infoWindow = new google.maps.InfoWindow();
  map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);


  if ( getParameterByName('lat') != '' && getParameterByName('long') != ''  ) {
    var position = {
      latitude: getParameterByName('lat'),
      longitude: getParameterByName('long')
    }
    console.log(position);
    positionSuccess(position);
  } else {
    prepareGeolocation();
    doGeolocation();
  }
}

function initialiseList() {
  prepareGeolocation();
  doListGeolocation();
}

function doGeolocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(positionSuccess, positionError);
  } else {
    positionError(-1);
  }
}

function doListGeolocation() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(listPositionSuccess, positionError);
  } else {
    positionError(-1);
  }
}

function positionError(err) {
  var msg;
  switch(err.code) {
    case err.UNKNOWN_ERROR:
      msg = "Unable to find your location.";
      break;
    case err.PERMISSION_DENINED:
      msg = "Permission denied in finding your location.";
      break;
    case err.POSITION_UNAVAILABLE:
      msg = "Your location is currently unknown.";
      break;
    case err.BREAK:
      msg = "Attempt to find location took too long.";
      break;
    default:
      msg = "Your location cannot be detected.";
  }
  window.location.href = '/mobile.php/find-a-coke/search?message='+encodeURIComponent(msg);
}

function listPositionSuccess(position) {
  var coords = position.coords || position.coordinate || position;
  var href = window.location.pathname;

  $.get(href + '/html?lat=' + encodeURIComponent( coords.latitude ) + '&long=' + encodeURIComponent( coords.longitude ),function(locationList){
    $('#location_list').html(locationList);
  });


}
function positionSuccess(position) {

  // Centre the map on the new location
  var coords = position.coords || position.coordinate || position;
  var latLng = new google.maps.LatLng(coords.latitude, coords.longitude);
  map.setCenter(latLng);
  map.setZoom(13);
  var marker = new google.maps.Marker({
    map: map,
    position: latLng,
    title: 'Your location'
  });

  var href = $('#list_view_link').attr('href');
  $('#list_view_link').attr('href', href + '?lat=' + encodeURIComponent( coords.latitude ) + '&long=' + encodeURIComponent( coords.longitude ) );


  //Pin the locations on the map
  $.get(href + '/json?lat=' + encodeURIComponent( coords.latitude ) + '&long=' + encodeURIComponent( coords.longitude ),function(locations){
    var bounds = new google.maps.LatLngBounds();
    $.each(locations, function(i,location){
      var latlng = new google.maps.LatLng(
          parseFloat(location.latitude),
          parseFloat(location.longitude)
      );
      createMarker(latlng, location.name, location.address, location.suburb, location.type_slug, location.distance );

    });
  });

  // And reverse geocode.
  (new google.maps.Geocoder()).geocode({latLng: latLng}, function(resp) {
    var place = "You're around here somewhere!";
    if (resp[0]) {
      var bits = [];
      for (var i = 0, I = resp[0].address_components.length; i < I; ++i) {
        var component = resp[0].address_components[i];
        if (contains(component.types, 'political')) {
          bits.push('<b>' + component.long_name + '</b>');
        }
      }
      if (bits.length) {
        place = bits.join(' > ');
      }
      marker.setTitle(resp[0].formatted_address);
    }

  });
}

function createMarker(latlng, name, address, suburb, type_slug, distance) {
  var html =
    '<div class="info_window">' +
      '<div class="icon icon_' + type_slug + '"></div>' +
      '<div class="location">'+
        '<span class="name">' + name + '</span><br>' +
        '<span class="address">' + address + '</span><br>' +
        '<span class="suburb">' + suburb + ' (' + distance + 'km)</span>' +
      '</div>' +
    '</div>';

  var marker_image = "";
  switch (type_slug) {
    case 'mcdonalds' :
      marker_image = 'map-marker-mcdonalds.png';
      break;
    case 'z-energy':
      marker_image = 'map-marker-zenergy.png';
      break;
    case 'caltex':
      marker_image = 'map-marker-caltex.png';
      break;
    case 'fix':
      marker_image = 'map-marker-fix.png';
      break;
    case 'bp':
      marker_image = 'map-marker-bp.png';
      break;
    case 'mobil':
      marker_image = 'map-marker-mobil.png';
      break;
    case 'hell-pizza':
      marker_image = 'map-marker-hellpizza.png';
      break;
    case 'wendys':
      marker_image = 'map-marker-wendys.png';
      break;
    default:
      marker_image = 'map-marker-2x.png';
  }



  var marker = new google.maps.Marker({
    map: map,
    position: latlng,
    optimized: false,
    icon: new google.maps.MarkerImage(
      '/images/mobile/' + marker_image,
      null,
      null,
      new google.maps.Point(14,40),
      new google.maps.Size(28,40)
    )
  });
  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.setContent(html);
    infoWindow.open(map, marker);
  });
  markers.push(marker);
}


function contains(array, item) {
  for (var i = 0, I = array.length; i < I; ++i) {
    if (array[i] == item) return true;
  }
  return false;
}

function getParameterByName(name)
{
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
  var regexS = "[\\?&]" + name + "=([^&#]*)";
  var regex = new RegExp(regexS);
  var results = regex.exec(window.location.search);
  if(results == null)
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

// From
// http://google-ajax-examples.googlecode.com/svn/trunk/whereareyou/scripts/geometa.js
function prepareGeolocation(opt_force) {
if ( opt_force || typeof navigator.geolocation == "undefined" || navigator.geolocation.shim ) (function(){

// -- BEGIN GEARS_INIT
(function() {
  // We are already defined. Hooray!
  if (window.google && google.gears) {
    return;
  }

  var factory = null;

  // Firefox
  if (typeof GearsFactory != 'undefined') {
    factory = new GearsFactory();
  } else {
    // IE
    try {
      factory = new ActiveXObject('Gears.Factory');
      // privateSetGlobalObject is only required and supported on WinCE.
      if (factory.getBuildInfo().indexOf('ie_mobile') != -1) {
        factory.privateSetGlobalObject(this);
      }
    } catch (e) {
      // Safari
      if ((typeof navigator.mimeTypes != 'undefined')
           && navigator.mimeTypes["application/x-googlegears"]) {
        factory = document.createElement("object");
        factory.style.display = "none";
        factory.width = 0;
        factory.height = 0;
        factory.type = "application/x-googlegears";
        document.documentElement.appendChild(factory);
      }
    }
  }

  // *Do not* define any objects if Gears is not installed. This mimics the
  // behavior of Gears defining the objects in the future.
  if (!factory) {
    return;
  }

  // Now set up the objects, being careful not to overwrite anything.
  //
  // Note: In Internet Explorer for Windows Mobile, you can't add properties to
  // the window object. However, global objects are automatically added as
  // properties of the window object in all browsers.
  if (!window.google) {
    google = {};
  }

  if (!google.gears) {
    google.gears = {factory: factory};
  }
})();
// -- END GEARS_INIT

var GearsGeoLocation = (function() {
    // -- PRIVATE
    var geo = google.gears.factory.create('beta.geolocation');

    var wrapSuccess = function(callback, self) { // wrap it for lastPosition love
        return function(position) {
            callback(position);
            self.lastPosition = position;
        }
    }

    // -- PUBLIC
    return {
        shim: true,

        type: "Gears",

        lastPosition: null,

        getCurrentPosition: function(successCallback, errorCallback, options) {
            var self = this;
            var sc = wrapSuccess(successCallback, self);
            geo.getCurrentPosition(sc, errorCallback, options);
        },

        watchPosition: function(successCallback, errorCallback, options) {
            geo.watchPosition(successCallback, errorCallback, options);
        },

        clearWatch: function(watchId) {
            geo.clearWatch(watchId);
        },

        getPermission: function(siteName, imageUrl, extraMessage) {
            geo.getPermission(siteName, imageUrl, extraMessage);
        }

    };
})();

var AjaxGeoLocation = (function() {
    // -- PRIVATE
    var loading = false;
    var loadGoogleLoader = function() {
        if (!hasGoogleLoader() && !loading) {
            loading = true;
            var s = document.createElement('script');
            s.src = 'http://www.google.com/jsapi?callback=_google_loader_apiLoaded';
            s.type = "text/javascript";
            document.getElementsByTagName('body')[0].appendChild(s);
        }
    };

    var queue = [];
    var addLocationQueue = function(callback) {
        queue.push(callback);
    }

    var runLocationQueue = function() {
        if (hasGoogleLoader()) {
            while (queue.length > 0) {
                var call = queue.pop();
                call();
            }
        }
    }

    window['_google_loader_apiLoaded'] = function() {
        runLocationQueue();
    }

    var hasGoogleLoader = function() {
        return (window['google'] && google['loader']);
    }

    var checkGoogleLoader = function(callback) {
        if (hasGoogleLoader()) return true;

        addLocationQueue(callback);

        loadGoogleLoader();

        return false;
    };

    loadGoogleLoader(); // start to load as soon as possible just in case

    // -- PUBLIC
    return {
        shim: true,

        type: "ClientLocation",

        lastPosition: null,

        getCurrentPosition: function(successCallback, errorCallback, options) {
            var self = this;
            if (!checkGoogleLoader(function() {
                self.getCurrentPosition(successCallback, errorCallback, options);
            })) return;

            if (google.loader.ClientLocation) {
                var cl = google.loader.ClientLocation;

                var position = {
                    latitude: cl.latitude,
                    longitude: cl.longitude,
                    altitude: null,
                    accuracy: 43000, // same as Gears accuracy over wifi?
                    altitudeAccuracy: null,
                    heading: null,
                    velocity: null,
                    timestamp: new Date(),

                    // extra info that is outside of the bounds of the core API
                    address: {
                        city: cl.address.city,
                        country: cl.address.country,
                        country_code: cl.address.country_code,
                        region: cl.address.region
                    }
                };

                successCallback(position);

                this.lastPosition = position;
            } else if (errorCallback === "function")  {
                errorCallback({ code: 3, message: "Using the Google ClientLocation API and it is not able to calculate a location."});
            }
        },

        watchPosition: function(successCallback, errorCallback, options) {
            this.getCurrentPosition(successCallback, errorCallback, options);

            var self = this;
            var watchId = setInterval(function() {
                self.getCurrentPosition(successCallback, errorCallback, options);
            }, 10000);

            return watchId;
        },

        clearWatch: function(watchId) {
            clearInterval(watchId);
        },

        getPermission: function(siteName, imageUrl, extraMessage) {
            // for now just say yes :)
            return true;
        }

    };
})();

// If you have Gears installed use that, else use Ajax ClientLocation
navigator.geolocation = (window.google && google.gears && google.gears.factory.create) ? GearsGeoLocation : AjaxGeoLocation;

})();
}
