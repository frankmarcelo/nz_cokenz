$(document).ready(function ()
{
  // see if we are creating/editing user
  // there's some sort of dependency on naming which is hopefully wont be changed
  if ($('#sf_guard_user_username'))
  {
    $('#sf_guard_user_email_address').on('keyup', function()
    {
      // copy content
      $('#sf_guard_user_username').val($('#sf_guard_user_email_address').val());
    })
  }

  $('*[rel=popover]').popover();

  $('.fancybox').fancybox();
});