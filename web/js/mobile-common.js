// Some app wide settings
var re_non_empty = /\S/;

$.mobile.pushStateEnabled = false;

$(document).bind('pageinit', function()
{

  // configure transitions
//  $.mobile.defaultPageTransition = 'fade';
  $.mobile.ajaxEnabled = false;

  // configure AJAX spinner
  $(document).ajaxStart(function()
  {
    $.mobile.showPageLoadingMsg('a', '');
  });
  $(document).ajaxComplete(function()
  {
    $.mobile.hidePageLoadingMsg();
  });

  // assign icons in the play section
  if ($('.coca-cola-play-wrap'))
  {
    $('.coca-cola-play-wrap a.coca-cola-app-button').each(function()
    {
      // icon names are set in data-icon attribute which supposed get use bu jQuery mobile,
      // but we define no separate style for that
      // and apply icons dynamically based on whatever name is provided
      $(this).find('.ui-icon').css('background-image', 'url(/images/mobile/game_button_icons/'+ $(this).attr('data-icon') +'.png)');
    });
  }

  /*
   * Handle registration form
   **/
  var $register_form = $('#register_form');
  if ($register_form)
  {
    $($register_form).find('input[type="submit"]').on('click', function(e)
      {
        var scroll_offset = null;

        // prevent anything by default
        e.preventDefault();

        // populate user name
        $('#username').val($('#email').val());

        // send to save
        $.post(
          $register_form.attr('action'),
          $register_form.serialize()
        )
          .success(function(data)
          {
            // see what is actual result
            if (data.success)
            {
              // redirect to requested URL
              $.mobile.changePage(data.target_url);
            }
            else if (data.underage)
            {
                $('#register_form').hide();
                $('#underage').show();
            }
            else
            {
              // reset error messages
              $($register_form).find('label span.error, legend span.error').text('');

              // sort out error messages
              $.each(data.message, function(i, val)
              {
                if (val)
                {
                  // target element
                  var $err_el = $($register_form).find('label[for="'+ i +'"], #fieldset_'+i).find('span.error');
                  $err_el.html(val);

                  // set initial position to scroll to
                  if($err_el.length)
                  {
                      if (null === scroll_offset || scroll_offset.top > $err_el.offset().top)
                      {
                        scroll_offset     = $err_el.offset();
                        scroll_offset.top = scroll_offset.top - $err_el.parent().outerHeight();
                      }
                  }
                }
              });

              // scroll to reveal first message
              if (scroll_offset)
              {
                window.scrollTo(scroll_offset.left, scroll_offset.top);
              }
            }
          })
          .error(function()
          {

          });
    }
    );
  }

  /*
   * Handle login form
   **/
  var $login_form = $('#login_form');
  if ($login_form)
  {
    $($login_form).find('input[type="submit"]').on('click', function(e)
      {
        var error_status  = false;
        var scroll_offset = null;

        // prevent anything by default
        e.preventDefault();

        // hide error messages
        $('.coca-cola-error-box').fadeOut(function()
        {
          // reset errors
          $('.coca-cola-error-box')
            .find('ul')
            .html('');
        });

        // Disallow empty fields
        if (!re_non_empty.test($login_form.find('#signin_username').val()) ||
            !re_non_empty.test($login_form.find('#signin_password').val())
          )
        {
          error_status = true;
          $('.coca-cola-error-box ul.client').append('<li>Email and password are required!</li>');
        }

        if (error_status)
        {
          $('.coca-cola-error-box').fadeIn();
        }
        else
        {
          // send signin request
          $.post(
            $login_form.attr('action'),
            $login_form.serialize()
          )
            .success(function (data)
            {
              if (data.success)
              {
                // redirect to requested URL
                window.location = data.target_url;
              }
              else
              {
                // display server side errors
                $('.coca-cola-error-box')
                  .append(data.message.global)
                  .append(data.message.username);

                $('.coca-cola-error-box').fadeIn();
              }
            })
            .error(function() {

            });
        }


      }
    );
  }

  $('#redeem_form').unbind('submit').bind('submit', function (e) {
      var $this = $(this);

      //prevent the form from submitting normally
      e.preventDefault();

      //show the default loading message while the $.post request is sent
      $.mobile.showPageLoadingMsg();

      //send $.post request to server, `$this.serialize()` adds the form data to the request
      $.post($this.attr('action'), $this.serialize(), function (response) {
        if (response.success) {
          // redirect to requested URL
          $.mobile.changePage(response.target_url, { reloadPage: true });
        } else {

          if( response.message.code ) {
            $('<div>').simpledialog2({
              mode: 'blank',
              headerText: 'Error',
              headerClose: true,
              dialogAllow: true,
              dialogForce: true,
              blankContent : $('#error_dialog_required').html()
            });
          } else {

            if( response.message.global.hasOwnProperty('invalid') ) {
              $('<div>').simpledialog2({
                mode: 'blank',
                headerText: 'Error',
                headerClose: true,
                dialogAllow: true,
                dialogForce: true,
                blankContent : $('#error_dialog_invalid').html()
              });
            }
            if( response.message.global.hasOwnProperty('min_time_period') ) {
              $('<div>').simpledialog2({
                mode: 'blank',
                headerText: 'Error',
                headerClose: true,
                dialogAllow: true,
                dialogForce: true,
                blankContent : $('#error_dialog_min_time_period').html()
              });

            }
          }
        }

        $.mobile.hidePageLoadingMsg();
      }, 'json');
  });

  $('#redeem_free_form').unbind('submit').bind('submit', function (e) {
      var $this = $(this);

      //prevent the form from submitting normally
      e.preventDefault();

      //show the default loading message while the $.post request is sent
      $.mobile.showPageLoadingMsg();

      //send $.post request to server, `$this.serialize()` adds the form data to the request
      $.post($this.attr('action'), $this.serialize(), function (response) {
        if (response.success) {
            $('<div>').simpledialog2({
              mode: 'blank',
              headerText: 'Success',
              headerClose: false,
              dialogAllow: true,
              dialogForce: true,
              blankContent : $('#success_dialog_reset').html()
            });

        } else {

          if( response.message.code ) {
            $('<div>').simpledialog2({
              mode: 'blank',
              headerText: 'Error',
              headerClose: true,
              dialogAllow: true,
              dialogForce: true,
              blankContent : $('#error_dialog_required').html()
            });
          } else {

            if( response.message.global.hasOwnProperty('invalid') ) {
              $('<div>').simpledialog2({
                mode: 'blank',
                headerText: 'Error',
                headerClose: true,
                dialogAllow: true,
                dialogForce: true,
                blankContent : $('#error_dialog_invalid').html()
              });
            }
            if( response.message.global.hasOwnProperty('insufficient_redeems') ) {
              $('<div>').simpledialog2({
                mode: 'blank',
                headerText: 'Error',
                headerClose: true,
                dialogAllow: true,
                dialogForce: true,
                blankContent : $('#error_dialog_insufficient_redeems').html()
              });

            }
          }
        }

        $.mobile.hidePageLoadingMsg();
      }, 'json');
  });


});



