$(document).ready(function() {

    $('a[rel=external]').click(function(){
        window.open($(this).attr('href'));
        return false;
    });
    $('#terms_link, .terms_link').fancybox({
        type:'ajax',
        afterShow: function(){
            Cufon.refresh();
        }
    });


    // Fonts
    
    /* Cufon Font Replacements*/
    Cufon('.you', {
        fontFamily: 'You'
    });
    /* Cufon Font Replacements*/
    Cufon('.gotham', {
        fontFamily: 'Gotham-Book'
    });

    Cufon('.gotham-b', {
        fontFamily: 'Gotham-Bold'
    });
    
    /* Make Cufon Behave */
    $(".nameslist .you").mouseover(function() {
        $(this).css('color', '#FFFFFF');
        Cufon.replace(this, {fontFamily: 'You'});
    });
    
    /* Is it activated by click? */
    $(".nameslist .you").mouseleave(function() {
        if ($(this).hasClass("active")) {
            $(this).css('color', '#FFFFFF');
            Cufon.replace(this, {fontFamily: 'You'});
        } else {
            $(this).css('color', '#EC1C15');
            Cufon.replace(this, {fontFamily: 'You'});
        }
    });
    
    Cufon.now();
    
    
    
    
    
    
    // Put selected text to Tall Bottle
    
    $(".highlight_bottle").click(function() {
        var newcoke_name = $(this).attr('title');
        
        $(".cokebottle_tall span").fadeOut("fast", function() {
            
            
            $(".cokebottle_tall span").html(newcoke_name);
            
            // Text size if over the bottle width, let's resize (normal is 45px)
            if ($(".cokebottle_tall span").width() >= '180') {
                
                $(".cokebottle_tall span").css('font-size', '40px');
            } else {
                $(".cokebottle_tall span").css('font-size', '45px');
            }
            
            Cufon.replace(this, {fontFamily: 'You'});
            
            $(".cokebottle_tall span").fadeIn("slow");
            
            
        });
        
        /* Disable all before making selection active */
        $('.nameslist .name').each(function(index) {
            $(this).removeClass("active");
            $(this).css('color', '#EC1C15');
        });
        
        /* Add active class on click */
        $(this).addClass("active");
        $(this).css('color', '#FFFFFF');
        Cufon.refresh();
    });
    
    
    
    
    
   
   
   
   
    // Names Filtering
    var typewatch = (function(){
        var timer = 0;
        return function(callback, ms){
            clearTimeout (timer);
            timer = setTimeout(callback, ms);
        }  
    })();
    
    $("#search_text").keyup(function(event) {

        var input_value = $("#search_text").val();

        $(".nameslist li").css('display', 'none')

        $(".nameslist li:regex(title, .*" + input_value + ".*)").css('display', 'block');

        typewatch(function () {
            
            // scroller's autoReinitialise flickers too much, so had to use manual call mixed with typewatch to avoid delays
            $('ul.nameslist').jScrollPane({
                showArrows: 'true'
            });
        }, 500);
    });
    
    
    
    
    
    
    
    // Scroller for names list
    if ($("ul.nameslist").length) {
        $('ul.nameslist').jScrollPane({
            showArrows: 'true'
        });
    }
    
    






    $(".create_button").click(function() {
        if (!selectedname) {
            $(".friends_container").effect("pulsate", { times:2 }, 300);
        }
    });
    
    
    
    
    
    
    
    if ($(".final_name_container").length) {
        
        if ($(".empty_can span").width() > 346) {
            $(".empty_can span").css('font-size', '60px');
            $(".empty_can .empty_can_container").css('top', '70px');
            Cufon.replace(".empty_can span", {fontFamily: 'You'});
        }
        
        if ($(".control_share span").width() > 180) {
            $(".control_share span").css('font-size', '20px');
            $(".control_share span").css('top', '43px');
            Cufon.replace(".control_share span", {fontFamily: 'Gotham Bold'});
        }
    }
    $("a[rel^='prettyPhoto']").prettyPhoto({
        social_tools: false
    });
    
    
    
   // Customise tabs
    $('.customise_page .tab').click(function(){
        var tab = $(this).data('target');
        $('.customise_page .tab').removeClass('active');
        $(this).addClass('active');
        Cufon.refresh();
        $('#200ml_locations, #600ml_locations').fadeOut(200, function(){
            $('#'+tab).fadeIn(200);
        })
    });
    

    
    
    // Share a Story textarea limit
    $("#sac_story_story").keyup(function() {
        if($("#sac_story_story").val().length > 500) {
            $("#sac_story_story").val($.trim($("#sac_story_story").val()).substring(0,500));
            $("#total_count").html("50");
        }
        $("#total_count").html($("#sac_story_story").val().length);
    });
});












// form validation:
var formName = 'sac_story_';
var toCheck = ["name","email","phone","story","location"];
        
function checkFields() {
    var error = false;

    // Check fields
    unHighLight();
    $.each(toCheck, function(index, value) {
        if(!$("#" + formName + value).val()) {
            highLight(formName + value);
            error = true;
        }
    });
    
    if (!$("#sac_story_accept_tc").attr('checked')) {
        $("#form_error_incomplete").show();
        error = true;
    } else {
        $("#form_error_incomplete").hide();
    }
    
    if (error) {
        $("#form_error_incomplete").show();
        return false;
    } else {
        $("#form_error_incomplete").hide();
        return true;
    }
}

function highLight(div_id) {
    $("#" + div_id).css('border', '1px solid red');
}
function unHighLight() {
    $.each(toCheck, function(index, value) {
        $("#" + formName + value).css('border', '1px solid #CCC');
    });
}