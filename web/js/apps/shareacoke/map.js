var friends_filter = false;
var geocode_result;
var markersArray = [];
var infoWindow;

$(document).ready(function(){
  initialize();

  $('#location_select').selectBox();

  $('#location_select').change(function(){
    var address = $(this).val() + ', New Zealand';

    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        geocode_result = results[0];
        map.fitBounds(results[0].geometry.bounds);
        map.setCenter(results[0].geometry.location);
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
  });

  $('#friends_filter').change(function(){
    if ($(this).is(':checked')) {
      friends_filter = true;
    } else {
      friends_filter = false;
    }
  });


  $('.selection .icon').click(function(){
    var name = $(this).data('name');

    if ($(this).data('status') == 'off') {
      $(this).addClass(name + '_active');
      $(this).data('status', 'on');
      addMarkers(name);
    } else {
      $(this).removeClass(name +'_active');
      $(this).data('status', 'off');
    }

  });

});

function initialize() {
  var mapOptions = {
    center: new google.maps.LatLng(-41.178654,174.287109),
    zoom: 5,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    mapTypeControl: false,
    streetViewControl: false
  };
  map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);
  geocoder = new google.maps.Geocoder();
  infoWindow = new google.maps.InfoWindow();
}

function addMarkers(type) {
  var api_url = '/apps.php/shareacoke/map-api/'+type+'.json';

  if (friends_filter) {
    api_url = api_url + '?friendsOnly=1';
  }

  $.getJSON(api_url, function(data){
    if (data.status == 'OK') {
//      console.log(data.payload);
      for (i in data.payload) {

        switch (type) {
          case 'story':

            var html = '<h3>Share a Story</h3><p>'+data.payload[i].story+'</p>';

            var geocoded = geocoder.geocode( { 'address': data.payload[i].location + ', New Zealand'}, function(result, status) {
              if (status == google.maps.GeocoderStatus.OK) {

                var lat = result[0].geometry.location.lat();
                var lng = result[0].geometry.location.lng();

                createMarker(type, randomiseLatLng(lat, lng) , html);
              }
            });
            break;

          case 'can':

            var html = '<h3>Share a Can</h3><p>'+data.payload[i].recipient_name+'</p>';

            var lat = data.payload[i].SACUser.latitude;
            var lng = data.payload[i].SACUser.longitude;

            if (lat && lng) {
              createMarker(type, randomiseLatLng(lat, lng), html);
            }

            break;


          case 'note':
            var html = '<h3>Share a Note</h3><p>'+data.payload[i].full_message+'</p>';

            var lat = data.payload[i].SACUser.latitude;
            var lng = data.payload[i].SACUser.longitude;

            if (lat && lng) {
              createMarker(type, randomiseLatLng(lat, lng), html);
            }


        }



      }
    }

  });
}

function randomiseLatLng(lat, lng) {

  var rnd_lat = parseFloat(lat) + (getRandomArbitrary(1,50) * randomisePositiveNegative() * getRandomArbitrary(0.00001,0.0001));
  var rnd_lng = parseFloat(lng) + (getRandomArbitrary(1,50) * randomisePositiveNegative() * getRandomArbitrary(0.00001,0.0001));

  return new google.maps.LatLng(rnd_lat, rnd_lng);
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function randomisePositiveNegative() {
  if (Math.random() < 0.5) {
    return 1;
  } else {
    return -1;
  }
}

function createMarker(type, latlng, html ) {
  var marker = new google.maps.Marker({
    map: map,
    position: latlng,
    optimized: false,
    icon: new google.maps.MarkerImage(
      '/images/app/shareacoke/marker-'+type+'.png',
      null,
      null,
      new google.maps.Point(19,51),
      new google.maps.Size(38,51)
    )
  });
  google.maps.event.addListener(marker, 'click', function() {
    infoWindow.setContent(html);
    infoWindow.open(map, marker);
  });
  markersArray.push(marker);
}

// Removes the overlays from the map, but keeps them in the array
function clearOverlays() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(null);
    }
  }
}

// Shows any overlays currently in the array
function showOverlays() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(map);
    }
  }
}

// Deletes all markers in the array by removing references to them
function deleteOverlays() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(null);
    }
    markersArray.length = 0;
  }
}