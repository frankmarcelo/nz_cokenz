(function($){

	var contactSuccess = function(response)
	{
		if (response && response.success)
		{
			$('#contact_form').replaceWith('<p>Thank you, your message has been sent.</p>');			
		}
		else
		{
			contactError(response);
		}
	}

	var contactError = function(response)
	{
		if (response && response.invalid && response.messages && response.messages.length)
		{
			alert(response.messages.join('\n'));
			return;
		}
		
		$('#contact_form').replaceWith('<p>Oops, your message could not be sent.</p>');
	}
	
	var contactComplete = function()
	{
		$('#submit').val('Submit');
	}

	var contactLoading = function()
	{
		$('#submit').val('Sending...');
	}
	
	var validateForm = function()
	{
		var fields = $(this).serializeArray();
		var messages = [];

		for (var x in fields)
		{
			field = fields[x];
			
			if (field.name == 'name')
			{
				if (!field.value) messages.push('Please enter your name.');
			}
			else if (field.name == 'email')
			{
				if (!field.value) messages.push('Please enter your email address.');
				
				else if (!/^[\w\.-]+@[\w\.-]+\.\w{2,4}$/.test(field.value)) messages.push('Please enter a valid email address.');
			}
			else if (field.name == 'subject')
			{
				if (!field.value) messages.push('Please select a subject.');
			}
			else if (field.name == 'message')
			{
				if (!field.value) messages.push('Please enter your message.');
			}
		}
		
		if (0 < messages.length)
		{
			$(this).data('isValid', false).data('validationMessage', messages.join('\n'));
		}
		else
		{
			$(this).data('isValid', true).data('validationMessage', null);
		}
	}

	$('#contact_form').bind('validate', validateForm);
	
	$('#contact_form').submit(function(){
		$(this).triggerHandler('validate');

		if (!$(this).data('isValid'))
		{
			alert($(this).data('validationMessage'));
			return false;
		}
		
		var contactUrl = 'https://{domain}/ssldocs/csol/contactus.do'.replace('{domain}', location.host);

		$.ajax({
			data: $(this).serialize(),
			dataType: 'json',
			beforeSend: contactLoading,
			error: contactError,
			success: contactSuccess,
			complete: contactComplete,
			type: 'post',
			url: contactUrl
		});

		return false;
	});
	
})(jQuery);
