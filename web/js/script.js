$(function() {

	$('#navigation .dropdown_trigger').click(function(){
		$(this).parent().toggleClass('open');
		return false;
	});
	
	$(document).click(function(ev){
		$('#navigation').removeClass('open');
	});
	
	$('.new_window').click(function(){
		window.open($(this).attr('href'));
		return false;
	});
	
	overlay.initialize();
	popup.initialize();
	promo_popup.initialize();
	
	$(".lightbox").fancybox({
		maxWidth	: 800,
		maxHeight	: 600,
		fitToView	: false,
		width		: '70%',
		height		: '70%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
	
});


function positionLoginPanel(){
	var bal = $("#loginShowStart");
	var pos = bal.offset(); 
		var left =(((pos.left + bal.width()) - $("#loginWidgetContainer").width()));
	    $("#loginWidgetContainer").css('left', left+'px');
}
/*
var loginloader = {
	show: function(){
		var overlay_div = '<div class="login_overlay"></div>';
		$('body').append(overlay_div);
		var loader_div = '<div class="login_loader">Logging in...</div>';
		$('body').append(loader_div);
		if(window.innerHeight) {
        	var viewport_offset = window.pageYOffset;
        } else {
        	var viewport_offset = document.documentElement.scrollTop;
        }
        var windowHeight = document.documentElement.clientHeight;

		$(".login_overlay").css({
            "height": (viewport_offset + windowHeight) +'px'
        });
		$(".login_overlay").fadeIn();
		
		$(".login_loader").show();
		loginloader.center();
	},
	center: function(){
        var windowHeight = document.documentElement.clientHeight;
        var windowWidth = document.documentElement.clientWidth;
        
        if(window.innerHeight) {
        	var viewport_offset = window.pageYOffset;
        } else {
        	var viewport_offset = document.documentElement.scrollTop;
        }

        var top_offset = windowHeight/2 - $(".login_loader").height()/2;
        if(top_offset < 50) {
        	top_offset = 50;
        }
        
        var left = (windowWidth/2) - ($(".login_loader").width()/2);
        
        //centering
        $(".login_loader").css({
            "top": top_offset + viewport_offset,
            "left": left
        });

    },
	hide: function(){
		$(".login_loader").hide();
		$(".login_overlay").fadeOut();
		$(".login_loader").remove();
		$(".login_overlay").remove();
	}
};*/

//overlay for popups
var overlay = {
	initialize: function(){
	 	var overlay_div = '<div class="black_overlay"></div>';
	 	$('body').append(overlay_div);
	 	
	 	 $('.black_overlay').click(function(){
	 		 popup.close();
	 		 overlay.close();
	     });
	},
	show: function(){
		if(window.innerHeight) {
        	var viewport_offset = window.pageYOffset;
        } else {
        	var viewport_offset = document.documentElement.scrollTop;
        }
        var windowHeight = document.documentElement.clientHeight;

		$(".black_overlay").css({
            "height": (viewport_offset + windowHeight) +'px'
        });
		$(".black_overlay").fadeIn();
	},
	close: function(){
        $('.black_overlay').fadeOut();
        //$('.black_overlay').remove();
    }
};

var popup = {
	    initialize: function() {
			var popup_div = '<div id="popup_wrapper"><div id="popup_header"><div id="popup_header_left"></div><div id="popup_header_middle"></div><div id="popup_header_right"></div></div><div id="popup_body"><div id="popup_close"></div><div id="popup_content"> </div></div><div id="popup_footer"><div id="popup_footer_left"></div><div id="popup_footer_middle"></div><div id="popup_footer_right"></div></div></div>';
			$('body').append(popup_div); 
	        $('#popup_close').click(function(){
	        	popup.close();
	        	overlay.close();
	        });
	    },

	    show: function(box_width, box_height, left_pos, top_pos){

	        overlay.show();
	        
	        $("#popup_header_middle, #popup_footer_middle").css({
	            "width": (box_width)+'px'
	        });
	        
	        $("#popup_content").css({
	            "height": box_height+'px',
	            "width": box_width+'px'
	        });
	        
	        $("#popup_wrapper").css({
	        	"width": (box_width + 20) + 'px'
	        });
	        
	        if(typeof(left_pos) !== 'undefined' && left_pos != null) {
	        	$("#popup_wrapper").css({
	        		"position": "absolute",
	        		"left": left_pos,
	        		"top": top_pos
	        	});
	        } else {
	        	popup.center();
	        }

	        $("#popup_wrapper").fadeIn();
	    },
	    
	    center: function(){
	        var windowHeight = document.documentElement.clientHeight;
	        var windowWidth = document.documentElement.clientWidth;
	        
	        if(window.innerHeight) {
	        	var viewport_offset = window.pageYOffset;
	        } else {
	        	var viewport_offset = document.documentElement.scrollTop;
	        }

	        var top_offset = windowHeight/2 - $("#popup_wrapper").height()/2;
	        if(top_offset < 50) {
	        	top_offset = 50;
	        }
	        
	        var left = (windowWidth/2) - ($("#popup_wrapper").width()/2);
	        
	        //centering
	        $("#popup_wrapper").css({
	            "top": top_offset + viewport_offset,
	            "left": left
	        });

	    },
	    loadWidget: function(width, height, widget_name, widget_view, param){ 
	    	popup.show(width,height); 
	    	gim.renderWidget(widget_name, widget_view, locale, param, 'popup_content'); 
	    },
	    load: function(url, box_width, box_height, left_pos, top_pos){
	    	$("#popup_content").html('');
	    	popup.show(box_width, box_height, left_pos, top_pos);
	    	$.get(url, function(data) {
	    		  $('#popup_content').html(data);
	    		});
	    },
	    close: function(){
	        $("#popup_wrapper").fadeOut();
	        $("#popup_content").html('');
	        //$("#popup_wrapper").remove();
	        
	        //close promo pop too
	        $("#promo_popup_wrapper").fadeOut();
	        $("#promo_popup_content").html('');
	        
	        overlay.close();
	    }
	};


var promo_popup = {
	    initialize: function() {
			var popup_div = '<div id="promo_popup_wrapper"><div id="promo_popup_header"><div id="promo_popup_header_left"></div><div id="promo_popup_header_middle"></div><div id="promo_popup_header_right"></div></div><div id="promo_popup_body"><div id="promo_popup_close"></div><div id="promo_popup_content"> </div></div><div id="promo_popup_footer"><div id="promo_popup_footer_left"></div><div id="promo_popup_footer_middle"></div><div id="promo_popup_footer_right"></div></div></div>';
			$('body').append(popup_div); 
	        $('#promo_popup_close').click(function(){
	        	promo_popup.close();
	        	overlay.close();
	        });
	    },

	    show: function(box_width, box_height, left_pos, top_pos){

	        overlay.show();
	        
	        $("#promo_popup_header_middle, #promo_popup_footer_middle").css({
	            "width": (box_width)+'px'
	        });
	        
	        $("#promo_popup_content").css({
	            "height": box_height+'px',
	            "width": box_width+'px'
	        });
	        
	        $("#promo_popup_wrapper").css({
	        	"width": (box_width + 20) + 'px'
	        });
	        
	        if(typeof(left_pos) !== 'undefined' && left_pos != null) {
	        	$("#promo_popup_wrapper").css({
	        		"position": "absolute",
	        		"left": left_pos,
	        		"top": top_pos
	        	});
	        } else {
	        	promo_popup.center();
	        }

	        $("#promo_popup_wrapper").fadeIn();
	    },
	    
	    center: function(){
	        var windowHeight = document.documentElement.clientHeight;
	        var windowWidth = document.documentElement.clientWidth;
	        
	        if(window.innerHeight) {
	        	var viewport_offset = window.pageYOffset;
	        } else {
	        	var viewport_offset = document.documentElement.scrollTop;
	        }

	        var top_offset = windowHeight/2 - $("#promo_popup_wrapper").height()/2;
	        if(top_offset < 50) {
	        	top_offset = 50;
	        }
	        
	        var left = (windowWidth/2) - ($("#promo_popup_wrapper").width()/2);
	        
	        //centering
	        $("#promo_popup_wrapper").css({
	            "top": top_offset + viewport_offset,
	            "left": left
	        });

	    },
	    loadWidget: function(width, height, widget_name, widget_view, param){ 
	    	promo_popup.show(width,height); 
	    	gim.renderWidget(widget_name, widget_view, locale, param, 'promo_popup_content'); 
	    },
	    load: function(url, box_width, box_height, left_pos, top_pos){
	    	$("#promo_popup_content").html('');
	    	promo_popup.show(box_width, box_height, left_pos, top_pos);
	    	$.get(url, function(data) {
	    		  $('#promo_popup_content').html(data);
	    		});
	    },
	    close: function(){
	        $("#promo_popup_wrapper").fadeOut();
	        $("#promo_popup_content").html('');
	       // $("#promo_popup_wrapper").remove();
	        
	        //close normal pop too
	        $("#popup_wrapper").fadeOut();
	        $("#popup_content").html('');
	        
	        overlay.close();
	    }
	};


/*-----------------------------------------------------*/

Cufon.replace('#container h1', { fontFamily: 'VAG Rounded' });
Cufon.replace('#container h2', { fontFamily: 'VAG Rounded' });
Cufon.replace('#container h3', { fontFamily: 'VAG Rounded' });
Cufon.replace('#container h4', { fontFamily: 'VAG Rounded' });
Cufon.replace('.vag', { fontFamily: 'VAG Rounded' });

if (navigator.userAgent.toLowerCase().indexOf("chrome") >= 0) {
    $(window).load(function(){
        $('input:-webkit-autofill').each(function(){
            var text = $(this).val();
            var id = $(this).attr('id');

            $(this).after(this.outerHTML).remove();
            $('#'+id).val(text);
        });
    });
}
