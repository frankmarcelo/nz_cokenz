 $(document).ready(function(){
	 	positionPhoneErr();
		
		$('.redlink').hover(function(){
			showImg($(this).offset());
		});
		$('.redlink').click(function(){
			showImg($(this).offset());
		});
		$('#clearform').click(function(){
			clearForm($('#contactform'));
			selectRadio(1,$('#contactform'));
			return false;
		});
		
	});
 
function positionPhoneErr(){
	var pos = $('#checkPhoneText').offset();
	var left = pos.left + 160;
	var top = pos.top;
	$("#phoneErr").css('left', left+'px');
	$("#phoneErr").css('top', top+'px');
}

function showImg(pos){
		
		var left = pos.left + 150;
		var top = pos.top;
	    $("#wheretofind").css('left', left+'px');
		$("#wheretofind").css('top', top+'px');
		$("#wheretofind").toggle();
}

function clearForm(form) {
  $(':input', form).each(function() {
    var type = this.type;
    var tag = this.tagName.toLowerCase(); 
    if (type == 'text' || type == 'password' || tag == 'textarea')
      this.value = "";
    else if (type == 'checkbox' || type == 'radio')
      this.checked = false;
    else if (tag == 'select')
      this.selectedIndex = -1;
  });
  $(".err").hide();
  $(".errormsg").hide();
};

//will "click" the radio button if it is selected, used after page reload
function showForm(form) {
	  $(':input', form).each(function() {
		var type = this.type;  
	    if (type == 'radio' && this.checked == true) {
	    	this.click();
	    }
	  });
	};

//will "click" radio button with provided value
function selectRadio(val, form) {
		  $(':input[type=radio]', form).each(function() {
			var value = this.value;  
		    if (value == val) {
		    	this.click();
		    }
		  });
		};