<?php

require_once(dirname(__FILE__).'/../config/environment.php');
require_once(dirname(__FILE__).'/../config/ProjectConfiguration.class.php');

$configuration = ProjectConfiguration::getApplicationConfiguration('apps', $environment, $debug);
sfContext::createInstance($configuration)->dispatch();
