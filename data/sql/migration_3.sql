CREATE TABLE share_a_coke (id NUMBER(20), name VARCHAR2(255), email VARCHAR2(255), phone VARCHAR2(255), story VARCHAR2(1500), pic VARCHAR2(255), created_at DATE NOT NULL, updated_at DATE NOT NULL, PRIMARY KEY(id))
/
CREATE SEQUENCE SHARE_A_COKE_seq START WITH 1 INCREMENT BY 1 NOCACHE
/

DECLARE
  constraints_Count NUMBER;
BEGIN
  SELECT COUNT(CONSTRAINT_NAME) INTO constraints_Count FROM USER_CONSTRAINTS WHERE TABLE_NAME = 'SHARE_A_COKE' AND CONSTRAINT_TYPE = 'P';
  IF constraints_Count = 0 THEN
    EXECUTE IMMEDIATE 'ALTER TABLE SHARE_A_COKE ADD CONSTRAINT SHARE_A_COKE_AI_PK_idx PRIMARY KEY (id)';
  END IF;
END;
/
CREATE TRIGGER SHARE_A_COKE_AI_PK
   BEFORE INSERT
   ON SHARE_A_COKE
   FOR EACH ROW
DECLARE
   last_Sequence NUMBER;
   last_InsertID NUMBER;
BEGIN
   IF (:NEW.id IS NULL OR :NEW.id = 0) THEN
      SELECT SHARE_A_COKE_seq.NEXTVAL INTO :NEW.id FROM DUAL;
   ELSE
      SELECT NVL(Last_Number, 0) INTO last_Sequence
        FROM User_Sequences
       WHERE UPPER(Sequence_Name) = UPPER('SHARE_A_COKE_seq');
      SELECT :NEW.id INTO last_InsertID FROM DUAL;
      WHILE (last_InsertID > last_Sequence) LOOP
         SELECT SHARE_A_COKE_seq.NEXTVAL INTO last_Sequence FROM DUAL;
      END LOOP;
   END IF;
END;
/