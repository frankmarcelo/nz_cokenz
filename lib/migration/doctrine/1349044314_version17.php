<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version17 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->addColumn('s_a_c_buy_redemption_code', 'allocated', 'boolean', '25', array(
             'default' => 'false',
             ));
        $this->changeColumn('s_a_c_buy', 'redemption_code_id', 'integer', '20', array(
             ));
    }

    public function down()
    {
        $this->removeColumn('s_a_c_buy_redemption_code', 'allocated');
    }
}