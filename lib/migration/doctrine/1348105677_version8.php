<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version8 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->dropTable('share_a_coke');
        $this->createTable('s_a_c_note', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'recipient_name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'recipient_id' => 
             array(
              'type' => 'integer',
              'length' => '20',
             ),
             'note_type' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'note_selections' => 
             array(
              'type' => 'array',
              'length' => '2000',
             ),
             'user_id' => 
             array(
              'type' => 'integer',
              'length' => '20',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             ));
        $this->createTable('s_a_c_story', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'name' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '255',
             ),
             'email' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '255',
             ),
             'phone' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '255',
             ),
             'story' => 
             array(
              'type' => 'string',
              'length' => '1500',
             ),
             'pic' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'location' => 
             array(
              'type' => 'string',
              'length' => '500',
             ),
             'status' => 
             array(
              'type' => 'string',
              'default' => 'pending',
              'length' => '255',
             ),
             'unique_key' => 
             array(
              'type' => 'string',
              'length' => '50',
             ),
             'user_id' => 
             array(
              'type' => 'integer',
              'length' => '20',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             ));
        $this->createTable('s_a_c_user', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'first_name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'last_name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'email' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'location' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'facebook_id' => 
             array(
              'type' => 'integer',
              'length' => '20',
             ),
             'facebook_token' => 
             array(
              'type' => 'string',
              'length' => '1000',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'latitude' => 
             array(
              'type' => 'double',
              'length' => '',
             ),
             'longitude' => 
             array(
              'type' => 'double',
              'length' => '',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             ));
        $this->createTable('s_a_c_virtual_can', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'recipient_name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'recipient_id' => 
             array(
              'type' => 'integer',
              'length' => '20',
             ),
             'user_id' => 
             array(
              'type' => 'integer',
              'length' => '20',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             ));
//        $this->addColumn('s_a_c_nominate_name', 'user_id', 'integer', '20', array(
//             ));
    }

    public function down()
    {
        $this->createTable('share_a_coke', array(
             'id' => 
             array(
              'type' => 'integer',
              'length' => '8',
              'autoincrement' => '1',
              'primary' => '1',
             ),
             'name' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '255',
             ),
             'email' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '255',
             ),
             'phone' => 
             array(
              'type' => 'string',
              'notnull' => '1',
              'length' => '255',
             ),
             'story' => 
             array(
              'type' => 'string',
              'length' => '1500',
             ),
             'pic' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'location' => 
             array(
              'type' => 'string',
              'length' => '500',
             ),
             'status' => 
             array(
              'type' => 'string',
              'default' => 'pending',
              'length' => '255',
             ),
             'unique_key' => 
             array(
              'type' => 'string',
              'length' => '50',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'type' => '',
             'indexes' => 
             array(
             ),
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => '',
             'charset' => '',
             ));
        $this->dropTable('s_a_c_note');
        $this->dropTable('s_a_c_story');
        $this->dropTable('s_a_c_user');
        $this->dropTable('s_a_c_virtual_can');
        $this->removeColumn('s_a_c_nominate_name', 'user_id');
    }
}