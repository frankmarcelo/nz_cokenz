<?php

/**
 * CocaColaArticle
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    coke_nz
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class CocaColaArticle extends BaseCocaColaArticle
{

  public function getArticleContent()
  {
    // If an Oracle database, a resource is returned instead of a string,
    // so read the resource and return it.
    if( $this->getTable()->getConnection()->getDriverName() == 'Oracle' ) {
      $handle = $this->_get('article_content');
      $contents = '';
      while (!feof($handle)) {
        $contents .= stream_get_line($handle, 1000000, "\n");
      }
      return $contents;
    }

    return $this->_get('article_content');

  }


}
