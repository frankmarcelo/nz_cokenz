<?php

/**
 * BaseGameScore
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $score
 * @property integer $game_id
 * @property integer $user_id
 * @property CocaColaGame $Game
 * @property sfGuardUser $User
 * 
 * @method integer      getScore()   Returns the current record's "score" value
 * @method integer      getGameId()  Returns the current record's "game_id" value
 * @method integer      getUserId()  Returns the current record's "user_id" value
 * @method CocaColaGame getGame()    Returns the current record's "Game" value
 * @method sfGuardUser  getUser()    Returns the current record's "User" value
 * @method GameScore    setScore()   Sets the current record's "score" value
 * @method GameScore    setGameId()  Sets the current record's "game_id" value
 * @method GameScore    setUserId()  Sets the current record's "user_id" value
 * @method GameScore    setGame()    Sets the current record's "Game" value
 * @method GameScore    setUser()    Sets the current record's "User" value
 * 
 * @package    Coke NZ
 * @subpackage model
 * @author     
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseGameScore extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('game_score');
        $this->hasColumn('score', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('game_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('user_id', 'integer', null, array(
             'type' => 'integer',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('CocaColaGame as Game', array(
             'local' => 'game_id',
             'foreign' => 'id'));

        $this->hasOne('sfGuardUser as User', array(
             'local' => 'user_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $this->actAs($timestampable0);
    }
}