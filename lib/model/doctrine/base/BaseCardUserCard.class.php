<?php

/**
 * BaseCardUserCard
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $user_location_id
 * @property boolean $is_active
 * @property date $redemption_date
 * @property Doctrine_Collection $Redemptions
 * @property CardUserLocation $UserLocation
 * 
 * @method integer             getUserLocationId()   Returns the current record's "user_location_id" value
 * @method boolean             getIsActive()         Returns the current record's "is_active" value
 * @method date                getRedemptionDate()   Returns the current record's "redemption_date" value
 * @method Doctrine_Collection getRedemptions()      Returns the current record's "Redemptions" collection
 * @method CardUserLocation    getUserLocation()     Returns the current record's "UserLocation" value
 * @method CardUserCard        setUserLocationId()   Sets the current record's "user_location_id" value
 * @method CardUserCard        setIsActive()         Sets the current record's "is_active" value
 * @method CardUserCard        setRedemptionDate()   Sets the current record's "redemption_date" value
 * @method CardUserCard        setRedemptions()      Sets the current record's "Redemptions" collection
 * @method CardUserCard        setUserLocation()     Sets the current record's "UserLocation" value
 * 
 * @package    Coke NZ
 * @subpackage model
 * @author     
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCardUserCard extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('card_user_card');
        $this->hasColumn('user_location_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('is_active', 'boolean', null, array(
             'type' => 'boolean',
             'default' => true,
             ));
        $this->hasColumn('redemption_date', 'date', null, array(
             'type' => 'date',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('CardUserRedemption as Redemptions', array(
             'local' => 'id',
             'foreign' => 'user_card_id'));

        $this->hasOne('CardUserLocation as UserLocation', array(
             'local' => 'user_location_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $this->actAs($timestampable0);
    }
}