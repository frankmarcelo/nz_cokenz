<?php

/**
 * BaseCokeLocationType
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $name
 * @property Doctrine_Collection $Locations
 * 
 * @method string              getName()      Returns the current record's "name" value
 * @method Doctrine_Collection getLocations() Returns the current record's "Locations" collection
 * @method CokeLocationType    setName()      Sets the current record's "name" value
 * @method CokeLocationType    setLocations() Sets the current record's "Locations" collection
 * 
 * @package    Coke NZ
 * @subpackage model
 * @author     
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCokeLocationType extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('coke_location_type');
        $this->hasColumn('name', 'string', 200, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 200,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('CokeLocation as Locations', array(
             'local' => 'id',
             'foreign' => 'type_id'));
    }
}