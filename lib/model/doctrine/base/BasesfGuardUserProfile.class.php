<?php

/**
 * BasesfGuardUserProfile
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $user_id
 * @property string $gender
 * @property date $dob
 * @property string $phone
 * @property boolean $email_updates
 * @property boolean $mobile_updates
 * @property string $region
 * @property string $postcode
 * @property enum $registration_source
 * @property sfGuardUser $User
 * 
 * @method integer            getId()                  Returns the current record's "id" value
 * @method integer            getUserId()              Returns the current record's "user_id" value
 * @method string             getGender()              Returns the current record's "gender" value
 * @method date               getDob()                 Returns the current record's "dob" value
 * @method string             getPhone()               Returns the current record's "phone" value
 * @method boolean            getEmailUpdates()        Returns the current record's "email_updates" value
 * @method boolean            getMobileUpdates()       Returns the current record's "mobile_updates" value
 * @method string             getRegion()              Returns the current record's "region" value
 * @method string             getPostcode()            Returns the current record's "postcode" value
 * @method enum               getRegistrationSource()  Returns the current record's "registration_source" value
 * @method sfGuardUser        getUser()                Returns the current record's "User" value
 * @method sfGuardUserProfile setId()                  Sets the current record's "id" value
 * @method sfGuardUserProfile setUserId()              Sets the current record's "user_id" value
 * @method sfGuardUserProfile setGender()              Sets the current record's "gender" value
 * @method sfGuardUserProfile setDob()                 Sets the current record's "dob" value
 * @method sfGuardUserProfile setPhone()               Sets the current record's "phone" value
 * @method sfGuardUserProfile setEmailUpdates()        Sets the current record's "email_updates" value
 * @method sfGuardUserProfile setMobileUpdates()       Sets the current record's "mobile_updates" value
 * @method sfGuardUserProfile setRegion()              Sets the current record's "region" value
 * @method sfGuardUserProfile setPostcode()            Sets the current record's "postcode" value
 * @method sfGuardUserProfile setRegistrationSource()  Sets the current record's "registration_source" value
 * @method sfGuardUserProfile setUser()                Sets the current record's "User" value
 * 
 * @package    Coke NZ
 * @subpackage model
 * @author     
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BasesfGuardUserProfile extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('sf_guard_user_profile');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('user_id', 'integer', null, array(
             'type' => 'integer',
             ));
        $this->hasColumn('gender', 'string', 8, array(
             'type' => 'string',
             'length' => 8,
             ));
        $this->hasColumn('dob', 'date', null, array(
             'type' => 'date',
             ));
        $this->hasColumn('phone', 'string', 16, array(
             'type' => 'string',
             'length' => 16,
             ));
        $this->hasColumn('email_updates', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
        $this->hasColumn('mobile_updates', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
        $this->hasColumn('region', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('postcode', 'string', 10, array(
             'type' => 'string',
             'length' => 10,
             ));
        $this->hasColumn('registration_source', 'enum', null, array(
             'type' => 'enum',
             'values' => 
             array(
              0 => 'desktop',
              1 => 'mobile',
             ),
             'default' => 'desktop',
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('sfGuardUser as User', array(
             'local' => 'user_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable();
        $this->actAs($timestampable0);
    }
}