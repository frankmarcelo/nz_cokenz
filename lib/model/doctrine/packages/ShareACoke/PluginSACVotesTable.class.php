<?php

/**
 * PluginSACVotesTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class PluginSACVotesTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object PluginSACVotesTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('PluginSACVotes');
    }
}