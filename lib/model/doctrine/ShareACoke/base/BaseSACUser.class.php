<?php

/**
 * BaseSACUser
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $location
 * @property integer $facebook_id
 * @property string $facebook_token
 * @property Doctrine_Collection $VirtualCan
 * @property Doctrine_Collection $Story
 * @property Doctrine_Collection $Note
 * @property Doctrine_Collection $Buy
 * @property Doctrine_Collection $Moment
 * 
 * @package    Coke NZ
 * @subpackage model
 * @author     
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseSACUser extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('s_a_c_user');
        $this->hasColumn('first_name', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('last_name', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('email', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('location', 'string', 255, array(
             'type' => 'string',
             'length' => 255,
             ));
        $this->hasColumn('facebook_id', 'integer', 20, array(
             'type' => 'integer',
             'length' => 20,
             ));
        $this->hasColumn('facebook_token', 'string', 1000, array(
             'type' => 'string',
             'length' => 1000,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('SACVirtualCan as VirtualCan', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $this->hasMany('SACStory as Story', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $this->hasMany('SACNote as Note', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $this->hasMany('SACBuy as Buy', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $this->hasMany('SACMoment as Moment', array(
             'local' => 'id',
             'foreign' => 'user_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             ));
        $geographical0 = new Doctrine_Template_Geographical(array(
             ));
        $this->actAs($timestampable0);
        $this->actAs($geographical0);
    }
}