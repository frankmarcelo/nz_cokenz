<?php

/**
 * SACStoryTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class SACStoryTable extends PluginSACStoryTable
{
    /**
     * Returns an instance of this class.
     *
     * @return object SACStoryTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('SACStory');
    }
    public function getOrderedList()
    {
        return $this->createQuery()
                ->orderBy('created_at')
                ->execute();
    }


    public function findAllJoinUser(Doctrine_Query $query)
    {
    	$rootAlias = $query->getRootAlias();

    	return $query
  	  	    ->leftJoin(sprintf('%s.SACUser u', $rootAlias))
    	;
    }
}