<?php

  /**
   * sfValidatorDoctrineUnique2 validates the uniqueness of a column.
   *
   * This is a simple wrapper to override default messaging style introduced by parent, which
   * injects field name into resulting error message.
   *
   */
final class sfValidatorDoctrineUnique2 extends sfValidatorDoctrineUnique
{
  /*
   * Catch thrown errors by parent and convert them to desired format
   */
  protected function doClean($value)
  {
    try
    {
      parent::doClean($value);
    }
    catch(sfValidatorErrorSchema $e)
    {
      throw new sfValidatorErrorSchema(
        $this,
        array(
          new sfValidatorError(
            $this,
            'invalid',
            array('column' => $e->getMessage())
          )
        )
      );
    }
    catch(sfValidatorError $e)
    {
      throw new sfValidatorError(
          $this,
          'invalid',
          array('column' => $e->getMessage()
        )
      );
    }

    return $value;
  }
}