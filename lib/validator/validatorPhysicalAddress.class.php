<?php

class validatorPhysicalAddress extends sfValidatorBase
{
  protected function configure($options = array(), $messages = array())
  {
    $this->addOption('address_field','address');
    $this->addOption('suburb_field','suburb');
    $this->addOption('city_field','city');
    $this->addOption('postcode_field','postcode');
    $this->addOption('lat_field','latitude');
    $this->addOption('long_field','longitude');
    
    $this->addMessage('server_error','An error occurred with address verification server. Please try again');
    $this->addMessage('no_results','Address not found. Check the spelling of all fields.');
    
    $this->setMessage('invalid','Address is not specific enough.');
  }
  
  protected function doClean($values)
  {
    
    if (null === $values)
    {
      $values = array();
    }

    if (!is_array($values))
    {
      throw new InvalidArgumentException('You must pass an array parameter to the clean() method');
    } 

    if( empty($values[$this->getOption('lat_field')]) and empty($values[$this->getOption('long_field')])){

      $geo = new Geocoder();
      $address = $values[$this->getOption('address_field')].', '.@$values[$this->getOption('suburb_field')];
      $geo->coordinates( $address, $values[$this->getOption('city_field')], @$values[$this->getOption('postcode_field')], 'New Zealand');

      if($geo->status == "ZERO_RESULTS")
        throw new sfValidatorError( $this, 'no_results', array('values' => $values) );

      if(in_array($geo->status,array('OVER_QUERY_LIMIT','REQUEST_DENIED','INVALID_REQUEST')))
        throw new sfValidatorError( $this, 'server_error', array('values' => $values) );


      if(in_array($geo->location_type,array('GEOMETRIC_CENTER')))
        throw new sfValidatorError( $this, 'invalid', array('values' => $values) );

      if( empty($values[$this->getOption('lat_field')]) )
        $values[$this->getOption('lat_field')] = $geo->coordinates['lat'];

      if( empty($values[$this->getOption('long_field')] ))
        $values[$this->getOption('long_field')] = $geo->coordinates['lng'];

    }

    return $values;
  }
  
}