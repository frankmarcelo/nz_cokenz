<?php

class validatorWordCount extends sfValidatorString
{
    protected function configure($options = array(), $messages = array())
    {
        $this->addMessage('max_word_count', 'Too many words (%max_word_count% words max).');
        $this->addMessage('min_word_count', 'Not enough words (%min_word_count% words min).');

        $this->addOption('max_word_count');
        $this->addOption('min_word_count');
    }

    protected function doClean($value)
    {
        $value = parent::doClean($value);

        if ($this->hasOption('max_word_count') and str_word_count($value, 0) > $this->getOption('max_word_count')) {
            throw new sfValidatorError($this, 'max_word_count', array('max_word_count' => $this->getOption('max_word_count')));
        }

        if ($this->hasOption('min_word_count') and str_word_count($value, 0) < $this->getOption('min_word_count')) {
            throw new sfValidatorError($this, 'min_word_count', array('min_word_count' => $this->getOption('min_word_count')));
        }

        return $value;
    }


}
