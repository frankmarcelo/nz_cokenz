<?php

class validatorLoyaltyCardFreeDrinkCode extends sfValidatorBase
{


  protected function configure($options = array(), $messages = array())
  {
    $this->addMessage('insufficient_redeems','Free drink can not be redeemed, as you don\'t have enough redemptions.');
    $this->setMessage('invalid', 'Code is invalid');

  }


  /**
   * Cleans the input value.
   *
   * Every subclass must implements this method.
   *
   * @param  mixed $value  The input value
   *
   * @return mixed The cleaned value
   *
   * @throws sfValidatorError
   */
  protected function doClean($values)
  {

    $user_card = Doctrine::getTable('CardUserCard')->find( $values['user_card_id'] );

    if( $user_card->getRedemptions()->count() !== 5 ) {
      throw new sfValidatorError($this,'insufficient_redeems');
    }

    $valid_code = Doctrine::getTable('CokeLocation')->validateRedeemCode( $values['code'], $user_card->UserLocation->location_id );

    if( !$valid_code )
    {
      throw new sfValidatorError($this,'invalid');
    }

    return $values;
  }

}
