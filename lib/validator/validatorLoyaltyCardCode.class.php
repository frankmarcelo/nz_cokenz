<?php

class validatorLoyaltyCardCode extends sfValidatorBase
{


  protected function configure($options = array(), $messages = array())
  {

    $this->addOption('min_time_period', 5 ); //86400 = 24hrs

    $this->addMessage('min_time_period','Code can not be redemeed at this time. Try again tomorrow');
    $this->setMessage('invalid', 'Code is invalid');

  }


  /**
   * Cleans the input value.
   *
   * Every subclass must implements this method.
   *
   * @param  mixed $value  The input value
   *
   * @return mixed The cleaned value
   *
   * @throws sfValidatorError
   */
  protected function doClean($values)
  {

    $user_card = Doctrine::getTable('CardUserCard')->find( $values['user_card_id'] );

    $valid_code = Doctrine::getTable('CardCode')->checkCodeIsValid( $values['code'], $user_card->UserLocation->location_id )->count();

    if( 0 === $valid_code )
    {
      throw new sfValidatorError($this,'invalid');
    }

    $last_redemption = Doctrine::getTable('CardUserRedemption')->getLastRedemptionForLocation($values['user_card_id']);

    if($last_redemption) {

      $last_redemption_date = new DateTime($last_redemption->created_at);
      $diff = time() - $last_redemption_date->getTimestamp();

      if( $diff < $this->getOption('min_time_period') )
      {
        throw new sfValidatorError($this,'min_time_period');
      }
    }

    return $values;
  }

}
