<?php

/**
 * Checks the application configuration to determine which modules/actions are supposed
 * to be secure and ensures they are using https.  This filter will also redirect https
 * requests to non-secured pages back to http if the strict option is set in the configuration
 * file.
 *
 * @package sslRedirectPlugin
 * @author Casey Cambra <casey@tigregroup.com>
 * @version 1.1
 * @url http://blog.tigregroup.com/programming/creating-an-ssl-redirect-filter-in-symfony/
 */

class sslRedirectFilter extends sfFilter{

    /**
     * executes the filter.  This filter will determine if a
     * request should be http or https and will redirect as such
     *
     * @param sfFilterChain $filterChain the current symfony filter chain
     * @return boolean redirect status
     */
    public function execute( $filterChain ){
        //only run once per request
        $request = $this->getContext()->getRequest();
        $env = (isset($_SERVER['SF_ENVIRONMENT']) ? $_SERVER['SF_ENVIRONMENT'] : '');
        if( $this->isFirstCall() and !$request->isXmlHttpRequest() and !in_array($env, array('niks','dev'))  ){

            //only filter is the request is get or head
            if( $request->isMethod( 'get' ) || $request->isMethod( 'head' ) ){
                $controller = $this->getContext()->getController();
                $stackEntry = $controller->getActionStack()->getLastEntry();
                $module = $stackEntry->getModuleName();
                $action = $stackEntry->getActionName();
                //get the module settings
                $moduleSettings = sfConfig::get( 'app_ssl_redirect_secure', false );
                //see if strict settings are on (non secure modules must be http)
                $strict = sfConfig::get( 'app_ssl_redirect_strict', true );
                //if there are settings for this module
                if( isset( $moduleSettings[$module] ) ){
                    //$strict = ( isset( $moduleSettings[ 'strict' ] ) && $moduleSettings[ 'strict' ] ) ? true:false;
                    //there are actions defined, check if this actions is secure
                    if( isset( $moduleSettings[ $module ][ 'actions' ] ) ){
                        //this is a secure action
                        if( !$request->isSecure() &&
                            is_array( $moduleSettings[ $module ][ 'actions' ] ) &&
                            in_array($action, $moduleSettings[ $module ][ 'actions' ])
                        ){

                            //we need to redirect to a secure url
                            return $this->redirectSecure( $request );
                        }//else: the request should be secure, and is. No more to be done.
                    //module was defined, but no actions were
                    }else if( !$request->isSecure() ){
                        //every action in this module is secure, redirect
                        return $this->redirectSecure( $request );
                    }
                }else if( $request->isSecure() && $strict ){
                    //redirect back to http, strict is set
                    return $this->redirectUnsecure( $request );
                }
            }
        }
        //no redirect necessary, continue the filter chain
        $filterChain->execute();
    }

    /**
     * redirects an http request to https
     *
     * @param sfWebRequest $request
     * @return boolean
     */
    protected function redirectSecure( $request ){
      //if Uri is an IP address, just swap to https

      if( preg_match( '/(([2]([0-4][0-9]|[5][0-5])|[0-1]?[0-9]?[0-9])[.]){3}(([2]([0-4][0-9]|[5][0-5])|[0-1]?[0-9]?[0-9]))/', $request->getHost()  ) ) {
        $url = str_replace( 'http', 'https', $request->getUri() );
      } else {
        //otherwise, replace the server name with the secure domain as well as change it to https
        $url = str_replace( $_SERVER['HTTP_HOST'], sfConfig::get('app_ssl_redirect_secure_domain', $_SERVER['HTTP_HOST']), $request->getUri()  );
        $url = str_replace( 'http', 'https', $url );
      }

      return $this->getContext()->getController()->redirect( $url, 0, 301 );
    }

    /**
     * redirects an https request to http
     *
     * @param sfWebRequest $request
     * @return boolean
     */
    protected function redirectUnsecure( $request ){
        //replace https w/ http
        $url = str_replace( 'https', 'http', $request->getUri() );
        return $this->getContext()->getController()->redirect( $url, 0, 301 );
    }
}
