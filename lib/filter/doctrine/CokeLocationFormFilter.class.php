<?php

/**
 * CokeLocation filter form.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     Nik Spijkerman
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CokeLocationFormFilter extends BaseCokeLocationFormFilter
{
  public function configure()
  {
  }
}
