<?php

/**
 * SACBuyRedemptionCode filter form.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SACBuyRedemptionCodeFormFilter extends BaseSACBuyRedemptionCodeFormFilter
{
  public function configure()
  {
      $this->widgetSchema['location'] = new sfWidgetFormChoice(array('choices' => array_merge(array(''=>''), $this->getTable()->getLocations())));
  }
}
