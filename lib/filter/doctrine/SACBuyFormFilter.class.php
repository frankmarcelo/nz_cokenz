<?php

/**
 * SACBuy filter form.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SACBuyFormFilter extends BaseSACBuyFormFilter
{
    public function configure()
    {
        $payment_status = $this->getTable()->getPaymentStatuses();
        $redemption_status = $this->getTable()->getRedemptionStatuses();

        $this->widgetSchema['location'] = new sfWidgetFormChoice(array('choices' => array_merge(array(''=>''), $this->getTable()->getLocations())));
        $this->widgetSchema['payment_status'] = new sfWidgetFormChoice(array('choices' => array_merge(array(''=>''), array_combine($payment_status, array_map('ucfirst', $payment_status)))));
        $this->widgetSchema['redemption_status'] = new sfWidgetFormChoice(array('choices' => array_merge(array(''=>''), array_combine($redemption_status, array_map('ucfirst', $redemption_status)))));
    }
}
