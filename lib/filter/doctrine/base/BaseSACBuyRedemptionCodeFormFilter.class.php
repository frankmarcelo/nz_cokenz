<?php

/**
 * SACBuyRedemptionCode filter form base class.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSACBuyRedemptionCodeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'location'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'used'      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'allocated' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
    ));

    $this->setValidators(array(
      'code'      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'location'  => new sfValidatorPass(array('required' => false)),
      'used'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'allocated' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
    ));

    $this->widgetSchema->setNameFormat('sac_buy_redemption_code_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SACBuyRedemptionCode';
  }

  public function getFields()
  {
    return array(
      'id'        => 'Number',
      'code'      => 'Number',
      'location'  => 'Text',
      'used'      => 'Boolean',
      'allocated' => 'Boolean',
    );
  }
}
