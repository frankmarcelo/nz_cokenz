<?php

/**
 * CocaColaArticle filter form base class.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCocaColaArticleFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'title'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'article_date'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'article_type'      => new sfWidgetFormChoice(array('choices' => array('' => '', 'article' => 'article', 'link' => 'link'))),
      'title_image'       => new sfWidgetFormFilterInput(),
      'thumb_image'       => new sfWidgetFormFilterInput(),
      'short_description' => new sfWidgetFormFilterInput(),
      'article_content'   => new sfWidgetFormFilterInput(),
      'image_gallery_url' => new sfWidgetFormFilterInput(),
      'video_gallery_url' => new sfWidgetFormFilterInput(),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'slug'              => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'title'             => new sfValidatorPass(array('required' => false)),
      'article_date'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'article_type'      => new sfValidatorChoice(array('required' => false, 'choices' => array('article' => 'article', 'link' => 'link'))),
      'title_image'       => new sfValidatorPass(array('required' => false)),
      'thumb_image'       => new sfValidatorPass(array('required' => false)),
      'short_description' => new sfValidatorPass(array('required' => false)),
      'article_content'   => new sfValidatorPass(array('required' => false)),
      'image_gallery_url' => new sfValidatorPass(array('required' => false)),
      'video_gallery_url' => new sfValidatorPass(array('required' => false)),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'slug'              => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('coca_cola_article_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CocaColaArticle';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'title'             => 'Text',
      'article_date'      => 'Date',
      'article_type'      => 'Enum',
      'title_image'       => 'Text',
      'thumb_image'       => 'Text',
      'short_description' => 'Text',
      'article_content'   => 'Text',
      'image_gallery_url' => 'Text',
      'video_gallery_url' => 'Text',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
      'slug'              => 'Text',
    );
  }
}
