<?php

/**
 * SACBuy filter form base class.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSACBuyFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SACUser'), 'add_empty' => true)),
      'user_phone'           => new sfWidgetFormFilterInput(),
      'location'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'recipient_first_name' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'recipient_last_name'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'recipient_email'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'recipient_mobile'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'payment_status'       => new sfWidgetFormFilterInput(),
      'redemption_status'    => new sfWidgetFormFilterInput(),
      'redemption_code_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('RedemptionCode'), 'add_empty' => true)),
      'redemption_date'      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'created_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'           => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'user_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('SACUser'), 'column' => 'id')),
      'user_phone'           => new sfValidatorPass(array('required' => false)),
      'location'             => new sfValidatorPass(array('required' => false)),
      'recipient_first_name' => new sfValidatorPass(array('required' => false)),
      'recipient_last_name'  => new sfValidatorPass(array('required' => false)),
      'recipient_email'      => new sfValidatorPass(array('required' => false)),
      'recipient_mobile'     => new sfValidatorPass(array('required' => false)),
      'payment_status'       => new sfValidatorPass(array('required' => false)),
      'redemption_status'    => new sfValidatorPass(array('required' => false)),
      'redemption_code_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('RedemptionCode'), 'column' => 'id')),
      'redemption_date'      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'created_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'           => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('sac_buy_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SACBuy';
  }

  public function getFields()
  {
    return array(
      'id'                   => 'Number',
      'user_id'              => 'ForeignKey',
      'user_phone'           => 'Text',
      'location'             => 'Text',
      'recipient_first_name' => 'Text',
      'recipient_last_name'  => 'Text',
      'recipient_email'      => 'Text',
      'recipient_mobile'     => 'Text',
      'payment_status'       => 'Text',
      'redemption_status'    => 'Text',
      'redemption_code_id'   => 'ForeignKey',
      'redemption_date'      => 'Date',
      'created_at'           => 'Date',
      'updated_at'           => 'Date',
    );
  }
}
