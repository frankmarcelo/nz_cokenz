<?php

/**
 * CokeLocation filter form base class.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCokeLocationFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'type_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Type'), 'add_empty' => true)),
      'name'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'address'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'suburb'           => new sfWidgetFormFilterInput(),
      'city'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'postcode'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'phone'            => new sfWidgetFormFilterInput(),
      'show_in_finder'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_card_location' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'card_redeem_code' => new sfWidgetFormFilterInput(),
      'created_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'       => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'latitude'         => new sfWidgetFormFilterInput(),
      'longitude'        => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'type_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Type'), 'column' => 'id')),
      'name'             => new sfValidatorPass(array('required' => false)),
      'address'          => new sfValidatorPass(array('required' => false)),
      'suburb'           => new sfValidatorPass(array('required' => false)),
      'city'             => new sfValidatorPass(array('required' => false)),
      'postcode'         => new sfValidatorPass(array('required' => false)),
      'phone'            => new sfValidatorPass(array('required' => false)),
      'show_in_finder'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_card_location' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'card_redeem_code' => new sfValidatorPass(array('required' => false)),
      'created_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'       => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'latitude'         => new sfValidatorPass(array('required' => false)),
      'longitude'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('coke_location_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CokeLocation';
  }

  public function getFields()
  {
    return array(
      'id'               => 'Number',
      'type_id'          => 'ForeignKey',
      'name'             => 'Text',
      'address'          => 'Text',
      'suburb'           => 'Text',
      'city'             => 'Text',
      'postcode'         => 'Text',
      'phone'            => 'Text',
      'show_in_finder'   => 'Boolean',
      'is_card_location' => 'Boolean',
      'card_redeem_code' => 'Text',
      'created_at'       => 'Date',
      'updated_at'       => 'Date',
      'latitude'         => 'Text',
      'longitude'        => 'Text',
    );
  }
}
