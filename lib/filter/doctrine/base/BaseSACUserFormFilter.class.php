<?php

/**
 * SACUser filter form base class.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseSACUserFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'first_name'     => new sfWidgetFormFilterInput(),
      'last_name'      => new sfWidgetFormFilterInput(),
      'email'          => new sfWidgetFormFilterInput(),
      'location'       => new sfWidgetFormFilterInput(),
      'facebook_id'    => new sfWidgetFormFilterInput(),
      'facebook_token' => new sfWidgetFormFilterInput(),
      'created_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'     => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'latitude'       => new sfWidgetFormFilterInput(),
      'longitude'      => new sfWidgetFormFilterInput(),
    ));

    $this->setValidators(array(
      'first_name'     => new sfValidatorPass(array('required' => false)),
      'last_name'      => new sfValidatorPass(array('required' => false)),
      'email'          => new sfValidatorPass(array('required' => false)),
      'location'       => new sfValidatorPass(array('required' => false)),
      'facebook_id'    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'facebook_token' => new sfValidatorPass(array('required' => false)),
      'created_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'     => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'latitude'       => new sfValidatorPass(array('required' => false)),
      'longitude'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sac_user_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SACUser';
  }

  public function getFields()
  {
    return array(
      'id'             => 'Number',
      'first_name'     => 'Text',
      'last_name'      => 'Text',
      'email'          => 'Text',
      'location'       => 'Text',
      'facebook_id'    => 'Number',
      'facebook_token' => 'Text',
      'created_at'     => 'Date',
      'updated_at'     => 'Date',
      'latitude'       => 'Text',
      'longitude'      => 'Text',
    );
  }
}
