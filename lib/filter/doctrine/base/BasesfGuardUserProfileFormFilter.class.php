<?php

/**
 * sfGuardUserProfile filter form base class.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasesfGuardUserProfileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'gender'              => new sfWidgetFormFilterInput(),
      'dob'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'phone'               => new sfWidgetFormFilterInput(),
      'email_updates'       => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'mobile_updates'      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'region'              => new sfWidgetFormFilterInput(),
      'postcode'            => new sfWidgetFormFilterInput(),
      'registration_source' => new sfWidgetFormChoice(array('choices' => array('' => '', 'desktop' => 'desktop', 'mobile' => 'mobile'))),
      'created_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'          => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'user_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('User'), 'column' => 'id')),
      'gender'              => new sfValidatorPass(array('required' => false)),
      'dob'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDate(array('required' => false)), 'to_date' => new sfValidatorDateTime(array('required' => false)))),
      'phone'               => new sfValidatorPass(array('required' => false)),
      'email_updates'       => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'mobile_updates'      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'region'              => new sfValidatorPass(array('required' => false)),
      'postcode'            => new sfValidatorPass(array('required' => false)),
      'registration_source' => new sfValidatorChoice(array('required' => false, 'choices' => array('desktop' => 'desktop', 'mobile' => 'mobile'))),
      'created_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'          => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('sf_guard_user_profile_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }

  public function getFields()
  {
    return array(
      'id'                  => 'Number',
      'user_id'             => 'ForeignKey',
      'gender'              => 'Text',
      'dob'                 => 'Date',
      'phone'               => 'Text',
      'email_updates'       => 'Boolean',
      'mobile_updates'      => 'Boolean',
      'region'              => 'Text',
      'postcode'            => 'Text',
      'registration_source' => 'Enum',
      'created_at'          => 'Date',
      'updated_at'          => 'Date',
    );
  }
}
