<?php

/**
 * CardCode filter form.
 *
 * @package    Coke NZ
 * @subpackage filter
 * @author     Nik Spijkerman
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CardCodeFormFilter extends BaseCardCodeFormFilter
{
  public function configure()
  {
  }
}
