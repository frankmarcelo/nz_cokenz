<?php

class restrictedAccessFilter extends sfFilter
{

  public function execute(sfFilterChain $filterChain)
  {
    if($this->isFirstCall()){

      $deny = sfConfig::get('app_restricted_access_deny','all');
      $allow = sfConfig::get('app_restricted_access_allow',array());
      $excluded = sfConfig::get('app_restricted_access_excluded_modules',array());
      $user_ip = $_SERVER['REMOTE_ADDR'];
      $module = $this->context->getRequest()->getParameter('module');
      $execute = false;


      //Add Auth module to excluded array
      $excluded = array_merge($excluded,array( sfConfig::get('app_restricted_access_auth_module','sfGuardAuth') ));

      //check if requested module has been excluded
      if( in_array($module,$excluded) ) {
        $execute = true;
      }

      //check if the request is coming from a local IP and allow pass through
      if( $user_ip === '127.0.0.1' or strstr($user_ip,'192.168.') ) {
        $execute = true;
      }

      //check if user is using the cookie to pass through
      if( $this->getCookie() ) {
        $execute = true;
      }

      //check if the user is explicitly denied
      if( is_array($deny) ) {
        if( in_array($user_ip, $deny) ) {
          $this->redirectToDenied();
        }
      }

      //finally, if still not allowed, check the allow parameter for 'ALL' or the user's IP
      if( $allow === 'all' ) {
        $execute = true;
      } else {
        if( is_array($allow) ){
          if(in_array($user_ip,$allow)){
            $execute = true;
          }
        }
      }

      if( $execute ) {
        $filterChain->execute();
      } else {
        $this->redirectToDenied();
      }

    }
  }



  private function getCookie()
  {
    $config = sfConfig::get('app_restricted_access_cookie',false);

    if( $config['enabled'] === true ) {
      if( $this->context->getRequest()->getCookie( $config['name']) ) {
        return true;
      }
    }
    return false;
  }

  private function redirectToDenied()
  {
    $this->context->getController()->redirect(sfConfig::get('app_restricted_access_deny_module','sfGuardAuth').'/'.sfConfig::get('app_restricted_access_deny_action','denied'));

    throw new sfStopException();
  }


}
