<?php

class sfWidgetFormSchemaFormatterMobileShareACoke extends sfWidgetFormSchemaFormatter
{
    protected
        $rowFormat                 = "<div class=\"form_row\">\n%error%%label%\n<div class=\"form_field_container\">%field%\n%help%</div>\n</div>%hidden_fields%\n",
        $helpFormat                = '<span class="form_help">%help%</span>',
        $errorRowFormat            = "<li>\n%errors%</li>\n",
        $errorListFormatInARow     = "<ul class=\"form_errors\">\n%errors%  </ul>\n",
        $errorRowFormatInARow      = "<li>%error%</li>\n",
        $namedErrorRowFormatInARow = "<li>%name%: %error%</li>\n",
        $decoratorFormat           = "%content%"
    ;


    public function formatRow($label, $field, $errors = array(), $help = '', $hiddenFields = null)
    {
      return strtr($this->getRowFormatCheckErrors($errors), array(
        '%label%'         => $label,
        '%field%'         => $field,
        '%error%'         => $this->formatErrorsForRow($errors),
        '%help%'          => $this->formatHelp($help),
        '%hidden_fields%' => null === $hiddenFields ? '%hidden_fields%' : $hiddenFields,
      ));
    }

    public function getRowFormatCheckErrors($errors)
    {
        if ($errors) {
            return "<div class=\"form_row error_text\">%label%\n<div class=\"form_field_container\">%field%\n%error%\n%help%</div>\n</div>%hidden_fields%\n";
        }
        return $this->rowFormat;
    }
}
