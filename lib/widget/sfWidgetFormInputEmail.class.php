<?php

/**
 * sfWidgetFormInputEmail represents an HTML email input tag.
 */
class sfWidgetFormInputEmail extends sfWidgetFormInput
{
  /**
   * Configures the current widget.
   *
   * @param array $options     An array of options
   * @param array $attributes  An array of default HTML attributes
   *
   * @see sfWidgetForm
   */
  protected function configure($options = array(), $attributes = array())
  {
    parent::configure($options, $attributes);

    $this->setOption('type', 'email');
  }
}
