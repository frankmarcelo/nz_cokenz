<?php

class sfWidgetFormSchemaFormatterMobile extends sfWidgetFormSchemaFormatter
{
  protected
    $rowFormat                 = "<div class=\"form_row\">\n%error%%label%\n<div class=\"form_field_container\">%field%\n%help%</div>\n</div>%hidden_fields%\n",
    $helpFormat                = '<span class="form_help">%help%</span>',
    $errorRowFormat            = "<li>\n%errors%</li>\n",
    $errorListFormatInARow     = "<ul class=\"form_errors\">\n%errors%  </ul>\n",
    $errorRowFormatInARow      = "<li>%error%</li>\n",
    $namedErrorRowFormatInARow = "<li>%name%: %error%</li>\n",
    $decoratorFormat           = "%content%"
  ;
}
