<?php

class sfWidgetFormChoiceNoList extends sfWidgetFormChoice
{

  protected function configure($options = array(), $attributes = array())
  {
    parent::configure($options, $attributes);
    $this->setOption('renderer_options', array('formatter' => array('sfWidgetFormChoiceNoList', 'myFormatter')));
  }

  public static function myFormatter($widget, $inputs)
  {

    $result = '';
    foreach ($inputs as $input) {
      $result .= $input ['label'] . '   ' . $input ['input'];
    }
    return $result;

  }

}
