<?php

class sfWidgetReadOnly extends sfWidgetForm
{

  public function render($name, $value = null, $attributes = array(), $errors = array())
  {
    if(null === $value) {
      $value = "&mdash;";
    }
    return $this->renderContentTag('p', self::escapeOnce($value), $attributes);
  }
}
