<?php

include "vendor/PxPay_PHP_Curl/PxPay_Curl.inc.php";

class ShareACokePayment
{
    const PXPAY_URL = "https://sec.paymentexpress.com/pxpay/pxaccess.aspx";
    const PXPAY_USERID = "ShareaCoke";
    const PXPAY_KEY = "193662ddc02831e11cb6ccb4f51c4e3df5c2c62d62687fd15300307c13684eb9";

    const PXPAY_DEV_USERID = "SatelliteMedia_dev";
    const PXPAY_DEV_KEY = "97358f0c56db0d4052d675b285cc978c9c59ab2e88a97126a6cdaac02c040efe";

    const PRICE = 3;
    const QUANTITY = 1;

    private $pxpay;
    private $response = null;

    public function getPxPay()
    {
        return $this->pxpay;
    }

    public function __construct($dev_mode = false)
    {
        $user_id = self::PXPAY_USERID;
        $key = self::PXPAY_KEY;

        if ($dev_mode) {
            $user_id = self::PXPAY_DEV_USERID;
            $key = self::PXPAY_DEV_KEY;
        }

        $this->pxpay = new PxPay_Curl(self::PXPAY_URL, $user_id, $key);
    }

    public function createRequest($MerchantReference, $return_url)
    {
        $request = new PxPayRequest();

        #Calculate AmountInput
        $AmountInput = self::PRICE * self::QUANTITY;

        #Generate a unique identifier for the transaction
        $TxnId = uniqid("ID");

        $request->setMerchantReference($MerchantReference);
        $request->setAmountInput($AmountInput);
        $request->setTxnType("Purchase");
        $request->setCurrencyInput("NZD");
        $request->setUrlFail($return_url);
        $request->setUrlSuccess($return_url);
        $request->setTxnId($TxnId);

        #Call makeRequest function to obtain input XML
        $request_string = $this->pxpay->makeRequest($request);

        #Obtain output XML
        $response = new MifMessage($request_string);

        #Parse output XML
        $url = $response->get_element_text("URI");
        $valid = $response->get_attribute("valid");

        return $url;
    }


    public function getResponse()
    {
        $enc_hex = sfContext::getInstance()->getRequest()->getParameter('result');
        $this->response = $this->pxpay->getResponse($enc_hex);
        return $this->response;
    }

    public function isSuccessful()
    {
        if ($this->response === null) {
            throw new sfException('Response is null.');
        }

        return ($this->response->getSuccess() == "1");
    }

}
