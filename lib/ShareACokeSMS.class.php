<?php

class ShareACokeSMS
{
    const WSDL_URL = 'http://gateway.sonicmobile.com/WSDLv2';
    const APPLICATION = 'gw_satellite_media7';
    const PASSWORD = 'prqwksi1m';
    const CUSTOMER = 'satellite_media';
    const MESSAGE_CLASS = 'mt_message';
    const MESSAGE_REFERENCE = '';

    public $return = null;

    public function sendMessage($to, $code, $location, $consumer_name, $recipient_name)
    {
        try {

            $message = strtr(
                sfConfig::get('app_shareacoke_sms_message_pattern'),
                array(
                    '$$recipient_name$$' => $recipient_name,
                    '$$consumer_name$$' => $consumer_name,
                    '$$location$$' => $location,
                    '$$uniqueID$$' => $code
                )
            );

            $client = new SoapClient(
                self::WSDL_URL,
                array(
                    'trace' => true,
                    'exceptions' => true,
                    'encoding' => 'utf-8',
                )
            );

            //Replace leading zero with +64
            $to = '+64'.substr($to,1,strlen($to)-1);

            $params = array(
                self::APPLICATION,
                self::PASSWORD,
                $to,
                $message,
                null,
                null,
                self::CUSTOMER,
                self::MESSAGE_CLASS
            );

            $this->return = $client->__call("sendMessage", $params);

        } catch (SoapFault $fault) {
            error_log("SOAP Fault: (faultcode: {$fault->faultcode}, faultstring: {$fault->faultstring})\n\nParams:\n".print_r($params, true), 1, 'nik@satellitemedia.co.nz');
            return false;
        }
    }

}
