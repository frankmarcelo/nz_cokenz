<?php
 
function youtube_link($video_id)
{
  if( is_android() ) {
    return 'vnd.youtube:'.$video_id;
  }

  if( is_iphone() ) {
    return 'youtube:'.$video_id;
  }

  return 'http://youtu.be/'.$video_id;
}

function is_android()
{
  $user_agent       = $_SERVER['HTTP_USER_AGENT'];

  if(preg_match('/android/i',$user_agent)){
    return true;
  }

  return false;
}


function is_iphone()
{
  $user_agent       = $_SERVER['HTTP_USER_AGENT'];

  if( preg_match('/ipod/i',$user_agent) or preg_match('/iphone/i',$user_agent)) {
    return true;
  }

  return false;
}

function slugify($text)
{
  // replace all non letters or digits by -
  $text = preg_replace('/\W+/', '-', $text);

  // trim and lowercase
  $text = strtolower(trim($text, '-'));

  return $text;

}