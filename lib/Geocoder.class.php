<?php

/*
##############################################################
Version: 1.0
Author: Abram Morphew. Modified and updated by Nik Spijkerman
Created: 2011-02-10

File: class.Geocoder.php
Desc: PHP class for geocoding with Google Geocoder API. No API
key is necessary to use this class.

##############################################################
*/


class Geocoder {
    public $status = null;

    public
        $subpremise = null,
        $street_number = null,
        $route = null,
        $sublocality = null,
        $locality = null,
        $area = null,
        $full_address = null,
        $postal_code = null
    ;

    public $location_type = null;
    public $response = array();
    public $coordinates = array();

    function __construct() { }

    function coordinates($address=null, $city=null, $postcode=null, $country=null) {
        $query = urlencode($address.", ".$city." ".$postcode.', '.$country);
        $url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$query."&sensor=false&region=nz";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);

        // set public vars and return...
        $this->response = json_decode($response,true);
        
        $full_address = '';
        if($this->response['status'] == 'ZERO_RESULTS' or empty($this->response['results']) ){
            $this->status = $this->response['status'];
            return null;
        }

//        var_dump($this->response['results'][0]);
//        var_dump($this->response['results'][0]['address_components']);exit;
        foreach( $this->response['results'][0]['address_components'] as $part  )
        {
            switch( $part['types'][0] )
            {
                case 'subpremise':
                    $this->subpremise = $part['long_name'];
                    break;
                case 'street_number':
                    $this->street_number = $part['long_name'];
                    break;
                case 'route':
                    $this->route = $part['long_name'];
                    break;
                case 'sublocality':
                    $this->sublocality = $part['long_name'];
                    break;
                case 'locality':
                    $this->locality = $part['long_name'];
                    break;
                case 'administrative_area_level_2':
                    $this->area = $part['long_name'];
                    break;
                case 'postal_code':
                    $this->postal_code = $part['long_name'];
                    break;
            }
        }

        
        $this->status = $this->response['status'];
        if( $this->status == 'OK' ){
          $this->full_address = $this->response['results'][0]['formatted_address'];
          $this->location_type = $this->response['results'][0]['geometry']['location_type'];
          $this->coordinates = $this->response['results'][0]['geometry']['location'];
          return $this->coordinates;
        } else {
          return null;
        }
    }

}