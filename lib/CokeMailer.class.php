<?php

class CokeMailer
{

  private static function translateRegoCompleteEmailBody($body, $first_name)
  {
    return strtr($body, array(
        '%%FIRST_NAME%%' => $first_name
    ));
  }

  public static function sendRegoCompleteEmail($to, $name)
  {
    $mailer = sfContext::getInstance()->getMailer();
    $config = sfConfig::get('app_register_form_email', array());
    $message = $mailer->compose(
                  array($config['from_email'] => $config['from_name']), /* From */
                  $to, /* To */
                  $config['subject'] /* Subject */
    );

    $message->setBody( self::translateRegoCompleteEmailBody( file_get_contents($config['text_template']), $name ));

    if (!$mailer->send($message))
    {
      throw new sfException('Could not send contact email.');
    }

  }

  private static function translateContactEmailBody($body, sfForm $form)
  {
    $types = sfConfig::get('app_contact_form_type_choices', array());

    $values = array(
        '%%TYPE%%' => $types[$form->getValue('type')],
        '%%FIRST_NAME%%' => $form->getValue('first_name'),
        '%%LAST_NAME%%' => $form->getValue('last_name'),
        '%%ADDRESS%%' => $form->getValue('address'),
        '%%PHONE_NUMBER%%' => $form->getValue('phone_number'),
        '%%EMAIL%%' => $form->getValue('email'),
        '%%ENQUIRY%%' => $form->getValue('enquiry')
    );

    if ($form->getValue('type') == 'product') {
      $values['%%PRODUCT_NAME%%'] = $form->getValue('product_name');
      $values['%%PACK_SIZE%%'] = $form->getValue('pack_size');
      $values['%%BEST_BEFORE_DATE%%'] = $form->getValue('best_before_date');
      $values['%%PRODUCTION_CODE%%'] = $form->getValue('production_code');
      $values['%%PROD_STORE%%'] = $form->getValue('prod_store');
      $values['%%PROD_SUBURB_TOWN%%'] = $form->getValue('prod_suburb_town');
    }

    if ($form->getValue('type') == 'promotion') {
      $values['%%PROMOTION_NAME%%'] = $form->getValue('promotion_name');
      $values['%%PROMO_STORE%%'] = $form->getValue('promo_store');
      $values['%%PROMO_SUBURB_TOWN%%'] = $form->getValue('promo_suburb_town');
    }

    return strtr($body, $values);

  }

  public static function sendContactEmail(sfMailer $mailer, sfForm $form)
  {

    $config = sfConfig::get('app_contact_form_email', array());
    $types = sfConfig::get('app_contact_form_type_choices', array());

    $message = $mailer->compose(
        /* From */     array($config['from_email'] => $config['from_name']),
        /* To */       array($config['to_email'] => $config['to_name']),
        /* Subject */  $config['subject'] . ' - ' . $types[$form->getValue('type')]
    );

    $message->setReplyTo(array($form->getValue('email') => sprintf('%s %s', $form->getValue('first_name'), $form->getValue('last_name'))));   

    if( get_class($form) === 'MobileContactForm' ) {
      $template  = 'general';
    } else {
      switch ($form->getValue('type')) {
        case 'product':
        case 'promotion':
          $template = $form->getValue('type');
          break;
        default:
          $template = 'general';
      }
    }
    $message->setBody( self::translateContactEmailBody( file_get_contents( $config['template_path'] . $template . '.txt' ), $form ));

    if (!$mailer->send($message))
    {
      throw new sfException('Could not send contact email.');
    }
  }
}
