<?php

class SecurePDOSessionStorage extends sfPDOSessionStorage
{
  public function regenerate($destroy = false)
  {
    parent::regenerate(true);
  }

    /**
    * Reads a session.
    *
    * @param  string $id  A session ID
    *
    * @return string      The session data if the session was read or created, otherwise an exception is thrown
    *
    * @throws <b>DatabaseException</b> If the session cannot be read
    */
    public function sessionRead($id)
    {
        // get table/columns
        $db_table    = $this->options['db_table'];
        $db_data_col = $this->options['db_data_col'];
        $db_id_col   = $this->options['db_id_col'];
        $db_time_col = $this->options['db_time_col'];

        try
        {
         $sql = 'SELECT '.$db_data_col.' FROM '.$db_table.' WHERE '.$db_id_col.'=?';

         $stmt = $this->db->prepare($sql);
         $stmt->bindParam(1, $id, PDO::PARAM_STR, 255);

         $stmt->execute();
         // it is recommended to use fetchAll so that PDO can close the DB cursor
         // we anyway expect either no rows, or one row with one column. fetchColumn, seems to be buggy #4777
         $sessionRows = $stmt->fetchAll(PDO::FETCH_NUM);

         if (count($sessionRows) == 1)
         {
            if (is_resource($sessionRows[0][0]) ) {
                return stream_get_contents($sessionRows[0][0]);
            } else {
                return $sessionRows[0][0];
            }
         }
         else
         {
           // session does not exist, create it
           $sql = 'INSERT INTO '.$db_table.'('.$db_id_col.', '.$db_data_col.', '.$db_time_col.') VALUES (?, ?, ?)';

           $stmt = $this->db->prepare($sql);
           $stmt->bindParam(1, $id, PDO::PARAM_STR);
           $stmt->bindValue(2, '', PDO::PARAM_STR);
           $stmt->bindValue(3, time(), PDO::PARAM_INT);
           $stmt->execute();

           return '';
         }
        }
        catch (PDOException $e)
        {
         throw new sfDatabaseException(sprintf('PDOException was thrown when trying to manipulate session data. Message: %s', $e->getMessage()));
        }
    }


}
