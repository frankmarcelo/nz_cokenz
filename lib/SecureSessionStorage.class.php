<?php

class SecureSessionStorage extends sfSessionStorage
{
  public function regenerate($destroy = false)
  {
    parent::regenerate(true);
  }
}
