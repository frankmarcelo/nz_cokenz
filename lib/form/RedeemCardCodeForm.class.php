<?php

class RedeemCardCodeForm extends BaseForm
{
  const TIME_PERIOD_ERROR_MESSAGE = 'SORRY, YOU CAN ONLY REDEEM ONE ACTIVATION CODE PER DAY. SEE YOU TOMORROW!';
  const INVALID_ERROR_MESSAGE = 'SORRY, WE DON\'T RECOGNISE THAT CODE, PLEASE TRY AGAIN.';

  public function configure()
  {

    $this->widgetSchema['code'] = new sfWidgetFormInputText(array(
      'label' => false,
    ),array(
      'placeholder' => 'enter code'
    ));

    $this->widgetSchema['user_card_id'] = new sfWidgetFormInputHidden();

    $this->validatorSchema['code'] = new sfValidatorString(array('trim' => true));
    $this->validatorSchema['user_card_id'] = new sfValidatorNumber();

    $this->validatorSchema->setPostValidator(
      new validatorLoyaltyCardCode(array(),array(
          'invalid'         => self::INVALID_ERROR_MESSAGE,
          'min_time_period' => self::TIME_PERIOD_ERROR_MESSAGE
        )
    ));

    $this->widgetSchema->setNameFormat('redeem[%s]');

  }

}
