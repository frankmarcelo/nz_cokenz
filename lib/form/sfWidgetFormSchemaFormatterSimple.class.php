<?php

class sfWidgetFormSchemaFormatterSimple extends sfWidgetFormSchemaFormatter
{
  protected
    $rowFormat                 = '<div class="row">%label% %field% %error% %help% %hidden_fields%</div>',
    $helpFormat                = '<span class="help">%help%</span>',
    $errorRowFormat            = '%errors%',
    $errorListFormatInARow     = '%errors%',
    $errorRowFormatInARow      = '<span class="errormsg">%error%</span>',
    $namedErrorRowFormatInARow = '<span class="errormsg">%name%: %error%</span>',
    $decoratorFormat           = '%content%';


}