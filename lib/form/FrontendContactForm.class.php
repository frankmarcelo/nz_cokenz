<?php

class FrontendContactForm extends MobileContactForm
{
  public function configure()
  {
    parent::configure();

    $this->widgetSchema['type'] = new sfWidgetFormChoice(array('choices' => $this->getChoicesForType(), 'expanded' => true, 'label' => false));

    $this->widgetSchema['date_of_birth']->setOption('empty_values' , array('year' => 'Year', 'month' => 'Month', 'day' => 'Day'));

    //Product enquiry fields
    $this->widgetSchema['product_name'] = new sfWidgetFormInputText();
    $this->widgetSchema['pack_size'] = new sfWidgetFormInputText();
    $this->widgetSchema['best_before_date'] = new sfWidgetFormInputText();
    $this->widgetSchema['production_code'] = new sfWidgetFormInputText();
    $this->widgetSchema['prod_store'] = new sfWidgetFormInputText();
    $this->widgetSchema['prod_suburb_town'] = new sfWidgetFormInputText();

    //Promotion enquiry fields
    $this->widgetSchema['promotion_name'] = new sfWidgetFormInputText();
    $this->widgetSchema['promo_store'] = new sfWidgetFormInputText();
    $this->widgetSchema['promo_suburb_town'] = new sfWidgetFormInputText();

    $this->validatorSchema['first_name']->setMessage('required','Please enter your first name.');
    $this->validatorSchema['last_name']->setMessage('required','Please enter your last name.');
    $this->validatorSchema['address']->setMessage('required', 'Please enter your address.');
    $this->validatorSchema['date_of_birth']->setMessage('required', 'Please enter your date of birth.');
    $this->validatorSchema['phone_number']->setMessage('required','Please enter your phone number.');
    $this->validatorSchema['email']->setMessage('required','Please enter your email.');
    $this->validatorSchema['enquiry']->setMessage('required','Please enter your enquiry.');
    $this->validatorSchema['security_code']->setMessage('required','Please enter the code as shown in the image.');
    $this->validatorSchema['security_code']->setMessage('invalid','Please enter the code as shown in the image.');

    //Product enquiry fields
    $this->validatorSchema['product_name'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['pack_size'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['best_before_date'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['production_code'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['prod_store'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['prod_suburb_town'] = new sfValidatorString(array('required' => false));

    //Promotion enquiry fields
    $this->validatorSchema['promotion_name'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['promo_store'] = new sfValidatorString(array('required' => false));
    $this->validatorSchema['promo_suburb_town'] = new sfValidatorString(array('required' => false));

    $this->widgetSchema->setFormFormatterName('simple');


  }

  public function getTaintedValue($field)
  {
    return (isset($this->taintedValues[$field])) ? $this->taintedValues[$field] : null;
  }

}
