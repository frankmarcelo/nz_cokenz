<?php

class SelectLocationForm extends BaseForm
{
  public function configure()
  {

    $this->setWidget('location', new sfWidgetFormDoctrineChoice(
      array('model' => 'CokeLocation', 'table_method' => 'getLocationsForSelectQuery', 'method' => 'getLocationLabel', 'label' => false, 'add_empty' => 'Select a location' ),
      array('data-theme' => 'c')
    ));
    $this->setValidator('location', new sfValidatorDoctrineChoice(array('model' => 'CokeLocation')));

    $this->widgetSchema->setNameFormat('locations[%s]');
  }

}
