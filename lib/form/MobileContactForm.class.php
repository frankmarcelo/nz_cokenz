<?php

class MobileContactForm extends BaseForm
{

  public function configure()
  {
    //Date of birth widget
    $years = range(date('Y'), date('Y') - 120);
    $months = array();
    for($i=1;$i<=12;$i++) $months[$i] = date('M',mktime(0,0,0,$i,1,2012));

    $this->setWidgets(array(
      'type'          => new sfWidgetFormChoiceNoList(array('choices' => $this->getChoicesForType(), 'expanded' => true)),
      'first_name'    => new sfWidgetFormInputText(array('label' => '*First Name')),
      'last_name'     => new sfWidgetFormInputText(array('label' => '*Last Name')) ,
      'address'       => new  sfWidgetFormInputText(),
      'phone_number'  => new  sfWidgetFormInputPhone(array('label' => '*Phone Number')),
      'email'         => new  sfWidgetFormInputEmail(array('label' => '*Email address')),
      'date_of_birth' => new sfWidgetFormDate(array(
        'years'         => array_combine($years, $years),
        'months'        => $months,
        'format'        => '%day%%month%%year%',
        'empty_values'  => array('year' => 'Y', 'month' => 'M', 'day' => 'D'),
        'label'         => '*Date of birth'
      )),
      'enquiry'       => new sfWidgetFormTextarea(array('label' => '*What is the nature of your enquiry?')),
      'security_code' => new sfWidgetCaptchaGD(array('label' => '*Security Code')),
      'accept_pp'     => new sfWidgetFormInputCheckbox()
    ));

    $this->setValidators(array(
      'type'          => new sfValidatorChoice(array('choices' => array_keys($this->getChoicesForType()))),
      'first_name'    => new sfValidatorString(),
      'last_name'     => new sfValidatorString(),
      'address'       => new sfValidatorString(array('required' => false)),
      'phone_number'  => new sfValidatorString(),
      'email'         => new sfValidatorEmail(),
      'date_of_birth' => new sfValidatorDate(),
      'enquiry'       => new sfValidatorString(),
      'security_code' => new sfCaptchaGDValidator(array('length' => 6)),
      'accept_pp'     => new sfValidatorBoolean(array('required' => true))
    ));

    $this->widgetSchema->setNameFormat('contact[%s]');


  }

  protected function getChoicesForType()
  {
    $choices = sfConfig::get('app_contact_form_type_choices', array());

    return $choices;
  }

}
