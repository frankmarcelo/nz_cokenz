<?php

class MobileSigninForm extends BaseForm
{

  public function setup()
  {
    $this->setWidgets(array(
      'username' => new sfWidgetFormInputText(array('label' => false)),
      'password' => new sfWidgetFormInputPassword(array('label' => false,'type'  => 'password')),
      '_target_path' => new sfWidgetFormInputHidden()
    ));

    $this->setValidators(array(
      'username' => new sfValidatorString(),
      'password' => new sfValidatorString(),
      '_target_path' => new sfValidatorPass()
    ));

    sfConfig::get('app_sf_guard_plugin_allow_login_with_email', true);

    $this->validatorSchema->setPostValidator(new sfGuardValidatorUser(array(), array('invalid' => 'Invalid email or password')));

    $this->widgetSchema->setNameFormat('signin[%s]');
  }

}