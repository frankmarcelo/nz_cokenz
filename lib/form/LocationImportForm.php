<?php

class LocationImportForm extends BaseForm
{

    public function configure()
    {
        $this->setWidgets(array(
            'location_type' => new sfWidgetFormDoctrineChoice(array(
                'model' => 'CokeLocationType',
                'method' => 'getName' ,
                'add_empty' => true
            )),
            'data' => new sfWidgetFormInputFile(array())
        ));


        $this->setValidators(array(
            'location_type' => new sfValidatorDoctrineChoice(array(
                'model' => 'CokeLocationType'
            )),
            'data' => new sfValidatorFile(array(
                'path'       => sfConfig::get('sf_upload_dir').'/locations',
                'mime_types' => array('text/csv', 'text/plain')
            ))
        ));

        $this->widgetSchema->setNameFormat('location_import[%s]');
    }

}
