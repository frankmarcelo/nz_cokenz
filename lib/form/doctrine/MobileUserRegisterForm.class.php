<?php

class MobileUserRegisterForm extends UserRegisterForm
{
  public function configure()
  {

    parent::configure();

    unset($this['security_code']);

    $this->widgetSchema['email_address'] = new sfWidgetFormInputEmail();
    $this->widgetSchema['Profile']['gender'] = new sfWidgetFormChoiceNoList( array('choices' => array('male'=>'Male', 'female'=>'Female'), 'expanded' => true));
    $this->widgetSchema['Profile']['phone'] = new sfWidgetFormInputPhone();
    $this->widgetSchema['Profile']['postcode'] = new sfWidgetFormInput(array('type' => 'tel'));

    if(!$this->isNew())
    {
      unset(
        $this['password'],
        $this['password_again'],
        $this['nz_resident'],
        $this['accept_privacy']
      );
    }


  }

//  public function bind(array $taintedValues = null, array $taintedFiles = null)
//  {
//    if( empty($taintedValues['password']) ){
//      unset($taintedValues['password'], $taintedValues['password_again'], $taintedValues['current_password'] );
//      $this->validatorSchema->setPostValidator(new sfValidatorPass());
//    }
//    parent::bind($taintedValues, $taintedFiles);
//  }

}
