<?php

/**
 * sfGuardUser Backend form.
 *
 * @package    coke_nz
 * @subpackage form
 * @author
 */
class sfGuardUserFormBackend extends BasesfGuardUserForm
{
  public function configure()
  {
    unset(
    $this['username'],
    $this['salt'],
    $this['algorithm']
    );

    if( $this->isNew() ) {
      unset(
        $this['last_login'],
        $this['created_at'],
        $this['updated_at']
      );
    } else {
      $this->widgetSchema['last_login'] = new sfWidgetReadOnly();
      $this->widgetSchema['created_at'] = new sfWidgetReadOnly();
      $this->widgetSchema['updated_at'] = new sfWidgetReadOnly();
      unset(
        $this->validatorSchema['last_login'],
        $this->validatorSchema['created_at'],
        $this->validatorSchema['updated_at']
      );
    }

    $this->widgetSchema['password'] = new sfWidgetFormInputPassword();
    $this->validatorSchema['password']->setOption('required', false);

    $this->widgetSchema['password_again'] = new sfWidgetFormInputPassword();
    $this->validatorSchema['password_again'] = clone $this->validatorSchema['password'];



    $this->embedRelation('Profile', 'sfGuardUserProfileForm');

    $this->widgetSchema->setNameFormat('sf_guard_user[%s]');
  }

  public function getModelName()
  {
    return 'sfGuardUser';
  }
}
