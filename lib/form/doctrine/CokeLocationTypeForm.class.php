<?php

/**
 * CokeLocationType form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     Nik Spijkerman
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CokeLocationTypeForm extends BaseCokeLocationTypeForm
{
  public function configure()
  {
  }
}
