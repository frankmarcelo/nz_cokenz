<?php

/**
 * CocaColaGame form.
 *
 * @package    coke_nz
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CocaColaGameForm extends BaseCocaColaGameForm
{
  public function configure()
  {

    unset($this['created_at'], $this['updated_at']);

    $this->widgetSchema['icon'] = new sfWidgetFormInputFileEditable(array
      (
        'file_src'  => '/uploads/game_button_icons/'. $this->getObject()->getIcon(),
        'is_image'  => true,
        'edit_mode' => !$this->isNew(),
        'template'  => '<div><div class="btn btn-danger">%file%</div><br /><br />%input%</div>',
      ));
    $this->widgetSchema['enabled'] = new sfWidgetFormSelectRadio(array('choices' => array('Y' => 'Yes', 'N' => 'No'), 'class' => 'radio-horizontal'));


    $this->validatorSchema['icon'] = new sfValidatorFile(array
      (
        'required'   => $this->isNew(),
        'path'       => sfConfig::get('sf_upload_dir').'/game_button_icons',
        'mime_types' => 'web_images',
      ));

    $this->widgetSchema->setNameFormat('coca_cola_game[%s]');
  }
}
