<?php

/**
 * SACUser form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SACUserForm extends BaseSACUserForm
{
  public function configure()
  {
      unset($this['created_at'], $this['updated_at']);
  }
}
