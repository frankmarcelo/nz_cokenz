<?php

/**
 * Project form base class.
 *
 * @package    coke_nz
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormBaseTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class BaseFormDoctrine extends sfFormDoctrine
{
  public function setup()
  {
  }
}
