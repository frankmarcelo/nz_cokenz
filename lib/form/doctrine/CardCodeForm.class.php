<?php

/**
 * CardCode form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     Nik Spijkerman
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CardCodeForm extends BaseCardCodeForm
{
  public function configure()
  {
      $this->widgetSchema['location_id'] = new sfWidgetFormDoctrineChoice(array(
          'model' => 'CokeLocation',
          'table_method' => 'findAllCardLocationsQuery',
          'add_empty' => false
      ));
  }
}
