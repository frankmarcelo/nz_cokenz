<?php

/**
 * SACNominateName form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SACNominateNameForm extends BaseSACNominateNameForm
{
    public function configure()
    {
        unset($this['created_at'], $this['updated_at']);

        $this->validatorSchema['email'] = new sfValidatorEmail();
        $this->validatorSchema['nominate_reason'] = new validatorWordCount(array('max_word_count' => 50));
        $this->validatorSchema['nominate_name']->setOption('max_length', 12);
        $this->validatorSchema['nominate_name']->setMessage('max_length', 'Name is too long<br>(12 characters max)');

        $this->widgetSchema['agree_tc'] = new sfWidgetFormInputCheckbox();
        $this->validatorSchema['agree_tc'] = new sfValidatorBoolean(array('required' => true));
    }
}
