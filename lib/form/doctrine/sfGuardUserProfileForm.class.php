<?php

/**
 * sfGuardUserProfile form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserProfileForm extends BasesfGuardUserProfileForm
{
  public function configure()
  {

    unset($this['verification_key'], $this['created_at'], $this['updated_at']);

    $years = range(date('Y'), date('Y') - 120);
    $months = array();
    for($i=1;$i<=12;$i++) $months[$i] = date('M',mktime(0,0,0,$i,1,2012));
    $this->widgetSchema['dob']->setOption('years' , array_combine($years, $years));
    $this->widgetSchema['dob']->setOption('months' , $months);
    $this->widgetSchema['dob']->setOption('format' , '%day%%month%%year%');
    $this->widgetSchema['dob']->setOption('empty_values' , array('year' => 'Y', 'month' => 'M', 'day' => 'D'));

    if(!$this->isNew()){
      $this->widgetSchema['user_id'] = new sfWidgetFormInputHidden();
    }

    $genders = array(''=>'','male' => 'Male', 'female' => 'Female', 'other' => 'Other');
    $this->widgetSchema['gender'] = new sfWidgetFormChoice(array('choices' => $genders));

    $this->validatorSchema['gender'] = new sfValidatorChoice(array('choices' => array_keys($genders), 'required' => true));

    $this->widgetSchema->setLabel('dob','Date of Birth');

    $regions = array_merge( array(''=>''), array_combine( sfConfig::get('app_register_form_regions'), sfConfig::get('app_register_form_regions')));
    $this->widgetSchema['region'] = new sfWidgetFormChoice(array('choices' => $regions));

    $this->validatorSchema['dob']->setOption('required' , true);
    $this->validatorSchema['phone']->setOption('required' , true);
    $this->validatorSchema['postcode']->setOption('required' , true);
    $this->validatorSchema['region']->setOption('required' , true);

    $this->widgetSchema->setLabels(array(
      'email_updates' => "Sign me up to receive <em>Email</em> updates from Coca-Cola Oceania including information on promotions and community activities, which may include information on 3rd party partners",
      'mobile_updates' => "Sign me up to receive <em>Mobile</em> updates from Coca-Cola Oceania including information on promotions and community activities, which may include information on 3rd party partners"
    ));

  }
}
