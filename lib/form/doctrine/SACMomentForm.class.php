<?php

/**
 * SACMoment form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SACMomentForm extends BaseSACMomentForm
{
    public function configure()
    {
        unset($this['created_at'], $this['updated_at']);

        $this->widgetSchema['original_image'] = new sfWidgetFormInputFile();

        $this->validatorSchema['original_image'] = new sfValidatorFile(array(
            'max_size'  => (1024 * 1024) * 2, //2mb
            'path'      => SACMoment::getOriginalFilePath(),
            'mime_types' => 'web_images'
        ));

    }
}
