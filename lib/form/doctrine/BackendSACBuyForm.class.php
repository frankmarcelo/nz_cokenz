<?php

/**
 * SACBuy form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class BackendSACBuyForm extends BaseSACBuyForm
{
  public function configure()
  {
      unset(
        $this['created_at'],
        $this['updated_at']
      );

      $payment_status = $this->getObject()->getPaymentStatuses();
      $redemption_status = $this->getObject()->getRedemptionStatuses();

      $this->widgetSchema['location'] = new sfWidgetFormChoice(array('choices' => array_merge(array(''=>''), $this->getObject()->getLocations())));
      $this->widgetSchema['payment_status'] = new sfWidgetFormChoice(array('choices' => array_combine($payment_status, array_map('ucfirst', $payment_status))));
      $this->widgetSchema['redemption_status'] = new sfWidgetFormChoice(array('choices' => array_combine($redemption_status, array_map('ucfirst', $redemption_status))));

      $this->validatorSchema['user_phone']->setOption('required', true);

      $this->validatorSchema['recipient_email'] = new sfValidatorEmail();

      $this->validatorSchema['recipient_mobile'] = new sfValidatorAnd(array(
            $this->validatorSchema['recipient_mobile'],
            new sfValidatorRegex(array('pattern' => '/^02[1-9]{1}[0-9]{6,8}/'), array('invalid' => 'Invalid number or format'))
          ));

      $this->validatorSchema['user_phone'] = new sfValidatorAnd(array(
            $this->validatorSchema['user_phone'],
            new sfValidatorRegex(array('pattern' => '/^0[2-9]{1,2}[0-9]{6,8}/'), array('invalid' => 'Invalid number or format'))
          ));
  }
}
