<?php

class ChangePasswordForm extends sfGuardRegisterForm
{
  public function configure()
  {

    unset(
      $this['first_name'],
      $this['last_name'],
      $this['email_address'],
      $this['username']
    );

    $this->widgetSchema['current_password'] = new sfWidgetFormInputPassword();

    $this->validatorSchema['password']->setOption('min_length', 6);
    $this->validatorSchema['password']->setMessage('min_length', 'Password is too short');

    $this->validatorSchema['current_password'] = new sfValidatorCallback(array('callback' => array($this,'checkPassword')),array('invalid' => 'Invalid password'));

  }


  public function checkPassword($validator, $value)
  {
    if( ! $this->getObject()->checkPassword($value) ) {
      throw new sfValidatorError($validator,'invalid');
    }
    return $value;
  }

}
