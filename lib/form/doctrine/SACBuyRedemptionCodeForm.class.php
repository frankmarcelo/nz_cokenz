<?php

/**
 * SACBuyRedemptionCode form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SACBuyRedemptionCodeForm extends BaseSACBuyRedemptionCodeForm
{
  public function configure()
  {
      $this->widgetSchema['location'] = new sfWidgetFormChoice(array('choices' => $this->getObject()->getLocations()));
  }
}
