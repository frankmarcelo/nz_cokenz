<?php

/**
 * CocaColaUserProfileBackend form.
 *
 * @package    coke_nz
 * @subpackage form
 * @author
 */
class CocaColaUserProfileFormBackend extends BaseFormDoctrine
{
  public function configure()
  {
    parent::configure();

    $years = range(date('Y') - 100, date('Y'));

    $this->setWidgets(array(
      'name'   => new sfWidgetFormInputText(),
      'gender' => new sfWidgetFormSelect(array('choices' => array('male'=>'Male', 'female'=>'Female'))),
      'dob'    => new sfWidgetFormDate(array(
        'format'       => '%day%/%month%/%year%',
        'can_be_empty' => false,
        'years'        => array_combine($years, $years)
      )),
      'phone'  => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'name'   => new sfValidatorString(array('trim' => true)),
      'gender' => new sfValidatorString(array('trim' => true)),
      'dob'    => new sfValidatorDate(),
      'phone'  => new sfValidatorString(array('trim' => true)),
    ));

    $this->widgetSchema->setNameFormat('sf_guard_user_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }
}
