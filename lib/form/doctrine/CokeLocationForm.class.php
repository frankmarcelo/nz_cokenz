<?php

/**
 * CokeLocation form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     Nik Spijkerman
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CokeLocationForm extends BaseCokeLocationForm
{
  public function configure()
  {
    unset($this['created_at'], $this['updated_at']);

    $this->validatorSchema['card_redeem_code'] = new sfValidatorCallback(array('callback' => array($this,'checkRedeemCodeIsUnique'), 'required' => false), array('invalid' => 'Redemption code already used'));

    $this->validatorSchema->setPostValidator( new validatorPhysicalAddress() );
  }

  public function checkRedeemCodeIsUnique($validator, $value)
  {

    if (!empty($value)) {
      $q = Doctrine::getTable('CokeLocation')->createQuery()
          ->where('is_card_location = ?',true)
          ->andWhere('card_redeem_code = ?', $value)
          ->execute()->count();

      if ($q > 0) {
        throw new sfValidatorError($validator, 'invalid');
      }
    }

    return $value;
  }

}
