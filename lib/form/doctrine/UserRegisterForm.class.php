<?php

class UserRegisterForm extends sfGuardRegisterForm
{

  public function configure()
  {
    parent::configure();

    unset($this['username']);

    $this->embedRelation('Profile');

    $this->widgetSchema['nz_resident']  = new sfWidgetFormInputCheckbox(array(
      'label' => "I am a resident of New Zealand"
    ));
    $this->widgetSchema['accept_privacy']  = new sfWidgetFormInputCheckbox();

    $this->validatorSchema['first_name']->setOption('required', true);
    $this->validatorSchema['last_name']->setOption('required', true);
    $this->validatorSchema['password']->setOption('min_length', 6);
    $this->validatorSchema['password']->setMessage('min_length', 'Password is too short');

    $this->validatorSchema['nz_resident'] = new sfValidatorBoolean(array('required' => true));
    $this->validatorSchema['accept_privacy'] = new sfValidatorBoolean(array('required' => true));


    if($this->isNew()){
      $this->widgetSchema['security_code'] = new sfWidgetCaptchaGD(array('label' => '*Security Code'));
      $this->validatorSchema['security_code'] = new sfCaptchaGDValidator(array('length' => 6));
    }
    if(!$this->isNew()){
      $this->validatorSchema['accept_privacy']->setOption('required',false);
      $this->validatorSchema['nz_resident']->setOption('required',false);
      unset($this['password'],$this['password_again']);
    }

    $this->validatorSchema['email_address'] = new sfValidatorAnd(array(
      new sfValidatorString(),
      new sfValidatorEmail()
    ));

    sfContext::getInstance()->getConfiguration()->loadHelpers(array('Url','Tag'));

    $post_validators = $this->validatorSchema->getPostValidator()->getValidators();

    $post_validators[0]->setMessage(
      'invalid',
      sprintf('Email address has already been registered. Please use the %s tool.', link_to('Forgot Password','@sf_guard_forgot_password'))
    );
    $post_validators[1]->setMessage('invalid','Passwords must match');

  }
}
