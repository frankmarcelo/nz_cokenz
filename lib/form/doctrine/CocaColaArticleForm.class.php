<?php

class CocaColaArticleForm extends BaseCocaColaArticleForm
{
  public function configure()
  {
    unset(
      $this['created_at'], $this['updated_at']
    );

    $this->widgetSchema['title_image'] = new sfWidgetFormInputFileEditable(array(
      'file_src'  => '/uploads/articles/'. $this->getObject()->getTitleImage(),
      'is_image'  => true,
      'edit_mode' => !$this->isNew(),
      'with_delete' => true,
      'template'  => '<div>%file%<br /><br />%input%</div>',
    ));

    $this->widgetSchema['thumb_image'] = new sfWidgetFormInputFileEditable(array(
      'file_src'  => '/uploads/articles/'. $this->getObject()->getThumbImage(),
      'is_image'  => true,
      'edit_mode' => !$this->isNew(),
      'with_delete' => true,
      'template'  => '<div>%file%<br /><br />%input%</div>',
    ));

    $this->widgetSchema['article_date'] = new sfWidgetFormDate(array('format' => '%day%/%month%/%year%', 'can_be_empty' => false));

    $this->validatorSchema['title_image'] = new sfValidatorFile(array(
        'required'   => $this->isNew(),
        'path'       => sfConfig::get('sf_upload_dir').'/articles',
        'mime_types' => 'web_images'
    ));

    $this->validatorSchema['thumb_image'] = new sfValidatorFile(array(
        'required'   => $this->isNew(),
        'path'       => sfConfig::get('sf_upload_dir').'/articles',
        'mime_types' => 'web_images'
    ));

    $this->validatorSchema['article_date']->setOption('required',true);

    $this->widgetSchema->setNameFormat('coca_cola_article[%s]');

  }

}