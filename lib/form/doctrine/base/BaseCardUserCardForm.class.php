<?php

/**
 * CardUserCard form base class.
 *
 * @method CardUserCard getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCardUserCardForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'user_location_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('UserLocation'), 'add_empty' => false)),
      'is_active'        => new sfWidgetFormInputCheckbox(),
      'redemption_date'  => new sfWidgetFormDate(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_location_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('UserLocation'))),
      'is_active'        => new sfValidatorBoolean(array('required' => false)),
      'redemption_date'  => new sfValidatorDate(array('required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('card_user_card[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CardUserCard';
  }

}
