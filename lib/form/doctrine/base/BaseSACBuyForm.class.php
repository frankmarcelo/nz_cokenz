<?php

/**
 * SACBuy form base class.
 *
 * @method SACBuy getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSACBuyForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                   => new sfWidgetFormInputHidden(),
      'user_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SACUser'), 'add_empty' => true)),
      'user_phone'           => new sfWidgetFormInputText(),
      'location'             => new sfWidgetFormInputText(),
      'recipient_first_name' => new sfWidgetFormInputText(),
      'recipient_last_name'  => new sfWidgetFormInputText(),
      'recipient_email'      => new sfWidgetFormInputText(),
      'recipient_mobile'     => new sfWidgetFormInputText(),
      'payment_status'       => new sfWidgetFormInputText(),
      'redemption_status'    => new sfWidgetFormInputText(),
      'redemption_code_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('RedemptionCode'), 'add_empty' => true)),
      'redemption_date'      => new sfWidgetFormDateTime(),
      'created_at'           => new sfWidgetFormDateTime(),
      'updated_at'           => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                   => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SACUser'), 'required' => false)),
      'user_phone'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'location'             => new sfValidatorString(array('max_length' => 50)),
      'recipient_first_name' => new sfValidatorString(array('max_length' => 255)),
      'recipient_last_name'  => new sfValidatorString(array('max_length' => 255)),
      'recipient_email'      => new sfValidatorString(array('max_length' => 255)),
      'recipient_mobile'     => new sfValidatorString(array('max_length' => 255)),
      'payment_status'       => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'redemption_status'    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'redemption_code_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('RedemptionCode'), 'required' => false)),
      'redemption_date'      => new sfValidatorDateTime(array('required' => false)),
      'created_at'           => new sfValidatorDateTime(),
      'updated_at'           => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('sac_buy[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SACBuy';
  }

}
