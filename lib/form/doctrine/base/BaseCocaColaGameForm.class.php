<?php

/**
 * CocaColaGame form base class.
 *
 * @method CocaColaGame getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCocaColaGameForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'title'           => new sfWidgetFormInputText(),
      'icon'            => new sfWidgetFormInputText(),
      'enabled'         => new sfWidgetFormInputText(),
      'directory_index' => new sfWidgetFormInputText(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
      'slug'            => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'           => new sfValidatorString(array('max_length' => 128)),
      'icon'            => new sfValidatorString(array('max_length' => 255)),
      'enabled'         => new sfValidatorString(array('max_length' => 1, 'required' => false)),
      'directory_index' => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
      'slug'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorAnd(array(
        new sfValidatorDoctrineUnique(array('model' => 'CocaColaGame', 'column' => array('title'))),
        new sfValidatorDoctrineUnique(array('model' => 'CocaColaGame', 'column' => array('slug'))),
      ))
    );

    $this->widgetSchema->setNameFormat('coca_cola_game[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CocaColaGame';
  }

}
