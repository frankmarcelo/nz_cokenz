<?php

/**
 * CocaColaArticle form base class.
 *
 * @method CocaColaArticle getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCocaColaArticleForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'title'             => new sfWidgetFormInputText(),
      'article_date'      => new sfWidgetFormDate(),
      'article_type'      => new sfWidgetFormChoice(array('choices' => array('article' => 'article', 'link' => 'link'))),
      'title_image'       => new sfWidgetFormInputText(),
      'thumb_image'       => new sfWidgetFormInputText(),
      'short_description' => new sfWidgetFormTextarea(),
      'article_content'   => new sfWidgetFormTextarea(),
      'image_gallery_url' => new sfWidgetFormTextarea(),
      'video_gallery_url' => new sfWidgetFormTextarea(),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
      'slug'              => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'title'             => new sfValidatorString(array('max_length' => 255)),
      'article_date'      => new sfValidatorDate(array('required' => false)),
      'article_type'      => new sfValidatorChoice(array('choices' => array(0 => 'article', 1 => 'link'))),
      'title_image'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'thumb_image'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'short_description' => new sfValidatorString(array('max_length' => 400, 'required' => false)),
      'article_content'   => new sfValidatorString(array('max_length' => 10000, 'required' => false)),
      'image_gallery_url' => new sfValidatorString(array('max_length' => 10000, 'required' => false)),
      'video_gallery_url' => new sfValidatorString(array('max_length' => 10000, 'required' => false)),
      'created_at'        => new sfValidatorDateTime(),
      'updated_at'        => new sfValidatorDateTime(),
      'slug'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'CocaColaArticle', 'column' => array('slug')))
    );

    $this->widgetSchema->setNameFormat('coca_cola_article[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CocaColaArticle';
  }

}
