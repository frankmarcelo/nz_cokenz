<?php

/**
 * CokeLocation form base class.
 *
 * @method CokeLocation getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCokeLocationForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'               => new sfWidgetFormInputHidden(),
      'type_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Type'), 'add_empty' => false)),
      'name'             => new sfWidgetFormInputText(),
      'address'          => new sfWidgetFormTextarea(),
      'suburb'           => new sfWidgetFormInputText(),
      'city'             => new sfWidgetFormInputText(),
      'postcode'         => new sfWidgetFormInputText(),
      'phone'            => new sfWidgetFormInputText(),
      'show_in_finder'   => new sfWidgetFormInputCheckbox(),
      'is_card_location' => new sfWidgetFormInputCheckbox(),
      'card_redeem_code' => new sfWidgetFormInputText(),
      'created_at'       => new sfWidgetFormDateTime(),
      'updated_at'       => new sfWidgetFormDateTime(),
      'latitude'         => new sfWidgetFormInputText(),
      'longitude'        => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'               => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'type_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Type'))),
      'name'             => new sfValidatorString(array('max_length' => 200)),
      'address'          => new sfValidatorString(array('max_length' => 500)),
      'suburb'           => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'city'             => new sfValidatorString(array('max_length' => 200)),
      'postcode'         => new sfValidatorString(array('max_length' => 50)),
      'phone'            => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'show_in_finder'   => new sfValidatorBoolean(array('required' => false)),
      'is_card_location' => new sfValidatorBoolean(array('required' => false)),
      'card_redeem_code' => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'created_at'       => new sfValidatorDateTime(),
      'updated_at'       => new sfValidatorDateTime(),
      'latitude'         => new sfValidatorPass(array('required' => false)),
      'longitude'        => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('coke_location[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CokeLocation';
  }

}
