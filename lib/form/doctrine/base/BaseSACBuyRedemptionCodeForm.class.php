<?php

/**
 * SACBuyRedemptionCode form base class.
 *
 * @method SACBuyRedemptionCode getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSACBuyRedemptionCodeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'        => new sfWidgetFormInputHidden(),
      'code'      => new sfWidgetFormInputText(),
      'location'  => new sfWidgetFormInputText(),
      'used'      => new sfWidgetFormInputCheckbox(),
      'allocated' => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'id'        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'      => new sfValidatorInteger(),
      'location'  => new sfValidatorString(array('max_length' => 50)),
      'used'      => new sfValidatorBoolean(array('required' => false)),
      'allocated' => new sfValidatorBoolean(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'SACBuyRedemptionCode', 'column' => array('code')))
    );

    $this->widgetSchema->setNameFormat('sac_buy_redemption_code[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SACBuyRedemptionCode';
  }

}
