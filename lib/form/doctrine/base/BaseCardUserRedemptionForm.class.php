<?php

/**
 * CardUserRedemption form base class.
 *
 * @method CardUserRedemption getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCardUserRedemptionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'user_card_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Card'), 'add_empty' => false)),
      'code_id'      => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Code'), 'add_empty' => false)),
      'created_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_card_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Card'))),
      'code_id'      => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Code'))),
      'created_at'   => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('card_user_redemption[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CardUserRedemption';
  }

}
