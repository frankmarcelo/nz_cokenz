<?php

/**
 * SACMoment form base class.
 *
 * @method SACMoment getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSACMomentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'              => new sfWidgetFormInputHidden(),
      'user_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('SACUser'), 'add_empty' => true)),
      'original_image'  => new sfWidgetFormInputText(),
      'generated_image' => new sfWidgetFormInputText(),
      'selection'       => new sfWidgetFormInputText(),
      'font_color'      => new sfWidgetFormInputText(),
      'tags'            => new sfWidgetFormInputText(),
      'created_at'      => new sfWidgetFormDateTime(),
      'updated_at'      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('SACUser'), 'required' => false)),
      'original_image'  => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'generated_image' => new sfValidatorString(array('max_length' => 250, 'required' => false)),
      'selection'       => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'font_color'      => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'tags'            => new sfValidatorPass(array('required' => false)),
      'created_at'      => new sfValidatorDateTime(),
      'updated_at'      => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('sac_moment[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SACMoment';
  }

}
