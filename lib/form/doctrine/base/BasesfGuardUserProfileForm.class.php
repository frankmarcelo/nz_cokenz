<?php

/**
 * sfGuardUserProfile form base class.
 *
 * @method sfGuardUserProfile getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasesfGuardUserProfileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                  => new sfWidgetFormInputHidden(),
      'user_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'gender'              => new sfWidgetFormInputText(),
      'dob'                 => new sfWidgetFormDate(),
      'phone'               => new sfWidgetFormInputText(),
      'email_updates'       => new sfWidgetFormInputCheckbox(),
      'mobile_updates'      => new sfWidgetFormInputCheckbox(),
      'region'              => new sfWidgetFormInputText(),
      'postcode'            => new sfWidgetFormInputText(),
      'registration_source' => new sfWidgetFormChoice(array('choices' => array('desktop' => 'desktop', 'mobile' => 'mobile'))),
      'created_at'          => new sfWidgetFormDateTime(),
      'updated_at'          => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                  => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'required' => false)),
      'gender'              => new sfValidatorString(array('max_length' => 8, 'required' => false)),
      'dob'                 => new sfValidatorDate(array('required' => false)),
      'phone'               => new sfValidatorString(array('max_length' => 16, 'required' => false)),
      'email_updates'       => new sfValidatorBoolean(array('required' => false)),
      'mobile_updates'      => new sfValidatorBoolean(array('required' => false)),
      'region'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'postcode'            => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'registration_source' => new sfValidatorChoice(array('choices' => array(0 => 'desktop', 1 => 'mobile'), 'required' => false)),
      'created_at'          => new sfValidatorDateTime(),
      'updated_at'          => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('sf_guard_user_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'sfGuardUserProfile';
  }

}
