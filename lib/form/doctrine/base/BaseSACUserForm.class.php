<?php

/**
 * SACUser form base class.
 *
 * @method SACUser getObject() Returns the current form's model object
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseSACUserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'first_name'     => new sfWidgetFormInputText(),
      'last_name'      => new sfWidgetFormInputText(),
      'email'          => new sfWidgetFormInputText(),
      'location'       => new sfWidgetFormInputText(),
      'facebook_id'    => new sfWidgetFormInputText(),
      'facebook_token' => new sfWidgetFormTextarea(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
      'latitude'       => new sfWidgetFormInputText(),
      'longitude'      => new sfWidgetFormInputText(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'first_name'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'last_name'      => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'email'          => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'location'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'facebook_id'    => new sfValidatorInteger(array('required' => false)),
      'facebook_token' => new sfValidatorString(array('max_length' => 1000, 'required' => false)),
      'created_at'     => new sfValidatorDateTime(),
      'updated_at'     => new sfValidatorDateTime(),
      'latitude'       => new sfValidatorPass(array('required' => false)),
      'longitude'      => new sfValidatorPass(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('sac_user[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'SACUser';
  }

}
