<?php

class sfGuardUserAjaxRegisterForm extends BaseForm
{

  public function setup()
  {
    $year = range(date('Y'),date('Y')-120,-1);
    $regions = array_combine( sfConfig::get('app_register_form_regions'), sfConfig::get('app_register_form_regions'));
    $this->setWidgets(array(
      'first_name'      => new sfWidgetFormInputText(),
      'last_name'       => new sfWidgetFormInputText(),
      'email_address'   => new sfWidgetFormInputEmail(),
      'password'        => new sfWidgetFormInputPassword(array('type' => 'password')),
      'gender'          => new sfWidgetFormChoiceNoList(array('choices' => array('male'=>'Male', 'female'=>'Female'), 'expanded' => true)),
      'dob'       => new sfWidgetFormDate(array(
        'years' => array_combine($year,$year),
        'empty_values' => array('year' => 'Year', 'month' => 'Month', 'day' => 'Day'),
        'format' => '%day%%month%%year%',
        'label' => false
      )),
      'phone'           => new sfWidgetFormInputPhone(),
      'region'          => new sfWidgetFormChoice(array('choices' => $regions)),
      'email_updates'   => new sfWidgetFormInputCheckbox(array(
        'label' => "Sign me up to receive <em>Email</em> updates from Coca-Cola Oceania including information on promotions and community activities, which may include information on 3rd party partners"
      )),
      'mobile_updates'  => new sfWidgetFormInputCheckbox(array(
        'label' => "Sign me up to receive <em>Mobile</em> updates from Coca-Cola Oceania including information on promotions and community activities, which may include information on 3rd party partners"
      )),
      'nz_resident'     => new sfWidgetFormInputCheckbox(array(
        'label' => "I am a resident of New Zealand"
      )),
      'agree-pp'        => new sfWidgetFormInputCheckbox(),
    ));

    $this->setValidators(array(
      'username'        => new sfValidatorString(array('required' => false)), // all the validation will be done for email address
      'first_name'      => new sfValidatorString(array('trim' => true)),
      'last_name'       => new sfValidatorString(array('trim' => true)),
      'email_address'   => new sfValidatorAnd(array(
        new sfValidatorEmail(),
        new sfValidatorDoctrineUnique2(array('model' => 'sfGuardUser', 'column' => 'email_address'), array('invalid' => 'Already registered.')),
      )),
      'password'        => new sfValidatorString(),
      'gender'          => new sfValidatorString(),
      'dob'             => new sfValidatorDate(array(
        'max' => mktime(0,0,0,date('m'),date('d'),(date('Y') - sfConfig::get('app_min_age',13)) )
      ), array(
        'max' => 'Sorry, we cannot accept your registration at this time'
      )),
      'phone'           => new sfValidatorString(array('trim' => true)),
      'region'          => new sfValidatorChoice(array('choices' => sfConfig::get('app_register_form_regions'))),
      'email_updates'   => new sfValidatorBoolean(),
      'mobile_updates'  => new sfValidatorBoolean(),
      'nz_resident'        => new sfValidatorBoolean(array('required' => true)),
      'agree-pp'        => new sfValidatorBoolean(array('required' => true)),
    ));

    $this->widgetSchema->setNameFormat('register[%s]');
  }

}