<?php

/**
 * SACStory form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SACStoryForm extends BaseSACStoryForm
{
  public function configure()
  {
      unset($this['created_at'], $this['updated_at']);

      if ($this->isNew()) {
          $this->widgetSchema['accept_tc'] = new sfWidgetFormInputCheckbox();
          $this->validatorSchema['accept_tc'] = new sfValidatorBoolean(array('required' => true));
          unset($this['status']);
      } else {
          $statuses = array(SACStory::STATUS_PENDING, SACStory::STATUS_APPROVED, SACStory::STATUS_DECLINED);
          $this->widgetSchema['status'] = new sfWidgetFormChoice(array('choices' => array_combine($statuses, $statuses)));
          $this->validatorSchema['status'] = new sfValidatorChoice(array('choices' => array_combine($statuses, $statuses)));
      }

      $this->widgetSchema['location'] = new sfWidgetFormInputText();
      $this->validatorSchema['story']->setOption('required', true);
      $this->validatorSchema['email'] = new sfValidatorEmail();

      $this->widgetSchema['pic'] = new sfWidgetFormInputFileEditable(array(
              'label' => 'Upload a pic',
              'file_src' => '/uploads/app/shareacoke/stories/thumb/'.$this->getObject()->getPic(),
              'is_image' => true,
              'edit_mode' => !$this->isNew(),
              'template' => '<div class="sublabel">%file%<br />%input%<br />%delete% %delete_label%</div>',
      ));

      $this->validatorSchema['pic'] = new sfValidatorFile(array(
              'required'   => false,
              'path'       => sfConfig::get('sf_upload_dir').'/app/shareacoke/stories',
              'mime_types' => 'web_images',
      ));
  }
}
