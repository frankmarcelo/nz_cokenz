<?php

/**
 * SACBuy form.
 *
 * @package    Coke NZ
 * @subpackage form
 * @author     
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class SACBuyForm extends BaseSACBuyForm
{
  public function configure()
  {
      unset(
        $this['created_at'],
        $this['updated_at'],
        $this['payment_status'],
        $this['redemption_status'],
        $this['redemption_code_id'],
        $this['redemption_date']
      );

      //Widgets
      $this->widgetSchema['user_id'] = new sfWidgetFormInputHidden();

      $this->widgetSchema['user_first_name'] = new sfWidgetFormInputText();

      $this->widgetSchema['user_last_name'] = new sfWidgetFormInputText();

      $this->widgetSchema['user_email'] = new sfWidgetFormInputText();

      $this->widgetSchema['location'] = new sfWidgetFormChoice(array('choices' => $this->getObject()->getLocations()));


      //Validators
      $this->validatorSchema['user_first_name'] = new sfValidatorString();

      $this->validatorSchema['user_last_name'] = new sfValidatorString();

      $this->validatorSchema['user_email'] = new sfValidatorString();

      $this->validatorSchema['user_phone']->setOption('required', true);

      $this->validatorSchema['recipient_email'] = new sfValidatorEmail();

      $this->validatorSchema['recipient_mobile'] = new sfValidatorAnd(array(
            $this->validatorSchema['recipient_mobile'],
            new sfValidatorRegex(array('pattern' => '/^02[1-9]{1}[0-9]{6,7}/'), array('invalid' => 'Invalid mobile number or format'))
          ));

      $this->validatorSchema['user_phone'] = new sfValidatorAnd(array(
            $this->validatorSchema['user_phone'],
            new sfValidatorRegex(array('pattern' => '/^02[1-9]{1}[0-9]{6,7}/'), array('invalid' => 'Invalid mobile number or format'))
          ));
  }
}
