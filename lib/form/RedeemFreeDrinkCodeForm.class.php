<?php

class RedeemFreeDrinkCodeForm extends BaseForm
{



  public function configure()
  {

    $this->widgetSchema['code'] = new sfWidgetFormInputText(array(
      'label' => false,
    ),array(
      'placeholder' => 'enter code'
    ));

    $this->widgetSchema['user_card_id'] = new sfWidgetFormInputHidden();

    $this->validatorSchema['code'] = new sfValidatorString(array('trim' => true));
    $this->validatorSchema['user_card_id'] = new sfValidatorNumber();

    $this->validatorSchema->setPostValidator(
      new validatorLoyaltyCardFreeDrinkCode(array(),array(
          'invalid'         => 'invalid'
        )
    ));

    $this->widgetSchema->setNameFormat('redeem_free[%s]');

  }

}
