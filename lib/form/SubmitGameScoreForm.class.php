<?php

class SubmitGameScoreForm extends GameScoreForm
{
  public function configure()
  {
    parent::configure();
    
    // Game ID
    $this->validatorSchema['game_id']->setOption('required', true);
    $this->validatorSchema['game_id']->setOption('query', $this->getQueryForGameId());
    
    // Score
    $this->validatorSchema['score']->setOption('required', true);
    
    // User ID
    $this->validatorSchema['user_id']->setOption('required', true);
    
    unset(
      $this['id'],
      $this['created_at'],
      $this['updated_at']
    );
  }
  
  protected function getQueryForGameId()
  {
    return Doctrine::getTable('CocaColaGame')->createQuery('g')
      ->where('g.enabled = ?', array('Y'));
  }
}