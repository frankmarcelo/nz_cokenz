<?php

class RequestForgotPasswordForm extends sfGuardRequestForgotPasswordForm
{
  public function configure()
  {
    $this->widgetSchema['security_code'] = new sfWidgetCaptchaGD(array('label' => '*Security Code'));
    $this->validatorSchema['security_code'] = new sfCaptchaGDValidator(array('length' => 4));


  }

}
